import pathlib
from setuptools import setup, find_packages
# The directory containing this file
HERE = pathlib.Path(__file__).parent
# The text of the README file
README = (HERE / "README.md").read_text()

setup(
    name='mdf_client',
    version='5.4',
    packages=find_packages(include=['mdf_client', 'mdf_client.*']),
    install_requires=['PyQt5-Qt5','pyinstaller','pygments','requests','Pillow','pyodbc',
                      #'pyqtgraph', 'PyOpenGL-accelerate', #'OpenGL', # enable for extrusion to work
                      'mdf_canon @ git+https://gitlab.com/expertlab/mdf_canon.git',
                      # 'veusz @ git+https://gitlab.com/expertlab/veusz.git',
                      ],
    
    description="MDF Client",
    long_description=README,
    author='Daniele Paganelli',
    author_email='dp@mythsmith.it',
    url='https://www.expertlabservice.it',
    license="GPLv2",
    classifiers=[
        "License :: GPLv2",
        "Programming Language :: Python :: 3",
    ],
    
)

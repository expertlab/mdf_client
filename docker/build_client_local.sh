#!/bin/bash

if [ -e /opt/mdf/mdf_client/mdf_client/installer/build ]; then
    sudo rm -rf /opt/mdf/mdf_client/mdf_client/installer/build /opt/mdf/mdf_client/mdf_client/installer/dist
fi    
# Do not overwrite veusz and use docker image helpers
#-v /opt/mdf/veusz:/opt/mdf/veusz \ 
docker run -it -v /opt/mdf/mdf_client:/opt/mdf/mdf_client \
    -v /opt/mdf/mdf_canon:/opt/mdf/mdf_canon \
    -v /opt/mdf/mdf_imago:/opt/mdf/mdf_imago \
    -v /opt/mdf/mdf_client/docker/linux_build:/opt/mdf/mdf_client/mdf_client/installer/build \
    -v /opt/mdf/mdf_client/docker/linux_dist:/opt/mdf/mdf_client/mdf_client/installer/dist \
    -v /opt/mdf/mdf_client/docker/docker_build_machine/executor.sh:/root/executor.sh \
    -v /output:/output \
    client_build_machine:latest $1

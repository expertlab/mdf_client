#!/bin/bash


if [ -e /opt/mdf/mdf_client/mdf_client/installer/build ]; then
    sudo rm -rf /opt/mdf/mdf_client/mdf_client/installer/build /opt/mdf/mdf_client/mdf_client/installer/dist
fi    

docker run -v /opt/mdf:/opt/mdf -v /opt/mdf/mdf_client/docker:/output \
    -v /opt/mdf/mdf_client/docker/wine_build:/opt/mdf/mdf_client/mdf_client/installer/build \
    -v /opt/mdf/mdf_client/docker/wine_dist:/opt/mdf/mdf_client/mdf_client/installer/dist \
    expertlabservice/client_wine_build_machine:latest

#!/bin/sh

set -e

cur=`pwd`
cd "docker_build_machine$2"

FORCE="$1"

docker buildx build --progress=plain --ssh default="$SSH_AUTH_SOCK" . -t client_build_machine$2 --build-arg FORCE=${FORCE}

cd $cur

#!/bin/sh

set -e

cur=`pwd`
cd docker_build_machine_windows

FORCE="$1"

docker buildx build --ssh default="$SSH_AUTH_SOCK" . -t client_wine_build_machine -f Dockerfile.mdf --build-arg FORCE=${FORCE}

cd $cur

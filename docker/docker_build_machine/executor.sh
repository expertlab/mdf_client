#!/bin/sh
set -e
COMMAND=$1

if [ "$COMMAND" = "test" ]; then
    cd /opt/mdf/mdf_client
    xvfb-run -a -s "-screen 0 1400x900x24 +extension RANDR" python3 -m unittest    
    exit $?
elif [ "$COMMAND" = "run" ]; then
    cd /opt/mdf/mdf_client/mdf_client/bin
    python3 main.py $2
    exit $?
fi

python3 -c "import veusz.helpers.qtloops"
echo "Veusz helpers OK"


cd /opt/mdf/mdf_client/mdf_client/installer
./make.sh "$COMMAND"
if [ $? != 0 ]; then
    echo "Build failed. Not proceeding"
    exit 1
fi

set +e
cp -v `ls -t /opt/mdf/mdf_client/mdf_client/installer/*.run|head -n 1` /output
cp -v `ls -t /opt/mdf/mdf_client/mdf_client/installer/*.exe|head -n 1` /output
cp -v `ls -t /opt/mdf/mdf_client/mdf_client/installer/*.tar.gz|head -n 1` /output
echo "Build completed."

exit 0

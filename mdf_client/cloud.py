#!/usr/bin/python
# -*- coding: utf-8 -*-
"""License update dialog"""
import json
from . import _
from .clientconf import confdb
from mdf_canon.csutil import cloud_query, executor, go
from mdf_canon.logger import get_module_logging
from . import parameters as params
import os
logging = get_module_logging(__name__)

import zlib
import base64


def get_version_file():
    current = os.path.join(params.pathClient, 'VERSION')
    if not os.path.exists(current):
        current += '.txt'
    if not os.path.exists(current):
        return '*** running source code ***'
    r = open(current, 'r').read()
    if 'compile Veusz on Windows' in r:
        return '*** running source code ***'
    return r


def gzinflate(s):
    dec = base64.b64decode(s)
    dec = zlib.decompress(dec)  # [2:-4]
    # return compressed_data
    return dec


def recent_serials():
    serials = []
    for recent in confdb['recent_server']:
        serials.append(recent[4])
    return serials 


def parse_query(opt=None):
    r = query(opt).read()
    r = json.loads(r)
    if r['statusCode'] != 200:
        raise BaseException('invalid statusCode')
    ret = json.loads(gzinflate(r['body']))
    return ret

    
def query(opt=None):
    opt = opt or {}
    opt['url'] = confdb['license_url']
    opt['v_client'] = get_version_file() 
    opt['client_hash'] = confdb['license_hash']
    if 'ident' not in opt:
        opt['client_ident'] = confdb['license_identifier']
    opt['client_local'] = confdb['cloud_local']
    opt['localNick'] = confdb['cloud_localNick']
    opt['recent_server'] = recent_serials()
    return cloud_query(opt)

    
offer_msg = _("""
You are starting a Pay-Per-Measurement test.
The following cost will be billed you your account:
    Base: {price:.1f}
    Discount: {discount:.1f} %
    Billed: {cost:.1f}""")


def ppm_command(server, instrument, command='ppm_quote'):
    opt = {'cmd': command,
           'serial': server['eq_sn'],
           'curve': server.kiln['curve'],
           'name': instrument.measure['name'],
           'uid': server.storage['nextUid'],
           'instrument': instrument['name'],
           'hash': server.license['hash'],
           }
    logging.debug('ppm_command', opt)
    ret = parse_query(opt)
    msg = offer_msg.format(**ret)
    return ret, msg


def ping(opt=None):
    if 'running source code' in get_version_file():
        logging.debug('NO PING - DEVEL')
        return
    return executor().submit(query, opt)


def notify_sound():
    file = os.path.abspath(confdb['logsound'])
    print('paplay', file)
    go('paplay ' + file)


def notify(server, notify, title, msg, **data):
    if not (confdb['cloud_user'] and confdb['cloud_password'] and confdb['license_url']):
        return False
    data.update({ 'cmd':'user_notify', 'notify':notify, 'message':msg,
                                'title': title,
                                'serial': server['eq_sn'],
                                'T': server.kiln['T'],
                                'user':confdb['cloud_user'],
                                'password':confdb['cloud_password'],
                                'url': confdb['license_url']
                            })
    executor().submit(cloud_query, data)
    return True

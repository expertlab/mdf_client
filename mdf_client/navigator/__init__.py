#!/usr/bin/python
# -*- coding: utf-8 -*-
from .navigator import Navigator, NavigatorToolbar
from .quick import QuickOps
from . import domains
from . import instrument_domains
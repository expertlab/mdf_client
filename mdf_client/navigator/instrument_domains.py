#!/usr/bin/python
# -*- coding: utf-8 -*-

from mdf_canon.logger import get_module_logging
from mdf_canon.reference.image import Image
from functools import partial
logging = get_module_logging(__name__)
from mdf_canon.plugin import navigator_domains, NavigatorDomain, node, nodes
from veusz.dialogs.plugin import PluginDialog
from veusz import document

from .. import _
from ..filedata import getFileProxy
from ..fileui import ImageSlider
from .. import axis_selection
from .. import iutils

from ..qt import QtWidgets

ism = isinstance

dilatometer_subsamples = ('Right', 'Left', 'Center', 'Height', 'Base')


class ImageAnalysisNavigatorDomain(NavigatorDomain):
    _report_template = 'report_horizontal.vsz'
    _report_dataset = 'd'
    _report_images = 'profile'
    
    def __init__(self, *a, **k):
        super(ImageAnalysisNavigatorDomain, self).__init__(*a, **k)
        self.storyboards = {}
        self.threads = {}
        self.extrusions = {}
        
    @node
    def report(self, node=False):
        """Execute HorzizontalReportPlugin on `node`"""
        from mdf_client import plugin
        measures = []
        for sub_name, sub_node in node.items():
            print(sub_name, sub_node, len(sub_node))
            # Skip non-dataset nodes
            if not hasattr(sub_node, 'is_derived'):
                continue
            # Skip derived nodes
            if sub_node.is_derived:
                continue
            if not len(sub_node.ds or []):
                continue
            measures.append(sub_name.split('/')[-1])
        images = []
        cf = node.get_configuration()
        if 'recProfile' in cf and cf['recProfile']:
            images.append('profile')
        if 'recFrame' not in cf or cf['recFrame']:
            images.append('frame')
        p = plugin.ReportPlugin(node, self._report_template, self._report_dataset, self._report_images,
                                measures, images)
        d = PluginDialog(self.mainwindow, self.doc, p, plugin.ReportPlugin)
        self.mainwindow.showDialog(d)
    
    def create_storyboard(self, node):
        obj = self.storyboards.get(node.path, False)
        if obj:
            return obj
        obj = ImageSlider()
        obj.set_doc(self.doc)
        path = '{}/profile'.format(node.path)
        logging.debug('Storyboard path', path)
        if not obj.setPath(path):
            n, p = path.split(':')
            path1 = '{}:ver_1/{}'.format(n, p)
            logging.debug('Retry with storyboard path', path, path1)
            if obj.setPath(path1) is False:
                logging.debug('Failed setting storyboard path!', path, path1)
                QtWidgets.QMessageBox.warning(None, _('Cannot create a storyboard'),
                                          _('Failed creating a storyboard for path:\n{}\n{}').format(path,
                                                                                                    path1))
                return False
        self.storyboards[node.path] = obj
        return obj
    
    @node
    def show_storyboard(self, node):
        obj = self.create_storyboard(node)
        if obj: obj.show()
        
    @node
    def render(self, node=False):
        """Render video from `node`"""
        from mdf_client import video
        sh = getFileProxy(node.linked.filename, mode='r')
        pt = '/' + \
            node.path.replace(node.linked.prefix, '').replace('summary', '')
        v = video.VideoExporter(sh, pt)
        v.exec_()
        sh.close()
        
    @node
    def extrude(self, node=False):
        """Build profile extrusion on `node`"""
        if node.path in self.extrusions:
            self.extrusions[node.path].show()
            return True
        if not node.linked:
            logging.debug('No linked file for node', node.path)
            return False
        proxy = self.doc.proxies.get(node.linked.filename, False)
        if not proxy:
            logging.debug('No opened file for node', node.path)
            return False
        from mdf_client import extrusion
        if not extrusion.enabled:
            logging.error('Extrusion is not enabled!')
            QtWidgets.QMessageBox.warning(None, _('Extrustion not enabled'),
                                      _('Extrusion module is missing or incomplete'))
            return False
        wg = extrusion.ExtrusionRender(proxy, node.model_path + '/profile')
        self.extrusions[node.path] = wg
        wg.show()
        return True
        
    @node
    def postanalysis(self, node=False):
        """Repeat image analysis on `node`"""

        if not node.linked:
            logging.debug('No linked file for node', node.path)
            return False
        proxy = self.doc.proxies.get(node.linked.filename, False)
        if not proxy:
            logging.debug('No opened file for node', node.path)
            return False
        from mdf_client import postanalysis
        if not postanalysis.enabled:
            logging.error('PostAnalysis is not enabled!')
            QtWidgets.QMessageBox.warning(None, _('PostAnalysis not enabled'),
                                      _('PostAnalysis module is missing or incomplete'))
            return False
        
        sample = node.get_configuration()
        th = postanalysis.deferred_postanalysis(proxy, sample['fullpath'])
        self.threads[node.path + '|postanalysis'] = th
        
    @node
    def showPoints(self, node=False):
        """Show characteristic points"""
        from mdf_client import plugin
        path = node.path
        target = ''
        v = path.split('/')
        while not v[-1].startswith('sample'):
            v.pop(-1)
            node = node.parent
            target = path
        p = plugin.ShapesPlugin(sample=node, target=target)
        d = PluginDialog(self.mainwindow, self.doc, p, plugin.ShapesPlugin)
        self.mainwindow.showDialog(d)
        
    def add_dataset_menu(self, menu, node):
        if self.is_plotted(node):
            menu.addAction(_('Show Characteristic Points'), partial(self.showPoints, node))
        return True
        
    def add_sample_menu(self, menu, node):
        if node.path.split('/')[-1] in dilatometer_subsamples:
            return True
        menu.addAction(iutils.theme_icon('report'), _('Report'), partial(self.report, node))
        menu.addAction(iutils.theme_icon('characteristic_points'), _('Show Characteristic Points'), partial(self.showPoints, node))
        menu.addAction(_('Open storyboard'), partial(self.show_storyboard, node))
        menu.addAction(iutils.theme_icon('video-x-generic'), _('Render video'), partial(self.render, node))
        menu.addAction(_('3D Extrusion'), partial(self.extrude, node))
        menu.addAction(_('Post-analysis'), partial(self.postanalysis, node))
        return True
        
    def add_group_menu(self, menu, node):
        spl = node.path.split('/')
        if spl[-1] in dilatometer_subsamples:
            menu.addAction(_('Open storyboard'), partial(self.show_storyboard, node))
            menu.addAction(iutils.theme_icon('video-x-generic'), _('Render video'), partial(self.render, node))
            from mdf_client import extrusion, postanalysis
            if extrusion.enabled:
                menu.addAction(_('3D Extrusion'), partial(self.extrude, node))
            if postanalysis.enabled:
                menu.addAction(_('Post-analysis'), partial(self.postanalysis, node))
        return True

      
class MicroscopeSampleNavigatorDomain(ImageAnalysisNavigatorDomain):
    _report_template = 'report_hsm.vsz'
    _report_dataset = 'Vol'
    
    def check_node(self, node):
        if not node:
            return False
        return 'hsm/sample' in node.path
        
    @node
    def viscosity(self, node=False):
        """Execute ViscosityPlugin on `node`"""
        from mdf_client import plugin
        # Default sample T
        T = node.path + '/T' 
        # If missing, get nearest T dataset
        if T not in self.doc.data:
            logging.debug('Sample temperature dataset is missing', T)
            T = self.xnames(node, page='/temperature')[0]
        p = plugin.ViscosityPlugin(ds_in=T, ds_out=node.path + '/viscosity')
        d = PluginDialog(self.mainwindow, self.doc, p, plugin.ViscosityPlugin)
        self.mainwindow.showDialog(d)

    @nodes
    def surface_tension(self, nodes=False):
        """Call the SurfaceTensionPlugin.
        - 1 node selected: interpret as a sample and directly use its beta,r0,Vol,T datasets
        - 2 nodes selected: interpret as 2 samples and search the node having beta,r0 children; use dil/T from the other
        - 4 nodes selected: interpret as separate beta, r0, Vol, T datasets and try to assign based on their name
        - 5 nodes selected: interpret as separate (beta, r0, T) + (dil, T) datasets and try to assign based on their name and path
        """
        if len(nodes) > 1:
            logging.debug('Not implemented')
            return False
        smp = nodes[0].children
        dbeta, nbeta = self.dsnode(smp['beta'])
        beta = nbeta.path
        dR0, nR0 = self.dsnode(smp['r0'])
        R0 = nR0.path
        ddil, ndil = self.dsnode(smp['Vol'])
        dil = ndil.path
        T = nbeta.linked.prefix + 'kiln/T'
        out = nbeta.linked.prefix + 'gamma'
        if T not in self.doc.data:
            T = ''
        # Load empty datasets
        if len(dbeta) == 0:
            self.navigator._load(nbeta)
        if len(dR0) == 0:
            self.navigator._load(nR0)
        if len(ddil) == 0:
            self.navigator._load(ndil)
        from mdf_client import plugin
        cls = plugin.SurfaceTensionPlugin
        p = cls(beta=beta, R0=R0, T=T,
                dil=dil, dilT=T, ds_out=out, temperature_dataset=self.doc.data[T].data)
        d = PluginDialog(self.mainwindow, self.doc, p, cls)
        self.mainwindow.showDialog(d)
        
    @node
    def export_images(self, node=False):
        storyboard = self.create_storyboard(node)
        if storyboard:
            storyboard.strip.export_images()
            
    def add_sample_menu(self, menu, node):
        ImageAnalysisNavigatorDomain.add_sample_menu(self, menu, node)
        j = 0
        k = ['beta', 'r0', 'Vol']
        for kj in k:
            j += kj in node.children
        if j == len(k):
            menu.addAction(_('Surface tension'), partial(self.surface_tension, node))
        menu.addAction(_('Viscosity'), partial(self.viscosity, node))
        menu.addAction(_('Export images'), partial(self.export_images, node))
        return True

    
class DilatometerNavigatorDomain(ImageAnalysisNavigatorDomain):
    _report_template = 'report_horizontal.vsz'

    @node
    def calibration(self, node=False):
        """Call the CalibrationFactorPlugin on the current node"""
        ds, node = self.dsnode(node)

        T = self.xnames(node, "/temperature")[0]  # in current page

        from mdf_client import plugin
        p = plugin.CalibrationFactorPlugin(d=node.path, T=T)
        d = PluginDialog(self.mainwindow, self.doc, p, plugin.CalibrationFactorPlugin)
        self.mainwindow.showDialog(d)
        
    @node
    def show_cte_table(self, node=False):
        sample = node.get_configuration()
        if sample['devpath'] == 'MAINSERVER':
            sample = getattr(sample, self._instrument_name).sample0
        elif sample['devpath'] == self._instrument_name:
            sample = sample.sample0
            
        opt = sample['fullpath'] + 'cte'
        graph = self.navigator.get_graph()
        name = node.path.replace('/', '_')
        ds = node.path + '/d'
        op = document.OperationWidgetAdd(graph, 'optionlabel', autoadd=False, name=name,
                                    showName=True, option=opt, dataset=ds)
        wg = self.doc.applyOperation(op)
        
    def add_dataset_menu(self, menu, node):
        r = ImageAnalysisNavigatorDomain.add_dataset_menu(self, menu, node)
        if not axis_selection.is_calibratable(node.path):
            return r
        menu.addAction(_('Calibration'), partial(self.calibration, node))
        return True
    
    def add_sample_menu(self, menu, node): 
        ImageAnalysisNavigatorDomain.add_sample_menu(self, menu, node)
        menu.addAction(iutils.theme_icon('cte_table'), _('Show CTE Table'), partial(self.show_cte_table, node))
        return True
    
    def add_file_menu(self, menu, node):
        ImageAnalysisNavigatorDomain.add_file_menu(self, menu, node)
        menu.addAction(iutils.theme_icon('cte_table'), _('Show CTE Table'), partial(self.show_cte_table, node))
        return True
    
    
class HorizontalSampleNavigatorDomain(DilatometerNavigatorDomain):
    _report_template = 'report_horizontal.vsz'
    _instrument_name = 'horizontal'

    def check_node(self, node):
        if not node:
            return False
        return 'horizontal/sample' in node.path
    
 
class VerticalSampleNavigatorDomain(DilatometerNavigatorDomain):
    _report_template = 'report_vertical.vsz'
    _instrument_name = 'vertical'
    
    def check_node(self, node):
        if not node:
            return False
        return 'vertical/sample' in node.path
    
        
class FlexSampleNavigatorDomain(ImageAnalysisNavigatorDomain):
    _report_template = 'report_flex.vsz'
    _instrument_name = 'flex'
    
    def check_node(self, node):
        if not node:
            return False
        return 'flex/sample' in node.path
        
    @node
    def absolute_fleximeter(self, node=False):
        from mdf_client import plugin
        # Load required datasets
        smp = node.children
        for sub in ('Center', 'Right', 'Left'):
            sub_ds, sub_node = self.dsnode(smp[sub].children['d'])
            if len(sub_ds) == 0:
                self.navigator._load(sub_node)
        
        n = node.path
        p = plugin.AbsoluteFlexPlugin(n + '/Center/d',
                                      n + '/Right/d',
                                      n + '/Left/d',
                                      ds_out=n + '/d2',
                                      ds_radius=n + '/radius2',)
        
        d = PluginDialog(self.mainwindow, self.doc, p, plugin.AbsoluteFlexPlugin)
        self.mainwindow.showDialog(d)
    
    def add_sample_menu(self, menu, node): 
        ImageAnalysisNavigatorDomain.add_sample_menu(self, menu, node)
        menu.addAction(_('Absolute fleximeter'), partial(self.absolute_fleximeter, node))
        return True
    
     
navigator_domains += [MicroscopeSampleNavigatorDomain,
            HorizontalSampleNavigatorDomain, VerticalSampleNavigatorDomain,
            FlexSampleNavigatorDomain]

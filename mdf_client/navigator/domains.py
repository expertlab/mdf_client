#!/usr/bin/python
# -*- coding: utf-8 -*-
import functools

from mdf_canon.logger import get_module_logging
from _functools import partial
from veusz.utils.search import searchFirstOccurrence
logging = get_module_logging(__name__)
from mdf_canon.plugin import navigator_domains, NavigatorDomain, node, nodes
from mdf_canon.plugin import domains as canon_domains
from mdf_canon.indexer import SharedFile
from mdf_canon import option

from .. import conf, units, iutils
from veusz.dialogs.plugin import PluginDialog

from mdf_client.qt import QtWidgets, QtGui, QtCore
canon_domains.QtUserRole = QtCore.Qt.UserRole

from .. import _
from ..filedata import MisuraDocument
from ..filedata import DatasetEntry
from ..filedata import getFileProxy
from ..fileui import VersionMenu
from ..clientconf import confdb, rule_suffixes
from ..widgets.aTable import table_model_export
from ..live import registry

default_style = ['', 0, '', '', '']

from mdf_client.fileui import version


class DataNavigatorDomain(NavigatorDomain):

    def __init__(self, *a, **k):
        super(DataNavigatorDomain, self).__init__(*a, **k)
        self.configuration_windows = {}
        self.data_tables = {}
        self.test_windows = {}
        
    @node
    def edit_dataset(self, node=False):
        """Slot for opening the dataset edit window on the currently selected entry"""
        ds, y = self.dsnode(node)
        name = node.path
        logging.debug('name', name)
        dialog = self.mainwindow.slotDataEdit(name)
        if ds is not y:
            dialog.slotDatasetEdit()
            
    def create_shortcuts(self):
        QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_V),
                        self.navigator,
                        self.edit_dataset,
                        context=QtCore.Qt.WidgetWithChildrenShortcut)
        QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_D),
                        self.navigator,
                        self.configure,
                        context=QtCore.Qt.WidgetWithChildrenShortcut)

    @node
    def viewFile(self, node=False):
        if not node.linked:
            return False
        win = self.test_windows.get(node.linked.filename, False)
        if win:
            win.show()
            return
        doc = MisuraDocument(node.linked.filename)
        from mdf_client import browser
        win = browser.TestWindow(doc)
        win.show()
        self.test_windows[node.linked.filename] = win
        
    def cleanup_title(self, title):
        ops = []
        for page in self.doc.basewidget.children:
            if not page.hasChild('title'):
                logging.debug('No title in page', page.path)
                continue
            obj = page.getChild('title')
            label = obj.settings['label']
            if title not in label:
                logging.debug('No title in label', label, '\n', title)
                continue
            logging.debug('Cleanup page title', label, '\n', 'From:', title)
            label = label.replace(title, '').replace(' |  | ', ' | ')
            if label.startswith(' | '):
                label = label[3:]
            if label.endswith(' | '):
                label = label[:-3]
            if not label:
                label = page.settings['notes']
            op = document.OperationSettingSet(obj.settings.get('label'), label)
            ops.append(op)
        if ops:
            self.doc.applyOperation(document.OperationMultiple(ops, 'Cleanup title'))
            
    @node
    def closeFile(self, node=False):
        # FIXME: model no longer have a "tests" structure.
        lk = node.linked
        if not lk:
            logging.debug('Node does not have linked file', node.path)
            return False
        for ds in list(self.doc.data.values()):
            if ds.linked == lk:
                self.navigator.deleteData(ds)

        for key, ds in list(self.doc.available_data.items()):
            if ds.linked == lk:
                del self.doc.available_data[key]
        
        self.cleanup_title(lk.get_title())
            
        self.model().refresh(True)

    @node
    def reloadFile(self, node=False, version=None):
        logging.debug('RELOADING', node.path, repr(version))
        linked = node.linked
        if not linked:
            logging.warning('Cannot reload unlinked dataset', node.path)
            return False
        if version is not None:
            # propagate new version information to all datasets
            for ds in self.doc.data.values():
                if ds.linked and ds.linked.params.filename == node.linked.params.filename:
                    ds.linked.params.version = version
            linked.params.version = version
        r = linked.reloadLinks(self.doc)
        self.doc.setModified()
        logging.debug('reloadLinks', r)

    @node
    def recalculate_metadata(self, node=False):
        iutils.with_waiting_mouse_cursor(lambda: self._recalculate_metadata(node))
        QtWidgets.QMessageBox.information(None, 'Info', 'Metadata recalculated')
        registry.force_redraw()
        self.doc.signalModified.emit(True)

    def _recalculate_metadata(self, node):
        shared_file = SharedFile(node.linked.filename, mode='r')
        shared_file.conf = node.get_configuration().root
        version.save_version(self.doc, shared_file, nosync=True, parent=self.navigator)
        shared_file.run_scripts()
        
    @node
    def log_panel(self, node=False):
        proxy = getFileProxy(node.linked.filename, mode='r')
        from mdf_client import fileui
        self._log_panel = QtWidgets.QWidget()
        self._log_panel.setLayout(QtWidgets.QHBoxLayout())
        self._log_panel.layout().addWidget(fileui.OfflineLog(proxy))
        self._log_panel.show()

    def load_version(self, LF, version):
        # FIXME: VERSIONING!
        logging.debug('LOAD VERSION')
        LF.params.version = version
        LF.reloadLinks(self.doc)

        fl = self.model().files
        logging.debug('got linked files', self.model().files[:])

    def add_load(self, menu, node):
        """Add load/unload action"""
        self.act_load = menu.addAction(_('Load'), self.navigator.load)
        self.act_load.setCheckable(True)
        is_loaded = True
        if node.linked is None:
            self.act_load.setVisible(False)
        else:
            is_loaded = (node.ds is not False) and (len(node.ds) > 0)
            self.act_load.setChecked(is_loaded)
            if not is_loaded:
                menu.addAction(iutils.theme_icon('help-about'), _('Plot'), partial(self.plot, node))
        return is_loaded

    @node
    def keep(self, node=False):
        """Inverts the 'keep' flag on the current dataset,
        causing it to be saved (or not) on the next file commit."""
        ds, node = self.dsnode(node)
        cur = getattr(ds, 'm_keep', False)
        ds.m_keep = not cur

    def add_keep(self, menu, node):
        temporary_disabled = True
        return temporary_disabled
        """Add on-file persistence action"""
        self.act_keep = menu.addAction(
            _('Saved on test file'), partial(self.keep, node))
        self.act_keep.setCheckable(True)
        self.act_keep.setChecked(node.m_keep)

    @node
    def save_on_current_version(self, node=False):
        # FIXME: Find out local t dataset!
        proxy = getFileProxy(node.linked.filename, mode='a')
        prefix = node.linked.prefix
        try:
            proxy.save_data(node.ds.m_col, node.ds.data, self.model().doc.data[prefix + "t"].data)
        except Exception as e:
            message = "Impossible to save data.\n\n" + str(e)
            QtWidgets.QMessageBox.warning(None, 'Error', message)
        proxy.close()

    @nodes
    def overwrite(self, nodes=False):
        """Overwrite first selected dataset with second selected."""
        from mdf_client import plugin
        p = plugin.OverwritePlugin(
            dst=nodes[0].path, src=nodes[1].path, delete=True)
        d = PluginDialog(self.mainwindow, self.doc, p, plugin.OverwritePlugin)
        self.mainwindow.showDialog(d)
        
    def activate_rule(self, node, act):
        line = '\n{}$'.format(node.path.split(':')[-1])
        rule = 'rule_' + rule_suffixes[act - 1]
        if line in confdb[rule]:
            logging.warning('Rule already exists: {}\n{}'.format(line, confdb[rule]))
        else:
            confdb[rule] += line
            logging.warning('Added rule:', rule, line, confdb[rule])
        
        # Remove any lower rule
        o = set(range(1, 5))
        o.remove(act)
        for n in o:
            logging.debug('REMOVING OTHER RULE', n)
            self.deactivate_rule(node, n)
        
        confdb._rule_dataset = False
        
    def deactivate_rule(self, node, act):
        line = '\n{}$'.format(node.path.split(':')[-1])
        rule = 'rule_' + rule_suffixes[act - 1]
        txt = confdb[rule]
        if line in txt:
            confdb[rule] = txt.replace(line, '')
            logging.debug('Removing rule', rule, line, confdb[rule])
        else:
            logging.warning('Error removing load rule', rule, line, txt)
            return False
        
        confdb._rule_dataset = False
        return True

    @node
    def change_rule(self, node=False, act=None):
        """Change current loading rule."""
        if act is None:
            return False
        checked = False
        for r in confdb.rule_dataset(node.path, latest='all'):
            if r[0] - 1 == act:
                checked = True
                break
        if checked:
            logging.debug('Activating rule', node, act)
            self.activate_rule(node, act)
        else:
            logging.debug('Deactivating rule', node, act)
            self.deactivate_rule(node, act)
        confdb.save()

    def update_role_actions(self, node):
        # Find the highest matching rule
        rules = confdb.rule_dataset(node.path, latest='all')
        if not rules:
            return
        for r in rules:
            self.act_rule[r[0] - 1].setChecked(True)
        # Resolve dependencies
        # Plot involves loading
        if self.act_rule[3].isChecked():
            self.act_rule[2].setChecked(True)
        # Loading involves listing
        if self.act_rule[2].isChecked():
            self.act_rule[1].setChecked(True)
        # Ignore disables load/plot
        if self.act_rule[0].isChecked():
            self.act_rule[2].setChecked(False)
            self.act_rule[3].setChecked(False)

    def add_rules(self, menu, node):
        """Add loading rules sub menu"""
        menu = menu.addMenu(_('Rules'))
        self.act_rule = []
        self.func_rule = []

        def gen(name, trname):
            f = functools.partial(self.change_rule, node=node, act=name)
            act = menu.addAction(trname, f)
            act.setCheckable(True)
            self.act_rule.append(act)
            self.func_rule.append(f)

        gen(1, _('Ignore'))
        gen(2, _('Available'))
        gen(3, _('Load'))
        gen(4, _('Plot'))
        
        self.update_role_actions(node)

    @node
    def configure(self, node):
        """Show node configuration panel"""
        path = node.path.split(':')[-1]
        if path[0] in '0123456789':
            path = ''
        cp = node.linked.conf
        if path:
            cp = cp.toPath(path)
        self.configure_proxy(cp, path)
        
    def configure_proxy(self, proxy, path=False):
        if not path:
            path = proxy['fullpath']
        win = conf.TreePanel(proxy, select=proxy)
        win.setWindowTitle('Configuration tree from: %s' % proxy['name'])
        win.show()
        # Keep a reference for Qt
        self.configuration_windows[path] = win     

    def add_configuration(self, menu, node):
        if node.linked and hasattr(node.linked, 'conf'):
            a = menu.addAction(iutils.theme_icon("details"), _('Details (D)'), partial(self.configure, node))
            a.setToolTip(node.path)
            
    def add_nodoc_menu(self, menu, proxy):
        menu.addAction(_('Configure'), functools.partial(self.configure_proxy, proxy))
        
    @node
    def add_versions_menu(self, node=False, menu=None):
        if not node:
            return False
        if not node.linked:
            return False
        local = False
        proxy = self.doc.proxies.get(node.linked.params.filename, None)
        if proxy is None:
            proxy = getFileProxy(node.linked.filename, mode='r')
            local = proxy
        self.doc.proxies[node.linked.filename] = proxy
        logging.debug('add_versions_menu', node, node.linked.params.filename, proxy)
        if proxy is not None:
            versions = VersionMenu(self.doc, proxy=proxy, parent=menu)
            if menu:
                menu.addMenu(versions)
            versions.versionChanged.connect(functools.partial(self.reloadFile, node))
            if local:
                local.close()
            return versions
        if local:
            local.close()
        logging.warning('Cannot create versions menu')
        return False

    def add_file_menu(self, menu, node):
        menu.addAction(iutils.theme_icon("add"), _('View'), partial(self.viewFile, node))
        menu.addAction(_('Reload'), partial(self.reloadFile, node))
        menu.addAction(_('Recalculate metadata'), partial(self.recalculate_metadata, node))
        menu.addAction(_('Logs'), partial(self.log_panel, node))
        a = menu.addAction(iutils.theme_icon('edit-delete'), _('Close'), partial(self.closeFile, node))
        self.versions = self.add_versions_menu(node, menu)
        self.add_configuration(menu, node)
        if len(self.data_tables):
            tab_menu = menu.addMenu('Tables')
            for tab_name, tab_window in self.data_tables.items():
                if len(tab_name) > 30:
                    tab_name = tab_name[:30] + '...'
                tab_menu.addAction(tab_name, tab_window.show)
        return True

    def add_group_menu(self, menu, node):
        self.add_configuration(menu, node)

    def add_sample_menu(self, menu, node):
        self.add_configuration(menu, node)
        menu.addAction(_('Delete'), partial(self.navigator.deleteChildren, node))
        return True

    def add_dataset_menu(self, menu, node):
        menu.addSeparator()
        self.add_load(menu, node)
        self.add_keep(menu, node)
        menu.addAction(iutils.theme_icon('edit'), _('Edit (V)'), partial(self.edit_dataset, node))
        menu.addAction(('Save on current version'), partial(self.save_on_current_version, node))
        self.add_rules(menu, node)
        menu.addAction(_('Delete'), partial(self.navigator.deleteData, node))
        return True

    def add_derived_dataset_menu(self, menu, node):
        self.add_keep(menu, node)
        menu.addAction(iutils.theme_icon('edit'), _('Edit (V)'), partial(self.edit_dataset, node))
        menu.addAction(_('Delete'), partial(self.navigator.deleteData, node))

    def create_table(self, header):
        tab_name = ', '.join(header)
        key = 'SummaryWidget:' + tab_name
        if self.navigator.show_widget_key(key):
            return
        
        from mdf_client.fileui import SummaryWidget
        tab = SummaryWidget()  # self.navigator)
        tab.set_doc(self.doc)
        tab.model().auto_load = False
        tab.model().set_loaded(header)
        
        tab.setWindowTitle('Table {} ({})'.format(len(self.data_tables), tab_name))
        self.navigator.show_widget(tab, key)
        # Keep another reference for nav menu
        self.data_tables[tab_name] = tab
    
    def export_csv(self, datasets):
        table_model_export(datasets, self.doc.get_column_func, None, self.navigator,
                           self.doc.get_unit_func,
                           self.doc.get_verbose_func)

    def add_multiary_menu(self, menu, nodes):
        header = self.get_datasets_from_selection()
        nodes = [h[1] for h in header]
        header = [h[0] for h in header]
        if len(header):
            menu.addAction(iutils.theme_icon('datasets_table'), _('Table from selection'), functools.partial(self.create_table, header))
            menu.addAction(iutils.theme_icon('edit'), _('Export to csv'), functools.partial(self.export_csv, header))
        menu.addAction(_('Delete selection'), partial(self.navigator.deleteDatas, nodes))
        if len(nodes) == 2:
            a = menu.addAction(_('Overwrite {} with {}').format(nodes[0].name(), nodes[1].name()), partial(self.overwrite, nodes))
            a.setToolTip(_('{} will be overwritten by {}').format(nodes[0].path, nodes[1].path))
            # Qt5.1: a.setToolTipVisible(True)


class PlottingNavigatorDomain(NavigatorDomain):

    def check_node(self, node):
        if not node:
            return False
        if not node.ds:
            return False
        is_loaded = len(node.ds) > 0
        return is_loaded

    @node
    def thermalLegend(self, node=False):
        """Write thermal cycle onto a text label"""
        lk = node.linked
        dsname = False
        for key, ds in self.doc.data.items():
            if ds.linked == lk:
                dsname = key
                break
        if not dsname:
            logging.error('Cannot any dataset loaded for the linked test file:', lk.filename)
            return False
        
        plt = searchFirstOccurrence(self.navigator.get_page(), 'graph')
        op = document.OperationWidgetAdd(plt, 'thermalcyclelabel', name=lk.prefix + 'thermalcycle',
                                         dataset=dsname, option='/kiln/curve') 
        return self.doc.applyOperation(op)
        
    @node
    def intercept(self, node=False):
        """Intercept all curves derived/pertaining to the current object"""
        if isinstance(node, DatasetEntry):
            dslist = [node.path]
        elif hasattr(node, 'datasets'):
            # FIXME: needs paths
            dslist = list(node.children.keys())
        else:
            dslist = []
        from mdf_client import plugin
        xnames = self.xnames(node, page='/time')
        xnames.append('')
        smooth = 0
        if xnames[0] in self.doc.data:
            ds = self.doc.data[xnames[0]]
            
        p = plugin.InterceptPlugin(target=dslist, axis='X', critical_x=xnames[0], smooth=smooth)
        d = PluginDialog(self.mainwindow, self.doc, p, plugin.InterceptPlugin)
        self.mainwindow.showDialog(d)
        
    @node
    def remove_jumps(self, node=False):
        page = self.navigator.get_page().path
        plotpath = self.navigator.widget_path_for(node, prefix=page)
        if not len(plotpath) > 1:
            return False
        from mdf_client import plugin
        p = plugin.RemoveJumpsPlugin(curve=plotpath, plotwindow=self.mainwindow.plot)
        op = document.OperationToolsPlugin(p, {'curve': plotpath})
        self.doc.applyOperation(op)

    def add_plotted(self, menu, node, is_plotted=False):
        """Add plot/unplot action"""
        self.act_plot = menu.addAction(iutils.theme_icon('help-about'), _('Plot'), self.plot)
        self.act_plot.setCheckable(True)
        self.act_plot.setChecked(is_plotted)

    @node
    def colorize(self, node=False):
        """Set/unset color markers."""
        plotpath = self.model().is_plotted(node.path)
        if not len(plotpath) > 0:
            return False
        x = self.xnames(node)[0]
        from mdf_client import plugin
        p = plugin.ColorizePlugin(curve=plotpath[0], x=x)
        d = PluginDialog(self.mainwindow, self.doc, p, plugin.ColorizePlugin)
        self.mainwindow.showDialog(d)

    def find_style_row(self, path):
        idx = -1
        row = False
        for i, row in enumerate(confdb.rule_style.tab):
            if row[0] == path:
                logging.debug('Overwriting rule', idx, row)
                idx = i
                break
        return idx, row
    
    def save_style(self, node, var, style):
        """Save current curve color, style, marker and axis ranges and scale."""
        page = self.navigator.get_page().path
        plotpath = self.navigator.widget_path_for(node, prefix=page)
        if not len(plotpath) > 1:
            return False
        
        style = style[:]

        plot = self.doc.resolveWidgetPath(None, plotpath)
        
        path = node.path.split(':')[-1] + '$'
        
        def sset(idx, val):
            if style[idx] == val:
                # Unset
                style[idx] = default_style[idx]
            else:
                style[idx] = val
        
        if plot.parent.hasChild(plot.settings.yAxis):
            yaxis = plot.parent.getChild(plot.settings.yAxis)
            if var in (False, 'range'):
                sset(confdb.RULE_RANGE, '{}:{}'.format(yaxis.settings.min, yaxis.settings.max))
            if var in (False, 'scale'):
                sset(confdb.RULE_SCALE, yaxis.settings.datascale)
        if var in (False, 'color'): 
            sset(confdb.RULE_COLOR, plot.settings.PlotLine.color)
        if var in (False, 'line'):
            sset(confdb.RULE_LINE, plot.settings.PlotLine.style)
        if var in (False, 'marker'):
            sset(confdb.RULE_MARKER, plot.settings.marker)
        
        if style[confdb.RULE_RANGE] == 'Auto:Auto':
            style[confdb.RULE_RANGE] = ''
        if style[confdb.RULE_MARKER] == 'none':
            style[confdb.RULE_MARKER] = ''
        if abs(style[confdb.RULE_SCALE] - 1) < 1e-32:
            style[confdb.RULE_SCALE] = 0
        
        idx, row = self.find_style_row(path)
        tab = confdb['rule_style']
        if style == default_style and idx >= 0:
            logging.debug('Removing style rule', path)
            tab.pop(idx + 1)
        else:
            style = [path] + style
            if idx < 0:
                tab.append(style)
            else:
                tab[idx + 1] = style
        
        confdb['rule_style'] = tab    
        logging.debug('Saving new style', tab)   
        confdb.save()
        confdb._rule_style = False

    @node
    def delete_style(self, node=False):
        """Delete style rule."""
        # TODO: delete_style
        path = node.path.split(':')[-1] + '$'
        idx, row = self.find_style_row(path)
        if idx < 0:
            logging.warning('Could not find saved style rule', path)
            return False
        tab = confdb['rule_style']
        r = tab.pop(idx + 1)
        logging.debug('Removed saved style rule', idx, r)
        confdb['rule_style'] = tab
        confdb._rule_style = False
        confdb.save()
        
    @node
    def edit_style(self, node):
        """Edit style rule"""
        i, rule = confdb.rule_style.query(node.path)
        tab = confdb['rule_style']
        if i is None:
            style = default_style[:]
            style.insert(0, node.path.split(':')[-1] + '$')
            tab.append(style)
            i = len(tab) - 1
        else:
            i += 1
        rule = tab[i]
        desc = {}
        option.ao(desc, 'rule', 'String', current=rule[0], name=_("Dataset rule"), attr=['ReadOnly', 'Config'])
        option.ao(desc, 'range', 'String', current=rule[1], factory_default='', name=_("Axis range (a:b)"))
        option.ao(desc, 'scale', 'Float', current=rule[2], factory_default=1, name=_("Axis scale"))
        option.ao(desc, 'color', 'String', current=rule[3], factory_default='', name=_("Curve color"))
        option.ao(desc, 'line', 'String', current=rule[4], factory_default='', name=_("Curve line"))
        option.ao(desc, 'marker', 'String', current=rule[5], factory_default='', name=_("Curve marker"))
        
        cp = option.ConfigurationProxy({'self': desc})
        chk = conf.InterfaceDialog(cp, cp, desc)
        chk.setWindowTitle(_("Edit Style Rule"))
        ok = chk.exec_()
        if ok:
            rule1 = [rule[0], cp['range'], cp['scale'], cp['color'], cp['line'], cp['marker']]
            tab = confdb['rule_style']
            logging.debug('edit_syle: old', i, tab[i], rule, '\nnew:', rule1)
            tab[i] = rule1
            confdb['rule_style'] = tab
            confdb.save()
         
    def add_styles(self, menu, node):
        """Styles sub menu"""
        plotpath = self.model().is_plotted(node.path)
        if not len(plotpath) > 0:
            return
        style_menu = iutils.QMenuWithToolTips(_('Style'), menu)
        menu.addMenu(style_menu)
        style_menu.addAction(_('Edit style'), partial(self.edit_style, node))
        
        rule = confdb.rule_style(node.path) 
        
        print('add_styles', node.path, rule)
        
        style_menu.addAction(_('Save curve and axis style'),
                             partial(self.save_style, node, False, rule or default_style))
        
        saved = 0
        for j, var in enumerate(('range', 'scale', 'color', 'line', 'marker')):
            name = 'axis ' if j < 2 else 'curve '
            name += var
            act = style_menu.addAction(_('Save {}').format(name),
                                       partial(self.save_style, node, var, rule or default_style))
            act.setCheckable(True)
            
            if rule and rule[j] != default_style[j]:
                act.setChecked(True)
                act.setToolTip('{}'.format(rule[j]))
                saved += 1
        
        self.act_delete_style = style_menu.addAction(
            _('Delete style'), partial(self.delete_style, node))
        
        if not rule:
            self.act_delete_style.setEnabled(False)
            return
        logging.debug('Menu actions for style rule', rule)
        self.act_delete_style.setEnabled(True)
        
        wg = self.doc.resolveWidgetPath(None, plotpath[0])
        self.act_color = style_menu.addAction(
            _('Colorize'), partial(self.colorize, node))
        self.act_color.setCheckable(True)
        if len(wg.settings.Color.points):
            self.act_color.setChecked(True)

    def build_file_menu(self, menu, node):
        menu.addAction(iutils.theme_icon('thermal_legend'), _('Thermal Legend'), partial(self.thermalLegend, node))
        menu.addAction(_('Intercept all curves'), partial(self.intercept, node))
        return True

    def add_sample_menu(self, menu, node):
        menu.addAction(_('Intercept all curves'), partial(self.intercept, node))
        menu.addAction(_('Delete'), partial(self.navigator.deleteChildren, node))
        return True

    def add_dataset_menu(self, menu, node, derived=False):
        menu.addSeparator()
        is_plotted = self.is_plotted(node) > 0
        self.add_plotted(menu, node, is_plotted)
        if is_plotted:
            menu.addAction(_('Intercept this curve'), partial(self.intercept, node))
            if not derived:
                menu.addAction(_('Remove jumps'), partial(self.remove_jumps, node))
            self.add_styles(menu, node)
        return True

    def add_derived_dataset_menu(self, menu, node):
        return self.add_dataset_menu(menu, node, derived=True)
    
    @nodes
    def synchronize(self, nodes=False):
        from mdf_client import plugin
        page = self.navigator.get_page().path
        paths = []
        for node in nodes:
            # node = self.model().data(node, role=QtCore.Qt.UserRole)
            p = self.navigator.widget_path_for(node, prefix=page)
            if p in ('/', ''):
                logging.warning('synchronize: invalid path', p)
                continue
            paths.append(p)

        sync_plugin = plugin.SynchroPlugin(*paths)

        dialog = PluginDialog(self.mainwindow,
                              self.doc,
                              sync_plugin,
                              plugin.SynchroPlugin)
        self.mainwindow.showDialog(dialog)

    def add_multiary_menu(self, menu, nodes):
        menu.addAction(iutils.theme_icon('sync_curves'), _('Synchronize curves'), partial(self.synchronize, nodes))
        menu.addAction(iutils.theme_icon('help-about'), _('Plot'), partial(self.plot, nodes))


import veusz.document as document


class MathNavigatorDomain(NavigatorDomain):

    def check_node(self, node):
        if not node:
            return False
        if not node.ds:
            return False
        istime = node.path == 't' or node.path.endswith(':t')
        is_loaded = len(node.ds) > 0
        return (not istime) and is_loaded

    @node
    def smooth(self, node=False, dialog=True):
        """Call the SmoothDatasetPlugin on the current node"""
        ds, node = self.dsnode(node)
        w = confdb.smooth_window(len(ds.data))
        from mdf_client import plugin
        out = node.path + '/sm'
        fields = dict(ds_in=node.path, ds_out=out, window=w)
        p = plugin.SmoothDatasetPlugin(**fields)
        if dialog:
            d = PluginDialog(
                self.mainwindow, self.doc, p, plugin.SmoothDatasetPlugin)
            self.mainwindow.showDialog(d)
        else:
            op = document.OperationDatasetPlugin(p, fields)
            self.doc.applyOperation(op)
        return out
    
    @node
    def smooth_no_dialog(self, node=False):
        if not node:
            return False
        return self.smooth(node, False)
        
    @node
    def smooth_and_plot(self, node=False):
        smooth_ds = self.smooth_no_dialog(node)
        plots = self.model().is_plotted(node.path)
        # Not plotted: add a new plot
        if not plots:
            # Remap children
            node.children
            n = node.root.traverse_path(smooth_ds)
            logging.debug('Plotting smoothed data', smooth_ds, n)
            self.plot(n)
            return
        # Already plotted: replace yData in all plots
        ops = []
        for path in plots:
            p = self.doc.resolveWidgetPath(None, path)
            op = document.OperationSettingSet(p.settings.get('yData'), smooth_ds)
            ops.append(op)
        self.doc.applyOperation(document.OperationMultiple(ops, 'SmoothAndPlot'))
        
    @node
    def bandpass(self, node=False):
        """Call the BandPassPlugin on the current node"""
        ds, node = self.dsnode(node)
        ds_t = self.xnames(node, '/time')[0]
        from mdf_client import plugin
        p = plugin.BandPassPlugin(
            ds_in=node.path, ds_t=ds_t, ds_out=node.m_name + '/bp')
        d = PluginDialog(
            self.mainwindow, self.doc, p, plugin.BandPassPlugin)
        self.mainwindow.showDialog(d)

    @node
    def coefficient(self, node=False):
        """Call the CoefficientPlugin on the current node"""
        ds, node = self.dsnode(node)
        w = confdb.smooth_window(len(ds.data))
        ds_x = self.xnames(node, '/temperature')[0]
        ini = getattr(ds, 'm_initialDimension', None)
        if not ini: 
            ini = 0.
        if getattr(ds, 'm_percent', False):
            ini = 0.  # No conversion if already percent
        from mdf_client import plugin
        p = plugin.CoefficientPlugin(
            ds_y=node.path, ds_x=ds_x, ds_out=node.m_name + '/cf', smooth=w, percent=ini)
        d = PluginDialog(
            self.mainwindow, self.doc, p, plugin.CoefficientPlugin)
        self.mainwindow.showDialog(d)

    @node
    def derive(self, node=False):
        """Call the DeriveDatasetPlugin on the current node"""
        ds, node = self.dsnode(node)
        w = confdb.smooth_window(len(ds.data))
        ds_x = self.xnames(node, "/time")[0]  # in current page

        from mdf_client import plugin
        p = plugin.DeriveDatasetPlugin(
            ds_y=node.path, ds_x=ds_x, ds_out=node.m_name + '/der', smooth=w)
        d = PluginDialog(
            self.mainwindow, self.doc, p, plugin.DeriveDatasetPlugin)
        self.mainwindow.showDialog(d)

    def add_dataset_menu(self, menu, node):
        menu.addSeparator()
        menu.addAction(_('Smooth (Alt+S)'), partial(self.smooth, node))
        menu.addAction(iutils.theme_icon('smooth'), _('Smooth+plot (S)'), partial(self.smooth_and_plot, node))
        
        menu.addAction(_('BandPass'), partial(self.bandpass, node))
        menu.addAction(iutils.theme_icon('derivative'), _('Derivatives'), partial(self.derive, node))
        menu.addAction(_('Linear Coefficient'), partial(self.coefficient, node))
        
    def create_shortcuts(self):
        QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.ALT + QtCore.Qt.Key_S),
                        self.navigator,
                        self.smooth_no_dialog,
                        context=QtCore.Qt.WidgetWithChildrenShortcut)
        QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_S),
                        self.navigator,
                        self.smooth_and_plot,
                        context=QtCore.Qt.WidgetWithChildrenShortcut)
    
    add_derived_dataset_menu = add_dataset_menu    
    
    @nodes
    def correct(self, nodes=False):
        """Call the CurveOperationPlugin on the current nodes"""
        ds0, node0 = self.dsnode(nodes[0])
        T0 = node0.linked.prefix + 'kiln/T'
        ds1, node1 = self.dsnode(nodes[1])
        T1 = node1.linked.prefix + 'kiln/T'
        from mdf_client import plugin
        p = plugin.CurveOperationPlugin(
            ax=T0, ay=node0.path, bx=T1, by=node1.path, ds_out=node0.path + '/corr')
        # TODO: TC comparison?
        d = PluginDialog(
            self.mainwindow, self.doc, p, plugin.CurveOperationPlugin)
        self.mainwindow.showDialog(d)

    def add_multiary_menu(self, menu, nodes):
        datasets = self.get_datasets_from_selection()
        nodes = [d[1] for d in datasets]
        if len(datasets) == 2:
            menu.addAction(iutils.theme_icon('correct_curves'), _('Correct'), functools.partial(self.correct, nodes))
        if len(datasets):
            menu.addAction(_('Smooth all'), functools.partial(self.iternodes, nodes, self.smooth, dialog=False))
            menu.addAction(_('Smooth+plot all'), functools.partial(self.iternodes, nodes, self.smooth_and_plot))


from veusz.plugins import datasetplugin as dp
veusz_unary = [dp.AddDatasetPlugin, dp.MultiplyDatasetPlugin, dp.ChopDatasetPlugin, dp.ThinDatasetPlugin,
               dp.SubtractMeanDatasetPlugin, dp.SubtractMinimumDatasetPlugin, dp.DivideMaxPlugin, dp.DivideNormalizePlugin,
               dp.CumulativePlugin, dp.FilterDatasetPlugin, dp.MovingAveragePlugin, dp.ReBinXYPlugin, dp.Histogram2D, dp.ClipPlugin, dp.LogPlugin, dp.ExpPlugin]
veusz_binary = [dp.SubtractDatasetPlugin, dp.DivideDatasetsPlugin]
veusz_multiary = [dp.AddDatasetsPlugin, dp.ConcatenateDatasetPlugin, dp.InterleaveDatasetPlugin, dp.MeanDatasetPlugin, dp.MultiplyDatasetsPlugin]


def set_default(pluginst, field, value):
    for f in pluginst.fields:
        if f.name == field:
            f.default = value
            return f
    return False


class DatasetPluginDomain(NavigatorDomain):
    
    @node
    def call_unary(self, plugin_class, node=False):
        ds_in = node.path
        p = plugin_class()
        set_default(p, 'ds_in', ds_in)
        set_default(p, 'ds_out', ds_in + '/' + plugin_class.name)
        d = PluginDialog(
            self.mainwindow, self.doc, p, plugin_class)
        self.mainwindow.showDialog(d)
    
    @nodes
    def call_binary(self, plugin_class, nodes=False):
        print('call_binary', plugin_class, nodes)
        ds_in = self.dsnode(nodes[0])[1].path
        ds_in2 = self.dsnode(nodes[1])[1].path
        p = plugin_class()
        set_default(p, 'ds_in1', ds_in)
        set_default(p, 'ds_in2', ds_in2)
        set_default(p, 'ds_out', ds_in + '/' + plugin_class.name)
        d = PluginDialog(
            self.mainwindow, self.doc, p, plugin_class)
        self.mainwindow.showDialog(d)
        
    @nodes
    def call_multiary(self, plugin_class, nodes=False):
        print('call_multiary', plugin_class, nodes)
        paths = [self.dsnode(n)[1].path for n in nodes]
        p = plugin_class()
        set_default(p, 'ds_in', paths)
        set_default(p, 'ds_out', plugin_class.name + '_{}'.format(len(paths)))
        d = PluginDialog(
            self.mainwindow, self.doc, p, plugin_class)
        self.mainwindow.showDialog(d)
    
    def add_dataset_menu(self, menu, node):
        m = menu.addMenu(_('Math'))
        for plugin_class in veusz_unary:
            m.addAction(plugin_class.description_short, functools.partial(self.call_unary, plugin_class, node=node))
            
    def add_multiary_menu(self, menu, nodes):
        print('add_multiary_menu', nodes[0])
        m = menu.addMenu(_('Math'))
        
        for plugin_class in veusz_binary:
            if len(nodes) != 2:
                break
            m.addAction(plugin_class.description_short, functools.partial(self.call_binary, plugin_class, nodes=nodes))
            
        for plugin_class in veusz_multiary:
            m.addAction(plugin_class.description_short, functools.partial(self.call_multiary, plugin_class, nodes=nodes))
    

class MeasurementUnitsNavigatorDomain(NavigatorDomain):

    def check_node(self, node):
        if not node:
            return False
        if not node.ds:
            return False
        return len(node.ds) > 0

    @node
    def setInitialDimension(self, node=False):
        """Invoke the initial dimension plugin on the current entry"""
        logging.debug('Searching dataset name', node, node.path)
        n = self.doc.datasetName(node.ds)
        ini = getattr(node.ds, 'm_initialDimension', False)
        if not ini:
            ini = 100.
        xname = self.xnames(node)[0]
        logging.debug('Invoking InitialDimensionPlugin', n, ini)
        from mdf_client import plugin
        p = plugin.InitialDimensionPlugin(ds=n, ini=ini, ds_x=xname)
        d = PluginDialog(
            self.mainwindow, self.doc, p, plugin.InitialDimensionPlugin)
        self.mainwindow.showDialog(d)

    @node
    def convertPercent(self, node=False):
        """Invoke the percentage plugin on the current entry"""
        n = self.doc.datasetName(node.ds)
        from mdf_client import plugin
        p = plugin.PercentPlugin(ds=n, propagate=True)
        d = PluginDialog(self.mainwindow, self.doc, p, plugin.PercentPlugin)
        self.mainwindow.showDialog(d)

    @node
    def set_unit(self, node=False, convert=False):
        logging.debug('set_unit:', node, node.unit, convert)
        if node.unit == convert or not convert or not node.unit:
            logging.debug('set_unit: Nothing to do')
            return
        n = self.doc.datasetName(node.ds)
        from mdf_client import plugin
        p = plugin.UnitsConverterTool(ds=n, convert=convert, propagate=True)
        d = PluginDialog(
            self.mainwindow, self.doc, p, plugin.UnitsConverterTool)
        self.mainwindow.showDialog(d)

    def add_percent(self, menu, node):
        """Add percentage conversion action"""
        self.act_percent = menu.addAction(
            _('Set Initial Dimension'), partial(self.setInitialDimension, node))
        self.act_percent = menu.addAction(iutils.theme_icon('percent'),
            _('Percentage'), partial(self.convertPercent, node))
        self.act_percent.setCheckable(True)
        self.act_percent.setChecked(node.m_percent)

    def add_unit(self, menu, node):
        """Add measurement unit conversion menu"""
        self.units = {}
        u = node.unit
        if not u:
            return
        kgroup, f, p = units.get_unit_info(u, units.from_base)
        same = list(units.from_base.get(kgroup, {u: lambda v: v}).keys())
        logging.debug(kgroup, same)
        for u1 in same:
            p = functools.partial(self.set_unit, node, convert=u1)
            act = menu.addAction(_(u1), p)
            act.setCheckable(True)
            if u1 == u:
                act.setChecked(True)
            # Keep reference
            self.units[u1] = (act, p)

    def add_dataset_menu(self, menu, node):
        menu.addSeparator()
        if not node.path.endswith(':t'):
            self.add_percent(menu, node)
        un = menu.addMenu(_('Units'))
        self.add_unit(un, node)


navigator_domains += PlottingNavigatorDomain, MathNavigatorDomain, DatasetPluginDomain, MeasurementUnitsNavigatorDomain, DataNavigatorDomain

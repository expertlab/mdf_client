#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Tree visualization of opened misura Files in a document."""
import functools

from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from mdf_canon.plugin.domains import node, nodes

from ..filedata import DatasetEntry
from .. import axis_selection
import numpy as np
import veusz.document as document
from mdf_client.qt import QtWidgets, QtGui

ism = isinstance


def paused(func):
    """Decorator for function to pause and resume Navigator operations"""
    @functools.wraps(func)
    def paused_wrapper(self, *a, **k):
        r = None
        try:
            self.pause(1)
            r = func(self, *a, **k)
        finally:
            self.pause(0)
        return r
    return paused_wrapper 

class QuickOps(object):

    """Quick interface for operations on datasets"""
    _mainwindow = False

    @property
    def mainwindow(self):
        if self._mainwindow is False:
            return self
        return self._mainwindow
    
    def get_page_number_from_path(self, page_name):
        # Search current page
        page_num = -1
        for wg in self.doc.basewidget.children:
            page_num += 1
            if wg.name == page_name:
                return page_num
        return -1
            

    @node
    def deleteChildren(self, node=False):
        """Delete all children of node."""
        logging.debug('deleteChildren', node, node.children)
        confirmation = QtWidgets.QMessageBox.warning(self, "Delete nodes",
                       "You are about to delete data. Do you confirm?",
                       QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.Yes)
        if confirmation != QtWidgets.QMessageBox.Yes:
            return

        for sub in node.children.values():
            if not sub.ds:
                continue
            self.deleteData(sub)
#
    def _load(self, node):
        """Load or reload a dataset"""
        self.doc._load(node.path, node.linked.filename, version=node.linked.params.version)
        
    @node
    def load_rule(self, node, rule, overwrite=True):
        self.doc.load_rule(node.linked.filename, rule, overwrite=overwrite)
        
    @node
    def load(self, node=False):
        logging.debug('load', node)
        if node.linked is None:
            logging.error('Cannot load: no linked file!', node)
            return
        if not node.linked.filename:
            logging.error('Cannot load: no filename!', node)
            return
        if len(node.data) > 0:
            logging.debug('Unloading', node.path)
            # node.ds.data = []
            ds = node.ds
            self.deleteData(node=node)
            # self.deleteData(node=node, remove_dataset=False, recursive=False)
            ds.data = np.array([])
            self.doc.available_data[node.path] = ds
            self.pause(False)
            self.doc.setModified()
            return
        self._load(node)
        # Return the new node in the new tree
        return self.doc.model.tree.traverse_path(node.path)
        
    @node
    def plot(self, node=False):
        """Slot for plotting by temperature and time the currently selected entry"""
        pt = self.model().is_plotted(node.path)
        if pt:
            logging.debug('UNPLOTTING', node)
            self.deleteData(node=node, remove_dataset=False, recursive=False)
            logging.debug('DONE UNPLOTTING', node)
            return True
        # Load if no data
        if len(node.data) == 0:
            node = self.load(node)
        yname = node.path

        from mdf_client import plugin
        # If standard page, plot both T,t
        page = self.model().page
        if page.startswith('/temperature/') or page.startswith('/time/'):
            logging.debug('Quick.plot', page)
            # Get X temperature names
            xnames = self.xnames(node, page='/temperature')
            if len(xnames) > 0:
                p = plugin.PlotDatasetPlugin()
                p.apply(self.cmd, {
                        'x': xnames, 'y': [yname] * len(xnames), 'currentwidget': '/temperature/temp'})

            # Get time datasets
            xnames = self.xnames(node, page='/time')
            if len(xnames) > 0:
                p = plugin.PlotDatasetPlugin()
                p.apply(self.cmd, {
                        'x': xnames, 'y': [yname] * len(xnames), 'currentwidget': '/time/time'})
        else:
            if page.startswith('/report'):
                page = page + '/temp'
            logging.debug('Quick.plot on currentwidget', page)
            xnames = self.xnames(node, page=page)
            assert len(xnames) > 0
            args = {'x': xnames, 'y': [yname] * len(xnames), 'currentwidget': self.cmd.currentwidget.path}
            if not self.cmd.currentwidget.path.startswith(page):
                logging.debug('FORCING currentwidget to', page, 'was', self.cmd.currentwidget.path)
                args['currentwidget'] = page
            p = plugin.PlotDatasetPlugin()
            p.apply(self.cmd, args)
        self.doc.setModified()
        return True

    @paused
    @node
    def deleteData(self, node=False, remove_dataset=True, recursive=True):
        """Delete a dataset and all depending graphical widgets."""
        if not node:
            return True
        node_path = node.path
        # Remove and exit if dataset was only in available_data
        if node_path in self.doc.available_data:
            self.doc.available_data.pop(node_path)
            if node_path not in self.doc.data:
                return True
        
        # Recursive call over derived datasets
        if recursive:
            for sub in node.children.values():
                if sub.path in self.doc.data:
                    self.deleteData(sub, remove_dataset, recursive)
            
        # Remove and exit if no plot is associated
        if node_path not in self.model().plots['dataset']:
            if remove_dataset:
                op = document.OperationDatasetDelete(node_path)
                self.doc.applyOperation(op)
            return True

        plots = self.model().plots['dataset'][node_path]
        # Collect involved graphs
        graphs = []
        # Collect plots to be removed
        remplot = []
        # Collect axes which should be removed
        remax = []
        # Collect objects which refers to xData or yData
        remobj = []
        # Remember to remove associated plots
        for p in plots:
            p = self.doc.resolveWidgetPath(None, p)
            g = p.parent
            if g not in graphs:
                graphs.append(g)
            remax.append(g.getChild(p.settings.yAxis))
            remplot.append(p)
        
        # Check if an ax is referenced by other plots
        for g in graphs:
            for obj in g.children:
                if obj.typename == 'xy':
                    y = g.getChild(obj.settings.yAxis)
                    if y is None:
                        continue
                    # If the axis is used by an existent plot, remove from the
                    # to-be-removed list
                    if y in remax and obj not in remplot:
                        remax.remove(y)
                    continue
                # Search for xData/yData generic objects

                for s in ['xData', 'yData', 'xy']:
                    o = getattr(obj.settings, s, None)
                    refobj = g.getChild(o)
                    if refobj is None:
                        continue
                    if refobj not in plots + [node_path]:
                        continue
                    if obj not in remplot + remax + remobj:
                        remobj.append(obj)

        # Save the style of the last plot it appears as yData
        # into the dataset. On replot, this style will be recovered
        from mdf_client import plugin
        for plot in remplot:
            if plot.settings['yData'] != node_path:
                continue
            plugin.save_plot_style_in_dataset_attr(plot, self.cmd)
        logging.debug('deleteData: removing {} axes, {} plots, {} objects'.format(len(remax), len(remplot), len(remobj)))
        # Remove object and unreferenced axes
        for obj in remplot + remax + remobj:
            if not obj: continue
            logging.debug('Removing obj', obj.name, obj.path)
            obj.parent.removeChild(obj.name)
        # Finally, delete dataset
        if remove_dataset:
            op = document.OperationDatasetDelete(node_path)
            self.doc.applyOperation(op)
            logging.debug('deleted', node_path)
            
        logging.debug('deleteData: DONE')
        return True

    @nodes
    def deleteDatas(self, nodes=False):
        """Call deleteData on each selected node"""
        for n in nodes:
            self.deleteData(node=n.path)


    def widget_path_for(self, node, prefix=''):
        result = '/'
        full_path = self.doc.model.is_plotted(node.path)
        for result in full_path:
            if result.startswith(prefix):
                return result
        return result

    def xnames(self, y, page=False):
        """Get X dataset name for Y node y, in `page`"""
        if page == False:
            page = self.model().page
        
        try:
            self.doc.resolveWidgetPath(None, page)
        except:
            logging.error('No page', page)
            #return []
        lk = y.linked 
        if y.parent and not lk:
            lk = y.parent.linked
        else:
            logging.debug('xnames: No linked file!', y.path, y.linked)
        pre = ''
        if lk:
            pre = getattr(lk,'prefix','')
        xname = axis_selection.get_best_x_for(y.path, pre, self.doc.data, page)

        return [xname]

    def dsnode(self, node):
        """Get node and corresponding dataset"""
        ds = node
        if isinstance(node, DatasetEntry):
            ds = node.ds
        return ds, node



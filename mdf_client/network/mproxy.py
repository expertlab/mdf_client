# -*- coding: utf-8 -*-
# from transport import MisuraTransport
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from time import time, sleep
import xmlrpc
from traceback import format_exc, print_exc
import threading
from mdf_canon.csutil import lockme
from mdf_canon.option import common_proxy
from mdf_canon.csutil import unquote, quote, xmlrpclib, httplib
# Disable ssl cert verification (MDF certs are self-signed)
import ssl
if hasattr(ssl, '_create_unverified_context'):
    ssl._create_default_https_context = ssl._create_unverified_context

sep = '/'

from urllib.request import build_opener, HTTPSHandler, HTTPDigestAuthHandler, HTTPPasswordMgrWithPriorAuth


def mdf_opener(uri, user, passwd, realm=b'MISURA'):
    ctx = ssl._create_unverified_context()
    https = HTTPSHandler(context=ctx)
    auth_handler = HTTPDigestAuthHandler(HTTPPasswordMgrWithPriorAuth())
    auth_handler.add_password(realm=realm, uri=uri, user=user, passwd=passwd)
    opener = build_opener(https, auth_handler)
    return opener


def urlauth(url):
    """Decode and strip away the auth part of an url.
    Returns user, password and clean url"""
    url = unquote(url)
    if not '@' in url:
        return False, False, url
    i = url.find('://') + 3
    e = url.find('@', i) + 1
    auth = url[i:e][:-1]
    user, passwd = auth.split(':')
    url_start = url[:i]
    url_end = url[e:].split('/')
    # Take the address
    url_start += url_end.pop(0)
    if not url_start.endswith('/'):
        url_start += '/'
    # Quote the item location part of the url
    url_end = '/'.join(url_end)
    url = url_start + quote(url_end)

    return user, passwd, url


def remote_dbdir(server):
    """Calc remote database directory path"""
    # Filter away the misura.sqlite filename
    p = server.storage.get_dbpath().split('/')[:-1]
    r = '/'.join(p)
    logging.debug('remote_dbdir', r)
    return r


def dataurl(server, uid):
    """Calc HTTPS/data url for test file `uid` on `server`"""
    p = server.storage.searchUID(uid)
    if not p:
        raise BaseException('Error opening remote file with uid ' + uid)
    # Remove remote db path from file path
    dbdir = remote_dbdir(server)
    if p.startswith(dbdir):
        p = p[len(dbdir):]
    if not p.startswith('/'):
        p = '/' + p
    # Prepend remote HTTPS/data path
    url = server.data_addr + quote(p.encode('utf8'))
    logging.debug('dataurl %s %s --> %s' % (server.data_addr, p, url))
    return url, p


class _Method:

    """Override xmlrpclib._Method in order to introduce our own separator"""

    def __init__(self, send, name):
        self.__send = send
        self.__name = name

    def __getattr__(self, name):
        return _Method(self.__send, "%s%s%s" % (self.__name, sep, name))

    def __call__(self, *args):
        # return self.__send(self.__name, args)
        for i in range(1, 4):
            try:
                return self.__send(self.__name, args)
            except ssl.SSLEOFError:
                logging.debug('SSL timeout; retry', i)
                continue
            except xmlrpc.client.ProtocolError:
                raise
            except:
                logging.error('_Method.__call__: While calling', self.__name, args, format_exc())
                return None


xmlrpclib._Method = _Method


class AutoDict(object):

    """A special dictionary-like class with automatic recursive creation of missing keys."""

    def __init__(self, autoclass=['self']):
        self.autoclass = autoclass
        self.cache = {}

    def __getitem__(self, k):
        if k not in self.cache:
            cls = self.autoclass[0]
            if cls == 'self':
                cls = AutoDict
            kw = {}
            if cls == AutoDict and len(self.autoclass) > 1:
                kw['autoclass'] = self.autoclass[1:]
            self.cache[k] = cls(**kw)
        return self.cache[k]

    def __setitem__(self, k, v):
        q = self[k]
        self.cache[k] = v

    def has_key(self, k):
        return k in self.cache
    
    def __contains__(self, k):
        return k in self.cache

    def get(self, k, *arg):
        if k in self.cache:
            return self.cache[k]
        elif len(arg) == 1:
            return arg[0]
        return self[k]


def reconnect(func):
    """Decorator function for automatic reconnection in case of failure"""

    def reconnect_wrapper(self, *a, **k):
        try:
            r = func(self, *a, **k)
        except (xmlrpclib.ProtocolError, httplib.CannotSendRequest, httplib.BadStatusLine, httplib.ResponseNotReady):
            logging.debug('RECONNNECTING', func)
            self.connect()
            return func(self, *a, **k)
        except:
            logging.debug('UNHANDLED EXCEPTION', func)
            logging.debug(format_exc())
            if self._reg:
                self._reg.connection_error()
            raise
        return r

    return reconnect_wrapper


connections = {}
import os
ssl_context = ssl.SSLContext(ssl.PROTOCOL_SSLv23)
proxy_kw = {"allow_none": True, "context": ssl_context}

# print('SSL CIPHERS:', ssl_context.get_ciphers())


class InvalidStreamAddress(BaseException):
    pass


class MisuraProxy(common_proxy.CommonProxy):
    """Classe wrapper per ServerProxy. Introduce le funzioni __getitem__ e __setitem__."""
    _Method__name = 'MAINSERVER'
    _leaf_name = ''
    _error = ''
    _lock = False
    _writeLevel = -1
    _readLevel = -1
    # _lock = threading.Lock()
    _recursiveModel = False
    """Cached recursive item model"""
    _remObj = False
    """Remote ServerProxy object"""
    addr = False
    """Remote server address"""
    realm = b"MISURA"
    _opener = False
    user = False
    """User name"""
    password = False
    """User password"""
    _remoteDict = {}
    """Remote methods and object paths"""
    _refresh_interval = 0
    """Wait time between getting again the same option"""
    _refresh_last = AutoDict(autoclass=['self', int])
    """Last refresh times"""
    _desc = {}
    _cache = AutoDict()
    """Local representation of remote description"""
    _dtime = time()
    """Client-server time delta"""
    _ctime = time()
    """Time of last remote call - use for logging"""
    _smartnaming = False
    _reg = False
    """Triggers remote get/set requests when accessing to un-protected local attributes"""
    _protect = set(['remObj', 'conn_addr', 'data_addr', 'to_root', 'toPath', 'root', 'connect', 'paste', 'copy', 'describe',
                    'info', 'lastlog', 'get', 'from_column', 'parent', 'child', 'call', 'devices'])
    """Local names which must not be accessed remotely"""
    
    def __init__(self, addr='', user='', password='', mac='', proxy=False, reg=False):
        # self._lock = threading.Lock()
        # Copy existing object
        if proxy:
            self.paste(proxy)
        # Create new connection
        else:
            self._reg = reg
            self.mac = mac
            self.addr = addr
            self.user = user
            self.password = password
            # self.connect()
            self._protect.update(dir(self))
            
    @property
    def log(self):
        return logging
            
    def is_live(self):
        return True
    
    @property
    def conn_addr(self):
        auth = 'https://{}:{}@'.format(self.user, self.password)
        return self.addr.replace('https://', auth)

    @property
    def data_addr(self):
        return self.conn_addr.replace('/RPC', '/data')

    def __str__(self):
        return '%r %s@%s /%s' % (self, self.user, self.addr, self._Method__name)
    
    _conn_addr = 0
    
    def _to_root(self):
        """Create new HTTP connection"""
        oldname = self._Method__name
        if not self._remObj:
            self._remObj = xmlrpclib.ServerProxy(self.conn_addr, verbose=False, **proxy_kw)
            a = self._remObj.echo('').split('=')
            self._readLevel, self._writeLevel = int(a[1].split(',')[0]), int(a[2].split(',')[0])
            self._dtime = self._remObj.time() - time()
            self._remoteDict = self.get_remoteDict()
        self._remObj.allow_none = True
        self._remObj._ServerProxy__allow_none = True
        self._remObj._Method__name = 'MAINSERVER'
        self._Method__name = 'MAINSERVER'
        
        return oldname
    
    def get_remoteDict(self):
        # Compile a list of both remote callable methods and remote walkable
        # objects
        d = {'MAINSERVER': []}
        if not self._smartnaming:
            return d
        for entry in self.remObj.system.listMethods():
            if entry.count(self.separator) == 0:
                d['MAINSERVER'].append(entry)
                continue
            entry = entry.split(self.separator)
            part = entry[0]
            if part not in d['MAINSERVER']:
                d['MAINSERVER'].append(part)
            if len(entry) == 1:
                continue
            for e in entry[1:]:
                if part not in d:
                    d[part] = []
                if e not in d[part]:
                    d[part].append(e)
                part += self.separator + e
        return d

    @lockme()
    def to_root(self):
        """Locked call for _to_root."""
        return self._to_root()

    def _toMethodName(self, name):
        """Changes current method name in-place"""
        if name in ['MAINSERVER', '']:
            return self._to_root()
        if isinstance(name, str):
            lst = name.split(self.separator)
        else:
            lst = name
        
        new = self._Method__name.strip('/').lstrip('MAINSERVER')
        method = self.remObj
        for i, p in enumerate(lst):
            if p in ['MAINSERVER', '', '/']:
                continue
            
            if not method or (i < len(lst) - 1 and not method.has_child(p)):
                logging.warning('Cannot find intermediate object', p, name)
                return False
            if new:
                new += self.separator
            new += p
            method = getattr(method, p)
        self._Method__name = new
        self._leaf_name = lst[-1]
        self._parent = False
        return True
    
    @property
    def remObj(self):
        if self._Method__name in ['MAINSERVER', '/', '', 0, None]:
            return self._remObj
        ret = self._remObj
        for p in self._Method__name.split(self.separator):
            ret = getattr(ret, p)
        return ret
    
    @lockme()
    def toPath(self, lst, cache=True):
        """Returns a copy of the object at the path expressed in list/string lst"""
        if type(lst) == type(''):
            if lst.endswith(self.separator):
                lst = lst[:-1]
            if lst.startswith(self.separator):
                lst = lst[1:]
            lst = lst.split(self.separator)
            if lst[0] == 'server':
                lst.pop(0)
        
        vlst = self.separator.join(lst)
        name = self._Method__name.lstrip('MAINSERVER/') + '/' + vlst
        tid = '{}:{}:{}'.format(os.getpid(), threading.get_ident(), name)
        obj = False
        if cache:
            obj = connections.get(tid, None)
        if obj:
            return obj
        obj = self.copy()
        ok = obj._toMethodName(vlst)
        if not ok:
            print('NOT FOUND', vlst)
            return None
        if cache:
            connections[tid] = obj
        return obj

    @lockme()
    def connect(self):
        oldname = self._to_root()
        # Restore object path's
        return self._toMethodName(oldname)

    def _wait(self, timeout=120):
        """Wait for connection"""
        t0 = time()
        while time() - t0 < timeout:
            try:
                self.connect()
                self['name']
                break
            except:
                print_exc()
                sleep(1)

    @property
    def root(self):
        """Return the root object"""
        r = self.copy()
        r.to_root()
        return r

    @property
    def _remoteNames(self):
        return self._remoteDict.get(self._Method__name, [])
    
    @property
    def islocal(self):
        return False
        return 'localhost:' in self.addr or '127.0.0.1:' in self.addr
    
    @property
    def opener(self):
        """Return the url opener with auth info"""
        if not self._opener:
            self._opener = mdf_opener(self.addr[:-4], self.user,
                                      self.password, realm=self.realm)
        return self._opener
    
    def stream_address(self, opt, **kw):
        """Calculate stream address for option named opt"""
        path = self._Method__name
        if path == 'MAINSERVER':
            path = ''
        else:
            path = '/' + path
        if '/None' in path:
            raise InvalidStreamAddress(path)
        ret = f'{self.addr[:-4]}/stream{path}?opt={opt}'
        for k, val in kw.items():
            ret += f'&{k}={val}'
        return ret

    @lockme()
    def paste(self, obj):
        """Paste foreign MisuraProxy settings into current instance"""
        oldsmart = obj._smartnaming  # remember smartnaming status
        # Stop smartnaming in order to make attribute definition quicker
        self._smartnaming = False
        obj._smartnaming = False
        
        self.user = obj.user
        self.password = obj.password
        self.addr = obj.addr
        self.mac = obj.mac
        self._reg = obj._reg
        
        self._Method__name = obj._Method__name
        self._remObj = xmlrpclib.ServerProxy(self.conn_addr, verbose=False, **proxy_kw)
        
        self._leaf_name = obj._leaf_name
        self._remoteDict = obj._remoteDict
        self._refresh_interval = obj._refresh_interval
        self._cache = obj._cache
        self._desc = obj._desc
        self._refresh_last = obj._refresh_last
        self._error = obj._error
        self._dtime = obj._dtime
        self._protect = obj._protect
        self._smartnaming = obj._smartnaming
        self._readLevel = obj._readLevel
        self._writeLevel = obj._writeLevel
        # Restore smartnaming, if it was enabled
        if oldsmart:
            self._smartnaming = True
            obj._smartnaming = True

    def copy(self):
        """Return a copy of this object with a new HTTP connection."""
        obj = MisuraProxy(self.addr, self.user,
                             self.password)
        obj.paste(self)
        return obj

    @lockme()
    def describe(self, *args):
        if self._refresh_interval <= 0:
            d = self.remObj.describe(*args)
            self._desc[self._Method__name] = d
            return d
        t = time()
        key = '::describe::'
        if t - self._refresh_last[self._Method__name].get(key, 0) > self._refresh_interval or \
                (self._desc.get(self._Method__name, False) is False):
            # Need to refresh
            self._refresh_last[self._Method__name][key] = t
            self._desc[self._Method__name] = self.remObj.describe(*args)
            self._ctime = time()
        return self._desc[self._Method__name]

    @lockme()
    def info(self, key):
        """Pretty print information about `key`"""
        logging.debug('Option:', key)
        e = self.remObj.gete(key)
        for k, v in e.items():
            logging.debug('\t', k, ':', v)

    @lockme()
    def lastlog(self):
        """Retrieve log messages near the last remote procedure call"""
        t = self._ctime + self._dtime
        r = self.remObj.search_log(t - 1, t + 1)
        return r
    
    def __len__(self):
        d = self._desc.get(self._Method__name, {})
        d = len(d)
        if not d:
            d = len(self.remObj.keys())
        return d

    @reconnect
    @lockme()
    def __getitem__(self, key, *fallback):
        """Cached get function"""
        t = time()
        if len(fallback) > 1:
            raise BaseException("Too many arguments")
        if t - self._refresh_last[self._Method__name][key] > self._refresh_interval:
            self._refresh_last[self._Method__name][key] = t
            
            try:
                r = self.remObj.get(key)
            except:
                if not self.remObj.has_key(key) and fallback:
                    return fallback[0]
                raise
            self._ctime = time()
            if key not in self._cache:
                self._cache[key] = r
            else:
                self._cache[key] = r
        else:
            pass
        return self._cache[key]

    @reconnect
    @lockme()
    def __setitem__(self, key, val):
        self._refresh_last[self._Method__name][key] = 0
        self._ctime = time()
        if isinstance(val, self.__class__):
            val = val.get('fullpath')
        return self.remObj.set(key, val)

    def __setattr__(self, key, val):
        if key.startswith('_') or (not self._smartnaming) or (key in self._protect):
            return object.__setattr__(self, key, val)
        if isinstance(val, self.__class__):
            val = val.get('fullpath')
        if self.remObj is not False:
            return self.__setitem__(key, val)
        elif key in self._remoteNames:
            logging.debug('Overwrite of a remote object name is forbidden!', key)
            return False
        else:
            return object.__setattr__(self, key, val)

    @lockme()
    def _has_key(self, path):
        r = self.remObj.has_key(path)
        return r
    
    def __contains__(self, path):
        return self._has_key(path)

    def __getattr__(self, path):
        """Override standard ServerProxy in order to provide useful shortcuts."""
        # Protect local names
        if (path in self._protect) or path.startswith('_'):
            return object.__getattribute__(self, path)
        # Detect option retrieval request
        if self._smartnaming:
            if self._Method__name == 'MAINSERVER':
                cpath = path
            else:
                cpath = self._Method__name + self.separator + path
            if (cpath not in self._remoteNames):
                if self._has_key(path):
                    return self.get(path)
        return self.child(path)
    
    def is_object(self):
        par = self.parent()
        if not par:
            return True
        if par and par.has_child(self._leaf_name):
            return True
        return False

    def from_column(self, col0):
        return common_proxy.from_column(col0, self.root)

    def parent(self, reset=False):
        """Get the parent object handling this one"""
        if (self._parent != False) and (not reset):
            return self._parent
        mn = self._Method__name.split(self.separator)
        while len(mn) and mn[-1] == '':
            mn.pop(-1)
        if not len(mn):
            return False
        if len(mn) == 1:
            return self.root
        self._parent = self.root.toPath(mn[:-1])
        return self._parent

    def child(self, name, *default):
        obj = self.toPath([name])
        if obj:
            obj._parent = self
        elif default:
            return default[0]
        return obj

    @reconnect
    @lockme()
    def __call__(self, *args, **kwargs):
        self._ctime = time()
        return self.remObj.__call__(*args, **kwargs)

    @reconnect
    @lockme()
    def call(self, path, *args, **kwargs):
        self._ctime = time()
        f = getattr(self.remObj, path)
        return f(*args, **kwargs)

    @property
    def devices(self):
        """List available devices retrieved via list() as MisuraProxy objects"""
        r = []
        for name, path in self.list():
            r.append(self.child(path))
        return r

    def role2dev(self, opt):
        """Return the device object associated with role option `opt`"""
        p = self[opt]
        if not p:
            return False
        p = p[0]
        if p in ('None', None):
            return False
        p = self.root.searchPath(p)
        if not p:
            return False
        obj = self.root.toPath(p)
        return obj
    

#!/usr/bin/python
# -*- coding: utf-8 -*-
import hashlib
import platform
import select
import socket
from time import sleep, time
from traceback import format_exc

from mdf_canon.csutil import httplib, xmlrpclib

from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)

from .mproxy import MisuraProxy, reconnect, urlauth, dataurl, remote_dbdir, mdf_opener
from .wake_on_lan import wake_on_lan

import sip
API_NAMES = ["QDate", "QDateTime", "QString",
             "QTextStream", "QTime", "QUrl", "QVariant"]
API_VERSION = 2
for name in API_NAMES:
    sip.setapi(name, API_VERSION)
from mdf_client.qt import QtCore

from .info import ServerInfo
from mdf_client.network.network_manager import NetworkManager

from .transfer_thread import TransferThread
import xmlrpc

global ess
ess = ServerInfo()
ess.fromAddr('misura.expertsystemsolutions.it:80')
ess.name = 'Simulation Server'
ess.host = 'misura.expertsystemsolutions.it'
ess.serial = 'ESimulServ'

manager = NetworkManager()

CONN_ERROR_LOGIN = 0
CONN_ERROR_SOCKET = -1
CONN_ERROR_UNKNOWN = -2
CONN_ERROR_ADDRESS = -3


def normalize_address(addr, user='', password='', port=3880):
    addr = str(addr)
    if '.' not in addr:
        if addr == '127':
            addr = '127.0.0.1'
        else:
            try:
                addr = '192.168.0.' + int(addr)
            except:
                pass
    if '//' in addr:
        addr = addr.split('//')[1]
    if '@' in addr:
        usrpass, addr = addr.split('@')
        addr = addr.lower().replace('/rpc', '')
        if ':' in usrpass:
            user, password = usrpass.split(':')
        else:
            user = usrpass
    addr = addr.split('/')[0]
    if ':' not in addr:
        addr += f':{port}'
    auth_addr = addr
    auth = ''
    if user:
        auth = user
        if password:
            auth += ':' + password
        auth_addr = auth + '@' + addr
    if not addr.startswith('https'):
        addr = 'https://' + addr
        auth_addr = 'https://' + auth_addr
    
    if not addr.endswith('/RPC'):
        addr += '/RPC'
        auth_addr += '/RPC'
    return addr, user, password, auth_addr


def connection(addr, user='', password='', mac=''):
    addr, user, password, auth = normalize_address(addr, user, password)
    try:
        obj = MisuraProxy(addr, user=user, password=password, mac=mac, proxy=False)
    except:
        logging.debug('FAILED at instantiation')
        logging.debug(format_exc())
        return CONN_ERROR_ADDRESS, None
    try:
        obj.connect()
    except xmlrpclib.ProtocolError as err:
        if err.errcode == 401:
            obj._error = 'Authorization Failed!'
        elif err.errcode == 409:
            obj._error = 'Another user is currently logged in.'
        return CONN_ERROR_LOGIN, obj
    except:
        logging.debug('UNKNOWN FAILURE')
        logging.debug(format_exc())
        return CONN_ERROR_ADDRESS, None
    return True, obj


def simpleConnection(addr, user='', password='', mac='', save=True, signal=True):
    try:
        err, obj = connection(addr, user=user, password=password, mac=mac)
    except xmlrpclib.ProtocolError as err:
        logging.debug(format_exc())
        if err.errcode == 401:
            obj._error = 'Authorization Failed!'
        elif err.errcode == 409:
            obj._error = 'Another user is currently logged in.'
        return CONN_ERROR_LOGIN, obj
    except:
        logging.debug('FAILED simpleConnection at instantiation')
        logging.debug(format_exc())
        return CONN_ERROR_ADDRESS, None
    if err < 0:
        return err, obj
    try:
        obj.remObj.echo('echo')
        if signal:
            manager.sig_connection.emit(addr, user, password, obj['eq_mac'], obj['name'], obj['eq_sn'], save)
        return True, obj
    except xmlrpclib.ProtocolError as err:
        logging.debug('FAILED simpleConnection at echo - protocol error')
        logging.debug(format_exc())
        if err.errcode == 401:
            obj._error = 'Authorization Failed!'
        elif err.errcode == 409:
            obj._error = 'Another user is currently logged in.'
        return CONN_ERROR_LOGIN, obj
    except socket.error as err:
        logging.debug('FAILED simpleConnection at echo - socket error')
        obj._error = 'Socket error [{}]: {}'.format(err.errno, err.strerror)
        logging.debug(format_exc())
        return CONN_ERROR_SOCKET, obj
    except:
        logging.debug('FAILED simpleConnection at echo - unknown')
        obj._error = 'Unknown Error'
        logging.debug(format_exc())
        return CONN_ERROR_UNKNOWN, obj


def getConnection(addr, user='', password='', mac='', save=True, smart=False):
    """Connects to a remote address"""
    global manager
    st, obj = simpleConnection(addr, user, password, mac, save)
    if st <= 0:
        logging.debug('Connection failed')
        return st, obj
    setRemote(obj)
    manager.remote.remObj.send_log(
        "Client connection: " + repr(platform.uname()))
    logging.debug('Connected to', addr)
    manager.connected = True
    manager.sig_connected.emit()
    manager.remote._smartnaming = smart
    return True, manager.remote


def setRemote(obj):
    global manager
    logging.debug('Setting network.manager.remote', repr(obj))
    manager.addr = obj.addr
    manager.user = obj.user
    manager.password = obj.password
    manager.remote = obj
    manager.error = obj._error
    manager.connected = True
    manager.sig_connected.emit()
    # Clear all registered widgets
    from ..live import registry
    registry.clear()


def closeConnection():
    """Disconnette"""
    manager.sig_disconnected.emit()
    manager.connected = False
    if manager.remote != None:
        manager.remote.users.logout()
    manager.remote = None
    logging.debug('Disconnected from', manager.addr)
    return True


if __name__ == '__main__':
    import sys
    from mdf_client import iutils
    iutils.initApp()
    app = iutils.app
# 	qb=ServerSelector()
# 	qb.show()
    sys.exit(app.exec_())

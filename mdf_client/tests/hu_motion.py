#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Display a list of `end` option for all detected motors and updates it."""
from mdf_client import widgets
import sys
from mdf_canon.logger import Log as logging
from mdf_client import iutils, network
from mdf_client.qt import QtWidgets, QtGui, QtCore


def main():
    iutils.initApp()
    network.getConnection('https://localhost:3880', 'admin', 'admin')
    srv = network.manager.remote
    qb = QtWidgets.QWidget()
    lay = QtWidgets.QFormLayout()
    wgs = []
    for d in srv.morla.devices:
        for m in d.devices:
            wg = widgets.build(srv, m, m.gete('end'))
            wgs.append(wg)
            lay.addRow(m['fullpath'] + m['name'], wg)

    do = False

    def up():
        logging.debug('up')
        global do
        if do:
            return
        do = True
        for wg in wgs:
            wg.async_get()
        do = False

    clock = QtCore.QTimer()
    clock.timeout.connect(up)
    clock.start(250)

    qb.setLayout(lay)
    qb.show()
    sys.exit(QtWidgets.qApp.exec_())

if __name__ == '__main__':
    main()

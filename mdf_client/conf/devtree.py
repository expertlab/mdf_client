#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Configuration interface for MDF.
Global instrument parametrization and setup."""

from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from .. import network
from mdf_canon.csutil import natural_keys
from .. import _
from mdf_client.qt import QtWidgets, QtGui, QtCore
from mdf_client.qt import Qt


class Item(object):

    def __init__(self, parent=False, name='server', idx=0):
        self.parent = parent
        self.name = name
        self.idx = idx
        self.title = ''
        self._model = False
        self.children = []
        self.names = []

    def __len__(self):
        return len(self.children)

    def __repr__(self):
        return '%s.%i.%i.(%s)' % (self.name, self.idx, len(self.children), self.parent)

    @property
    def root(self):
        if self.parent == False:
            return self
        return self.parent.root

    @property
    def model(self):
        if self.parent == False:
            return self._model
        return self.root.model

    @property
    def path(self):
        """Recursively rebuilds the path of the node as parent0...parentN.nodename"""
        path = []
        node = self
        while node.name != 'server':
            path = [node.name] + path
            node = node.parent
        logging.debug("path", path)
        return path

    def index_path(self, path):
        """Rebuilds the sequence of indexes needed to reach a path"""
        if isinstance(path, str):
            path = path.split('/')
        if path[0] == 'server':
            path.pop(0)
        node = self
        idx = []
        for name in path:
            # logging.debug('index_path searching', name, node.names)
            if name not in node.names:
                logging.debug('NAME NOT FOUND', name, node.name, node.names)
                return False, False
            i = node.names.index(name)
            node = node.children[i]
            idx.append(i)
            # logging.debug('index_path', name, i, node)
        return idx, node


def item_natural_keys(item):
    return natural_keys(item.name)


def recursiveModel(base, parent=False, model=False, force=False):
    if parent == False:
        parent = Item()
        model = base.rmodel()
        parent._model = model
        print(model)
    # Caching management
    if (model != base._rmodel) or (not base._recursiveModel) or force:
        base._rmodel = model
    else:
        return base._recursiveModel

    for i, (path, name) in enumerate(model.items()):
        if path == 'self':
            parent.title = name
            continue
        item = Item(parent, path, i)
        obj = base.child(path)
        item = recursiveModel(obj, item, model[path], force=force)
        parent.children.append(item)
        
    parent.children.sort(key=item_natural_keys)
    parent.names = [item.name for item in parent.children]
    base._recursiveModel = parent
    return parent


class ServerModel(QtCore.QAbstractItemModel):
    ncolumns = 2
    modelReset = QtCore.pyqtSignal()

    def __init__(self, server, parent=None):
        QtCore.QAbstractItemModel.__init__(self, parent)
        self.server = server
        self.header = ['Object', 'Description']
        self.refresh()

    def setNcolumns(self, val):
        self.ncolumns = val

    def refresh(self, force=False):
        self.item = Item(idx=0)
        if force:
            self.server.dump_model()
        tree = recursiveModel(self.server, force=force)
# 		print tree.children
        tree.parent = self.item
        self.item.children.append(tree)
        self.modelReset.emit()

    def index_path(self, path):
        """Returns the sequence of model indexes starting from a path: parent0...parentN.nodename"""
        # Get the full sequence of indexes and the parent node
        idx, node = self.item.children[0].index_path(path)
        logging.debug('ServerModel.index_path: FOUND', path, idx, node)
        if not idx:
            return []
# 		return self.createIndex(idx[-1], 0, node),idx
        jdx = []
        jdx.append(self.createIndex(0, 0, self.item.children[0]))
        for i in idx:
            logging.debug('indexing', jdx[-1], i)
            jdx.append(self.index(i, 0, jdx[-1]))
        return jdx

    def nodeFromIndex(self, index):
        if not index.isValid():
            return self.item
        ptr = index.internalPointer()
        return ptr

    def rowCount(self, parent):
        node = self.nodeFromIndex(parent)
        if not node:
            return 0
        return len(node)

    def columnCount(self, parent):
        return self.ncolumns

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if orientation != QtCore.Qt.Horizontal:
            return
        if role == QtCore.Qt.DisplayRole:
            return self.header[section]

    def index(self, row, column, parent):
        if row < 0:
            return QtCore.QModelIndex()
        node = self.nodeFromIndex(parent)
# 		print node,len(node),row
        assert len(node) > row
        idx = self.createIndex(row, column, node.children[row])
        return idx

    def parent(self, child):
        row, col = 0, 0
        node = self.nodeFromIndex(child)
        if node == None:
            return QtCore.QModelIndex()
        parent = node.parent
        if parent:
            row = parent.idx
        return self.createIndex(row, col, parent)

    def data(self, index, role=QtCore.Qt.DisplayRole):
        col = index.column()
        row = index.row()
        if role not in [Qt.DisplayRole, -1]:
            return None
        node = self.nodeFromIndex(index)
# 		print 'data',node
        if role == -1:
            return node
# 		print 'data',node.name
        if col == 0:
            return node.name
        elif col == 1:
            return node.title
        else:
            return None


class ServerView(QtWidgets.QTreeView):
    
    sig_objViewCamera = QtCore.pyqtSignal(QtCore.QModelIndex)
    sig_objNewTab = QtCore.pyqtSignal(QtCore.QModelIndex)
    sig_objTable = QtCore.pyqtSignal(QtCore.QModelIndex)
    sig_presetsTable = QtCore.pyqtSignal(QtCore.QModelIndex)
    sig_wiringGraph = QtCore.pyqtSignal(QtCore.QModelIndex)
    
    def __init__(self, server=False, parent=None):
        QtWidgets.QTreeView.__init__(self, parent)
        if not server:
            server = network.manager.remote
        self.treemodel = ServerModel(server, self)
        self.setModel(self.treemodel)
        self.setUniformRowHeights(True)
        self.server = server
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.showContextMenu)
        self.menu = QtWidgets.QMenu(self)
        self.expandToDepth(0)

    @property
    def ncolumns(self):
        self.model().ncolumns()

    @ncolumns.setter
    def ncolumns(self, val):
        self.model().setNcolumns(val)

    def select_path(self, path):
        """Select fullpath object `path`"""
        jdx = self.model().index_path(path)
        n = len(jdx)
        for i, idx in enumerate(jdx):
            if i < n - 1:
                # Expand all parent objects
                self.setExpanded(idx, True)
            else:
                # Select the leaf
                self.selectionModel().setCurrentIndex(
                    jdx[-1], QtCore.QItemSelectionModel.Select)

    def current_fullpath(self):
        idx = self.selectionModel().currentIndex()
        path = self.model().nodeFromIndex(idx).path
        path = '/'.join(path)
        path = '/' + path + '/'
        return path

    def update(self):
        self.model().refresh(force=True)
        self.expandToDepth(0)

    def showContextMenu(self, pt):
        self.menu.clear()
        self.menu.addAction('Open', self.objOpen)
        fp = self.current_fullpath()
        if fp.startswith('/beholder/') and fp.count('/') == 3:
            self.menu.addAction(_('View camera'), self.objViewCamera)
        self.menu.addAction(_('Open in New Tab'), self.objNewTab)
        self.menu.addAction(_('Object Table'), self.objTable)
        self.menu.addAction(_('Presets Table'), self.presetsTable)
        self.menu.addAction(_('Wiring Graph'), self.wiringGraph)
        self.menu.addAction(_('Update View'), self.update)
        self.menu.popup(self.mapToGlobal(pt))

    def objOpen(self):
        self.activated.emit(self.currentIndex())

    def objViewCamera(self):
        self.sig_objViewCamera.emit(self.currentIndex())

    def objNewTab(self):
        self.sig_objNewTab.emit(self.currentIndex())

    def objTable(self):
        self.objOpen()
        self.sig_objTable.emit(self.currentIndex())
    
    def presetsTable(self):
        self.objOpen()
        self.sig_presetsTable.emit(self.currentIndex())
        
    def wiringGraph(self):
        self.objOpen()
        self.sig_wiringGraph.emit(self.currentIndex())
        

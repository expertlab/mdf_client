#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Configuration interface for misura.
Global instrument parametrization and setup."""
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from . import constructor
from .devtree import ServerView
from .. import _
from .. import network
from ..live import registry
from ..clientconf import confdb
from ..connection import ServerSelector, ConnectionStatus, addrConnection
from ..confwidget import ClientConf, RecentMenu

from mdf_client.qt import QtWidgets

def replace_role(dev, replace=False, paths=None, roles=None, apply=False):
    if paths is None:
        paths = set([])
    if roles is None:
        roles = []
    paths.add(dev['fullpath'])
    presets = dev.gete('preset').get('options',['factory_default'])
    presets.remove('factory_default')
    for k,o in dev.describe().items():
        if o['type'].startswith('Role'):
            #TODO: iterate all presets
            for preset in presets:
                cur = dev.get_from_preset(k, preset)
                if not (isinstance(cur,list) and len(cur)>1):
                    continue
                p = cur[0]
                if p in ('None', '',None):
                    continue
                if replace and p.startswith(replace[0]):
                    p1 = p.replace(*replace)
                    if p1==p:
                        continue
                    print('Updating',preset, k, dev['fullpath'], p,'to',p1, cur[0])
                    cur[0]=p1
                    roles.append([o['kid'], preset, p,p1])
                    if apply:
                        print('APPLY',o['kid'],k,preset,cur)
                        dev.set_to_preset(k, preset, cur)
                    else:
                        print('not applied', apply, o['kid'],k,preset,cur)
                elif not replace:
                    # Just collect all defined roles
                    roles.append([o['kid'], preset, cur[0]])
    for sub in dev.devices:
        replace_role(sub,replace,paths,roles, apply=apply)
    return paths,roles

def replace_device(server, parent=None):
    paths, roles = replace_role(server)
    dia = QtWidgets.QDialog(parent)
    lay = QtWidgets.QVBoxLayout()
    dia.setLayout(lay)
    lay.addWidget(QtWidgets.QLabel('Select device to be removed (edit to take parent)'))
    old = QtWidgets.QComboBox(dia)
    old.setEditable(True)
    old.insertItems(0,sorted(list(set([r[-1] for r in roles]))))
    lay.addWidget(old)
    lay.addWidget(QtWidgets.QLabel('Select device taking its role'))
    new = QtWidgets.QComboBox(dia)
    new.insertItems(0,sorted(list(paths)))
    lay.addWidget(new)
    ok =QtWidgets.QPushButton('Preview')
    ok.clicked.connect(dia.accept)
    lay.addWidget(ok)
    dia.exec_()
    if not dia.result():
        return False
    old = old.currentText()
    new = new.currentText()
    logging.debug('Replacing',old,'with',new)
    paths_dry,roles_dry = replace_role(server,(old,new), apply=False)
    msg = ""
    for r in roles_dry:
        msg += "Opt:{}, preset:{}, from:{} to:{}\n".format(r[0],r[1],r[2],r[3])
    dia = QtWidgets.QMessageBox.question(parent, "Save changes?",msg)
    if dia==QtWidgets.QMessageBox.No:
        return False
    paths_apply, roles_apply = replace_role(server,(old,new), apply=True)
    logging.debug('Applied',roles_apply)
    dia = QtWidgets.QMessageBox.information(parent,"Saved","Changes were saved. Please restart now.")

class TreePanel(QtWidgets.QSplitter):

    def __init__(self, server=False, parent=None, select=False):
        QtWidgets.QSplitter.__init__(self, parent)
        self.dialogs = {}
        self.remote = server
        self.server = server.root
        self.view = ServerView(server, parent=self)
        self.addWidget(self.view)
        self.tab = QtWidgets.QTabWidget(self)
        self.tab.setTabsClosable(True)
        self.tab.setDocumentMode(True)
        self.addWidget(self.tab)
        
        self.view.activated.connect(self.select)
        self.view.sig_objViewCamera.connect(self.objViewCamera)
        self.view.sig_objTable.connect(self.objTable)
        self.view.sig_presetsTable.connect(self.presetsTable)
        self.view.sig_wiringGraph.connect(self.wiringGraph)
        self.view.sig_objNewTab.connect(self.objNewTab)
        self.tab.tabCloseRequested.connect(self.closeTab)
        
        self.setSizes([20, 80])
        self.setHandleWidth(10)
        if select is not False:
            self.select_remote(select)
        logging.debug('TreePanel.__init__', self.server, self.remote)
        
    
    def objViewCamera(self):
        fp = self.view.current_fullpath()
        cam = self.server.toPath(fp)
        smp = cam.role2dev('smp0')
        if not smp:
            cam['smp0'] = cam.sample
        logging.debug('objViewCamera',cam, cam['smp0'], smp, self.server, self.dialogs)
        viewerDialog = self.dialogs.get(fp, False)
        if viewerDialog and viewerDialog.viewer:
            viewerDialog.close()
        from mdf_client.beholder.dialog import ViewerDialog
        viewerDialog = ViewerDialog(self.server, cam, parent=self)
        viewerDialog.show()
        viewerDialog.toggle_stream(True)
        self.destroyed.connect(viewerDialog.close)
        self.dialogs[fp] = viewerDialog
        

    def objTable(self):
        self.tab.currentWidget().show_details()
        
    def presetsTable(self):
        self.tab.currentWidget().presets_table()
        
    def wiringGraph(self):
        self.tab.currentWidget().wiring_graph()

    def objNewTab(self, index):
        """Opens in a new tab"""
        self.select(index, keepCurrent=True)

    def closeTab(self, i):
        """Closing action"""
        page = self.tab.widget(i)
        self.tab.removeTab(i)
        page.hide()
        page.deleteLater()
#		page.close()
#		del page

    def setPage(self, widget, title=False, keepCurrent=False):
        if not title:
            title = ''
        current = self.tab.currentIndex()
        if current >= 0 and not keepCurrent:
            logging.debug('Removing current tab')
            self.tab.currentWidget().close()
            self.closeTab(current)
        elif current < 0:
            current = 0
        logging.debug('Inserting tab', current)
        i = self.tab.insertTab(current, widget, title)
        self.tab.setTabToolTip(i, widget.name)
        logging.debug('Setting current index', i)
        self.tab.setCurrentIndex(i)

    def select(self, index, keepCurrent=False):
        model = self.view.model()
        node = model.data(index, role=-1)
        path = node.path
        logging.debug('selecting remote path', path)
        obj = self.remote.toPath(path)
        logging.debug('found object', obj)
        if obj is None:
            obj = self.remote.root
        self.select_remote(obj, keepCurrent)

    def select_remote(self, obj, keepCurrent=False):
        logging.debug("select_remote obj", obj, obj.parent())
        logging.debug('mro', obj['mro'])
        path = obj['fullpath'].replace('MAINSERVER', 'server').split('/')
        if len(path) == 0:
            path = [self.remote['devpath']]
            obj = self.remote
        page = constructor.Interface(self.server, obj, obj.describe(), parent=self)
        logging.debug("constructor", page, path[-1])
        self.setPage(page, '/'.join(path), keepCurrent=keepCurrent)
        logging.debug("page ok")


class MConf(QtWidgets.QMainWindow):
    tree = False
    fixed_path = False

    def __init__(self, server=False, fixed_path=False, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)
        self.setWindowTitle(_('MDF Configuration Panel'))
        self.setGeometry(100, 100, 800, 600)
        self.tab = QtWidgets.QTabWidget(self)
        self.setCentralWidget(self.tab)
        self.server = server
        self.fixed_path = fixed_path
        self.menu = RecentMenu(confdb, 'server', parent=self)
        self.menu.setTitle('Server')
        self.menu.server_disconnect.connect(self.server_disconnect)
        self.menu.server_shutdown.connect(self.server_shutdown)
        self.menu.server_restart.connect(self.server_restart)
        self.menu.select.connect(self.setAddr)
        self.cmenu = QtWidgets.QMenu('Client')
        self.cmenu.addAction(_('Configuration'), self.clientConf)
        self.cmenu.addAction(_('Pending Tasks'), self.show_tasks)
        self.menuBar().addMenu(self.menu)
        self.menuBar().addMenu(self.cmenu)
        self.umenu = QtWidgets.QMenu('Utils')
        self.umenu.addAction('Replace device', self.replace_device)
        self.menuBar().addMenu(self.umenu)
        network.manager.sig_connected.connect(self.resetServer)
        if not server:
            self.getIP()
        else:
            self.redraw()
            
    def replace_device(self):
        replace_device(self.server,self)
        

    def getIP(self):
        ss = ServerSelector(self.menu)
        network.manager.sig_connected.connect(ss.close)
        self.tab.addTab(ss, _('Server Selection'))
        self.tab.setCurrentIndex(self.tab.count() - 1)
        self.tab.addTab(ConnectionStatus(self), _('Current Connection Status'))
        self.server = network.manager.remote

    def setAddr(self, addr):
        addr = str(addr)
        obj = addrConnection(addr)
        if not obj:
            logging.debug('MConf.setAddr: Connection to address failed')
            return
        network.setRemote(obj)

    def server_disconnect(self):
        network.manager.remote.disconnect()

    def server_shutdown(self):
        self.server.shutdown()

    def server_restart(self):
        self.server.shutdown(0, 1)

    def resetServer(self):
        self.server = network.manager.remote
        if self.fixed_path:
            self.server = network.manager.remote.toPath(self.fixed_path)
        self.redraw()

    def redraw(self):
        self.tab.removeTab(2)
        if self.tree:
            self.tree.close()
            del self.tree
        self.tree = TreePanel(self.server, parent=self)
        logging.debug(self.tree.view.model().item.children)
        self.tab.addTab(self.tree, _("Tree Panel"))
        self.tab.setCurrentIndex(2)

    def clientConf(self):
        self.cc = ClientConf()
        self.cc.show()

    def show_tasks(self):
        registry.taskswg.user_show = True
        registry.taskswg.show()

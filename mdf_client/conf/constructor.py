#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Programmatic interface construction utilities"""
from mdf_canon.logger import get_module_logging
from functools import cmp_to_key

import functools
import os
import collections
from traceback import print_exc
import threading

from mdf_canon import option

from mdf_canon.option import sorter, prop_sorter, sections, groups
from mdf_canon.csutil import lockme
from .. import _
from .. import widgets
from ..configuration_check import recursive_configuration_check
from .. import iutils, open_url
from ..clientconf import confdb
from mdf_client.qt import QtWidgets, QtGui, QtCore, QtSvg
from .chronology import ChronologyDialog

logging = get_module_logging(__name__)


def desc2html(desc):
    """Crea una rappresentazione HTML del dizionario di descrizione."""
    logging.debug('show details')
    t = '<h1> Properties for: %s </h1>' % desc.get(
        'name', {'current': 'Object'})['current']
    items = list(desc.items())
    
    items = sorted(items, key=cmp_to_key(sorter))
    t += '<table border="1" cellpadding="4" font="Monospace">'
    t += '<tr> <td>Property</td> <td>Current Value</td> <td>Factory Default</td> <td> ... </td></tr>'
    for prop, val in items:
        if val['type'] == 'Section':
            continue
        if prop != 'controls':
            more = 'type: %r <br/> attr: %r' % (val['type'], val['attr'])
            t += '<tr> <td><big><u>%s</u>  </big></td> <td>  <b>%r</b></td> <td>  %r</td> <td>  %s</td></tr>' % (prop, val['current'],
                                                                                                    val.get('factory_default', ''),
                                                                                                    more)
    t += '</table>'
    t = t.replace('\\n', '<br/>')
    return t


class ClickableLabel(QtWidgets.QPushButton):
    clicked = QtCore.pyqtSignal()
    right_clicked = QtCore.pyqtSignal()
        
    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.clicked.emit()
        else: 
            self.right_clicked.emit()
        return QtWidgets.QPushButton.mousePressEvent(self, event)


class ChildSection(QtWidgets.QWidget):

    def __init__(self, *a, **k):
        super(QtWidgets.QWidget, self).__init__(*a, **k)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        self.widgetsMap = {}
        
    def closeEvent(self, ev):
        ev.accept()
        return default_closeEvent(self)


class OptionsGroup(QtWidgets.QGroupBox):

    def __init__(self, wg, child_section, parent=None):
        QtWidgets.QGroupBox.__init__(self, parent=parent)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        self.wg = wg
        self.child_section = child_section
        
        self.more = ClickableLabel()
        self.more.setIcon(iutils.theme_icon('add'))
        self.more.setMaximumWidth(30)
        self.more.clicked.connect(self.hide_show)
        self.more.right_clicked.connect(self.show_menu)
        self.menu = QtWidgets.QMenu()
        self.compare_menu = self.menu.addMenu(_('Compare'))
        self.compare_menu.aboutToShow.connect(self.build_compare_menu)
        self.setFlat(False)
        title = wg.prop.get('group', False)
        if title:
            self.setTitle(title)
            self.setStyleSheet("""QGroupBox { background-color: transparent; border: 1px solid gray; border-radius: 0px; }
        QGroupBox::title { subcontrol-position: top center; padding: 0 5px; }""")
        else:
            self.setStyleSheet("""QGroupBox { background-color: transparent; border: 0px solid gray; border-radius: 0px; }
        QGroupBox::title { subcontrol-position: top center; padding: 0 0px; }""")
        out = QtWidgets.QWidget(self)
        lay = QtWidgets.QHBoxLayout()
        lay.addWidget(wg.label_widget)
        lay.addWidget(wg)
        lay.addWidget(self.more)
        out.setLayout(lay)
        
        glay = QtWidgets.QVBoxLayout()
        glay.addWidget(out)
        glay.addWidget(child_section)
        self.setLayout(glay)
    
    def mousePressEvent(self, event):
        if not self.title():
            return
        # only active near the title bar
        if event.pos().y() > 10:
            return 
        button = event.button()
        if button == QtCore.Qt.LeftButton:
            self.hide_show() 
        elif button == QtCore.Qt.RightButton:
            event.accept()
            self.show_menu(event.pos())
            return
        
    def expand(self):
        self.child_section.show()
        self.more.setIcon(iutils.theme_icon('list-remove'))
        self.setChecked(True)
        
    def collapse(self):
        self.child_section.hide()
        self.more.setIcon(iutils.theme_icon('add'))
        self.setChecked(False)        
        
    def hide_show(self):
        """Hide or show option's child_section"""
        if self.child_section.isVisible():
            self.collapse()
        else:
            self.expand()
            
    def show_menu(self, pos=False):
        if not pos:
            pos = self.more.pos()
        self.menu.popup(self.mapToGlobal(pos))
        
    def build_compare_menu(self):
        self.comparisons = {}
        self.compare_menu.clear()
        wm = self.child_section.widgetsMap.copy()
        wm[self.wg.handle] = self.wg
        comparison = self.wg.remObj.compare_option(*list(wm.keys()))
        set_func = lambda keyvals: [wm[k].set_raw(v) for k, v in keyvals]
        widgets.active.build_option_menu(comparison, self.comparisons, self.compare_menu, set_func)
        
    @property
    def widgetsMap(self):
        return self.child_section.widgetsMap
    
    @widgetsMap.setter
    def widgetsMap(self, w):
        self.child_section.widgetsMap = w

    def check_visibility(self, filtered):
        vis = set(self.widgetsMap.values()) - filtered
        if vis:
            self.show()
            if self.wg.isVisible():
                self.more.show()
            return False
        else:
            self.more.hide()
            self.hide()
            return True
        
    def closeEvent(self, ev):
        ev.accept()
        return default_closeEvent(self)


class VerticalFormLayout(QtWidgets.QVBoxLayout):

    def addRow(self, *widgets):
        for w in widgets:
            self.addWidget(w)
                
    def setLabelAlignment(self, *a, **k):
        pass
        
    def setRowWrapPolicy(self, *a, **k):
        pass


class AbstractSection(object):

    def __init__(self, server, remObj, prop_dict):
        self.p = []
        self.server = server
        self.remObj = remObj
        self.path = remObj._Method__name
        self.lay = QtWidgets.QFormLayout() if len(prop_dict) > 1 else VerticalFormLayout()
        self.lay.setLabelAlignment(QtCore.Qt.AlignRight)
        self.lay.setRowWrapPolicy(QtWidgets.QFormLayout.WrapLongRows)
        self.setLayout(self.lay)
        self.widgetsMap = {}
        self.parentsMap = {}
        self.groupsMap = {}
        prop_list = sorted(prop_dict.values(), key=cmp_to_key(prop_sorter))
        for prop in prop_list:
            self.build(prop)
            
    def add_children(self, main_widget, prop):
        parent_layout = main_widget.layout()
        parent_widget = widgets.build(
            self.server, self.remObj, prop, parent=main_widget)
        if not parent_widget:
            return False
        self.widgetsMap[prop['handle']] = parent_widget
        if main_widget != self:
            main_widget.widgetsMap[prop['handle']] = parent_widget
            
        # No children: just add the option and return
        if len(prop.get('children', [])) == 0:
            parent_layout.addRow(parent_widget.label_widget, parent_widget)
            return True
        # Widget hosting the children form
        child_section = ChildSection(parent_widget)
        children_lay = QtWidgets.QFormLayout()
        child_section.setLayout(children_lay)
        # Add the parent option plus the expansion button
        group = OptionsGroup(parent_widget, child_section, parent=self)
        self.groupsMap[parent_widget.handle] = group
        parent_layout.addRow(group)
        for handle, child in prop['children'].items():
            if handle == prop['handle']:
                logging.error(
                    'Option parenthood loop detected', handle, list(prop['children'].keys()))
                continue
            self.add_children(child_section, child)
            self.parentsMap[handle] = child_section
        child_section.hide()

    def build(self, prop):
        self.add_children(self, prop)
        return True


class BaseSection(AbstractSection, QtWidgets.QWidget):

    def __init__(self, server, remObj, prop_dict, parent=None, context='Option'):
        QtWidgets.QWidget.__init__(self, parent)
        AbstractSection.__init__(self, server, remObj, prop_dict)

        
class Section(AbstractSection, QtWidgets.QGroupBox):
    """Form builder for a list of options"""
    
    def __init__(self, server, remObj, prop_dict, title='Group', color='gray', parent=None, context='Option'):
        QtWidgets.QGroupBox.__init__(self, parent)
        AbstractSection.__init__(self, server, remObj, prop_dict)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        self.setTitle(title)
        self.setStyleSheet("""QGroupBox { background-color: transparent;
        border: 1px solid %s; border-radius: 5px;
        margin-top: 10px; margin-bottom: 10px; }
        QGroupBox::title { subcontrol-origin: margin; subcontrol-position: top left; padding: 0px 5px; }""" % (color,))
        self.setCheckable(True)
        self.setChecked(True)
        self.clicked.connect(self.enable_disable)
        
    def closeEvent(self, ev):
        ev.accept()
        return default_closeEvent(self)
    
    def hide_frame(self):
        self.setStyleSheet("""QGroupBox { background-color: transparent; 
        border: 0px solid gray; border-radius: 0px; 
        margin-top: 0px; margin-bottom: 0px; }
        """)
        self.setTitle('')
        self.setChecked(True)
        self.setCheckable(False)
        
    def check_visibility(self, filtered):
        for group in self.groupsMap.values():
            group.check_visibility(filtered)
        vis = set(self.widgetsMap.values()) - filtered
        if vis:
            self.show()
            return False
        else:
            self.hide()
            return True

    def enable_disable(self, status=True):
        if status:
            self.expand()
        else:
            self.collapse()

    def expand(self):
        for w in self.groupsMap.values():
            w.show()
            # w.expand()
        widths = []
        heights = 0
        for w in self.widgetsMap.values():
            w.show()
            w.label_widget.show()
            s = w.sizeHint()
            widths.append(s.width())
            heights += s.height()
        self.setMinimumHeight(int(heights * 0.8))
        self.setMaximumHeight(16777215)
        self.setChecked(True)

    def collapse(self):
        if not self.isCheckable():
            return False
        for w in self.groupsMap.values():
            w.hide()
        for w in self.widgetsMap.values():
            w.hide()
            w.label_widget.hide()
        self.setMinimumHeight(0)
        self.setMaximumHeight(40)
        self.setChecked(False)
        return True
            
    def expand_children(self):
        for group in self.groupsMap.values():
            group.expand()

    def collapse_children(self):
        for group in self.groupsMap.values():
            group.collapse()

    def highlight_option(self, handle):
        self.show()
        wg = self.widgetsMap.get(handle, False)
        if wg is False:
            logging.error('Section.highlight_option: not found', handle)
            return False
        parent = self.groupsMap.get(handle, False)
        if parent is not False:
            parent.expand()
        wg.label_widget.set_highlighted(True)
        self.expand()
        return wg


CONFIG_SEC = 0
STATUS_SEC = 1
RESULT_SEC = 2


def match_widget_filter(wg, query):
    query = query.lower()
    if query in wg.handle.lower():
        return True
    if query in wg.name.lower():
        return True
    if query in wg.label.lower():
        return True
    if query in str(wg.current).lower():
        return True
    return False


class SectionBox(QtWidgets.QWidget):
    """Divide section into Status, Configuration and Results toolboxes."""
    sigScrollTo = QtCore.pyqtSignal(int, int)

    def __init__(self, server, remObj, prop_dict, parent=None, context='Option'):
        QtWidgets.QWidget.__init__(self, parent=parent)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        self.lay = QtWidgets.QVBoxLayout()
        self.setLayout(self.lay)
        self.server = server
        self.remObj = remObj
        
        # Do not draw sections if parent is flat
        if parent.flat:
            config_dict = prop_dict
            prop_dict = {}
            base_dict, results_dict, status_dict = [{}, {}, {}]
        else:
            base_dict, config_dict, results_dict, status_dict = groups(prop_dict)
            
        self.base_section = BaseSection(server, remObj, base_dict, parent=self, context=context)
        self.status_section = Section(server, remObj, status_dict, title=_('Status'), color='red',
                                      parent=self, context=context)
        self.results_section = Section(server, remObj, results_dict, title=_('Results'), color='green',
                                       parent=self, context=context)
        self.config_section = Section(server, remObj, config_dict, title=_('Configuration'), color='gray',
                                      parent=self, context=context)
        
        self.subsections = [self.config_section, self.status_section, self.results_section]
        [sec.hide() for sec in self.subsections]
        self.widgetsMap = {}
        self.widgetsMap.update(self.status_section.widgetsMap)
        self.widgetsMap.update(self.results_section.widgetsMap)
        self.widgetsMap.update(self.config_section.widgetsMap)
        self.widgetsMap.update(self.base_section.widgetsMap)
        
        lens = [len(sec.widgetsMap) > 0 for sec in self.subsections]
        # None active!
        if sum(lens) < 1:
            return
        # Just one active section:
        if sum(lens) == 1:
            i = lens.index(True)
            sec = self.subsections[i]
            self.lay.addWidget(sec)
            sec.show()
            sec.hide_frame()
            return

        def add_section(sec):
            """Add section only if has widgets to show"""
            if not sec.widgetsMap:
                sec.hide()
            else:
                self.lay.addWidget(sec)
                sec.show()

        # Prioritize sections
        add_section(self.base_section)
        add_section(self.config_section)

        if not getattr(remObj, 'remObj', False):
            add_section(self.results_section)
            add_section(self.status_section)
        else:
            add_section(self.status_section)
            add_section(self.results_section)

        self.lay.addStretch(10)
        self.reorder()
        
    def closeEvent(self, ev):
        ev.accept()
        return default_closeEvent(self)

    def check_visibility(self, filtered):
        r = 0
        for sec in self.subsections:
            r += sec.check_visibility(filtered)
        return bool(r)
        
    def expand(self):
        for s in self.subsections:
            s.expand()
            
    def collapse(self):
        for s in self.subsections:
            s.collapse()

    def get_status(self):
        status = [True, True, True]
        # Hide everything
        for i, w in enumerate(self.subsections):
            status[i] = w.isChecked() and len(w.widgetsMap) > 0
        return status
    
    def set_status(self, status):
        for i, st in enumerate(status):
            w = self.subsections[i]
            if st and len(w.widgetsMap) > 0:
                w.expand()
                self.sigScrollTo.emit(w.pos().x(), w.pos().y() + 50)
            else:
                w.collapse()

    def reorder(self):
        isOnline = hasattr(self.server, '_remObj')
        isRunning = isOnline and ('isRunning' in self.server) and self.server['isRunning']
        status = self.get_status()
        # Select what to show
        if isRunning:
            status[CONFIG_SEC] = False
            status[STATUS_SEC] = True
            status[RESULT_SEC] = True
        else:
            status[CONFIG_SEC] = isOnline
            status[RESULT_SEC] = True
            status[STATUS_SEC] = False
        self.set_status(status)

    def highlight_option(self, handle):
        for sec in [self.config_section, self.status_section, self.results_section]:
            if handle in sec.widgetsMap:
                break
            else:
                sec = False
        if not sec:
            logging.debug(
                'SectionBox.highlight_option: not visible/found', handle)
            return False
        wg = sec.highlight_option(handle)
        x = sec.x()
        y = sec.y()
        parent = wg
        while parent != sec:
            y += parent.y()
            x += parent.x()
            parent = parent.parent()
        self.sigScrollTo.emit(x, y)
        return wg
    

class SectionScroll(QtWidgets.QScrollArea):
    """Disable wheelEvent on Interface area, as it interferes with complex 
    ActiveWidgets (aChooser, aNumber)"""

    def wheelEvent(self, ev):
        ev.ignore()

        
def default_closeEvent(obj):
    objs = getattr(obj, 'subsections', [])
    if objs:
        obj.subsections = []
    for attr in ('sectionsMap', 'widgetsMap', 'groupsMap'):
        sub = getattr(obj, attr, False)
        if sub:
            objs += list(sub.values())
            setattr(obj, attr, {})
    obj.blockSignals(True)
    for sub in objs:
        sub.close()
        sub.deleteLater()
        sub.blockSignals(True)
    

class Interface(QtWidgets.QTabWidget):

    """Tabbed interface builder for dictionary of options"""
    first_show = True
    changeset = -1
    flat = False
    _lockme_error = False

    def __init__(self, server, remObj=False, prop_dict=False, parent=None, context='Option', fixed=False, flat=False):
        QtWidgets.QTabWidget.__init__(self, parent)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
        self._lock = threading.Lock()
        self.server = server
        if remObj is False:
            remObj = server
        if remObj.doc:
            remObj.doc.sigConfProxyModified.connect(functools.partial(self.rebuild, False, True, True))
        self.remObj = remObj
        self.prop_dict = {}
        self.prop_keys = []
        self.sectionsMap = False
        self.fixed = fixed
        self.flat = flat
        self.rebuild(prop_dict)
        self.menu = QtWidgets.QMenu(self)
        self.menu.addAction(_('Search (Ctrl+F)'), self.activate_search)
        self.menu.addAction(_('Exit search (Esc)'), self.deactivate_search)
        self.menu.addAction(_('Chronology'), self.show_chronology)
        self.menu.addAction(_('Table'), self.show_details)
        self.menu.addAction(_('Rebuild'), functools.partial(self.rebuild, force=True, redraw=True))
        self.menu.addAction(_('Help'), self.help)
        
        self.search = QtWidgets.QLineEdit(self)
        self.search.setPlaceholderText(_('Search option'))
        self.search.hide()
        self.search.textChanged.connect(self.filter_text)
        self.filtered = set()
        QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.CTRL + QtCore.Qt.Key_F),
                        self,
                        self.activate_search)
        QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Escape),
                        self,
                        self.deactivate_search)
        
    def closeEvent(self, ev):
        ev.accept()
        self.clear()
        self._deleted = True
        return default_closeEvent(self)
    
    def help(self):
        cls = self.remObj['mro'][0]
        url = f'https://app.ceramics-genome.ai/#help/en/mdf/options/{cls}.html'
        open_url(url)
    
    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.RightButton:
            event.accept()
            self.showMenu(event.pos())
        return super(Interface, self).mousePressEvent(event)

    def show_section(self, name, hide=False):
        """Set `name` as section the active tab. 
        If `hide`, removes all other tabs."""
        sections = list(self.sectionsMap.keys())
        if name not in sections:
            logging.warning('No section named', name)
            return False
        for i, sec in enumerate(sections[:]):
            if sec == name and not hide:
                self.setCurrentIndex(i)
            elif sec != name and hide:
                j = sections.index(sec)
                self.removeTab(j)
                sections.pop(j)
        return True

    def check_visibility(self, visibility=None):
        if visibility:
            self.filtered = visibility
        for sec in self.sectionsMap.values():
            sec.check_visibility(self.filtered)

    def showEvent(self, event):
        if self.first_show:
            self.first_show = False
            return
        self.rebuild(force=True)

    _prev_section = False
    _prev_section_status = False
    _deleted = False

    @lockme()
    def rebuild(self, prop_dict=False, force=False, redraw=False):
        """Rebuild the full widget"""
        if self._deleted:
            logging.debug('Interface.rebuild: deleted')
            return False
        if not self.remObj.is_live() and 'fullpath' in self.remObj:
            logging.debug('Check changeset', self.remObj['fullpath'], self.remObj._changeset, self.changeset, id(self))
            if self.remObj._changeset <= self.changeset:
                logging.debug('rebuild: no change')
                return False
            self.changeset = self.remObj._changeset
            
        if self.sectionsMap:
            try:
                i = self.currentIndex()
            except:
                print_exc()
                return
            self._prev_section = list(self.sectionsMap.keys())[i]
            self._prev_section_status = self.sectionsMap[self._prev_section].get_status() 
        if not prop_dict and self.fixed:
            prop_dict = self.prop_dict
        elif not prop_dict:
            self.remObj.connect()
            k = set(self.prop_keys)
            rk = set(self.remObj.keys())
            d = k.symmetric_difference(rk)
            if len(d) == 0 and not (force or redraw):
                logging.debug(
                    'Interface.rebuild not needed: options are equal.')
                return
            prop_dict = self.remObj.describe()
            # If a prop_dict was set, just pick currently defined options
            if len(k) and len(prop_dict) and not redraw:
                visible = set(prop_dict.keys()).intersection(k)
                prop_dict = {key: prop_dict[key] for key in visible}
        if not prop_dict:
            logging.critical('Impossible to get object description',
                             self.remObj._Method__name)
            return
        # Check if the option should be hidden by configuration opt_hide:
        fp = ''
        if 'fullpath' in prop_dict:
            fp = prop_dict['fullpath']['current']
        if not fp.endswith('/'): fp += '/'
        for handle in prop_dict.keys():
            opt = prop_dict[handle]
            a = list(set(opt['attr']))
            if confdb.rule_opt_hide(fp + handle): 
                if 'ClientHide' not in a:
                    a.append('ClientHide')
                    logging.debug('Set ClientHide attributes', fp, handle, a)
                    self.remObj.setattr(handle, 'attr', a)
                    opt['attr'] = a
            else:
                if 'ClientHide' in a:
                    a.remove('ClientHide')
                    self.remObj.setattr(handle, 'attr', a)
                    opt['attr'] = a
            
        self.sections = sections(prop_dict, self.flat, self.remObj._readLevel)
        self.prop_dict = prop_dict
        self.prop_keys = list(self.prop_dict.keys())
        self.name = ''
        if 'name' in prop_dict:
            self.name = prop_dict['name']['current']
            self.setWindowTitle('Configuring: ' + self.name)
        default_closeEvent(self)
        self.sectionsMap = False
        self.widgetsMap = {}
        self.redraw()
        if 'preset' in self.sectionsMap['Main'].widgetsMap:
            preset = self.sectionsMap['Main'].widgetsMap['preset']
            preset.sig_changed.connect(self.update)
            self.scSave = QtWidgets.QShortcut(
                QtGui.QKeySequence(QtCore.Qt.CTRL + QtCore.Qt.Key_S), self, context=QtCore.Qt.WidgetWithChildrenShortcut)
            self.scSave.activated.connect(preset.save_current)
            
        # Restore previous status
        if self._prev_section and self._prev_section in self.sectionsMap:
            self.show_section(self._prev_section)
            self.sectionsMap[self._prev_section].set_status(self._prev_section_status)

    def reorder(self):
        """Switch toolbox currentIndexes"""
        if not self.sectionsMap:return
        for sec in self.sectionsMap.values():
            sec.reorder()

    def redraw(self, foo=0):
        if self._deleted:
            logging.debug('Interface.redraw: deleted')
            return False
        self.clear()
        wg = SectionBox(
            self.server, self.remObj, self.sections['Main'], parent=self)
        self.sectionsMap = collections.OrderedDict({'Main': wg})
        self._funcs = []
        area = SectionScroll(self)
        area.setWidget(wg)
        area.setWidgetResizable(True)
        self.areasMap = collections.OrderedDict({'Main': area})
        f = functools.partial(self.scroll_to, area)
        wg.sigScrollTo.connect(f)
        self._funcs.append(f)
        sname = 'Main'
        if sname in self.prop_dict:
            if self.prop_dict[sname]['type'] == 'Section':
                sname = self.prop_dict[sname]['name']
        self.addTab(area, _('Main'))

        for section, prop_dict in self.sections.items():
            if section == 'Main':
                continue
            wg = SectionBox(self.server, self.remObj, prop_dict, parent=self)
            # Ignore empty sections
            if not len(wg.widgetsMap):
                continue
            self.sectionsMap[section] = wg
            area = SectionScroll(self)
            area.setWidget(wg)
            area.setWidgetResizable(True)
            self.areasMap[section] = area
            sname = section
            if sname in self.prop_dict:
                if self.prop_dict[sname]['type'] == 'Section':
                    sname = self.prop_dict[sname]['name']
            self.addTab(area, _(sname))

            f = functools.partial(self.scroll_to, area)
            wg.sigScrollTo.connect(f)
            self._funcs.append(f)

        # hide tabBar if just one section
        self.tabBar().setVisible(len(self.sections.keys()) > 1)
        for sec in self.sectionsMap.values():
            self.widgetsMap.update(sec.widgetsMap)

    def highlight_option(self, handle):
        """Ensure `handle` widget is visible"""
        sec = 'Main'
        if '_' in handle and len(self.sectionsMap) > 1:
            sec = handle.split('_')[0]
        area = self.areasMap[sec]
        sec = self.sectionsMap[sec]
        self.setCurrentWidget(area)
        wg = sec.highlight_option(handle)
        logging.debug('HIGHLIGHT OPT', handle)
        return wg
    
    def hide_widget(self, wg):
        """Hide `handle` widget from the interface"""
        wg.hide()
        wg.label_widget.hide()
        self.filtered.add(wg)
        
    def hide_option(self, handle):
        self.hide_widget(self.widgetsMap[handle])
        
    def show_widget(self, wg):
        wg.show()
        wg.label_widget.show()
        if wg in self.filtered:
            self.filtered.remove(wg)
            
    def show_option(self, handle):
        self.show_widget(self.widgetsMap[handle])
        
    def show_only_options(self, keys):
        self.expand_all()
        for key, wg in self.widgetsMap.items():
            if key in keys:
                self.show_widget(wg)
            else:
                self.hide_widget(wg)
        self.check_visibility()

    def scroll_to(self, area, x, y):
        area.ensureVisible(x, y - 50)

    def __close(self):
        if not self.sectionsMap:
            return
        for wg in self.sectionsMap.values():
            wg.close()
            wg.deleteLater()
        for i in range(self.count()):
            logging.debug('remove tab', i)
            self.removeTab(self.currentIndex())
        self.clear()
        self.sectionsMap = {}
        self._deleted = True
        self.blockSignals(True)

    def update(self):
        """Cause all widgets to re-register for an update"""
        for s, sec in self.sectionsMap.items():
            for w, wg in sec.widgetsMap.items():
                if wg.type == 'Button':
                    continue
                wg.register()

    def showMenu(self, pt):
        self.menu.popup(self.mapToGlobal(pt))
        
    _chron = None

    def show_chronology(self):
        """Show chronology explorer for global reset"""
        if self._chron:
            self._chron.hide()
            self._chron.close()
        handles = False
        i = self.currentIndex()
        if self.count() and i >= 0:
            area = self.currentWidget()
            handles = list(area.widget().widgetsMap.keys())
        self._chron = ChronologyDialog(self, handles, parent=None)
        self._chron.exec_()
        self._chron.table.sig_applied.connect(self._chron.close)

    def show_details(self):
        """Show full configuration as HTML table"""
        self.desc = self.remObj.describe()
        widgets.info_dialog(desc2html(self.desc), 'Details for Object: %s' % self.desc.get(
            'name', {'current': 'Object'})['current'], parent=self)

    def presets_table(self):
        self.desc = self.remObj.describe()
        output = recursive_configuration_check(self.remObj)
        widgets.info_dialog(output, 'Presets Comparison, %s' % self.desc.get(
            'name', {'current': 'Object'})['current'], parent=self)

    _wiring = False

    def wiring_graph(self):
        if self._wiring:
            self._wiring.hide()
            self._wiring.close()
            self._wiring = False
        # Temp file
        svg = self.remObj.render_wiring()
        svg_filename = 'wiring_graph.svg'
        open(svg_filename, 'w').write(svg)

        # Scene
        scene = QtWidgets.QGraphicsScene()
        view = QtWidgets.QGraphicsView()
        view.setScene(scene)
        svg = QtSvg.QGraphicsSvgItem(svg_filename)
        scene.addItem(svg)
        # Display widget
        wg = QtWidgets.QWidget()
        lay = QtWidgets.QVBoxLayout()
        wg.setLayout(lay)
        lay.addWidget(view)
        wg.show()
        self._wiring = wg
        # Cleanup
        os.remove(svg_filename)
        
    def resizeEvent(self, ev):
        self.move_search()
        return QtWidgets.QTabWidget.resizeEvent(self, ev)
        
    def move_search(self):
        w = self.size().width()
        self.search.move(w - self.search.width() - 10, 5)
        
    def expand_all(self):
        for sec in self.sectionsMap.values():
            sec.expand()
            for s in sec.subsections:
                s.expand_children()
                
    def collapse_all(self):
        for sec in self.sectionsMap.values():
            for s in sec.subsections:
                s.collapse_children()
        
    def deactivate_search(self):
        self.search.hide()
        self.search.setText('')
        self.search.releaseKeyboard()
        self.collapse_all()
        self.check_visibility()
        
    def activate_search(self):
        self.expand_all()
        self.search.show()
        self.search.raise_()
        self.search.setFocus()
        self.move_search()
            
    def filter_text(self, query=False):
        if not query or not self.search.isVisible():
            for wg in self.filtered.copy():
                self.show_widget(wg)
            self.check_visibility()
            return 0
        i = 0
        for wg in self.widgetsMap.values():
            if match_widget_filter(wg, query):
                self.show_widget(wg)
                i += 1
            else:
                self.hide_widget(wg)
        self.check_visibility()
        return i
    
    def connect_client_changed(self, callable):
        """Connects all clientChanged signals to `callable`"""
        for wg in self.widgetsMap.values():
            wg.client_changed.connect(callable)


class InterfaceDialog(QtWidgets.QDialog):

    def __init__(self, server, remObj=False, prop_dict=False, parent=None):
        QtWidgets.QDialog.__init__(self, parent=parent)
        lay = QtWidgets.QVBoxLayout()
        self.setLayout(lay)
        
        self.interfaces = []
        if type(server) not in (list, tuple):
            server = [server]
            remObj = [remObj]
            prop_dict = [prop_dict]
            
        for i, srv in enumerate(server):
            intf = Interface(srv, remObj[i], prop_dict[i], self)
            self.interfaces.append(intf)
            lay.addWidget(intf)
            
        self.ok = QtWidgets.QPushButton('Ok')
        self.ok.setDefault(True)
        lay.addWidget(self.ok)
        self.ok.clicked.connect(self.ok_clicked)
        
        self.ko = QtWidgets.QPushButton(_('Cancel'))
        lay.addWidget(self.ko)
        self.ko.clicked.connect(self.reject)
        
    def ok_clicked(self):
        for intf in self.interfaces:
            intf.close()
            intf.hide()
            intf.deleteLater()
        
        self.accept()

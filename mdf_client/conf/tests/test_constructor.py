#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Testing the programmatic widget construction."""
import unittest
import os
from mdf_canon.option import ConfigurationProxy
from mdf_client.conf import constructor
from mdf_client import filedata
from mdf_client.tests import iutils_testing

nativem4 = os.path.join(iutils_testing.data_dir, 'test_video.h5')


class TestConstructor(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.file_proxy = filedata.getFileProxy(nativem4, mode='r')
        self.file_proxy.load_conf()
        self.conf = self.file_proxy.conf
    
    @classmethod
    def tearDownClass(cls):
        cls.file_proxy.close()
        
    def test_section(self):
        std = self.conf.hsm.measure
        sec = constructor.sections(std.describe())['Main']
        obj = constructor.Section(self.conf, std, sec)
        iutils_testing.show(obj, __name__)
    
    def test_interface(self):
        rem = self.conf.hsm.measure
        obj = constructor.Interface(self.conf, rem, rem.describe())
        iutils_testing.show(obj, __name__)
        
    def test_nested_options(self):
        p = ConfigurationProxy()
        p.add_option('a', 'String', '')
        p.add_option('b', 'String', '', parent='a')
        p.add_option('c', 'String', '', parent='b')
        p.add_option('sec_d', 'String', '')
        obj = constructor.Interface(p, p, p.describe())
        iutils_testing.show(obj, __name__)


if __name__ == "__main__":
    unittest.main()

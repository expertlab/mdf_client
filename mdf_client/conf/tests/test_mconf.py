#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Tests Archive"""
import unittest
import os
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)

from mdf_client import conf

from mdf_client.tests import iutils_testing 
from mdf_client import filedata
from mdf_client.qt import QtWidgets, QtGui

nativem4 = os.path.join(iutils_testing.data_dir, 'test_video.h5')
logging.debug('Importing', __name__)
main = __name__ == '__main__'


def setUpModule():
    logging.debug('setUpModule', __name__)


def tearDownModule():
    logging.debug('tearDownModule', __name__)


class TestTreePanel(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.file_proxy = filedata.getFileProxy(nativem4, mode='r')
        cls.file_proxy.load_conf()
        
        cls.server = cls.file_proxy.conf
        
    @classmethod
    def tearDownClass(cls) -> None:
        cls.file_proxy.close()

    def test_recursiveModel(self):
        logging.debug('######## STARTING MCONF #######' * 10)
        m = conf.TreePanel(self.server.users, select=self.server.users)
        if main:
            m.show()
            QtWidgets.qApp.exec_()


if __name__ == "__main__":
    unittest.main()

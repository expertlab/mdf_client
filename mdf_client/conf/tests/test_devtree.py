#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Tests Archive"""
import unittest
import os
from mdf_client.tests import iutils_testing

from mdf_client.conf import devtree
from mdf_client import filedata

from mdf_canon.plugin import dataimport

app = False

nativem4 = os.path.join(iutils_testing.data_dir, 'measure.h5')


class TestRecursiveModel(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.file_proxy = filedata.getFileProxy(nativem4, mode='r')
        cls.file_proxy.load_conf()
        cls.server = cls.file_proxy.conf
        
    @classmethod
    def tearDownClass(cls) -> None:
        cls.file_proxy.close()

    def test_recursiveModel(self):
        m = devtree.recursiveModel(self.server)

# TODO: test su fileproxy!


class TestServerModel(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.file_proxy = filedata.getFileProxy(nativem4, mode='r')
        cls.file_proxy.load_conf()
        
        cls.server = cls.file_proxy.conf
        
    @classmethod
    def tearDownClass(cls) -> None:
        cls.file_proxy.close()

    def setUp(self):
        self.m = devtree.recursiveModel(self.server)

    def test_init(self):
        mod = devtree.ServerModel(self.server)
        
    def test_add_child(self):
        mod = devtree.ServerModel(self.server)
        base = dataimport.base_dict()
        base['name']['current'] = 'pippo'
        n0 = len(mod.item.children[0].children[1].children)
        self.server.instrument.add_child('new', base)
        self.assertIn('new', self.server.instrument.children)
        mod.refresh()
        n1 = len(mod.item.children[0].children[1].children)
        self.assertEqual(n1, n0 + 1)
        self.assertIn('new', self.server.instrument.children)


if __name__ == "__main__":
    unittest.main()

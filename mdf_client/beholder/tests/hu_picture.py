#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Camera ViewerPicture, Human Interactive Test"""
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
import unittest
import functools
# from mdf_server import utils_testing as ut
from mdf_client.tests import iutils_testing
from mdf_client.beholder import picture
from misura.beholder import sim_camera

from misura.microscope import Hsm
from mdf_client.qt import QtWidgets, QtGui

logging.debug('Importing', __name__)


def setUpModule():
    logging.debug('setUpModule', __name__)


def tearDownModule():
    logging.debug('tearDownModule', __name__)
    logging.debug('Quitting app')


class HuViewerPicture(unittest.TestCase):

    def setUp(self):
        self.server = None  # ut.dummyServer(Hsm)
        self.server.hsm.init_instrument()
        self.server.hsm.sample0.analyzer.autoroi['Hmargin'] = 25
        self.server.hsm.sample0.analyzer.autoroi['Vmargin'] = 25
        self.server.hsm['nSamples'] = 1
        self.rem = sim_camera.SimCamera(parent=self.server)
        self.rem.encoder['react'] = 'Strictly Follow'
        self.rem['smp0'] = [self.server.hsm.sample0['fullpath'], 'default']
        self.rem.start_acquisition = functools.partial(
            self.server.set, 'isRunning', True)
        self.rem.parent = lambda: self.server
        self.obj = picture.ViewerPicture(self.rem, self.server)
        self.rem.copy = lambda: self.rem

    def tearDown(self):
        self.obj.close()

    def test_exec(self):
        self.obj.show()
        QtWidgets.qApp.exec_()


if __name__ == "__main__":
    unittest.main()

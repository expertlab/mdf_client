#!/usr/bin/python
# -*- coding: utf-8 -*-
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from mdf_client.qt import QtWidgets, QtGui, QtCore
from mdf_client import _
from . import picture


class ViewerControl(QtWidgets.QWidget):

    """A viewer widget with the possibility to add lateral sliders."""
    # Layout: 3 cols 3 rows. Upper/bottom-left/right cells remains empty
    # (1.1,1.3,3.1,3.3).
    positions = {'bottom': (3, 2), 'up': (1, 2),
                 'left': (2, 1), 'right': (2, 3)}
    """Default grid coordinates for widgets placing"""

    def __init__(self, remote, server, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.setWindowTitle(_('Camera Viewer: ') + remote['name'])
        self.controls = {'bottom': None, 'up': None,
                         'left': None, 'right': None}
        """Keeps track of the control widgets positions"""
        self.server = server
        self.remote = remote

        self.lay = QtWidgets.QGridLayout()
        self.lay.setSpacing(0)
        self.lay.setContentsMargins(0, 0, 0, 0)
        self.viewer = picture.ViewerPicture(remote, server, self)

        # Viewer has the middle cell
        self.lay.addWidget(self.viewer, 2, 2)

        self.setLayout(self.lay)

        # Proxy for viewer functions
        self.setSampleProcessor = self.viewer.setSampleProcessor
        self.setFrameProcessor = self.viewer.setFrameProcessor

    @property
    def processor(self):
        if not self.viewer:
            return False
        return self.viewer.processor

    def close(self):
        logging.debug('ViewerControl.close')
        for ctrl in self.controls.values():
            if ctrl is None:
                continue
            ctrl.close()
            self.lay.removeWidget(ctrl)
            ctrl.deleteLater()
        if self.viewer:
            self.viewer.close()
            self.viewer = False

    def closeEvent(self, ev):
        self.close()
        QtWidgets.QWidget.closeEvent(self, ev)

    def setControl(self, widget, position, tooltip='', inversion=0):
        """Set a slider widget in the given position. Available positions: upper, bottom, left, right"""
        if position not in self.positions:
            logging.debug('Impossible position requested', position)
            return False
        if self.controls[position] is not None:
            logging.debug('Position already has a control', position,
                          self.controls[position])
            return False
        pos = self.positions[position]
        widget.label_widget.hide()
        if widget.slider:
            widget.slider.setToolTip(tooltip)
        # Rotate if in left/right position
        if position in ['left', 'right']:
            widget.setOrientation(QtCore.Qt.Vertical)
        else: 
            widget.setOrientation(QtCore.Qt.Horizontal)
            
        widget.set_inverted_slider(inversion)
        self.lay.addWidget(widget, *pos)
        self.controls[position] = widget
        logging.debug('ViewerControl.setControl', widget, position, pos)

    def delControl(self, position):
        """Removes a control widget placed in position"""
        wdg = self.controls[position]
        if wdg is None:
            logging.debug('Impossible to remove the control widget', position)
            return False
        self.lay.removeWidget(wdg)
        wdg.hide()
        wdg.close()
        wdg.destroy()
        self.controls[position] = None

    def delControls(self):
        """Clears all control widgets."""
        for pos in self.positions.keys():
            self.delControl(pos)


#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Graphical overlay for image analysis"""
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from mdf_client.qt import QtWidgets, QtGui, QtCore
colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255),
          (0, 125, 125), (125, 125, 0), (125, 0, 125)] * 3


class Overlay(QtWidgets.QGraphicsItem):

    """Active graphical item, which is connected with remote options. 
    Its position and dimension vary according to remote options, 
    as well as an update of remote options will modify the item."""
    moving = False
    opt = set(['roi', 'crop'])
    pen_width = 3

    def __init__(self, parentItem=None, Z=1):
        QtWidgets.QGraphicsItem.__init__(self, parent=parentItem)
        self.opt = set([])
        self.color = QtGui.QColor(*colors[Z % len(colors)])
        pen = QtGui.QPen(self.color)
        pen.setWidthF(self.pen_width)
        self.pen = pen
        self.Z = Z
        self.current = {}
        self.ovname = self.__class__.__name__ + str(Z)
        self.hide()

    def dim(self, factor=100, minimum=5):
        """Factor is part of the sceneRect"""
        r = self.scene().sceneRect()
        s = self.scene().views()[0].transform().m11()
        if s == 0:
            return minimum
        r = self.parentItem().boundingRect()
        d0 = max((r.width(), r.height())) / s
        d = int(max((1. * d0 / factor, minimum)))
        return d

    @property
    def zoom_factor(self):
        t = self.scene()
        if t is None:
            self.hide()
            return 1
        t = t.views()[0].transform()
        return abs(t.m11() + t.m21())

    def unscale(self, factor):
        self.pen_width = (self.pen_width * 1. / factor)
        self.pen.setWidthF(max(3, self.pen_width))

    def blockUpdates(self):
        self.moving = True

    def unblockUpdates(self):
        self.moving = False

    def slot_update(self, multiget):
        """Slot to update current value from a multiget result dispatched by a SampleProcessor."""
        if self.moving:
            return False
        for opt in self.opt:
            if opt not in multiget:
                continue
            self.current[opt] = multiget[opt]
        if self.validate():
            self.up()
            return True
        return False

    def up(self):
        """Redraw using current value."""
        pass

    def validate(self):
        for opt in self.opt:
            if opt not in self.current:
                logging.debug('Validation Failed', self.ovname, self.Z)
                return False
        return True

    def boundingRect(self):
        return QtCore.QRectF(0, 0, 0, 0)

    def paint(self, *a, **kw):
        return None

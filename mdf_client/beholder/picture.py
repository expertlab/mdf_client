#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
from functools import partial
from mdf_client.iutils import theme_icon
from mdf_client import widgets, _
from .. import conf
from .. import parameters
from ..live import FrameProcessor, SampleProcessor
from .motionplane import SensorPlane, should_camera_be_rotated, motor_align_factor
from .sample_picture import SamplePicture
from . import calibration
from mdf_client.qt import QtWidgets, QtGui, QtCore, uconnect, udisconnect
from mdf_canon.logger import get_module_logging
from mdf_canon import csutil
from time import time
logging = get_module_logging(__name__)


class Zoomer(QtWidgets.QWidgetAction):
    multiplier = 100

    def __init__(self, parent=None):
        QtWidgets.QWidgetAction.__init__(self, parent)
        self.w = QtWidgets.QWidget()
        self.lay = QtWidgets.QVBoxLayout()
        self.w.setLayout(self.lay)
        self.wdg = QtWidgets.QSlider()
        self.wdg.setOrientation(QtCore.Qt.Horizontal)
        self.wdg.setRange(1, 10 * self.multiplier)
        self.wdg.setValue(int(self.multiplier * parent.zooming_factor))
        self.lay.addWidget(QtWidgets.QLabel(_('Zoom')))
        self.lay.addWidget(self.wdg)
        self.lay.setContentsMargins(0, 0, 0, 0)
        self.lay.setSpacing(0)
        self.setDefaultWidget(self.w)
        self.wdg.valueChanged.connect(self.new_factor)
        
    def new_factor(self, factor0):
        factor = factor0 / self.parent().zooming_factor / self.multiplier
        print('new_factor', factor0, factor)
        
        self.parent().scale(factor, factor)
        self.parent().unscale(factor)

    def showEvent(self, event):
        self.wdg.setValue(int(self.multiplier * self.parent().zooming_factor))


class ViewerPicture(QtWidgets.QGraphicsView):

    """Display widget for camera frames"""
    role = 'camera'
    sampleProcessor = False
    processor = False
    toolbar = False

    @property
    def inv(self):
        self._inv = should_camera_be_rotated(self.remote)
        return self._inv

    def __init__(self, remote, server, parent=None,
                 frameProcessor=False, sampleProcessor=False):
        QtWidgets.QGraphicsView.__init__(self, parent)
        self.windows = {}
        self.objects = {}
        self.conf_win = {}
        self.conf_act = {}
        self.motor_ctrl = {}
        self._res = {'x':-1, 'y':-1}
        self._inv = 0
        self.samples = []
        self.parent = parent
        self.setBackgroundBrush(QtGui.QBrush(QtCore.Qt.red))
        self.remote = remote
        self.server = server
        self.fullpath = self.remote['fullpath']
        self.setWindowTitle('Camera Viewer: %s' % remote['name'])

        # Setup the graphical scene
        self.gscene = QtWidgets.QGraphicsScene(self)
        self.plane = SensorPlane(remote)
        self.gscene.addItem(self.plane)
        self.plane.setVisible(True)
        self.plane.setPos(0, 0)

        self.setScene(self.gscene)

        if self.inv != 0:
            self.rotate(self.inv * 90.)

        self.saveDir = False
        self.saveN = 0

        self.calibrationTool = False
        self.menu = QtWidgets.QMenu(self)
        self.addActions()
        self.menu.aboutToShow.connect(self.updateActions)

        self.setFrameProcessor(frameProcessor)
        self.setSampleProcessor(sampleProcessor)
        self.reconnectSample()
        
    def set_toolbar(self, toolbar):
        self.toolbar = toolbar
        if toolbar:
            self.add_base_actions(toolbar)
            self.add_analysis_actions(toolbar)
        
    def check_samples(self):
        n = self.remote['nSamples']
        logging.debug('check_samples', n, len(self.samples))
        if n == len(self.samples):
            logging.debug('no sample change')
        else:
            self.toggle(False)
            self.toggle(True)

    def close(self):
        logging.debug('Closing ViewerPicture')
        if self.processor:
            logging.debug('Closing FrameProcessor')
            logging.debug('quitting FrameProcessor')
            self.processor.toggle_run(False)
        if self.sampleProcessor:
            logging.debug('Closing SampleProcessor')
            self.sampleProcessor.toggle_run(False)
        if self.calibrationTool:
            self.calibrationTool.close()

    _oldproc = None

    def setFrameProcessor(self, fp=False):
        """Create or set the thread receiving frames from the remote camera"""
        if not fp:
            fp = FrameProcessor(self.remote, self)
        if self.processor is not False:
            self.processor.toggle_run(False)
            self.processor.deleteLater()
            self._oldproc = self.processor
        self.processor = fp
        uconnect(self.processor.readyImage, self.updateImage, QtCore.Qt.BlockingQueuedConnection)
        self.processor.crop.connect(self.set_crop)
        self.processor.finished.connect(self.check_samples)
        self.processor.finished.connect(self.update_cursor)
        self.processor.started.connect(self.update_cursor)
        # self.termProcessor = lambda * foo: self.processor.toggle_run(False)
        # self.destroyed.connect(self.termProcessor)

    def set_crop(self, x, y, w, h):
        """Broadcast new image crop value to all overlays. It is used as the region of interest."""
        self.plane.set_crop(x, y, w, h)
        g = {'x': w, 'y': h}
        if self._inv:
            g = {'y': w, 'x': h}
        # Update motor control stepping
        for c in ('x', 'y'):
            # skip if equal to cached resolution
            if g[c] == self._res[c]:
                continue
            # skip if no motor control defined
            m = self.motor_ctrl.get(c, False)
            if not m:
                continue
            
            motor, align = motor_align_factor(self.remote, c)
            ps = abs(int(1. * g[c] / align))  # page step
            ps = max(ps, 50)  # at least 50 steps
            s = int(0.2 * ps)  # single step
            s = max(s, 10)  # at least 10 steps
            # Invert controls and appearance?
            orient = m.slider.orientation()
            # invc = align<0
            invc = (align > 0 and c == 'x') or (align < 0 and c == 'y')
            logging.debug('############ INVERSION?', m.prop['kid'], invc)
            # Skip if no change
            if ps != m.slider.pageStep() or s != m.slider.singleStep() or invc != m.slider.invertedControls():
                m.slider.setPageStep(ps)
                m.slider.setSingleStep(s)
                m.remObj.setattr(m.handle, 'step', s)
                m.set_inverted_slider(invc)
        self._res = g  # cache resolution
        # Update all overlays
        for smp in self.samples:
            smp.update({'crop': [x, y, w, h]})

    def setSampleProcessor(self, rp=False):
        """Create or set the thread receiving sample information from the remote camera"""
        if not rp:
            rp = SampleProcessor(parent=self)
        if self.sampleProcessor is not False:
            self.sampleProcessor.toggle_run(False)
            del self.sampleProcessor
        self.sampleProcessor = rp

    def unscale(self, factor):
        """Keep dimension of UI elements while zooming in/out"""
        self.plane.unscale(factor)
        for smp in self.samples:
            smp.unscale(factor)
        r = self.plane.boundingRect() & self.plane.box.boundingRect(
        ) & self.plane.cropBox.boundingRect()
        r.adjust(-50, -50, 50, 50)
        self.scene().setSceneRect(r)
        self.plane.pt.ensureVisible()
        
        self.processor.scale = self.zooming_factor

    def wheelEvent(self, event):
        """Zoom in/out, center the scene and unscale all UI elements."""
        self.setTransformationAnchor(QtWidgets.QGraphicsView.NoAnchor)
        self.setResizeAnchor(QtWidgets.QGraphicsView.NoAnchor)
        target_viewport_pos = self.mapToScene(event.pos())
        self.translate(target_viewport_pos.x(), target_viewport_pos.y())
        factor = 1 + event.angleDelta().y() / 2000.0
        self.scale(factor, factor)
        self.unscale(factor)
        self.translate(-target_viewport_pos.x(), -target_viewport_pos.y())
        
    def add_zoom_action(self, menu):
        # Zoom
        act = Zoomer(self)
        menu.addAction(act)
        
    @property
    def zooming_factor(self):
        return self.transform().m11() or 1

    def unzoom(self):
        """Remove user zoom"""
        # Remove current zooming factor
        self.unscale(1. / self.zooming_factor)
        self.fitInView(self.plane.boundingRect(), QtCore.Qt.KeepAspectRatio)

    def bool_action(self, name, prop, obj=False, menu=False, icon=False):
        """Create a menu action representing a remote obj true/false option"""
        if obj is False:
            obj = self.remote
        if menu is False:
            menu = self.menu
        if isinstance(prop, str):
            prop = obj.gete(prop)
        if prop is None:
            logging.debug('bool_action: Sample not found!', name, prop)
            return False
        handle = prop['handle']
        fp = obj['fullpath']
        act = widgets.aBooleanAction(obj, prop, menu)
        if icon:
            act.setIcon(icon)
        act.setText(name)
        menu.addAction(act)
        return act

    def button_action(self, name, label=False, obj=False, menu=False):
        if obj is False:
            obj = self.remote
        if menu is False:
            menu = self.menu
        fp = obj['fullpath']
        f = partial(obj.get, name)
        if not label:
            label = name.capitalize()
        act = menu.addAction(label, f)

    def updateActions(self):
        """Set actions status."""
        self.menu.streamAct.setChecked(self.processor.isRunning())
        if self.calibrationTool:
            self.calAct.setChecked(self.calibrationTool.isVisible())
        else:
            self.calAct.setChecked(False)
        for fp, win in self.conf_win.items():
            act = self.conf_act[fp]
            act.setChecked(2 * win.isVisible())
            logging.debug('updateActions', fp, act.isChecked())
        notRunning = not self.server['isRunning']
        self.menu.restartAct.setEnabled(notRunning)
        self.menu.analysisAct.setEnabled(notRunning)

    def configure_object(self, obj):
        """Show object configuration window"""
        fp = obj['fullpath']
        logging.debug('configure object', fp)
        act = self.conf_act.get(fp, False)
        if act is False:
            logging.debug(
                'configure_object: Object path not found', fp)
            return
        win = self.conf_win.get(fp, False)
        if win is False:
            win = conf.TreePanel(obj, select=obj)
            win.setWindowTitle('Configuration tree from: %s' % obj['name'])
            self.conf_win[fp] = win
        if act.isChecked() or not win.isVisible():
            logging.debug('SHOWING', fp)
            win.show()
            win.activateWindow()
            win.raise_()
        else:
            logging.debug('HIDING', fp)
            win.hide()

    def add_conf_action(self, menu, obj, fp=False, txt=False):
        """Create a "Configure" action in menu and return it."""
        if not fp:
            fp = obj['fullpath']
        if txt is False:
            txt = 'Configure'
        act = menu.addAction(theme_icon('details'),
            _(txt), partial(self.configure_object, obj))
        act.setCheckable(True)
        self.conf_act[fp] = act
        return act
    
    def add_analysis_actions(self, amenu):
        amenu.aboutToShow.connect(self.update_amenu)
        
        # amenu.hovered.connect(self.update_amenu)
        # General view entries
        amenu.roiAct = amenu.addAction(theme_icon('cam_view_regions'), _('View Regions'),
                                           partial(self.over_by_name,
                                                             'roi', amenu))
        amenu.roiAct.setCheckable(True)
        
        amenu.gridAct = amenu.addAction(theme_icon('cam_grids'), _('Display Grids'), partial(self.display_grids, amenu))        
        amenu.gridAct.setCheckable(True)

        roiResetAct = amenu.addAction(theme_icon('cam_reset_regions'), _('Reset Regions'), self.reset_regions)
        roiResetAct.setCheckable(False)
        
        self.analysis_actions = []

        amenu.profileAct = amenu.addAction(theme_icon('cam_profile'), _('Profile'),
                                               partial(self.over_by_name,
                                                                 'profile', amenu))
        amenu.profileAct.setCheckable(True)
        self.analysis_actions.append(amenu.profileAct)

        amenu.labelAct = amenu.addAction(theme_icon('cam_values'), _('Values Label'),
                                             partial(self.over_by_name,
                                                               'label', amenu))
        amenu.labelAct.setCheckable(True)
        self.analysis_actions.append(amenu.labelAct)

        # Shape entries
        amenu.pointsAct = amenu.addAction(_('Points'),
                                              partial(self.over_by_name,
                                                                'points', amenu))
        amenu.pointsAct.setCheckable(True)
        self.analysis_actions.append(amenu.pointsAct)
        
        is_hsm = '/hsm/' in self.remote['smp0'][0]
        if is_hsm:
            amenu.baseHeightAct = amenu.addAction(_('Base and Height'),
                                                      partial(self.over_by_name,
                                                                        'baseHeight', amenu))
            amenu.baseHeightAct.setCheckable(True)
            self.analysis_actions.append(amenu.baseHeightAct)

            amenu.circleAct = amenu.addAction(_('Circle Fitting'),
                                                  partial(self.over_by_name,
                                                                    'circle', amenu))
            amenu.circleAct.setCheckable(True)
            self.analysis_actions.append(amenu.circleAct)

    def add_base_actions(self, menu):
        menu.streamAct = menu.addAction(theme_icon('cam_stream'), _('Stream'), self.toggle)
        menu.streamAct.setCheckable(True)
        
        menu.autoresAct = menu.addAction(theme_icon('cam_fit'), _('Fit view'), self.unzoom)
        if 'Analysis_blackwhite' in self.remote:
            self.bool_action(_('Black/White'), 'Analysis_blackwhite', menu=menu, icon=theme_icon('cam_black_white'))
        # Camera configuration option
        self.add_conf_action(menu, self.remote, self.fullpath)
        menu.analysisAct = self.bool_action(_('Analysis'), 'Analysis_Simulation', menu=menu, icon=theme_icon('cam_analysis'))
        menu.restartAct = menu.addAction(theme_icon('exit'), _('Force restart'), self.restart)

    def addActions(self):
        """Add actions to right-click context menu."""
        menu = self.menu
        menu.setTitle('Cam %s /dev/%s' % 
                           (self.remote['name'], self.remote['dev']))
        
        self.add_base_actions(menu)
        
        # Per-Sample Menu
        self.menus = {}

        # Controls Menu
        self.imenu = menu.addMenu(_('Imaging'))
        self.add_imaging_actions(self.imenu)

        #########
        # Analysis menu
        #########
        self.amenu = menu.addMenu(QtGui.QIcon(os.path.join(parameters.pathArt, 'overlay.svg')),
                                       _('Overlay'))
        
        self.add_analysis_actions(self.amenu)
        
        # Border entries
        # TODO: Border overlays

        # Pixel Calibration
        self.calAct = self.amenu.addAction(
            'Pixel Calibration', self.calibration)
        self.calAct.setCheckable(True)
        
        # Remove tool action if modification is not allowed
        if not self.remote.check_write('Analysis_umpx'):
            self.amenu.removeAction(self.calAct)
        #########
        # Motion menu
        #########
        self.mmenu = menu.addMenu(_('Motion'))
        self.add_motion_actions(self.mmenu)
        # Other stuff
        menu.addAction('Save frame', self.save_frame)
        
    def update_amenu(self):
        """Enable analysis menu actions only if an analysis is really running"""
        en = self.remote['Analysis_Simulation']
        for act in self.analysis_actions:
            # Ignore already checked entries
            if act.isChecked():
                continue
            act.setEnabled(en)

    def reset_regions(self):
        """Re-init samples, resetting regions of interest."""
        r = self.remote.init_samples()
        logging.debug('reset_regions:', r)

    def hide_x_controls(self):
        r = self.server['lastInstrument'] in ('flex', 'flex3')
        if not r:
            return r
        if self.server._readLevel >= 4:
            return False
        return True

    def add_motion_actions(self, menu):
        """Create menu actions for motion control"""
        cpos = {'x': 'bottom', 'y': 'left'}
        self.motor_ctrl = {}

        def add_coord(name):
            # TODO: replace with enc.role2dev('motor')
            obj, align = motor_align_factor(self.remote, name)
            if obj in [False, None]:
                logging.debug('Cannot find motor for coord', name)
                return False
            submenu = menu.addMenu(_(name.capitalize()))
            act = widgets.MotorSliderAction(self.server, obj, submenu)
            submenu.addAction(act)
            invc = (align > 0 and name == 'x') or (align < 0 and name == 'y')
            if name == 'y' or (name == 'x' and not self.hide_x_controls()):
                slider = widgets.MotorSlider(
                    self.server, obj, parent=self.parent)
                slider.layout().removeWidget(slider.spinbox)
                p = cpos[name]
                inv = invc
                if self.remote.get('motor_switch', False):
                    v = list(cpos.values())
                    v.remove(p)
                    p = v[0]
                    inv = not inv
                self.parent.setControl(slider, p, inversion=inv)
                self.motor_ctrl[name] = slider

            act = widgets.aBooleanAction(
                obj, obj.gete('moving'), parent=submenu)
            submenu.addAction(act)
            self.button_action(
                'findLow', label=_('Find zero'), obj=obj, menu=submenu)
            self.button_action(
                'zero', label=_('Set zero position'), obj=obj, menu=submenu)
            self.add_conf_action(submenu, obj, obj['fullpath'], txt='Configure motor')
        
        encoders = [('x', 'x'), ('y', 'y'), ('z', 'z')]
        if self.remote.has_child('encoder'):
            encoders = self.remote.encoder.list()
        for label, name in encoders:
            add_coord(name)
        
    def keyPressEvent(self, e):
        """Transmit arrow presses to the controls"""
        if e.modifiers():
            return QtWidgets.QWidget.keyPressEvent(self, e)

        def increment(c, s):
            if not c:
                return
            slider = c.slider
            d = s * slider.singleStep()
            if slider.invertedControls():
                d *= -1
            slider.setValue(slider.value() + d)

        # Left/right bottom control slider
        c = self.parent.controls['bottom']
        if e.key() == QtCore.Qt.Key_Left:
            increment(c, -1)
            return
        if e.key() == QtCore.Qt.Key_Right:
            increment(c, +1)
            return
        # Up/down left control slider
        c = self.parent.controls['left']
        if e.key() == QtCore.Qt.Key_Down:
            increment(c, -1)
            return
        if e.key() == QtCore.Qt.Key_Up:
            increment(c, +1)
            return
        return QtWidgets.QWidget.keyPressEvent(self, e)
    
    def add_imaging_actions(self, menu):
        """Create menu actions for imaging controls"""
        for h in ['exposureTime', 'exposureAbsolute', 'exposureTimeAbsolute',
                  'contrast', 'brightness', 'gamma', 'gain', 'saturation', 'hue']:
            if h not in self.remote:
                continue
            act = widgets.aNumberAction(
                self.server, self.remote, self.remote.gete(h), parent=menu)
            menu.addAction(act)
        
        self.add_zoom_action(menu)

    def over_by_name(self, name, obj=None):
        """Show or hide overlay by name"""
        print('OVER_BY_NAME', name, self.samples)
        obj = obj or self
        act = getattr(obj, name + 'Act')
        opt = set([])
        for smp in self.samples:
            overlay = getattr(smp, name, False)
            container = overlay
            if not container:
                logging.debug('No overlay named:', name)
            if act.isChecked():
                logging.debug('show', name)
                overlay.show()
                uconnect(self.sampleProcessor.sig_updated, self.updateSample)
            else:
                container.hide()
                udisconnect(self.sampleProcessor.sig_updated, self.updateSample)
            # Update active options list
            opt = opt.union(smp.opt)
        # Update sampleProcessor options:
        self.sampleProcessor.opt = opt
        if act.isChecked() and not self.sampleProcessor.isRunning():
            self.sampleProcessor.start()

    def calibration(self):
        """Show/hide the pixel calibration tool"""
        # TODO: coherent way to fire caltool in multicrop mode
        if self.calibrationTool:
            self.calibrationTool.close()
        self.calibrationTool = calibration.CalibrationTool(
            self.plane, self.remote)
        self.calibrationTool.show()

    def save_frame(self):
        """Save current frame for each sample"""
        if not self.saveDir:
            self.saveDir = QtWidgets.QFileDialog.getExistingDirectory(
                self, "Images destination folder", "/tmp")
        self.saveN += 1
        for i, smp in enumerate(self.samples):
            out = '{}/{}_smp{}_{}.jpg'.format(
                    self.saveDir, self.remote['name'], i, self.saveN)
            smp.pix.save(out, 'JPG', 25)

    def toggle(self, do=None):
        """Toggle data streams"""
        if not self.processor:
            logging.debug('No processor. Cannot start/stop.')
            return False
        if do == None:
            do = not self.processor.isRunning()
        
        self.processor.toggle_run(False)
        self.sampleProcessor.toggle_run(False)
        if do > 0:
            logging.debug('proc start')
            try:
                self.reconnectSample()
            except:
                pass
            self.processor.toggle_run(True)
            self.sampleProcessor.toggle_run(True)
        else:
            logging.debug('proc stop')
        
        self.update_cursor()
        
    def restart(self):
        logging.debug('Forcing camera process restart...')
        self.remote['running'] = False
        
    def mouseDoubleClickEvent(self, event): 
        self.toggle()
        self.update_cursor()
        
    def enterEvent(self, event):
        self.update_cursor()
        
    def update_cursor(self):
        if self.processor.stream:
            self.viewport().setCursor(QtCore.Qt.ArrowCursor)
            self.setBackgroundBrush(QtGui.QBrush(QtCore.Qt.lightGray))
        else:
            self.viewport().setCursor(QtCore.Qt.BusyCursor)
            self.setBackgroundBrush(QtGui.QBrush(QtCore.Qt.red))

    def contextMenuEvent(self, event):
        item = self.itemAt(event.pos())
        logging.debug('got item', item)
        if hasattr(item, 'build_menu'):
            self._menu = QtWidgets.QMenu(self)
            item.build_menu(self._menu)
            self._menu.popup(event.globalPos())
        else:
            self.menu.popup(event.globalPos())

    def updateSample(self, i, multiget):
        logging.debug('updateSample', i, list(multiget.keys()))
        smp = self.samples[i]
        r = smp.update(multiget)
        if smp.opt_changed:
            opt = set([])
            for smp in self.samples:
                opt = opt.union(smp.opt)
            self.sampleProcessor.opt = opt

    def updateImage(self, *args):
        self.updateFrame(*args, img=True)

    _unzoomed = False
    _nSamples_time = 0
    _nSamples = 0

    def updateFrame(self, i, x, y, w, h, data, img=False):
        """Called when a new frame is available from processor"""
        # Nota: non posso usare direttamente QPixmap.loadFromData per via di un
        # memory leak in Qt
        if img:
            qimg = data
        else:
            qimg = QtGui.QImage()
            qimg.loadFromData(data)
        n = len(self.samples)
        t = time()
        if t - self._nSamples_time > 5:
            self._nSamples = self.remote['nSamples']
            self._nSamples_time = t
        if n == 0 or i >= n or self._nSamples != n:
            print('Samples changed was', n, self._nSamples, i)
            try:
                self.reconnectSample()
            except:
                pass
            return
        smp = self.samples[i]
        if not smp.isVisible():
            smp.show()
        qimg = qimg.scaled(w, h)
        smp.pix = QtGui.QPixmap.fromImage(qimg)
        smp.pixItem.setPixmap(smp.pix)
        smp.update({'roi': [x, y, w, h]})
        if self._oldproc is None and not self._unzoomed:
            self.unzoom()
            self._unzoomed = True
            
    def display_grids(self, amenu, *foo):
        for smp_pix in self.samples:
            if amenu.gridAct.isChecked():
                if not smp_pix.roi.isVisible():
                    amenu.roiAct.setChecked(True)
                    self.over_by_name('roi', amenu)
                smp_pix.roi.grid.set_enabled(True)
            else:
                smp_pix.roi.grid.set_enabled(False)
    
    initializing = False

    @csutil.initializeme(attribute=True)
    def reconnectSample(self):
        """Remove and regenerate all samples."""
        self.sampleProcessor.toggle_run(False)
        stream = self.processor.stream
        self.processor.toggle_run(False)
        
        server = self.server.copy()
        remote = self.remote.copy()
        
        for smp_pix in self.samples:
            logging.debug('reconnectSample: closing', smp_pix)
            smp_pix.close()
            # self.gscene.removeItem(smp_pix)
        
        self.samples = []
        samples = []
        n = remote['nSamples']
        for i in range(n):
            logging.debug('RECONNECTING SAMPLES', i, n, remote['fullpath'])
            name = 'smp%i' % i
            if name not in remote:
                logging.debug('Sample not found', name)
                continue
            # Get the current analysis sample
            path = remote[name][0]
            if path in [None, 'None']:
                logging.debug('No sample path defined', path)
                continue
            sample = server.toPath(path)
            if sample is None:
                logging.debug('Sample path not found', path)
                continue
            fp = sample['fullpath']
            samples.append(sample)
            smp_pix = SamplePicture(self.plane, sample, i, remote['Analysis_umpx'])
            smp_pix.roi.get()
            # Show the roi only if option is checked
            if self.amenu.roiAct.isChecked():
                smp_pix.roi.show()
                smp_pix.roi.grid.set_enabled(self.amenu.gridAct.isChecked())
            self.samples.append(smp_pix)
            # Menu structure for samples
            m = self.menus.get(name, False)
            if m is False:
                m = self.menu.addMenu(_('Sample ') + str(i))
                self.menus[name] = m
            m.clear()
            self.add_conf_action(m, sample, fp)
            if sample.has_child('analyzer'):
                self.bool_action(
                    'Black/white levelling', 'blackWhite', sample.analyzer, m)
                self.bool_action(
                    'Adaptive Threshold', 'adaptiveThreshold', sample.analyzer, m)
                self.bool_action(
                    'Dynamic Regions', 'autoregion', sample.analyzer, m)
                if fp.startswith('/hsm/'):
                    self.bool_action('Surface Tension',
                                     'sessileEnable',
                                     sample.analyzer, m)

        self.sampleProcessor.set_samples(samples)
        print('RECONNECT SAMPLES', self.samples)
        

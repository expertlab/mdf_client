#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Graphical overlay for sample shape image analysis"""
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from .overlay import Overlay
from mdf_client.qt import QtWidgets


class SamplePoints(Overlay):

    def __init__(self, parentItem, indexes=['iA', 'iB', 'iC', 'iD'], coords=[['xmass', 'ymass']], Z=1):
        Overlay.__init__(self, parentItem, Z=Z)
        self.indexes = indexes
        self.coords = coords
        # Create Points attributes, iA, iB, etc
        for name in self.indexes:
            self.add_point(name)
        
        self.coords_opts = []
        self.coord_names = {}
        for c in coords:
            c = list(c)
            name = '_'.join(c)
            self.coords_opts += c
            self.coord_names[name] = c 
            self.add_point(name)
        self.opt = set(['profile', 'roi'] + indexes + self.coords_opts)
           
    def add_point(self, name):
        g = QtWidgets.QGraphicsEllipseItem(parent=self)
        g.setPen(self.pen)
        g.setZValue(self.Z)
        g.setPos(0, 0)
        setattr(self, name, g)

    def unscale(self, factor):
        Overlay.unscale(self, factor)
        for pt in self.indexes:
            getattr(self, pt).setPen(self.pen)
            
    def update_index(self, name, diameter, radius=0):
        sz, vx, vy = self.current['profile']
        nx, ny = len(vx), len(vy)
        rx, ry, rw, rh = self.current['roi']
        i = int(self.current.get(name, 0))
        if i >= nx or i >= ny:
            logging.debug('Invalid point', name, i, nx, ny)
            return False
        logging.debug(name, i)
        x = vx[i]  # -rx+cx
        y = vy[i]  # -ry+cy
        radius = radius or diameter / 2
        g = getattr(self, name)
        g.setRect(x - radius, y - radius, diameter, diameter)
        
    def update_coord(self, name, diameter, radius):
        rx, ry, rw, rh = self.current['roi']
        g = getattr(self, name)
        xname, yname = self.coord_names[name]
        x = self.current.get(xname, 0) + rx
        y = self.current.get(yname, 0) + ry
        radius = radius or diameter / 2
        g.setRect(x - radius, y - radius, diameter, diameter)
    
    def up(self):
        if self.moving:
            return False
        logging.debug('updating points', list(self.current.keys()))
        diameter = int(self.dim(factor=75, minimum=12)) + 10  # pt diameter
        radius = diameter / 2
        for name in self.indexes:
            self.update_index(name, diameter, radius)
        for name in self.coord_names:
            self.update_coord(name, diameter, radius)
        return True


class BaseHeight(Overlay):

    def __init__(self, parentItem, Z=1, umpx=1):
        Overlay.__init__(self, parentItem, Z=Z)
        self.opt = set(
            ['profile', 'iA', 'angle', 'w', 'h', 'xmass', 'ymass'])
        # Create base and height lines
        self.base = QtWidgets.QGraphicsLineItem(parent=self)
        self.base.setPen(self.pen)
        self.height = QtWidgets.QGraphicsLineItem(parent=self)
        self.height.setPen(self.pen)
        self.umpx = umpx
        
    def unscale(self, factor):
        Overlay.unscale(self, factor)
        self.base.setPen(self.pen)
        self.height.setPen(self.pen)     

    def up(self):
        if self.moving:
            return False
        self.setRotation(0)
        sz, x, y = self.current['profile']
        i = int(self.current.get('iA', 0))
        a = self.current.get('angle', 0)
        w = self.current.get('w', 0) / self.umpx
        h = self.current.get('h', 0) / self.umpx
        x = x[i]  # +rx-cx
        y = y[i]  # +ry-cy
        self.base.setLine(x, y, x + w, y)
        self.height.setLine(x, y, x, y - h)
        self.setTransformOriginPoint(x, y)
        self.setRotation(a)
        return True


class CircleFit(Overlay):

    def __init__(self, parentItem, Z=1, umpx=1):
        Overlay.__init__(self, parentItem, Z=Z)
        self.opt = set(['xmass', 'ymass', 'radius', 'roi'])
        self.circle = QtWidgets.QGraphicsEllipseItem(parent=self)
        self.circle.setPen(self.pen)
        self.circle.setPos(0, 0)
        self.umpx = umpx

    def up(self):
        if self.moving:
            return False
        rx, ry, rw, rh = self.current['roi']
        x = self.current['xmass'] + rx
        y = self.current['ymass'] + ry
        r = self.current['radius'] / self.umpx
        self.circle.setRect(x - r, y - r, 2 * r, 2 * r)
        return True

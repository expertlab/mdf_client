#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Graphical overlay for image analysis"""
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from .overlay import Overlay
from .hook import HookPoint, HookRect
from mdf_client.qt import QtWidgets, QtGui, QtCore
from functools import partial
from .grid import Grid


def sample_label_from_fullpath(fullpath, Z):
    # breaks a/b/sample0/.../ into a/b/sample and 0/.../
    if not fullpath:
        return '{}'.format(Z)
    n = fullpath.split('sample')[1]
    # breaks 0/... into 0, ..., ''
    n = n.split('/')
    # takes 0 and ..., for example 0/Right
    m = n[0]
    if len(n) > 1 and n[1]:
        m += '/' + n[1]
    if n[0] != 'Surface' and  int(n[0] or Z) == Z:
        return m
    return '{}/{}'.format(m, Z)


class BoxRegion(Overlay):

    """Region of Interest visualization"""

    def __init__(self, parentItem, sample=False, Z=1):
        Overlay.__init__(self, parentItem, Z)
        self.setParentItem(parentItem.parentItem())
        self.setFlag(QtWidgets.QGraphicsItem.ItemStacksBehindParent, False)
        self._opt = 'roi'
        self.remObj = sample
        self.moving = False
        self.pen.setStyle(QtCore.Qt.DotLine)
        self.current = {self._opt: [0, 0, 0, 0]}

        # Graphical elements:
        # ROI Box
        self.box = HookRect(parent=self)
        self.box.hookPress = self.blockUpdates
        self.box.hookRelease = self.setCurrent
        self.box.hook = self.move
        self.box.setPen(self.pen)
        self.box.setZValue(100 + self.Z)
        self.grid = Grid(self, self.Z)
        
        # Text label
        lbl = str(self.Z)
        if sample:
            lbl = sample_label_from_fullpath(sample['fullpath'], Z)
        self.label = QtWidgets.QGraphicsTextItem(lbl, parent=self)
        self.label.setDefaultTextColor(self.color)
        self.label.setFlag(
            QtWidgets.QGraphicsItem.ItemIgnoresTransformations, True)
        self.font = self.label.font()
        # Point handlers
        self.ptul = self.create_point(1)
        self.ptur = self.create_point(2)
        self.ptbr = self.create_point(3)
        self.ptbl = self.create_point(4)
        
    def create_point(self, Z):
        p = HookPoint(
            w=10, h=10, pen=self.pen, Z=self.Z + Z + 100, parent=self)
        p.setFlag(QtWidgets.QGraphicsItem.ItemIgnoresTransformations, True)
        p.setParentItem(self.parentItem())
        p.hook = partial(self.reDim, p)
        p.hookPress = self.blockUpdates
        p.hookRelease = self.setCurrent
        return p

    def cleanUp(self, *foo):
        s = self.parentItem().scene()
        lst = self.box, self.ptul, self.ptbr, self.grid
        for item in lst:
            s.removeItem(item)
            del item

    def unscale(self, factor):
        """Nullify the zooming factor on UI elements"""
        self.box.unscale(factor)

    @property
    def opt(self):
        return set([self._opt])

    @opt.setter
    def opt(self, val):
        if len(val) == 0:
            return
        self._opt = list(val)[0]

    def get(self):
        logging.debug('BoxRegion.get', self.remObj, self._opt)
        re = self.remObj.get(self._opt)
        self.current = {self._opt: re}
        return re

    def setCurrent(self):
        """Sets server-side option according to visible rectangle."""
        r = self.box.rect()
        r = [self.box.x() + r.x(), self.box.y() + r.y(), r.width(), r.height()]
        logging.debug('setting rect', r)
        self.remObj[self._opt] = r
        self.syncPoints()
        self.get()
        self.unblockUpdates()
        
    def sync_point(self, point, x0, y0):
        d = point.width() / self.zoom_factor
        d2 = d / 2  # pt radius
        point.setPos(x0 - d2, y0 - d2)

    def syncPoints(self):
        """Resize points on the rectangle"""
        r = self.box.rect()
        # upper
        self.sync_point(self.ptul, r.x(), r.y())
        self.sync_point(self.ptur, r.x() + r.width(), r.y())
        self.sync_point(self.ptbl, r.x(), r.y() + r.height())
        self.sync_point(self.ptbr, r.x() + r.width(), r.y() + r.height())
        
        self.grid.set_length()

    def up(self):
        """Read and redraw server values."""
        if self.moving:
            logging.debug('BoxRegion is moving')
            return
        if not self.isVisible():
            logging.debug('BoxRegion not visible')
            return False
        if not self.validate():
            logging.debug('BoxRegion not validated')
            return False
# 		r = self.get()
        r = self.current[self._opt]
        if r is None:
            logging.debug('Region UP error: no values', r)
        x, y, w, h = r
        self.box.setPos(0, 0)
        self.box.setRect(QtCore.QRectF(x, y, w, h))
        self.syncPoints()
        self.label.setPos(QtCore.QPointF(x, y))
# 		print 'updated', self, self.current['roi']
        return True

    def pos(self, point):
        d2 = point.width() / 2 / self.zoom_factor
        r = point.rect()
        return point.x() + r.x() + d2, point.y() + r.y() + d2

    def reDim(self, source):
        """Redraw rect so it corresponds to hook points positions."""
        x, y = self.pos(self.ptul)
        w, h = self.pos(self.ptbr)
        if source == self.ptur:
            w, y = self.pos(self.ptur)
        elif source == self.ptbl:
            x, h = self.pos(self.ptbl)
        self.box.setRect(QtCore.QRectF(x, y, w - x, h - y))
        self.grid.set_length()

    def move(self):
        """Moving the whole region"""
        r = self.box.rect()
        x, y = r.x() + self.box.x(), r.y() + self.box.y()
        w, h = r.width(), r.height()
        d = self.ptul.width()
        d2 = d / 2
        x0, x1 = x - d2, x + w - d2
        y0, y1 = y - d2, y + h - d2
        self.ptul.setPos(x0, y0)
        self.ptbr.setPos(x1, y1)
        self.ptur.setPos(x1, y0)
        self.ptbl.setPos(x0, y1)
        logging.debug('move:', x, y, w, h)
        self.grid.set_length()

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Graphical overlay for image analysis"""
from mdf_client.qt import QtWidgets, QtGui, QtCore

colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255),
          (0, 125, 125), (125, 125, 0), (125, 0, 125)] * 3
iflags = QtWidgets.QGraphicsItem.ItemIsSelectable | \
    QtWidgets.QGraphicsItem.ItemIsFocusable | QtWidgets.QGraphicsItem.ItemIsMovable


class HookItem(object):

    """A movable graphics point which calls hook functions each time it is moved on the scene"""
    hook = lambda foo: 0
    hookRelease = lambda foo: 0
    hookPress = lambda foo: 0
    pen_width = 3

    def __init__(self, pen=False, Z=100):
        self.setFlags(iflags)
        if pen == False:
            pen = QtGui.QPen(QtGui.QColor(*colors[0]))
            pen.setWidth(int(self.pen_width))
        self.setPen(pen)
        self.setZValue(Z)

    def mouseMoveEvent(self, event):
        """Each time the point is moved, calls the hook function."""
        self.hook()
        return super(HookItem, self).mouseMoveEvent(event)

    def mouseReleaseEvent(self, event):
        self.hookRelease()
        return super(HookItem, self).mouseReleaseEvent(event)

    def mousePressEvent(self, event):
        self.hook()
        self.hookPress()
        return super(HookItem, self).mousePressEvent(event)

    def width(self):
        return self.rect().width()

    def height(self):
        return self.rect().height()

    def unscale(self, factor):
        """Nullify zooming factor on Item's QPen"""
        self.pen_width = int(self.pen_width / factor)
        p = self.pen()
        p.setWidthF(max(3, self.pen_width))
        self.setPen(p)


class HookPoint(HookItem, QtWidgets.QGraphicsEllipseItem):

    """Elliptical HookItem object, with cross cursor"""

    def __init__(self, x=0, y=0, w=0, h=0, pen=False, Z=100, parent=None):
        QtWidgets.QGraphicsEllipseItem.__init__(self, x, y, w, h, parent)
        HookItem.__init__(self, pen=pen, Z=Z)


class HookRect(HookItem, QtWidgets.QGraphicsRectItem):

    """Rectangular HookItem object, with cross cursor"""

    def __init__(self, x=0, y=0, w=0, h=0, pen=False, Z=100, parent=None):
        QtWidgets.QGraphicsRectItem.__init__(self, x, y, w, h, parent)
        HookItem.__init__(self, pen=pen, Z=Z)

#!/usr/bin/python
# -*- coding: utf-8 -*-
from mdf_canon.logger import get_module_logging
from functools import partial
logging = get_module_logging(__name__)

from mdf_client.qt import QtWidgets, QtCore
from collections import defaultdict


class CameraToolbar(QtWidgets.QToolBar):
    aboutToShow = QtCore.pyqtSignal()
    hovered = QtCore.pyqtSignal()
    
    def __init__(self, *a, **k):
        super().__init__(*a, **k)
        self._functions = defaultdict(dict)
        self._actions = {}
        self.setWindowTitle('Camera')
        
    def enterEvent(self, e):
        self.aboutToShow.emit()
        self.hovered.emit()
        return super().enterEvent(e)
        
    def setTitle(self, *a, **k):
        return self.setWindowTitle(*a, **k)
        
    def addAction(self, *a):
        if len(a) == 3:
            icon, name, function = a
        elif len(a) == 1:  # aBooleanAction
            a = a[0]
            icon = a.icon()
            name = a.text()
            function = a.toggle
        else:
            logging.debug('CameraToolbar: no icon, discard action', a)
            return QtWidgets.QAction()
        key = repr(function)
        if name not in self._actions:
            act = super().addAction(icon, name, partial(self._call, name))
            self._actions[name] = act
        else:
            logging.debug('CameraToolbar: added function to action', a)
        self._functions[name][key] = function
        return self._actions[name]
        
    def _call(self, name):
        for caller, function in self._functions[name].items():
            print('Calling', name, caller, function)
            function()

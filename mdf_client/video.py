#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Video export tools"""
from mdf_canon.option import ConfigurationProxy
from . import conf
import platform
from mdf_client.qt import QtWidgets
from mdf_client import _
from mdf_canon.reference.video_export import export, skeleton
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
try:
    import cv2 as cv
except:
    logging.debug('OpenCV is not available. Video Export is disabled.')
    cv = False
    
if 'Linux' in platform.platform() and cv:
    default_fourcc = cv.VideoWriter_fourcc('M', '4', 'S', '2')
else:
    default_fourcc = -1

# TODO: profile reconstruction. Use cvpolyfill


class ProgressProxy(QtWidgets.QProgressBar):

    def setValue(self, *a, **k):
        QtWidgets.qApp.processEvents()
        return super().setValue(*a, **k)


class VideoExporter(QtWidgets.QDialog):

    def __init__(self, sh, src='/hsm/sample0', parent=None):
        QtWidgets.QDialog.__init__(self, parent=parent)
        self.setWindowTitle(_('Video Export Configuration'))
        self.lay = QtWidgets.QFormLayout()
        self.sh = sh
        src = src.split('/')
        exts = ('profile', 'frame', 'filteredProfile')
        ext = 0
        if src[-1] in exts:
            ext = exts.index(src.pop(-1))
        src = '/'.join(src)
        T = src + '/T'
        ropts = skeleton()
        ropts['src']['current'] = src
        ropts['T']['current'] = T
        if ext:
            ropts['ext']['current'] = ext
        if 'Linux' in platform.platform():
            ropts.pop('codec')
        self.cfg = ConfigurationProxy({'self': ropts})
        self.wg = conf.Interface(self.cfg, self.cfg, ropts)
        self.lay.addRow(self.wg)

        self.out = QtWidgets.QLineEdit(self)
        self.out.setText(sh.get_path() + '.avi')
        self.lbl_out = QtWidgets.QPushButton(_("Output file"))
        self.lbl_out.pressed.connect(self.change_output)
        self.lay.addRow(self.lbl_out, self.out)

        self.btn_ok = QtWidgets.QPushButton("Start")
        self.btn_ok.pressed.connect(self.export)
        self.btn_ko = QtWidgets.QPushButton("Cancel")
        self.btn_ko.pressed.connect(self.cancel)
        self.btn_ko.setEnabled(False)
        self.lay.addRow(self.btn_ko, self.btn_ok)
        self.prog = False
        self.setLayout(self.lay)

    def export(self):
        """Start export thread"""
        self.btn_ko.setEnabled(True)
        prog = ProgressProxy()
        self.lay.addRow(_('Rendering:'), prog)
        src = str(self.cfg['src'])
        ext = str(self.cfg['ext'])
        out = str(self.out.text())
        self.prog = prog
        export(self.sh, frame=[src + '/' + ext], T=self.cfg['T'],
               fourcc=self.cfg.get('frm', None),
               output=out, framerate=self.cfg['fps'], prog=prog,
               acquisition_start_temperature=self.cfg['startTemp'],
               Tstep=self.cfg['Tstep'],
               tstep=self.cfg['tstep'],
               centerx=self.cfg['centerx'],
               centery=self.cfg['centery'])
        self.done(0)

    def cancel(self):
        """Interrupt export thread"""
        logging.debug('Cancel clicked!', self.prog)
        self.btn_ko.setEnabled(False)
        if self.prog:
            self.prog.setValue(0)

    def change_output(self):
        new = QtWidgets.QFileDialog.getSaveFileName(
            self, _("Video output file"), filter=_("Video (*avi)"))
        if new and len(new):
            self.out.setText(new[0])

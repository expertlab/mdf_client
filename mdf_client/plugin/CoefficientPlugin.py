#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Tools and plugins for Veusz, providing MDF Thermal Analysis functionality"""
import veusz.plugins as plugins
import veusz.document as document
import numpy as np
from .utils import smooth
from .utils import MisuraPluginDataset1D

from mdf_canon.logger import get_module_logging
logging = get_module_logging(__file__)


class CoefficientPlugin(plugins.DatasetPlugin):

    """Calculate a coefficient"""
    # tuple of strings to build position on menu
    menu = ('MDF', 'Coefficient')
    # internal name for reusing plugin later
    name = 'Coefficient'
    # string which appears in status bar
    description_short = 'Calculate coefficient'

    # string goes in dialog box
    description_full = (
        'Calculate a coefficient between a fixed start value and any subsequent value in a curve')

    def __init__(self, ds_x='', ds_y='', start=50., percent=0., factor=1.,
                 reconfigure='Stop', smooth=5, smode='Output', linearize=50, ds_out='coeff'):
        """Define input fields for plugin."""
        self.fields = [
            plugins.FieldDataset('ds_x', 'X Dataset', default=ds_x),
            plugins.FieldDataset('ds_y', 'Y Dataset', default=ds_y),
            plugins.FieldFloat('start', 'Starting X value', default=start),
            plugins.FieldFloat(
                'percent', descr='Initial dimension', default=percent),
            plugins.FieldFloat(
                'factor', descr='Divide by', default=factor),
            plugins.FieldCombo('reconfigure', descr='When cooling is found', items=[
                               'Continue', 'Restart', 'Stop', 'Reverse'], default=reconfigure),
            plugins.FieldInt('smooth', 'Smoothing Window', default=int(smooth)),
            plugins.FieldCombo('smode', descr='Apply Smoothing to', items=[
                               'X and Y', 'Y alone', 'Output'], default=smode),
            plugins.FieldFloat('linearize', 'Linearization window (X units)', default=float(linearize)),
            plugins.FieldDataset(
                'ds_out', 'New output dataset name', default=ds_out),
        ]

    def getDatasets(self, fields):
        """Returns single output dataset (self.ds_out).
        This method should return a list of Dataset objects, which can include
        Dataset1D, Dataset2D and DatasetText
        """
        # raise DatasetPluginException if there are errors
        if fields['ds_x'] == fields['ds_y']:
            raise plugins.DatasetPluginException(
                'X and Y datasets cannot be the same')
        if fields['ds_out'] in (fields['ds_x'], fields['ds_y'], ''):
            raise plugins.DatasetPluginException(
                'Input and output datasets cannot be the same.')
        # make a new dataset with name in fields['ds_out']
        # self.ds_out = plugins.Dataset1D(fields['ds_out'])
        self.ds_out = MisuraPluginDataset1D(fields['ds_out'])
        self.ds_out.unit = '-'
        self.ds_out.old_unit = '-'
        # return list of datasets
        return [self.ds_out]

    def updateDatasets(self, fields, helper):
        """Calculate coefficient
        """
        w_smooth = fields['smooth']
        recon = fields['reconfigure']
        smode = fields['smode']
        start = fields['start']
        initial_dimension = fields['percent']

        xds = helper._doc.data[fields['ds_x']]
        yds = helper.getDataset(fields['ds_y'])
        _yds = helper._doc.data.get(fields['ds_y'])
        x = xds.data
        y = yds.data
        
        m_percent = getattr(_yds, 'm_percent', None)
        # Remove/guess initial_dimension if an m_percent flag exists
        if m_percent is False and initial_dimension <= 0:
            initial_dimension = getattr(_yds, 'm_initialDimension', initial_dimension)
        elif m_percent is True:
            initial_dimension = 0
        i = np.where(x > start)[0][0]
        
        # Define the end of calc
        j = np.where(x == x.max())[0][0]
        max_index = j
        # If no initial dimension and no % units,
        # Calculate an ad-hoc initial dimension
        if m_percent is not True and initial_dimension <= 0:
            m_percent = False  # avoid None
            di = i - 10 if i > 10 else i
            dj = i + 10 if j - i < 10 else j
            initial_dimension = y[di:dj].mean()
        
        # Smooth input curves
        if w_smooth > 0:
            if smode != 'Output':
                y[i:j] = smooth(y[i:j], w_smooth, 'hanning')
            if smode == 'X and Y':
                x[i:j] = smooth(x[i:j], w_smooth, 'hanning')

        xstart = start
        d = 15
        pre = max((0, i - d))
        post = min(i + d, len(y) - 1)
        ystart = y[pre:post].mean() or 1

        out = calculate_coefficient(x, y, xstart, ystart, initial_dimension,
                                    getattr(_yds, 'm_percent', False))
        linearize = fields.get('linearize', 0)
        out = linearization(out, x, linearize, end=max_index)
        out[:i + d] = np.nan
        out[x < start] = np.nan
        # TODO: multiple ramps
        # Detect the maximum temperature
        # and start a new coefficient point
        cut = None
        
        if recon == 'Stop':
            dT = np.concatenate(([1], np.diff(smooth(x, w_smooth, 'hanning'))))
            out[dT < 0] = np.nan
        elif recon == 'Continue': 
            print('AAAAAAAA')
            out = linearization(out, x, linearize, cooling=True, start=max_index)
            print('BBBBBBB')
        elif recon in ('Restart', 'Reverse'):
            
            x1, y1 = x[max_index:], y[max_index:]
            cooling = True
            
            if recon == 'Reverse':
                x1 = x1[:cut:-1]
                y1 = y1[:cut:-1]
                # Re cut
                if start > x1[0]:
                    xstart = start
                    cut = np.where(x < start)[0][0]
                    x1 = x1[cut:]
                    y1 = y1[cut:]
                
                cooling = False
                        
            xstart = x1[0]
            ystart = y1[0]    
            print('RECONFIG', x1[0], y1[0], xstart, ystart, 'cut', cut)
                
            cool = calculate_coefficient(x1, y1, xstart, ystart,
                                                         initial_dimension,
                                                         getattr(_yds, 'm_percent', False))
            cool = linearization(cool, x1, linearize, cooling=cooling)
            # Erase any subsequent heat/cool ramp
            # res[x1 > x[max_index] - 1] = np.nan
            if w_smooth > 0 and smode == 'Output':
                cool = smooth(cool, w_smooth, 'hanning')
            
            if recon == 'Reverse':
                cool = cool[::-1]
                    
            out[max_index:cut] = cool
            if cut:
                out[-cut:] = np.nan
        # Apply factor
        out /= fields.get('factor', 1.)
        # Smooth output curve
        if w_smooth > 0 and smode == 'Output':
            out[i:j] = smooth(out[i:j], w_smooth, 'hanning')
        self.ds_out.update(data=out)
        u = xds.attr.get('unit', False)
        u = '-' if not u else '1/' + u
        self.ds_out.unit = u
        self.ds_out.old_unit = u
        return [self.ds_out]


def calculate_coefficient(x_dataset, y_dataset, x_start, y_start,
                          initial_dimension, is_percent):
    denominator = initial_dimension or 100.
    if not initial_dimension:
        is_percent = True
    if not is_percent:
        denominator = (initial_dimension + y_start)
    out = (y_dataset - y_start) / (x_dataset - x_start) / denominator
    return out

    
def linearization(y, x, linearize=0, cooling=False, start=None, end=None):
    select = slice(start, end)
    x_dataset = x[select]
    out = y[select]
    N = len(out) - 1
    if not linearize or N <= 10:
        return y
    
    # linearization in x units
    endfunc = (max, min)[cooling]
    linearize *= (1, -1)[cooling]
     
    cut = np.where(x_dataset == endfunc(x_dataset))[0][0]
    if cut == 0:
        print('No linearization possible')
        return y
    dx = abs(x_dataset[:cut] - x_dataset[0] - linearize)
    start = np.where(dx == min(dx))[0][0]
    if start > N // 8:
        start = N // 8
    if start < 10:
        logging.info('cannot linearize with only points', start)
        return y
    end = 2 * start
    logging.debug('linearize', linearize, x_dataset[0], start, end, N)
    post = out[start:end]
    factors, res, rank, sing, rcond = np.polyfit(np.arange(len(post)) + start, post, deg=1, full=True)
    func = np.poly1d(factors)
    pre = func(np.arange(start))
    out[:start] = pre
    y[select] = out
    return y


# add plugin classes to this list to get used
plugins.datasetpluginregistry.append(CoefficientPlugin)

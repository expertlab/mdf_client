#!/usr/bin/python
# -*- coding: utf-8 -*-

import veusz.document as document
from veusz.widgets import Line


class DataPointLine(Line):
    typename = 'datapointline'
    description = "Line for datapoints"

    @classmethod
    def allowedParentTypes(klass):
        """Get types of widgets this can be a child of."""
        from mdf_client.plugin.datapoint import DataPoint
        return (DataPoint,)
    
    def updateControlItem(self, cgi, *a, **k):
        """If control items are moved, update line."""
        # First do any changes to the datapoint.
        s = self.settings
        old = (s.xPos[0], s.xPos[0])
        old2 = (s.xPos2[0], s.xPos2[0])        
        super().updateControlItem(cgi, *a, **k)
        
        if (s.xPos[0], s.xPos[0]) != old:
            self.parent.dict_toset(self.parent, {'xPos': s.xPos[0],
                                                 'yPos': s.yPos[0]})
            self.parent.apply_ops('DataPoint: Label line moved the datapoint')
            self.parent.actionUp()
        if (s.xPos2[0], s.xPos2[0]) != old2:
            label = self.parent.settings.get('coordLabel').findWidget()
            if label:
                label.user_positioned = True
                self.parent.dict_toset(label, {'xPos': s.xPos2[0],
                                               'yPos': s.yPos2[0]})
                self.parent.apply_ops('DataPoint: Label line moving the label')
                self.parent.up_labelLine()
                self.parent.apply_ops('DataPoint: Label line moved the label')


document.thefactory.register(DataPointLine)

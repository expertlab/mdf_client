#!/usr/bin/python
# -*- coding: utf-8 -*-
import textwrap
import os
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
import veusz.document as document
from veusz.widgets.textlabel import TextLabel
from veusz import widgets
import veusz.setting as setting
from ..clientconf import confdb
from ..iutils import getUsedPrefixes
from . import utils

def calculate_plot_title(doc, separator='|'):
    rt = confdb['rule_title']
    title = ''
    if not separator.startswith(' '):
        separator = ' '+separator
    if not separator.endswith(' '):
        separator += ' '
    for linked in getUsedPrefixes(doc).values():
        if hasattr(linked,'get_title'):
            t = linked.get_title(comment=rt=='Comment')
        else:
            t = os.path.basename(linked.filename)
        t = t.replace('_',r'\_').replace('^',r'\^')
        if title:
            title += separator
        title += t
    return title

class LinkedFilesLabel(utils.OperationWrapper, TextLabel):
    typename = 'linkedfileslabel'
    description = "Label listing all linked files"
    
    def __init__(self, *args, **kwargs):
        TextLabel.__init__(self, *args, **kwargs)
            
        self.addAction(widgets.widget.Action('up', self.actionUp,
                                        descr='Update Title',
                                        usertext='Update Title'))
        
    
    @classmethod
    def addSettings(klass, s):
        """Construct list of settings."""
        widgets.TextLabel.addSettings(s)
        s.add(setting.BoolSwitch('auto', confdb['rule_title']!='None',
                                 descr='Automatic title',
                                 usertext='Automatic title',
                                 settingsfalse=['labelText']),
            1)
        s.add(setting.Int(
            'length', 65,
            descr='Max length',
            usertext='Newline after this length'),
            2)
        
        s.add(setting.Str(
            'separator', '|',
            descr='Separator',
            usertext='Separator',
            formatting=True),
            2)
    
    def actionUp(self):
        if not self.settings['auto']:
            logging.debug('Automatic plot title is disabled')
            return False
        label = calculate_plot_title(self.document, self.settings['separator'])
        if not label:
            logging.debug('Not concatenating empty title')
            return False
        label = r'\\'.join(textwrap.wrap(label, 
                                         self.settings['length'],
                                         replace_whitespace=False,
                                         drop_whitespace=False))
        self.toset(self, 'label', label)
        self.apply_ops('New title')
        logging.debug('New title:', label)
        return True
        
        
document.thefactory.register(LinkedFilesLabel)


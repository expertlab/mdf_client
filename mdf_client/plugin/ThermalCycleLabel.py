#!/usr/bin/python
# -*- coding: utf-8 -*-
import textwrap
import numpy as np

from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
import veusz.document as document
import veusz.setting as setting
from .OptionLabel import OptionLabel

def cycle_format(t, T):
    """Convert a t,T cycle to a T,rate,stasis list for display"""
    t = np.array(t)
    T = np.array(T)
    dt = np.diff(t)
    dT = np.diff(T)
    rates = dT / dt
    istasis = np.where(rates < 0.01)[0]
    stasis = np.zeros(len(rates))
    for i in istasis:
        stasis[i-1] = dt[i]
    mask = rates > 0.01
    T = T[1:]
    return [T[mask], rates[mask], stasis[mask]]

def create_cycle_text(tTc,  title='Thermal Cycle', header=True, units=True):
    t = []
    T = []
    for v in tTc:
        vt, vT = v
        if isinstance(vT, str):
            continue
        t.append(vt)
        T.append(vT)
    nT, rates, stasis = cycle_format(t, T)
    
    label1 = u'<math>\n'
    # Add title frac
    if title:
        label1 += u'<mfrac><mtext>{}</mtext>'.format(title)
    label1 +=u'<mtable columnlines="solid">\n'
    if header:
        label1 += u'<mtr> <mtd>T max</mtd> <mtd>Rate</mtd> <mtd>Stasis</mtd> </mtr>\n'
    if units:
        label1 += u'<mtr> <mtd>°C</mtd> <mtd>°C/m</mtd> <mtd>m</mtd> </mtr>\n'
    for i, iT in enumerate(nT):
        ir = rates[i]
        ist = stasis[i]
        iT = '%4.0f' % iT
        ir = '%3.1f' % (ir * 60)
        ist = '%3.1f' % (ist / 60)
        label1 += u'<mtr> <mtd>%-6s</mtd> <mtd>%-7s</mtd> <mtd>%-7s</mtd> </mtr>\n' % (iT, ir, ist)
    label1 += u'</mtable>\n'
    # Close title tag
    if title:
        label1 += u'</mfrac>\n'
    label1 += u'</math>'
    return label1
    
    
    
class ThermalCycleLabel(OptionLabel):
    typename = 'thermalcyclelabel'
    description = "Label for thermal cycle"
    
    def __init__(self, *args, **kwargs):
        super(ThermalCycleLabel, self).__init__(*args, **kwargs)
        for name in ('title', 'header', 'units','showTitle'):
            self.settings.get(name).setOnModified(self.update)
        
    
    @classmethod
    def addSettings(klass, s):
        """Construct list of settings."""
        OptionLabel.addSettings(s)
        s.showHidden=True  
                
        s.add(setting.BoolSwitch('showTitle', True,
                                 descr='Show title',
                                 usertext='Show title',
                                 settingstrue=['title']),
            1)
        s.add(setting.Str('title', '',
                                 descr='Test title',
                                 usertext='Test title'),
            2)

        s.add(setting.Bool('header', True,
                                 descr='Show column header',
                                 usertext='Show column header'),
            3)
        s.add(setting.Bool('units', True,
                                 descr='Show column units',
                                 usertext='Show column units'),
            4)
        
    
    def cvt_List(self, val, opt):
        title = False
        if self.settings['showTitle']:
            title = self.settings['title']
            if not title:
                title = self.linked.get_title()
        label = create_cycle_text(val,  title=title,
                                  header = self.settings['header'],
                                  units = self.settings['units'])
        return label
    
    cvt_Hidden = cvt_List
        
        
document.thefactory.register(ThermalCycleLabel)




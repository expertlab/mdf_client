#!/usr/bin/python
# -*- coding: utf-8 -*-
"""A point connected to an xy plot."""
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from mdf_client.qt import QtWidgets, QtGui
from mdf_client.qt import QtCore
from functools import partial
import veusz.utils
import veusz.widgets
import veusz.document as document
import veusz.setting as setting
from veusz.widgets.point import PointPlotter
from mdf_client.clientconf import confdb
import numpy as np
from scipy import interpolate
from . import utils
from mdf_canon import csutil
from mdf_client.plugin.RemoveGaps import remove_gaps_from
from copy import copy
from mdf_client import _
from mdf_client.plugin.DataPointLabel import DataPointLabel
from mdf_client.plugin.DataPointLine import DataPointLine


def searchWidgetName(widget, name):
    # get parent widget
    while not widget.iswidget and widget is not None:
        widget = widget.parent

    while widget is not None:
        for w in widget.children:
            if w.name == name:
                return w
        widget = widget.parent
    return None


def remove_out_of_bound_indexes(indexes, max_index):
    return [index for index in indexes if index < max_index]


def search_setfunction(val):
    if val == 'None':
        return ((), ('searchRange', 'critical_x'))
    return (('searchRange', 'critical_x'), ())


class DataPoint(utils.OperationWrapper, veusz.widgets.BoxShape):
    typename = 'datapoint'
    description = 'Data Point'
    allowusercreation = True
    point_index = 0
    xAx = None
    yAx = None
    x, y = None, None
    iswidget = True
    _updaters = ('search', 'searchRange', 'critical_x', 'showLabel', 'labelText',
                     'coordLabel', 'showTangent', 'showPerpendicular', 'showLabelLine', 'secondPoint', 'smooth')
    
    @classmethod
    def allowedParentTypes(klass):
        """Get types of widgets this can be a child of."""
        return (PointPlotter,)

    def __init__(self, parent, name=None):
        veusz.widgets.BoxShape.__init__(self, parent, name=name)
        
        self.addAction(veusz.widgets.widget.Action('up', self.actionUp,
                                                   descr='Update Data Point',
                                                   usertext='Update Data Point'))

        self.addAction(
            veusz.widgets.widget.Action('removeGaps', self.removeGaps,
                                        descr='Remove Gaps',
                                        usertext='Remove Gaps'))

        self.settings.Fill.add(setting.Bool('extended_clickable_area', True,
                descr=_('If enabled, datapoint is easier to select, but can create problems when printing.'),
                usertext=_('Extend clickable area')))
        self.updaters = {}

    def draw(self, posn, phelper, outerbounds=None):
        self.parent.getAxes = self.getAxes
        veusz.widgets.BoxShape.draw(self, posn, phelper, outerbounds)

        for c in self.children:
            c.draw(posn, phelper, outerbounds)

    def getAxes(self, *args, **kwargs):
        """Needed to allow children drawing"""
        graph_ancestor = utils.searchFirstOccurrence(self, "graph", -1)
        return graph_ancestor.getAxes(*args, **kwargs)
    
    def set_remove_gaps_threshold(self):
        gap_range = int(self.settings.setdict['remove_gaps_range'].val)
        start_index = int(round(self.point_index - gap_range / 2.0))
        end_index = start_index + gap_range
        data = self.parent.settings.get('yData').getFloatArray(self.document)
        if data is None or not len(data):
            return False
        y = np.copy(data)[start_index:end_index]
        if len(y) < 10:
            logging.error('Cannot calculate remove gaps threshold', len(y), start_index, end_index)
            return False
        x = np.arange(len(y))
        th = abs(y - interpolate.UnivariateSpline(x, y)(x)).sum() / len(x)
        if np.isnan(th):
            logging.error('Remove gaps threshold is nan')
            return False
        logging.debug('Setting remove gaps threshold to', th)
        self.settings.setdict['remove_gaps_thershold'].val = th
        return True

    def removeGaps(self):
        gap_range = int(self.settings.setdict['remove_gaps_range'].val)
        gaps_thershold = self.settings.setdict['remove_gaps_thershold'].val
        start_index = int(round(self.point_index - gap_range / 2.0))
        end_index = start_index + gap_range
        data = self.parent.settings.get('yData').getFloatArray(self.document)      
        dataset_name = self.parent.settings.get('yData').val
        dataset = copy(self.document.data[dataset_name])
        data_without_gap = remove_gaps_from(
            data, gaps_thershold, start_index, end_index)

        dataset.data = data_without_gap
        operation = document.OperationDatasetSet(dataset_name, dataset)

        self.ops.append(operation)
        self.up_coord(yData=data_without_gap)
        self.apply_ops('Remove Gap')

    @classmethod
    def addSettings(klass, s):
        """Construct list of settings."""
        veusz.widgets.BoxShape.addSettings(s)

        s.add(setting.Float(
            'remove_gaps_range', 100,
            descr="Remove gaps range",
            usertext="Remove gaps range"),
            1)

        s.add(setting.Float(
            'remove_gaps_thershold', 10,
            descr="Remove gaps thershold",
            usertext="Remove gaps thershold"),
            2
        )
        
        s.add(setting.ChoiceSwitch(
            'search', ['Nearest (Fixed X)', 'Nearest', 'Maximum', 'Minimum',
                       'Inflection', 'Stationary', 'None'], 'Nearest (Fixed X)',
            showfn=search_setfunction,
            descr='Search nearest critical point',
            usertext='Place nearest'),
            4)
        s.add(setting.Float(
            'searchRange', 10,
            descr='Critical search range',
            usertext='Search range'),
            5)

        s.add(setting.Dataset(
            'critical_x', '',
            descr='Critical search X dataset',
            usertext='Critical X'),
            6)
        
        s.add(setting.Int(
            'smooth', -1,
            descr='Implicit smoothing',
            usertext='Stationary/Inflection smoothing'),
            7)

        s.add(setting.WidgetChoice(
            'secondPoint', '',
            descr='Second Data Point for passing-through line placement.',
            widgettypes=('datapoint',),
            usertext='Second Data Point', formatting=True),
            8)
        s.add(setting.WidgetChoice(
            'pt2ptLine', '',
            descr='Dispose this line as passing through this and second data point.',
            widgettypes=('datapointline',),
            usertext='Passing-through Line', formatting=True),
            9)
        
        s.add(setting.Int(
            'pointIndex', 10,
            descr='Point index',
            usertext='Point index'),
            10)

        # OVERRIDES
        n = setting.Choice('positioning',
                           ['axes'], 'axes',
                           descr='Axes positioning',
                           usertext='Position mode',
                           formatting=False)
        n.parent = s
        s.setdict['positioning'] = n

        n = setting.DatasetExtended('width', [0.04],
                                    descr='List of fractional widths or dataset',
                                    usertext='Widths',
                                    formatting=False)
        n.parent = s
        s.setdict['width'] = n
        n = setting.DatasetExtended('height', [0.04],
                                    descr='List of fractional heights or dataset',
                                    usertext='Heights',
                                    formatting=False)
        n.parent = s
        s.setdict['height'] = n

        # FORMATTING
        s.add(setting.Marker('marker',
                             'linecross',
                             descr='Type of marker to plot',
                             usertext='Marker', formatting=True), 0)

        s.add(setting.BoolSwitch('showLabel', True,
                                 descr='Show Label',
                                 usertext='Show Label',
                                 settingstrue=['labelText', 'coordLabel', 'labelLine'],
                                 formatting=True),
              1)
        s.add(setting.Str(
            'labelText', r'%(ylabel)s=%(y).4g\\T=%(T).1f\\t=%(t)i',
            descr='Output label text',
            usertext='Output label text',
            formatting=True),
            2)
        
        s.add(setting.WidgetChoice(
            'coordLabel', '',
            descr='Write coordinates to this label.',
            widgettypes=('datapointlabel',),
            usertext='Coord. label',
            formatting=True),
            3)
        
        s.add(setting.BoolSwitch('showLabelLine', True,
                                 descr='Show Label Line',
                                 usertext='Show Label line',
                                 settingstrue=['labelLine'],
                                 formatting=True),
            4)
        
        s.add(setting.WidgetChoice(
            'labelLine', '',
            descr='Connect label and datapoint',
            widgettypes=('datapointline',),
            usertext='Label Line',
            formatting=True),
            5)
        
        s.add(setting.BoolSwitch(
            'showTangent', False,
            descr='Show Tangent',
            usertext='Show tangent line',
            settingstrue=['tangentLine'],
            formatting=True),
            6)
        
        s.add(setting.WidgetChoice(
            'tangentLine', '',
            descr='Dispose this line as tangent to curve xy.',
            widgettypes=('datapointline',),
            usertext='Tangent Line',
            formatting=True),
            7)

        s.add(setting.BoolSwitch('showPerpendicular', False,
                                 descr='Show Perpend.',
                                 usertext='Show perpendicular line',
                                 settingstrue=['perpendicularLine'],
                                 formatting=True),
            8)
        s.add(setting.WidgetChoice(
            'perpendicularLine', '',
            descr='Dispose this line as perpendicular to curve xy.',
            widgettypes=('datapointline',),
            usertext='Perpendicular Line',
            formatting=True),
            9)
        
        s.Fill.hide = False

    def drawShape(self, painter, rect):
        s = self.settings
        w, h = rect.width(), rect.height()
        veusz.utils.plotMarker(painter, rect.x() + w // 2, rect.y() + h // 2, s.marker, min(h, w) - 1)
        
        path = QtGui.QPainterPath()
        veusz.utils.brushExtFillPath(painter, s.Fill, path, stroke=painter.pen())

        if s.Fill.extended_clickable_area:
            brush = setting.collections.BrushExtended('tempbrush')
            brush.hide = False
            brush.transparency = 99
            brush.color = QtCore.Qt.darkGray
            path.addRect(rect)
            veusz.utils.brushExtFillPath(painter, brush, path)

    def check_axis(self):
        aligned = sum([self.toset(self, 'positioning', 'axes'),
                       self.eqset(self.parent, 'xAxis'),
                       self.eqset(self.parent, 'yAxis')]) == 3
        color = self.parent.settings.PlotLine.color
        logging.debug('check_axis', aligned, color)
        # Styling
        self.toset(self, 'Border/color', color)
        self.toset(self, 'Fill/color', color)
        return aligned
    
    def actionUp(self):
        self.addDefaultSubWidgets()
        return True
    
    def addDefaultSubWidgets(self):
        self.ops = []
        aligned = self.check_axis()
        if not aligned:
            logging.debug('Impossible to align DataPoint settings')
            return False
        
        self.dependencies()
        self.up_coord()
        self._tg_ang = None
        self.updateOutputLabel()
        self.apply_ops('DataPoint: Up')
        self.up_tangent()
        self.up_perpendicular()
        self.up_passing()
        self.up_labelLine()
        self.set_remove_gaps_threshold()
        self.apply_ops('DataPoint: Lines')
        return

    def dependencies(self):
        """Create/destroy dependent widgets"""
        s = self.settings
        # Tangent
        tg = s.get('tangentLine').findWidget()
        if tg is None:
            logging.debug('No tangent line specified')
            if s.showTangent:
                name = 'tg_' + self.name
                self.ops.append(
                    document.OperationWidgetAdd(self, 'datapointline', name=name))
                self.toset(self, 'tangentLine', name)
        # Destroy if not needed
        elif not s.showTangent:
            self.ops.append(document.OperationWidgetDelete(tg))
            
        # Perpendicular
        tg = s.get('perpendicularLine').findWidget()
        if tg is None:
            logging.debug('No pp line specified')
            if s.showPerpendicular:
                name = 'pp_' + self.name
                self.ops.append(
                    document.OperationWidgetAdd(self, 'datapointline', name=name))
                self.toset(self, 'perpendicularLine', name)
        # Destroy if not needed
        elif not s.showPerpendicular:
            self.ops.append(document.OperationWidgetDelete(tg))

        # PT2PT
        p2 = s.get('secondPoint').findWidget()
        if p2 == self: 
            p2 = None
        p2l = None
        if p2:
            p2l = s.get('pt2ptLine').findWidget()
        if p2 and (p2l is None):
            logging.debug('No p2p line specified')
            if p2 is not None:
                name = 'p2p_%s_%s' % (self.name, p2.name)
                self.ops.append(
                    document.OperationWidgetAdd(self, 'datapointline', name=name))
                self.toset(self, 'pt2ptLine', name)
        # Destroy if not needed
        elif (p2 is None) and (p2l is not None):
            self.ops.append(document.OperationWidgetDelete(p2l))

        # Label
        lbl = s.get('coordLabel').findWidget()
        if not lbl and s.showLabel:
            name = 'lbl_' + self.name
            logging.debug('Creating missing label', name)
            self.ops.append(
                document.OperationWidgetAdd(self, 'datapointlabel', name=name))
            self.toset(self, 'coordLabel', name)
        elif lbl and not s.showLabel:
            logging.debug('Deleting unneeded label', name, lbl)
            self.ops.append(document.OperationWidgetDelete(lbl))
        # Label line
        lbll = s.get('labelLine').findWidget()
        showLabelLine = s.showLabel and s.showLabelLine
        if not lbll and showLabelLine:
            logging.debug('Creating missing label line', name)
            name = 'lbln_' + self.name
            self.ops.append(
                    document.OperationWidgetAdd(self, 'datapointline', name=name))
            self.toset(self, 'labelLine', name)
        # Destroy if not needed
        elif lbll and not showLabelLine:
            logging.debug('Delete unneded label line', name, lbll)
            self.ops.append(document.OperationWidgetDelete(lbll))

        self.apply_ops('Datapoint: Dependencies')

    def distance(self, x, y, xData=None, yData=None, xRange=None, yRange=None):
        """Curve-point distance"""
        if xData is None:
            xData = self.xData
        if yData is None:
            yData = self.yData
        if xRange is None:
            xRange = self.xRange
        if yRange is None:
            yRange = self.yRange

        dst = ((xData - x) / xRange) ** 2 + ((yData - y) / yRange) ** 2
        i = np.where(dst == np.nanmin(dst))[0][0]
        return i, xData[i], yData[i]

    def distance_fixed_x(self, x, y, xData=None, yData=None, xRange=None, yRange=None):
        """Curve-point distance with fixed x"""
        if xData is None:
            xData = self.xData
        if yData is None:
            yData = self.yData
        if xRange is None:
            xRange = self.xRange
        if yRange is None:
            yRange = self.yRange
        y = np.nan_to_num(y)
        yData0 = yData[:]
        yData = np.nan_to_num(yData0)
        x_distances = np.abs(xData - x)
        sorted_by_x_distance_indexs = remove_out_of_bound_indexes(x_distances.argsort()[:10], len(yData))
        y_distances_of_nearest_points = np.abs(yData[sorted_by_x_distance_indexs] - y)
        index = sorted_by_x_distance_indexs[np.nanargmin(y_distances_of_nearest_points)]
        if np.isnan(yData0[index]):
            logging.debug('Selected point falls onto NaN', index, xData[index], yData[index])
            return self.distance(x, y, xData, yData0, xRange, yRange)
        return index, xData[index], yData[index]

    def up_coord(self, oldx=None, oldy=None, xData=None, yData=None):
        """Place in the nearest point to the current x,y coord"""
        d = self.document
        s = self.settings
        xSet = self.parent.settings.get('xData')
        ySet = self.parent.settings.get('yData')

        if (xData is None):
            xData = xSet.getFloatArray(d)
        if (yData is None):
            yData = ySet.getFloatArray(d)
        if xData is None or yData is None:
            logging.error('DataPoint without data', xData, yData)
            return False
        N = len(xData)

        # Compute visible ranges
        xAx = searchWidgetName(s.parent, s.xAxis)
        yAx = searchWidgetName(s.parent, s.yAxis)
        if None in [xAx, yAx]:
            logging.error('No visible ranges', xAx, yAx)
            return False
        m = lambda v, alt: v if v != 'Auto' else alt
        xMax = m(xAx.settings.max, np.nanmax(xData))
        xMin = m(xAx.settings.min, np.nanmin(xData))
        xRange = xMax - xMin
        yMax = m(yAx.settings.max, np.nanmax(yData))
        yMin = m(yAx.settings.min, np.nanmin(yData))
        yRange = yMax - yMin

        # Calc new coord
        ox_set = s.get('xPos')
        oy_set = s.get('yPos')
        if oldx is None:
            oldx = ox_set.get()
        if oldy is None:
            oldy = oy_set.get()

        if (0 in [xRange, yRange]) or np.isnan(xRange) or np.isnan(yRange):
            logging.error('Datapoint divide for ranges', xRange, yRange)
            return False

        # Store values in class attributes in order for other methods to find
        # them
        self.xData = xData
        self.yData = yData
        self.xRange = xRange
        self.yRange = yRange
        self.N = N
        
        type_of_point = self.settings.search
        search_func = self.distance
        if type_of_point == 'Nearest (Fixed X)':
            search_func = self.distance_fixed_x

        self.point_index, self.x, self.y = search_func(oldx, oldy)
        self.point_index = int(self.point_index)

        self.xMax = xMax
        self.xMin = xMin
        self.yMax = yMax
        self.yMin = yMin
        self.xAx = xAx
        self.yAx = yAx

        # perform nearest critical point search
        if type_of_point not in ('Nearest (Fixed X)', 'Nearest'):
            self.critical_search(type_of_point)
        else:
            self.x = self.xData[self.point_index]
            self.y = self.yData[self.point_index]

        self.ops.append(document.OperationSettingSet(ox_set, float(self.x)))
        self.ops.append(document.OperationSettingSet(oy_set, float(self.y)))
        self.ops.append(document.OperationSettingSet(self.settings.get('pointIndex'),
                                                     self.point_index))
        
        logging.debug('actionUp:', self.point_index, self.x, self.y)
        return True

    def critical_search(self, src):
        """Search for critical points in curve"""

        # Calculate slice
        rg = self.settings.searchRange / 2.

        # x, xd, err = utils.rectify(self.xData)
        x = self.xData
        xm, ym = x, self.yData
        
        iL = 0
        iR = len(xm) - 1
        if rg > 0:
            i1 = self.point_index
            xmi = xm[i1]
            logging.debug('searching', xmi, i1, rg, xm)
            vL = csutil.find_nearest_brute(xm, xmi - rg, sort=True)
            iv = csutil.find_nearest_brute(vL, i1)
            iL = vL[iv]
            vR = csutil.find_nearest_brute(xm, xmi + rg, sort=True)
            iv = csutil.find_nearest_brute(vR, i1)
            iR = vR[iv]
        if iL == iR:
            iL = int(max(0, iL - rg))
            iR = int(min(len(xm) - 1, iR + rg))
        if iR < iL:
            iL1 = iL
            iL = iR
            iR = iL1
            
        logging.debug('Slicing', iL, iR)
        sl = slice(iL, iR)

        def result(i):
            """Set result"""
            i += sl.start
            self.point_index = int(i)
            self.x = self.xData[self.point_index]
            self.y = self.yData[self.point_index]
            logging.debug('result', src, self.point_index, self.x, self.y)
        
        def find_maxmin(target):
            # Compute x range
            f = max if src == 'Maximum' else min
            ym1 = ym[sl]
            y = f(ym1)
            return np.where(ym1 == y)[0][0]
            
        if src in ('Maximum', 'Minimum'):
            result(find_maxmin(src))
            return True
        
        # Substitute X with critical X
        if self.settings.get('critical_x').get() != '':
            crix = self.settings.get('critical_x').get()
            crix = self.document.data[crix].data
            xm = crix
        w = self.settings.smooth
        if w < 0:
            w = confdb.smooth_window(len(xm))
        if w > 0:
            logging.debug('DataPoint search: smoothing', w)
            xm = utils.smooth(xm, w)
            ym = utils.smooth(ym, w)
        xm1 = xm[sl]
        ym1 = ym[sl]
        
        n = len(xm1) / 2
        
        def derive_roots(xm1, ym1, der):
            """Returns nearest minimum or maximum"""
            
            ym1 = utils.xyderive(xm1, ym1, der)
            ia = np.where(ym1 == max(ym1))[0][0]
            ib = np.where(ym1 == min(ym1))[0][0]
            if abs(ia - n) < abs(ib - n):
                return ia, ym1
            return ib, ym1
        
        i = None
        if src == 'Stationary':
            flex, ym1 = derive_roots(xm1, ym1, 1)
            iM = find_maxmin('Maximum')
            im = find_maxmin('Minimum')
            if abs(flex - n) <= abs(iM - n):
                if abs(flex - n) <= abs(im - n):
                    i = flex
                else:
                    i = im
            elif abs(iM - n) < abs(im - n):
                i = iM
            else:
                i = im
            
        elif src == 'Inflection':
            i, ym1 = derive_roots(xm1, ym1, 1)
            
        else:
            logging.debug('NO CRITICAL POINTS FOUND', src)
            return False
        result(i)
        return True

    _tg_ang = None

    @property
    def tg_ang(self):
        """Calculate the tangent angle in current point"""
        # use one-tenth of available points
        if self._tg_ang is not None:
            return self._tg_ang
        slice_length = 8
        if self.N < slice_length * 3:
            logging.debug('Too few points in order to calculate the tangent line')
            self._tg_ang = None
            return None
        # Select a sequence of points around datapoint. Translate to the dp is
        # the origin.
        left = slice(max(self.point_index - slice_length, 0), self.point_index)
        right = slice(self.point_index + 1, min(self.point_index + slice_length, self.N - 1))
        vx = np.concatenate((self.xData[left], self.xData[right]))
        vy = np.concatenate((self.yData[left], self.yData[right]))
        vx -= self.x
        vy -= self.y
        logging.debug('vx', vx)
        logging.debug('vy', vy)
        # Angle as a mean of arctan2 values (avoid zero-denominator problems)
        # v = np.arctan2(vy,vx)
        v1 = np.arctan(vy / vx)
        self._tg_ang = v1.mean()
        return self._tg_ang

    def check_point(self, pt, name=False):
        """Check point settings coherence"""
        aligned = sum([self.toset(pt, 'positioning', 'axes'),
                   self.cpset(self, pt, 'xAxis'),
                   self.cpset(self, pt, 'yAxis')]) == 3
        if name:
            self.toset(pt, name, self.name)
            self.cpset(self, pt, 'xAxis')
            self.cpset(self, pt, 'yAxis')
        return aligned

    def check_line(self, ln, mode='point-to-point'): 
        """Check line settings coherence"""
        aligned = self.check_point(ln)
        if not self.toset(ln, 'mode', mode):
            aligned = False
        if not self.toset(ln, 'clip', True):
            aligned = False
        self.toset(ln, 'Line/color', self.settings.Fill.color)
        return aligned

    def line_points(self, line, ang):
        """Get delta x,y for positioning a line passing through datapoint with `ang`"""
        # Check settings coherence
        self.check_line(line)
        c = abs(np.cos(ang))
        s = abs(np.sin(ang))
        # Scale the length inside graph margins
        d = {}
        t = 0
        
        if c > s:
            L = self.xRange
            d['xPos'] = self.x - L
            d['xPos2'] = self.x + L
            if s > 0:
                t = np.tan(ang)
                dL = L * t
                d['yPos'] = self.y - dL
                d['yPos2'] = self.y + dL
            else:
                d['yPos'] = self.y
                d['yPos2'] = self.y
        else:
            # Use the inverse coordinate system
            L = self.yRange
            d['yPos'] = self.y - L
            d['yPos2'] = self.y + L
            if c > 0:
                t = 1. / np.tan(ang)
                dL = L * t
                d['xPos'] = self.x - dL
                d['xPos2'] = self.x + dL
            else:
                d['xPos'] = self.x
                d['xPos2'] = self.x
        
        logging.debug('line_points', line.name, L, dL, c, s, 'angle',
                      ang * 180 / np.pi, t, d)
        self.dict_toset(line, d)
        return d

    def up_tangent(self):
        """Update tangent line widget position"""
        s = self.settings

        tg = s.get('tangentLine').findWidget()
        if tg is None:
            return
        if self.tg_ang is None:
            return
        
        self.line_points(tg, self.tg_ang)

    def up_perpendicular(self):
        """Update perpendicular line widget position"""
        s = self.settings
        pp = s.get('perpendicularLine').findWidget()
        if pp is None:
            logging.debug('No perpendicular line specified')
            return
        
        if self.tg_ang is None:
            return
        
        ang = self.tg_ang + (np.pi / 2)
        if ang > np.pi / 2:
            ang = ang - np.pi
        # print('ZZZ', self.tg_ang * 180 / np.pi, ang * 180 / np.pi)
        self.line_points(pp, ang)
    
    def up_point_to_point(self, line_name, second_name, stop=False):
        """Update widget-to-widget line"""
        if self.x is None:
            return
        s = self.settings
        pp = s.get(line_name).findWidget()
        if pp is None:
            logging.debug('No target line defined', line_name)
            return
        pt2 = s.get(second_name).findWidget()
        if pt2 is None:
            logging.debug('No target point defined', second_name)
            return
        aligned = self.check_line(pp)
        
        if stop:
            ret = {'xPos': self.x, 'yPos': self.y,
             'xPos2': pt2.settings.xPos[0], 'yPos2': pt2.settings.yPos[0]}
            self.dict_toset(pp, ret)
        else:
            aligned = aligned and self.check_point(pt2, name=second_name)
            self.cpset(self, pt2, line_name)
            dx = self.x - pt2.settings.xPos[0]
            dy = self.y - pt2.settings.yPos[0]
            ang = np.arctan2(dy, dx)
            ret = self.line_points(pp, ang)
        if not aligned:
            logging.debug('References were not aligned')
        return ret

    def up_passing(self):
        """Update datapoint-to-datapoint line"""
        return self.up_point_to_point('pt2ptLine', 'secondPoint')
        
    def up_labelLine(self):
        return self.up_point_to_point('labelLine', 'coordLabel', stop=True)

    def updateOutputLabel(self):
        """Call update() on any children defining it"""
        for label in self.children:
            getattr(label, 'update', lambda:1)()
        
    def updateControlItem(self, cgi):
        """If control item is moved or resized, this is called.
        Disabled graphical resizing, equals to moving."""
        s = self.settings
        
        # calculate new position coordinate for item
        xpos, ypos = self._getGraphCoords(cgi.widgetposn,
                                          cgi.posn[0], cgi.posn[1])
        if xpos is None or ypos is None:
            return

        xw = abs(cgi.dims[0] / (cgi.widgetposn[2] - cgi.widgetposn[0]))
        yw = abs(cgi.dims[1] / (cgi.widgetposn[1] - cgi.widgetposn[3]))

        # actually do the adjustment on the document
        xp = list(s.get('xPos').getFloatArray(self.document))
        yp = list(s.get('yPos').getFloatArray(self.document))
        w = list(s.get('width').getFloatArray(self.document))
        h = list(s.get('height').getFloatArray(self.document))
        r = list(s.get('rotate').getFloatArray(self.document))

        def i(v): return min(cgi.index, len(v) - 1)

        dw = (xw - w[i(w)])
        if xp[i(xp)] > xpos:
            xpos += dw
        else:
            xpos -= dw
        dh = (yw - h[i(h)])
        if yp[i(yp)] > ypos:
            ypos -= dh
        else:
            ypos += dh
        xp[i(xp)] = xpos
        yp[i(yp)] = ypos
        r[i(r)] = cgi.angle
        operations = (
            document.OperationSettingSet(s.get('xPos'), xp),
            document.OperationSettingSet(s.get('yPos'), yp),
            document.OperationSettingSet(s.get('rotate'), r)
        )
        
        self.document.applyOperation(
            document.OperationMultiple(operations, descr=_('adjust shape')))
        # Then re-attach it to the curve
        self.actionUp()


document.thefactory.register(DataPoint)

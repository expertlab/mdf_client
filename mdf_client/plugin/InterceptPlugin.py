#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Intercept all curves in a given x or y by placing datapoints."""
from mdf_canon.logger import get_module_logging
from mdf_client.plugin.datapoint import DataPoint
logging = get_module_logging(__name__)
import veusz.widgets
import veusz.plugins as plugins
import veusz.document as document
from veusz.dialogs.plugin import PluginDialog
import numpy as np
from . import utils
from mdf_client.clientconf import confdb


class InterceptPlugin(utils.OperationWrapper, plugins.ToolsPlugin):

    """Intercept all curves at a given x or y by placing datapoints"""
    # a tuple of strings building up menu to place plugin on
    menu = ('MDF', 'Intercept')
    # unique name for plugin
    name = 'Intercept'
    # name to appear on status tool bar
    description_short = 'Intercept all sample\'s curves in a plot'
    # text to appear in dialog box
    description_full = 'Intercept all curves pertaining to a file, sample or dataset by placing descriptive datapoints'

    def __init__(self, target=[], axis='X', val=0., search='Nearest (Fixed X)', searchRange=25, critical_x='', smooth=0,
                 text=''):
        """Make list of fields."""
        self.fields = [
            plugins.FieldDatasetMulti(
                "target", descr="Datasets whose curves to intercept", default=target),
            plugins.FieldCombo(
                "axis", descr="Intercept on X or Y axis", items=['X', 'Y'], default=axis),
            plugins.FieldFloat('val', 'Value', default=val),
            plugins.FieldCombo("search", descr="Place nearest", items=[
                               'Nearest (Fixed X)', 'Nearest', 'Maximum', 'Minimum', 'Inflection', 'Stationary', 'None'], default=search),
            plugins.FieldFloat(
                'searchRange', descr='Nearest search range', default=searchRange),
            plugins.FieldDataset('critical_x', descr="Critical search X dataset", default=critical_x),
            plugins.FieldInt('smooth', 'Smoothing for search', default=smooth),
            plugins.FieldText('text', 'Label text', default=text),
        ]
        
    @staticmethod
    def intercept_coords(curve, x, main_window=False):
        n = len(main_window._document.data[curve].data)
        w = confdb.smooth_window(n)
        p = InterceptPlugin(target=[curve],
                            axis='X',
                            critical_x='0:t',
                            val=x,
                            smooth=w)
        d = PluginDialog(main_window, main_window._document, p, InterceptPlugin)
        main_window.showDialog(d)
        return d

    @staticmethod
    def clicked_curve(pickinfo, main_window=False):
        logging.debug('clicked_curve', pickinfo.widget.path, pickinfo.coords[0])
        main_window.treeedit.selectWidget(pickinfo.widget)
        curve_to_intercept = pickinfo.widget.settings['yData']
        d = InterceptPlugin.intercept_coords(curve_to_intercept, pickinfo.coords[0], main_window)
        return d

    def apply(self, cmd, fields):
        """Do the work of the plugin.
        cmd: veusz command line interface object (exporting commands)
        fields: dict mapping field names to values
        """
        self.doc = cmd.document
        cur = fields['currentwidget']
        g = self.doc.resolveWidgetPath(None, cur)
        g = utils.searchFirstOccurrence(g, 'graph')
        if g is None or g.typename != 'graph':
            raise plugins.ToolsPluginException(
                'You should run this tool on a graph\n' + repr(g) + cur)
        axn = fields['axis']
        val = fields['val']
        text = fields['text']
        targetds = fields['target']
        doc = cmd.document

        hor = axn == 'X'
        datapoint_paths = []
        logging.debug('targets', targetds)
        for datapoint_parent in g.children:
            if not isinstance(datapoint_parent, veusz.widgets.point.PointPlotter):
                continue
            if datapoint_parent.settings.hide:
                logging.debug('Skipping hidden object', datapoint_parent.path)
                continue
            if datapoint_parent.settings.yData not in targetds and len(targetds) > 0:
                logging.debug('Skipping non-targeted object', datapoint_parent.path,
                              datapoint_parent.settings.yData)
                continue
            # Search the nearest point
            x = datapoint_parent.settings.get('xData').getFloatArray(doc)
            y = datapoint_parent.settings.get('yData').getFloatArray(doc)
            dst = abs(x - val) if hor else abs(y - val)
            i = np.where(dst == dst.min())[0]
            if len(i) == 0:
                raise plugins.ToolsPluginException(
                    'Impossible to find required value: %E' % val)
            i = i[0]
            # Add the datapoint
            cmd.To(g.path)
            name = datapoint_parent.createUniqueName('datapoint')
            lblname = 'lbl_' + name

            # Create the datapoint
            datapoint_settings = {'name': name,
                                  'xAxis': datapoint_parent.settings.xAxis,
                                  'yAxis': datapoint_parent.settings.yAxis,
                                  'xPos': float(x[i]),
                                  'yPos': float(y[i]),
                                  'coordLabel': lblname,
                                  'search': fields['search'],
                                  'searchRange': fields['searchRange'],
                                  'smooth':fields.get('smooth', -1), }
            if text:
                datapoint_settings['labelText'] = text

            if 'critical_x' in fields:
                datapoint_settings['critical_x'] = fields['critical_x']

            self.ops.append(document.OperationWidgetAdd(datapoint_parent,
                                                        'datapoint',
                                                        **datapoint_settings))

            datapoint_paths.append(datapoint_parent.path + '/' + name)

        logging.debug('Intercepting', self.ops)
        self.apply_ops('Intercept')

        # for path in datapoint_paths:
        #    cmd.To(path)
            # cmd.Action('up')


plugins.toolspluginregistry.append(InterceptPlugin)

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Testing CalibrationFactorPlugin."""
import unittest
import os
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
import veusz.document as document
from mdf_client import filedata
from mdf_client.tests import iutils_testing as iut
from mdf_client import plugin

nativem4 = os.path.join(iut.data_dir, 'calibration.h5')
nativem4 = '/home/daniele/MisuraData/sample_data/vertical/Calibration Vertical Dilatometer.h5'


def setUpModule():
    print('Starting ' + __name__)
    

def tearDownModule():
    print('Ending ' + __name__)


class CalibrationFactorPlugin(unittest.TestCase):

    """Tests the CalibrationFactorPlugin"""
    
    def test_theory(self):
        S = plugin.standards['Al2O3-NETZSCH']
        sT = S[:, 0][9:]
        sd = S[:, 1][9:]
        error = 1.1
        d = sd * error
        r = plugin.calibration_factor(sT, d, sT, sd, 0, 1000, 3)
        self.assertAlmostEqual(1 / error, r[0])
        r = plugin.calibration_factor(sT, d, sT, sd, 400, 1000, 3)
        self.assertAlmostEqual(1 / error, r[0])

    @unittest.skip("WAITING FOR A CALIBRATION TEST FILE!!!")
    def test(self):
        # Simulate an import
        imp = filedata.OperationMisuraImport(
            filedata.ImportParamsMisura(filename=nativem4))
        doc = filedata.MisuraDocument()
        self.cmd = document.CommandInterface(doc)
        plugin.makeDefaultDoc(self.cmd)
        imp.do(doc)
        print(list(doc.data.keys()))
        fields = {'d': '0:vertical/sample0/d', 'T': '0:kiln/T',
                  'std': 'Al2O3-NETZSCH', 'start': 50, 'end': 0, 'label': 1, 'add': 1,
                  'currentwidget': '/', 'idx0':0}
        p = plugin.CalibrationFactorPlugin()
        p.apply(self.cmd, fields)
        self.assertIn(fields['d'] + '/Al2O3-NETZSCH', doc.data)
        doc.model.refresh(force=True)


if __name__ == "__main__":
    unittest.main(verbosity=2)

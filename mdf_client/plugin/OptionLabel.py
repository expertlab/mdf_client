#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Render a MDF Option object into a text label"""
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
import veusz.document as document

from veusz import widgets
import veusz.setting as setting
from . import utils
from mdf_client import _
from .OptionAbstractWidget import OptionAbstractWidget
from mdf_canon.csutil import basestring


def clean_row(row, header):
    for i, h in enumerate(header):
        if h['type'] == 'Float':
            if row[i] is None:
                row[i] = 0
    return row


class OptionLabel(utils.OperationWrapper, OptionAbstractWidget, widgets.TextLabel):
    typename = 'optionlabel'
    description = "Label for option"
    
    def __init__(self, *args, **kwargs):
        self.connected = False
        self.opt_name = []
        widgets.TextLabel.__init__(self, *args, **kwargs)
            
        self.addAction(widgets.widget.Action('up', self.update,
                                                   descr='Update Label',
                                                   usertext='Update Label'))
        for name in ('prefix', 'format', 'option', 'dataset', 'showName', 'showTestName'):
            self.settings.get(name).setOnModified(self.update)
            
    def get_proxies_and_options(self, conf):
        opts = self.settings.option.replace(' ', '')
        split = ',' if ',' in opts else ';'
        for opt in opts.split(split):
            proxy, name = conf.from_column(opt)
            print('get_proxies_and_options', repr(opts), proxy, repr(name), proxy['fullpath'])
            yield proxy, name
        print('get_proxies_and_options end', opts)
        
    @classmethod
    def addSettings(klass, s):
        """Construct list of settings."""
        widgets.TextLabel.addSettings(s)
        s.add(setting.Dataset(
            'dataset', '',
            descr=_('Dataset pointing to the containing options'),
            usertext=_('Dataset')), 0) 
        
        s.add(setting.Str('option', '',
                          descr='Option path',
                          usertext='Option path'),
              1)
        s.add(setting.Str('prefix', '',
                          descr='Prefix',
                          usertext='Prefix'),
              2)
        s.add(setting.Str('format', '',
                          descr='Formatter',
                          usertext='Formatter'),
              3)
        s.add(setting.Bool('showName', False,
                          descr='Show name',
                          usertext='Show name',
                          formatting=True),
              1)
        s.add(setting.Bool('showTestName', False,
                          descr='Show test name',
                          usertext='Show test name',
                          formatting=True),
              4)
        s.add(setting.Bool('showHidden', False,
                          descr='Show hidden options',
                          usertext='Show even hidden options',
                          formatting=True),
              1)
    
    def create_label(self, proxy, opt_name):
        """Create label fragment for `opt_name` of `proxy`"""
        if not proxy:
            logging.debug('create_label no proxy', proxy)
            return None
        if not opt_name in proxy:
            logging.debug('create_label no option', opt_name)
            return None
        opt = proxy.gete(opt_name)
        if (not self.settings['showHidden']) and 'Hidden' in opt['attr'] or 'ClientHide' in opt['attr']:
            logging.debug('Hidden option', opt_name, opt['attr'])
            return None
        typ = opt['type']
        # Resolve RoleIO pointers
        while typ == 'RoleIO':
            proxy = proxy.root.toPath(opt['options'][0])
            if not proxy:
                logging.info('Cannot resolve RoleIO', opt_name, opt['options'])
                return None
            opt_name = opt['options'][2]
            if not opt_name in proxy:
                logging.info('Role option maps to an object lacking target option',
                             opt['options'], proxy.keys())
                return None
            opt = proxy.gete(opt_name)
            typ = opt['type']
        fmt = self.settings.format
        val = proxy[opt_name]
        name = []
        if self.settings.showTestName:
            name.append(self.get_test_name())
        if self.settings.showName:
            name.append(opt['name'] + ': ')
        name = '\\\\'.join(name)
        
        if fmt and typ != 'Table':
            fmt = '{:' + fmt + '}'
            val = name + fmt.format(val)
        else:
            if typ in ['String', 'TextArea', 'Chooser', 'Date']:
                func = lambda val, opt: '{}'.format(val).replace('\n', '\\\\')
            else:
                func = getattr(self, 'cvt_' + typ,
                               lambda *a: 'NotSupported: {}'.format(typ))
            val = func(val, opt)
            if name and typ != 'Table':
                val = name + val
        return val
    
    def cvt_String(self, val, opt):
        r = []
        if self.settings.showTestName:
            r.append(self.get_test_name())
        if self.settings.showName:
            r.append()
        r.append('{}'.format(val).replace('\n', '\\\\'))
        return r
        
    def update(self):
        """Update OptionLabel with the current option text"""
        OptionAbstractWidget.update(self)
        logging.debug('OptionLabel', repr(self.settings.option), self.settings.dataset,
                    self.proxy, self.opt_name, self.changeset)
        split = ',' if ',' in self.settings.option else ';'
        opts = self.settings.option.split(split)
        label = ''
        newline = '\\\\'
        for i, proxy in enumerate(self.proxy):
            new = self.create_label(proxy, self.opt_name[i])
            if new is None:
                logging.info('Not creating option for', i, opts[i])
                continue
            # Remove closing tag
            if '</math>' in new:
                new = new.replace('</math>', '')
                newline = '\n'
            # Remove also opening tag if already found
            if '<math>' in label:
                new = new.replace('<math>', '')
            label += new
            if i < len(self.proxy):
                label += newline
        
        pre = self.settings.prefix
        # Add closing tag
        if '<math>' in label:
            label += '</math>'
        # Add prefix:
        elif pre:
            label = pre + ' ' + label
        self.toset(self, 'label', label)
        self.apply_ops()
    
    def cvt_Integer(self, val, opt):
        if val < 1e6:
            return str(val)
        return '{:E}'.format(val)
    
    def cvt_Float(self, val, opt):
        if opt.get('precision', -1) > 0:
            fmt = '{:.' + str(opt['precision']) + 'f}'
            return fmt.format(val)
        return '{:.4E}'.format(val)
    
    def cvt_Meta(self, val, opt):
        r = ''
        for k, v in val.items():
            if v in ('None', None):
                v = 0
            r += '{}:{:.2E}\\\\'.format(k, v)
        return r
    
    def cvt_Table(self, val, opt):
        """Create a MathML table"""
        if len(val) <= 1:
            return 'Empty table'
        header0 = opt.get('header')
        header = []
        precision = []
        visible = []
        for i, h in enumerate(header0):
            if 'Hidden' in h['attr']:
                visible.append(0)
                continue
            visible.append(1)
            header.append(h)
            precision.append(h.get('precision', None))
        N = len(header)
        # Build table
        r = u'<math>\n'
        pre = self.settings.prefix
        if not pre:
            pre = []
            if self.settings.showTestName:
                pre.append(self.get_test_name())
            if self.settings.showName:
                pre.append(opt['name'])
        else:
            pre = [pre]
        if len(pre) == 1:
            r += u'<mfrac><mtext>{}</mtext>\n'.format(pre[0])
        if len(pre) == 2:
            r += u'<mfrac><mfrac><mtext>{}</mtext><mtext>{}</mtext></mfrac>'.format(*pre)
        r += u'<mtable columnlines="solid">\n'
        
        header_fmt = u'<mtr> ' + (u'<mtd><mtext>{}</mtext></mtd> ') * N + u'</mtr>\n'
        r += header_fmt.format(*[ h['name'].replace('&', '+') for h in header])
        
        # Build row format
        fmt = self.settings.format.replace(' ', '').split(',')
        if len(fmt) == 1:
            fmt = fmt * N
        row_fmt = u'<mtr> '
        for i, h in enumerate(header):
            row_fmt += u'<mtd>{'
            p = precision[i]
            if h['type'] in ('Float', 'Integer'):
                if fmt[i]:
                    if not fmt[i].startswith(u':.'):
                        if not fmt[i].startswith(u'.'):
                            fmt[i] = u'.' + fmt[i]
                        fmt[i] = u':' + fmt[i]
                    row_fmt += fmt[i]
                elif isinstance(p, basestring):
                    row_fmt += u':' + p
                elif p:
                    row_fmt += u':.' + str(p) + u'g'
                else:
                    # Generic formatter
                    # TODO: use extend_decimals?
                    row_fmt += u':.4g'
            row_fmt += u'}</mtd>'
        row_fmt += u'</mtr>\n'
        
        # Rows
        for row0 in val:
            row = []
            for i, z in enumerate(row0):
                if i >= N or visible[i]:
                    row.append(z)
            r += row_fmt.format(*clean_row(row, header))
        
        r += u'</mtable>\n'
        if pre:
            r += u'</mfrac>\n'
        r += u'</math>'
        return r


document.thefactory.register(OptionLabel)

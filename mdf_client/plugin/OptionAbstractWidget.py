#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Genering rendering utilities for a MDF Option object"""
from mdf_canon.logger import get_module_logging
from collections import defaultdict
from PyQt5 import QtCore
logging = get_module_logging(__name__)
from .. import conf
from .. import _
from mdf_client.fileui import version


def edit_option_dialog(doc, options, proxies, testfile, parent=None):
    
    dialog = conf.InterfaceDialog(
        proxies, proxies, options, parent=None)
    dialog.setWindowTitle(_('Edit option and recalculate'))
    if dialog.exec_():
        testfile.conf = proxies[0].root
        version.save_version(doc, testfile, nosync=False, parent=parent)
        r = testfile.run_scripts()
        print('RUN SCRIPTS', testfile, r)
        return True
    return False


class OptionAbstractWidget(object):
    current_changeset = 0
    _proxy = False
    connected = False
    _linked = False
    
    @property
    def changeset(self):
        if not self.proxy:
            return -1
        return max([p._changeset for p in self.proxy])
        
    def check_update(self, *a):
        # logging.debug('check_update', self.path, a, self.current_changeset, self.changeset)
        if self.changeset and self.current_changeset < self.changeset:
            self.update()
            return True
        return False
    
    @property
    def linked(self):
        if self._linked:
            return self._linked
        ds = self.settings.dataset
        if not ds:
            logging.debug('No dataset defined', ds, self.path)
            return False
        y = self.document.get_from_data_or_available(ds, False)
        # Ensure it is not a derived or native dataset
        y = y if hasattr(y, 'linked') else False
        # Search for a datasets starting with y:
        if not y and '/' in ds:
            prefix = ds.split('/')[0]
            logging.debug('Dataset not found, scanning prefix', ds, prefix)
            for d, yds in self.document.iter_data_and_available():
                if d.startswith(prefix) and hasattr(yds, 'linked'):
                    y = yds
                    break
        if not y:
            logging.debug('Dataset not found: will return an empty widget', ds)
            return False
        if not y.linked:
            logging.debug('No linked file for', ds)
            return False
        if not y.linked.conf:
            logging.debug('No configuration for linked file', ds)
            return False
        self._linked = y.linked
        return self._linked
    
    @property
    def proxy(self):
        """Iter configuration proxy"""
        if self._proxy:
            return self._proxy
        self._proxy = []
        self.opt_name = []
        from mdf_client.live import registry
        self.plain_kids = []
        linked = self.linked
        if not linked:
            return []
        for p, n in self.get_proxies_and_options(linked.conf):
            if not p or not n:
                logging.debug('Skipping', p, n)
                continue
            self._proxy.append(p)
            self.opt_name.append(n)
            if p and n and n in p:
                self.plain_kids.append(p.getattr(n, 'kid'))
        registry.sig_updated_kid.connect(self.update_from_registry, QtCore.Qt.QueuedConnection)
        return self._proxy
    
    def get_test_name(self):
        ins = self.linked.conf['runningInstrument']
        return getattr(self.linked.conf, ins).measure['name']
    
    def get_proxies_and_options(self):
        """List all involved options"""
        return []
    
    def update_from_registry(self, kid):
        if kid not in self.plain_kids:
            return 
        self.update()
    
    def update(self):
        self.doc = self.document
        if not self.connected:
            self.document.signalModified.connect(self.check_update)
            self.connected = True
        # Force proxy reset
        self._proxy = False
        self.current_changeset = self.changeset
    
    def draw(self, *a, **k):
        self.check_update()
        return super(OptionAbstractWidget, self).draw(*a, **k)
    
    def edit_option_dialog(self):
        testfile = self.document.get_proxy(self.linked.params.filename)
        proxies = defaultdict(dict)
        for i, proxy in enumerate(self.proxy):
            opt = proxy.gete(self.opt_name[i])
            proxies[proxy][opt['handle']] = opt
        r = edit_option_dialog(self.document, list(proxies.values()), list(proxies.keys()), testfile)
        if r:
            print('UPDATING after edit')
            self.update()


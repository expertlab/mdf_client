from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
import datetime
from textwrap import wrap
from .. import units


def wr(k, v, n=18, inter=' '):
    k = wrap(u'{}:'.format(k), n)
    for i, e in enumerate(k):
        k[i] = u'\\bold{{' + e + u'}}'
    k = u'\\\\'.join(k)
    k = k.replace(u'_', u'\\_')
    if isinstance(v, bytes):
        v = v.decode('utf8')
    v = wrap(u'{}'.format(v), n)
    v = u'\\\\'.join(v)
    v = v.replace(u'_', u'\\_')
    r = u'{}{}{}'.format(k, inter, v)
    logging.debug('wrapped', k, v, r)
    return r


invalid = (None, 'None', '')


def render_meta(obj_or_meta, notfound=False, n=30, inter=' ', time=False, value=False, zt=0):
    msg = ''
    if hasattr(obj_or_meta, 'describe'):
        meta = []
        for k, m in obj_or_meta.describe().items():
            if m['type'] != 'Meta':
                continue
            meta.append(m)
    else:
        meta = obj_or_meta
    meta = sorted(meta, key=lambda m: m['priority'])
    for m in meta:
        if m['handle'] == 'maxHeatingRate':
            continue
        c = m['current']
        if c['temp'] in invalid:
            if not notfound:
                continue
            v = notfound
        else:
            v = u'{} {{\\deg}}C'.format(int(c['temp']))
            if value:
                if c['value'] not in invalid:
                    v += u', {:.2f}'.format(c['value'])
                    u = m.get('unit', None)
                    if u not in (None, 'None'):
                        if hasattr(u, 'items'):
                            u = u.get('value', None)
                        v += units.hsymbols.get(u, '')
            if time: 
                if c['time'] not in invalid:
                    t = c['time']
                    if t > zt:
                        t -= zt
                    logging.debug('render time', c['time'], t, zt)
                    v += u', {}'.format(datetime.timedelta(seconds=int(t)))
        name = m['name'].replace(' deformation', '').replace('Maximum ', 'Max ').replace('Minimum ', 'Min ')
        w = wr(name, v, n=n, inter=inter)
        if msg:
            msg += u'\\\\' 
        msg += w
    return msg

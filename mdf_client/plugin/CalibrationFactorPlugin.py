#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Get calibration factor from standard expansion curve"""
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from copy import copy
import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline

import veusz.plugins as plugins
import veusz.document as document
from . import utils
from mdf_client import _, units

from mdf_canon.csutil import find_nearest_val
from mdf_canon.calibration import standards
from scipy.optimize import minimize


def calibrate(d, factor):
    cal = d * factor
    cal -= cal[0] - d[0]
    return cal


def factor_error(fac, d, std):
    return sum((calibrate(d, fac) - std) ** 2)


def calibration_factor(temperature, expansion, standard_temperature, standard_expansion, start, end, poly=2):
    """`expansion` must be %, same length as `temperature`"""
    T = temperature
    d = expansion
    sT = standard_temperature
    sd = standard_expansion
    # Cut datasets near start/end temperatures
    start_index = find_nearest_val(T, start, get=T.__getitem__)
    end_index = find_nearest_val(T, end, get=T.__getitem__)
    T = T[start_index:end_index]
    d = d[start_index:end_index]
    logging.debug('T start, end', start, T[0], end, T[-1])
    # Standard slope
    f = InterpolatedUnivariateSpline(sT, sd, k=3)
    std = f(T)
    # Make coincident at T0
    std -= (std[0] - d[0])
    s0 = f(T[0])
    
    # Find the calibration factor which minimizes the error
    opt = minimize(factor_error, [1], (d, std))
    factor = opt.x[0]
    initial = factor_error(1, d, std)
    s_slope = (f(T[-1]) - s0) / (T[-1] - T[0]) 
    z_slope = (d[-1] - d[0]) / (T[-1] - T[0])  # Sample slope
    
    # Just use to get linearity residuals
    fitting = np.polyfit(T, d, poly, full=True)
    poly, res, rank, sing, rcond = fitting
    p = np.poly1d(fitting[0])
    polyfit = p(T)
    cal = d * factor
    res = np.sqrt(res[0] / len(T))
    
    if 0:
        from matplotlib import pylab
        pylab.plot(T, d, label='d')
        pylab.plot(T, polyfit, label='d interp')
        pylab.plot(T, std, label='standard')
        pylab.plot(T, calibrate(d, factor), '.', label='calibrated')
        pylab.legend()
        pylab.show()
    
    return factor, polyfit, cal, std, f, T, d, sT, sd, opt, res, fitting, \
            start_index, end_index, initial


label_format = """Factor: {:.8g}
Reference: {}
Standard deviation:
{:.3E} %; {:.4g} {}"""


class CalibrationFactorPlugin(utils.OperationWrapper, plugins.ToolsPlugin):

    """Dilatometry calibration factor calculation"""
    # a tuple of strings building up menu to place plugin on
    menu = ('MDF', _('Calibration factor'))
    # unique name for plugin
    name = 'Calibration Factor'
    # name to appear on status tool bar
    description_short = _('Dilatometry calibration factor')
    # text to appear in dialog box
    description_full = _(
        'Get calibration factor from standard expansion curve')
    preserve = True

    def __init__(self, d='', T='', std='NIST-SRM738', start=50, end=0, label=True, add=True, idx0=0):
        """Make list of fields."""

        self.fields = [
            plugins.FieldDataset("d", descr=_("Expansion dataset"), default=d),
            plugins.FieldDataset(
                "T", descr=_("Temperature dataset"), default=T),
            plugins.FieldCombo(
                "std", descr=_("Calibraiton Standard"), items=list(standards.keys()), default=std),
            plugins.FieldFloat(
                'start', descr=_('First temperature margin'), default=start),
            plugins.FieldFloat(
                'end', descr=_('Last temperature margin'), default=end),
            plugins.FieldBool(
                'label', _('Draw calibration label'), default=bool(label)),
            plugins.FieldBool(
                'add', _('Add calibration datasets'), default=bool(add)),
            plugins.FieldInt(
                'idx0', descr=_('Start index'), default=int(idx0)),
            plugins.FieldInt(
                'poly', descr=_('Polynomial degree'), default=2),
        ]

    def apply(self, cmd, fields):
        """Do the work of the plugin.
        cmd: veusz command line interface object (exporting commands)
        fields: dict mapping field names to values
        """
        self.ops = []
        self.doc = cmd.document
        idx0 = int(fields['idx0'])
        ds = self.doc.data[fields['d']]
        Ts = self.doc.data[fields['T']]
        # Convert to percent, if possible
        self.inidim = getattr(ds, 'm_initialDimension', False)
        if not getattr(ds, 'm_percent', False):
            if self.inidim:
                ds = units.percent_conversion(ds, 'To Percent', auto=False)
        T = Ts.data[idx0:]
        # Cut away any cooling
        while max(T) != T[-1]:
            i = int(np.where(T == max(T))[0][0])
            T = T[:i]
        d = ds.data[idx0:len(T) + idx0]

        # Standard
        S = standards[fields['std']]
        sT = S[:, 0]
        sd = S[:, 1]
        # Find start/end T
        start = max(sT[0], T[0]) + fields['start']
        end = min(sT[-1], T[-1]) - fields['end']
        
        factor, polyfit, cal, std, f, T, d, sT, sd, opt, res, fitting, si, ei, initial = calibration_factor(T, d, sT, sd, start, end, fields['poly'])
        # ????
        z_const = ds.data[0]
        #########
        
        # Convert from percentage to micron
        um = res * self.inidim / 100
        # factor = s_slope / z_slope
        
        micron = u'\u03bcm'
        msg = _(label_format).format(factor, fields['std'],
                                     res, um, micron)
        
        self.msg = msg
        logging.debug(msg)
        
        self.fld, self.ds, self.T, self.d, self.sT, self.sd = fields, ds, T, d, sT, sd
        self.factor, self.res, self.um, self.opt = factor, res, um, opt
        if fields['label']:
            self.label()
        if fields['add']:
            self.add_datasets(si + idx0, ei + idx0, polyfit, cal, std)
        self.apply_ops()
        self.doc.model.refresh()
        return factor, res

    def add_datasets(self, start_index, end_index, polyfit, cal, std):
        """Add standard and fitted datasets for further evaluations (plotting, etc)"""
        # TODO: convert these into derived datasets
        # Adding plot data
        
        fields = self.fld
        name = fields['std'].replace(' ', '_')
        p = fields['d'] + '/' + name
        Tds = self.doc.data[fields['T']]
        orig = self.doc.data[fields['d']].data.copy()
        orig[:start_index] = None
        orig[end_index:] = None
        old_unit = getattr(self.ds, 'old_unit', 'percent')
        
        # FITTING
        df1 = orig.copy()
        df1[start_index:end_index] = polyfit
        df = df1
        dsf = copy(Tds)
        dsf.attr = dict({}, **Tds.attr)
        dsf.tags = set([])
        dsf.data = plugins.numpyCopyOrNone(df)
        dsf.m_var = name + '_fit'
        dsf.m_pos = 2
        dsf.m_name = dsf.m_var
        dsf.m_col = dsf.m_var
        dsf.old_unit = old_unit
        dsf.unit = 'percent'
        dsf.m_initialDimension = self.inidim
        dsf.m_label = _('Polynomial Fitting')
        self.ops.append(
            document.OperationDatasetSet(p + '_fit', dsf))
        
        # Calibrated
        df1 = orig.copy()
        df1[start_index:end_index] = cal
        df = df1
        dsf = copy(Tds)
        dsf.attr = dict({}, **Tds.attr)
        dsf.tags = set([])
        dsf.data = plugins.numpyCopyOrNone(df)
        dsf.m_var = name + '_cal'
        dsf.m_pos = 2
        dsf.m_name = dsf.m_var
        dsf.m_col = dsf.m_var
        dsf.old_unit = old_unit
        dsf.unit = 'percent'
        dsf.m_initialDimension = self.inidim
        dsf.m_label = _('Calibrated')
        self.ops.append(
            document.OperationDatasetSet(p + '_cal', dsf))
        
        # STANDARD DATA
        df1 = orig.copy()
        df1[start_index:end_index] = std
        df = df1
        
        dsd = copy(Tds)
        dsd.attr = dict({}, **Tds.attr)
        dsd.tags = set([])
        dsd.data = plugins.numpyCopyOrNone(df)
        dsd.m_var = name
        dsd.m_pos = 1
        dsd.m_name = name
        dsd.m_col = name
        dsd.unit = 'percent'
        dsd.old_unit = old_unit
        dsd.m_initialDimension = self.inidim
        dsd.m_label = _(name)
        self.ops.append(
            document.OperationDatasetSet(p + '_d', dsd))

    def label(self):
        """Draw label"""
        cur = self.fld.get('currentwidget')
        g = self.doc.resolveWidgetPath(None, cur)
        g = utils.searchFirstOccurrence(g, ['graph', 'page'])
        if g is None or g.typename not in ['graph', 'page']:
            raise plugins.ToolsPluginException(
                'Impossible to draw the label. Select a page or a graph.')
        stdname = self.fld['std'].replace(' ', '_')
        dsname = self.fld['d'].replace('summary/', '').replace('/', '_')
        name = f'lbl_{dsname}_{stdname}'
        if not g.getChild(name):
            self.ops.append(document.OperationWidgetAdd(g, 'label', name=name))
            self.apply_ops(self.name + ':CreateLabel')
        lbl = g.getChild(name)
        self.toset(lbl, 'label', self.msg.replace('\n', '\\\\'))


plugins.toolspluginregistry.append(CalibrationFactorPlugin)

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Generic Kalman plugin"""
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
import veusz.plugins as plugins
import numpy as np
from mdf_canon import kalman
from collections import defaultdict
from .utils import CachedResultFragment


class KalmanPlugin(plugins.DatasetPlugin, CachedResultFragment):

    """Dataset plugin to apply a Kalman filter"""
    # tuple of strings to build position on menu
    menu = ('Filtering', 'Kalman')
    # internal name for reusing plugin later
    name = 'Kalman'
    # string which appears in status bar
    description_short = 'Kalman'

    # string goes in dialog box
    description_full = 'Kalman filter'
    cached_dataset_fields = ['ds_in', 'ds_t']

    def __init__(self, ds_in='', ds_t='', ds_out='', **kw):
        """Define input fields for plugin."""
        self.fields = [
            plugins.FieldDataset(
                'ds_in', 'Target dataset', default=ds_in),
            plugins.FieldDataset(
                'ds_t', 'X dataset', default=ds_t),
            plugins.FieldDataset(
                'ds_out', 'Output dataset', default=ds_out)
        ]
        desc = kalman.iter_kalman_definitions('')
        for handle in ('devfactor', 'sensorvar', 'duration', 'update', 'maxbuffer', 'minbuffer'):
            opt = desc[handle]
            cls = plugins.FieldFloat if opt['type'] == 'Float' else  plugins.FieldInt
            default = kw.get(handle, None)
            if default is None:
                default = opt['current']
            kwargs = {}
            if opt.get('max', None) is not None:
                kwargs['maxval'] = opt.get('max', None)
            if opt.get('min', None) is not None:
                kwargs['minval'] = opt.get('min', None)
                
            field = cls(handle, opt['name'], default=default, **kwargs)
            self.fields.append(field)

    def getDatasets(self, fields):
        """Returns single output dataset (self.ds_out).
        This method should return a list of Dataset objects, which can include
        Dataset1D, Dataset2D and DatasetText
        """
        # raise DatasetPluginException if there are errors
        if fields['ds_out'] == '':
            raise plugins.DatasetPluginException('Invalid output dataset name')
        # make a new dataset with name in fields['ds_out']
        logging.debug('DSOUT', fields)
        self.ds_out = plugins.Dataset1D(fields['ds_out'])
        out = [self.ds_out]
        kf = kalman.KalmanFilter()
        kf.sensorvar = fields['sensorvar']
        kf.devfactor = fields['devfactor']
        for k in kf.outputs:
            ds = plugins.Dataset1D(fields['ds_out'] + '_' + k)
            out.append(ds)
            setattr(self, 'ds_out_' + k, ds)
        # return list of datasets
        return out
    
    def updateDatasets(self, fields, helper):
        """Do shifting of dataset.
        This function should *update* the dataset(s) returned by getDatasets
        """
        if self.is_cache_dirty(fields, helper):
            out = self.calculate_filtered(fields, helper)
            self.cached_result = out 
        else:
            out = self.cached_result
        return [out[0]]
    
    def calculate_filtered(self, fields, helper):
        # get the input dataset - helper provides methods for getting other
        # datasets from Veusz
        ds_in = helper.getDataset(fields['ds_in'])
        ds_t = helper.getDataset(fields['ds_t'])
        if helper.getDataset(fields['ds_out']) in (ds_in, ds_t, ''):
            raise plugins.DatasetPluginException(
                "Input and output datasets should differ.")
        kw = {k: fields.get(k, 0) for k in kalman.KalmanFilter.options}
        kfilter = kalman.KalmanFilter(**kw)
        data = np.array([ds_t.data, ds_in.data]).T
        filtered = []
        results = defaultdict(list)
        for point in data:
            print(point)
            filtered.append(kfilter(*point))
            for k in kalman.KalmanFilter.outputs:
                results[k].append(getattr(kfilter, k))
        # update output dataset with input dataset (plus value) and errorbars
        self.ds_out.update(data=np.array(filtered))
        out = [self.ds_out]
        for k in kalman.KalmanFilter.outputs:
            ds = getattr(self, 'ds_out_' + k)
            ds.update(data=np.array(results[k]))
            out.append(ds)
        return out


# add plugin classes to this list to get used
plugins.datasetpluginregistry.append(KalmanPlugin)

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Custom MDF plotting facilities."""
from time import time
import veusz.plugins as plugins
import veusz.document as document
from .ArrangePlugin import ArrangePlugin
from .CalibrationFactorPlugin import standards
from mdf_client.clientconf import confdb 
from mdf_client.iutils import get_plotted_tree
from mdf_canon.logger import get_module_logging
from mdf_canon.csutil import unicode_func
logging = get_module_logging(__name__)
from . import utils
from . import PercentPlugin
from .. import units

default_curves = ['T', 'P', 'S', 'h', 'Vol', 'd', 'err']
an_default_curves = ['T', 'Vol', 'd', 'Dil', 'Sint', 'Flex']


def dataset_curve_name(ds, dsn, doc):
    """Generation of unambiguous and traceable curve and axis names"""
    entry = doc.model.tree.traverse_path(dsn)
    sampleName = entry.sample_name
    dsname = getattr(ds, 'm_name', dsn).replace(
        'summary/', '')
    if not dsname:
        dsname = dsn
    instrument_name = dsn.split('/')[0]
    if ':' in instrument_name:
        instrument_name = instrument_name.split(':')[1]
    dsname = dsname.replace("/", ":")
    dsvar = getattr(entry, 'm_var', '')
    if not dsvar:
        dsvar = dsn.split('/')[-1]
    entry = doc.model.tree.traverse_path(dsn)
    if entry:
        keyvar = entry.legend
    else:
        keyvar = dsvar
        
    if dsn.endswith('sample0/d') and instrument_name in ('flex', 'flex3'):
        keyvar = instrument_name + keyvar[1:]
        
    fileName = entry.file_name
        
    if sampleName and sampleName != fileName:
        curve_name = unicode_func(dsname + ' - ' + sampleName + ' - ' + fileName)
        key = unicode_func(keyvar)
    else:
        curve_name = unicode_func(dsname + ' - ' + fileName)
        key = unicode_func(keyvar)
    curve_name = curve_name.replace('/', '\\')
    i, bounded_name = confdb.rule_axis.query(dsn)
    if bounded_name:
        bounded_name = bounded_name[0]
    else:
        bounded_name = dsvar
    
    ax_label = bounded_name
    unit = getattr(ds, 'unit', False)
    if not unit:
        unit = getattr(getattr(ds, 'parent', False), 'unit', False)
    if unit:
        # FIXME: used for tables; should lookup proper column
        if isinstance(unit, list):
            unit = unit[-1]
        u = units.symbols.get(unit, unit)
        ax_label += ' ({{{}}})'.format(u)
    ax_name = 'ax:' + bounded_name.replace("/", ":")
    return curve_name, ax_name, ax_label, key


class PlotDatasetPlugin(utils.OperationWrapper, plugins.ToolsPlugin):

    """Plot MDF datasets."""
    # a tuple of strings building up menu to place plugin on
    menu = ('MDF', 'Plot a list of misura x,y couples')
    # unique name for plugin
    name = 'Plot datasets'
    # name to appear on status tool bar
    description_short = 'Plot lists of MDF datasets'
    # text to appear in dialog box
    description_full = 'Plot lists of MDF datasets in a consistent way'

    def __init__(self, x=[], y=[]):
        """Make list of fields."""

        self.fields = [
            plugins.FieldDatasetMulti("x", 'X Coord datasets', default=[]),
            plugins.FieldDatasetMulti("y", 'Y Coord datasets', default=[]),
        ]

    def initCurve(self, name='Curve', xData='xD', yData='yD',
                  yAxis='y', axisLabel='Curve', graph='/page/graph', key=False):
        """Configure a new curve (appearance, axes positions, data labels"""
        logging.debug('initCurve', name, xData, yData)

        doc = self.doc

        gobj = doc.resolveWidgetPath(None, graph)
        preop = []
        # Create only if missing
        if not gobj.getChild(name):
            preop.append(document.OperationWidgetAdd(gobj, 'xy', name=name))
        create = True
        for obj in gobj.children:
            if obj.name == yAxis:
                create = False
                break
        if create:
            preop.append(document.OperationWidgetAdd(
                gobj, 'axis-function', name=yAxis, direction='vertical'))

        # Create graph and axis (if needed)
        if len(preop):
            logging.debug('applying operations', preop)
            doc.applyOperation(
                document.OperationMultiple(preop, descr='PlotDataset:Create'))

        obj = gobj.getChild(name)
        n = len(doc.data[yData].data)
        thin = int(max(1, n / 100))
        if n > 10:
            self.toset(obj, 'marker', u'none')
        if thin > 1:
            self.toset(obj, 'thinfactor', thin)
        self.toset(obj, 'yData', yData)
        self.toset(obj, 'xData', xData)
        key = key or name
        self.toset(obj, 'key', key.replace('_', '\\_'))
        self.toset(obj, 'yAxis', yAxis)
        yax = gobj.getChild(yAxis)
        self.toset(
            yax, 'label', axisLabel.replace('_', '\\_').replace('/', '.'))
        self.apply_ops('PlotDataset:Associate')
        return True

    def auto_percent(self, ds, dsn, gname, ax_name):
        """Find if the dataset ds should be converted to percentage based on other datasets sharing the same Y ax."""
        g = self.doc.resolveWidgetPath(None, gname)
        tree = get_plotted_tree(g)
        dslist = tree['axis'].get(g.path + '/' + ax_name, [])

        is_derived = getattr(ds, 'ds', ds) 
        is_derived = isinstance(is_derived, document.datasets.Dataset1DPlugin)
        if is_derived:
            ds.m_initialDimension = getattr(ds.parent.ds,
                                            'm_initialDimension',
                                            None)

        pc = getattr(ds, 'm_percent', None)
        if pc is None:
            logging.debug('No m_percent attribute defined', dsn)
            return False

        cvt = None
        for nds in dslist:
            if nds == dsn:
                continue  # itself
            cvt = getattr(self.doc.data[nds], 'm_percent', None)
            if cvt is not None:
                break
        if cvt is None or cvt == pc:
            return False
        # A conversion should happen
        logging.debug('CONVERTING', cvt, pc)
        self.ops.append(document.OperationToolsPlugin(
            PercentPlugin.PercentPlugin(),
            {
                'ds': dsn,
                'propagate': False,
                'action': 'Invert',
                'auto': False
            })
        )
        return True

    def arrange(self, graphics_names, plotted_dataset_names=False):
        arrange_fields = {'currentwidget': '/time/time',
                          'space': True,
                          'plotted_dataset_names': plotted_dataset_names}

        for gname in graphics_names:
            arrange_fields['currentwidget'] = gname
            self.ops.append(
                document.OperationToolsPlugin(ArrangePlugin(),
                                              arrange_fields.copy()))

        self.apply_ops('PlotDataset: Arrange')

    def apply(self, cmd, fields):
        """Do the work of the plugin.
        cmd: veusz command line interface object (exporting commands)
        fields: dict mapping field names to values
        """
        self.ops = []
        doc = cmd.document
        self.doc = doc
        self.cmd = cmd

        cur = fields['currentwidget']
        g = self.doc.resolveWidgetPath(None, cur)
        g = utils.searchFirstOccurrence(g, 'graph')
        if g is None or g.typename != 'graph':
            raise plugins.ToolsPluginException(
                'You should run this tool on a graph')

        logging.debug('Plotting:', fields['x'], fields['y'])
        cnames = {}
        doc = cmd.document
        t = time()
        gnames = []
        n = 0
        valid_x, valid_y = [], []
        for i, x in enumerate(fields['x']):
            y = fields['y'][i]
            if not (x in doc.data and y in doc.data):
                continue
            valid_x.append(x)
            valid_y.append(y)
            n += self.validate_datasets((doc.data[x], doc.data[y]))
        if n > 0:
            # TODO: reload document!
            pass

        for i, x in enumerate(valid_x):
            y = valid_y[i]
            logging.debug('plotting value:', y, 'data:', doc.data[y])
            ds = doc.data[y]
            # If the ds is recursively derived, substitute it by its entry
            if not hasattr(ds, 'm_smp'):
                logging.debug('Retrieving ent', y)
                ds = doc.model.tree.traverse(y)

            # Get the curve and axis name
            cname, ax_name, ax_lbl, key = dataset_curve_name(ds, y, doc)

            gname = g.path

            self.auto_percent(ds, y, gname, ax_name)

            self.initCurve(name=cname, xData=x, yData=y,
                           yAxis=ax_name, axisLabel=ax_lbl, graph=gname, key=key)
            if gname not in gnames:
                gnames.append(gname)
            cnames[y] = cname

        self.apply_ops('PlotDataset: Customize')
        if len(fields) > 0 and len(fields['y']) > 0:
            self.arrange(gnames, fields['y'])

        return cnames

# INTENTIONALLY NOT PUBLISHED
# plugins.toolspluginregistry.append(PlotDatasetPlugin)
# plugins.toolspluginregistry.append(DefaultPlotPlugin)

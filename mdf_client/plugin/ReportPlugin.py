#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Tools and plugins for Veusz, providing MDF Thermal Analysis functionality"""
import os
import datetime
from mdf_canon.logger import get_module_logging
import veusz
import veusz.plugins as plugins
import veusz.document as document
from veusz.document import CommandInterpreter
from .. import parameters as params
from .FieldMisuraNavigator import FieldMisuraNavigator
from . import PlotPlugin
from .utils import OperationWrapper
from .report_plugin_utils import wr, render_meta
from veusz.document.operations import OperationWidgetDelete
from mdf_client.clientconf import confdb
from .ShapesPlugin import standards, list_sample_shapes
 
logging = get_module_logging(__name__)


class ReportPlugin(OperationWrapper, plugins.ToolsPlugin):
    menu = ('MDF', 'Create Sample Report')
    # unique name for plugin
    name = 'MDF Report'
    # name to appear on status tool bar
    description_short = 'Create Report for MDF sample'
    # text to appear in dialog box
    description_full = 'Create a Report page for a MDF sample'
    templates = {}

    def __init__(self, sample=None, template_file_name='default.vsz', measure_to_plot='d', image_dataset='profile',
                 available_measures=['d'], available_images=['profile', 'frame']):
        """Make list of fields."""
        if 'T' in available_measures:
            available_measures.remove('T')
        if measure_to_plot not in available_measures:
            measure_to_plot = available_measures[0]
        if not available_images:
            available_images.append('')
        if image_dataset not in available_images:
            image_dataset = available_images[0]
        
        self.templates = {}
        for path in (params.pathArt, confdb['templates']):
            for fn in os.listdir(path):
                if not fn.endswith('.vsz'):
                    continue
                if not fn.startswith('report_'):
                    continue
                self.templates[fn] = path
        
        self.fields = [
            FieldMisuraNavigator(
                "sample", descr="Target sample:", depth='sample', default=sample),
            plugins.FieldCombo('measure_to_plot', 'Measure to plot',
                              default=measure_to_plot, items=available_measures),
            plugins.FieldCombo('image_dataset', 'Image sequence, if any',
                              default=image_dataset, items=available_images),
            plugins.FieldCombo('template_file_name', 'Template filename',
                               default=template_file_name, items=list(self.templates.keys())),
        ]

    def apply(self, command_interface, fields):
        doc = command_interface.document
        self.doc = doc
        command_interpreter = CommandInterpreter(doc)
        smp_path0 = fields['sample'].path
        prefix, smp_path = smp_path0.split(':')
        vsmp = smp_path.split('/')
        smp_name = vsmp[-1]  # sample name
        smp_path = '/' + '/'.join(vsmp)  # cut summary/ stuff
        report_path = '/' + prefix + '_' + smp_path[1:].replace('/', '_') + '_report'
        logging.debug(smp_path, smp_name, report_path)
        test = fields['sample'].linked
        from .. import filedata
        if not filedata.ism(test, filedata.LinkedMisuraFile):
            logging.debug(type(test), test)
            raise plugins.ToolsPluginException(
                'The target must be MDF test file')
        # Search for test name
        uk = test.prefix + 'unknown'
        kiln = getattr(test.conf, 'kiln', False)
        if not kiln:
            raise plugins.ToolsPluginException('No Kiln configuration found.')
        instr = getattr(test.conf, test.instrument, False)
        if not instr:
            raise plugins.ToolsPluginException(
                'No measure configuration found.')
            
        # Delete an existing report page to refresh on second run
        try:
            page = doc.resolveWidgetPath(None, report_path)
            op = OperationWidgetDelete(page)
            self.doc.applyOperation(op)
        except:
            pass
        measure = instr.measure
        sample = getattr(instr, smp_name)
        
        tpath = self.templates.get(fields['template_file_name'], False)
        if not tpath:
            d = fields['template_file_name']
        else:
            d = os.path.join(tpath, fields['template_file_name'])
        logo = confdb['rule_logo']
        if not os.path.exists(logo):
            logo = os.path.join(params.pathArt, 'logo.png')
        tpl = open(d, 'r').read().replace("u'report'", "u'{}'".format(report_path[1:]))
        tpl = tpl.replace('$prefix$', prefix)
        command_interpreter.run(tpl)
        
        std = 'MDF'
        if 'Standard: MDF 3' in tpl:
            std = 'Misura3'
        elif 'Standard: MDF' in tpl:
            std = 'MDF'
        elif 'Standard: Misura 3' in tpl:
            std = 'Misura3'
        elif 'Standard: CEN/TS' in tpl:
            std = 'CEN/TS' 
        
        # list of handle,opt
        shape_options = list_sample_shapes(sample, std) 
        
        page = doc.resolveWidgetPath(None, report_path)
        # Substitutions
        tc = kiln['curve']
        lbl_tc = page.getChild('tc').getChild('lbl_tc')
        if len(tc) <= 8:
            self.toset(lbl_tc, 'dataset', test.prefix + 'kiln/T')
        else:
            self.toset(lbl_tc, 'hide', True)
            
        zt = test.conf['zerotime']
            
        msg = wr('Measure', measure['name'])
        msg += '\\\\' + wr('Sample', sample['name'])
        self.toset(page.getChild('name'), 'label', msg)
        
        zd = datetime.date.fromtimestamp(zt)
        dur = datetime.timedelta(seconds=int(measure['elapsed']))
        sdur = '{}'.format(dur)
        msg = wr('Date', zd.strftime("%d/%m/%Y"))
        msg += r'\\' + wr('Duration', sdur) + r'\\'
        msg += render_meta(measure, inter=r'\\', zt=zt, time=True, value=True)
        self.toset(page.getChild('metadata'), 'label', msg)
        
        oname = measure.desc.get('operator', {'current': False})['current']
        if oname:
            self.toset(
                page.getChild('operator'), 'label', wr('Operator', oname))
        self.toset(
            page.getChild('uid'), 'label', wr('UID', measure['uid'], 34))
        self.toset(
            page.getChild('serial'), 'label', wr('Serial', test.conf['eq_sn']))
        
        ksn = 'undefined'
        if 'ksn' in kiln:
            ksn = kiln['ksn']
        
        self.toset(
                page.getChild('furnace'), 'label', wr('Furnace', ksn))
        self.toset(
            page.getChild('logo'), 'filename', logo)
        
        is_hsm = test.instrument == 'hsm'
        inter = ' ' if is_hsm else r'\\'
        notfound = 'None' if is_hsm else False
        lbl_shapes = render_meta([s[1] for s in shape_options], inter=inter, zt=zt,
                                 time=False, value=not is_hsm, notfound=notfound)
        self.toset(page.getChild('lbl_shapes'), 'label', lbl_shapes)
            
        should_draw_shapes = bool(fields['image_dataset']) and is_hsm
        
        if should_draw_shapes:
            for sh, opt in shape_options:
                pt = opt['current']
                shn = opt['name']
                if pt['time'] in ['None', None, '']:
                    msg += 'None\\\\'
                    self.toset(page.getChild('lbl_' + sh), 'label', shn + ', ?')
                    continue
                image_reference = {'dataset': smp_path + '/' + fields['image_dataset'],
                                   'filename': test.params.filename,
                                   'target': pt['time']}
                imgref = page.getChild(sh)
                self.dict_toset(imgref, image_reference)
                T = '%i{{\\deg}}C' % round(pt['temp'], 0)
                img_label = 'lbl_' + sh
                self.toset(page.getChild(img_label), 'label', shn + ', ' + T)
            
            self.dict_toset(page.getChild('initial'), {'dataset': smp_path + '/' + fields['image_dataset'],
                                                       'filename': test.params.filename, 'target': 0})
            T = doc.data.get(test.prefix + smp_path[1:] + '/T', False)
            if T is False:
                T = doc.data.get(test.prefix + 'kiln/T')
            T = T.data[0]
            self.toset(page.getChild('lbl_initial'), 'label',
                       'Initial, %.2f{{\\deg}}C' % T)

        # Thermal cycle plotting
        from ..procedure.thermal_cycle import ThermalCyclePlot
        from ..procedure.model import clean_curve
        graph = report_path + '/tc'
        p0 = test.prefix + sample['fullpath'][1:]
        cf = {'graph': graph, 'xT': p0 + 'reportxT',
              'yT': p0 + 'reportyT', 'xR': p0 + 'reportxR', 'yR': p0 + 'reportyR'}
        ThermalCyclePlot.setup(command_interface, with_progress=False, topMargin='4.7cm', **cf)
        tc = clean_curve(tc, events=False)
        ThermalCyclePlot.importCurve(command_interface, tc, **cf)
        cf = {'Label/font': 'Bitstream Vera Sans', 'Label/size': '6pt',
              'TickLabels/font': 'Bitstream Vera Sans',
              'TickLabels/size': '6pt'}
        self.dict_toset(doc.resolveWidgetPath(None, graph + '/y'), cf)
        cf['datascale'] = 60.
        self.dict_toset(doc.resolveWidgetPath(None, graph + '/y1'), cf)
        self.toset(doc.resolveWidgetPath(None, graph + '/x'), 'datascale', 1 / 60.)
        self.ops.append(document.OperationToolsPlugin(PlotPlugin.PlotDatasetPlugin(),
                                                      {'x': [test.prefix + 'kiln/T'],
                                                       'y': [smp_path0 + '/' + fields['measure_to_plot']],
                                                       'currentwidget': report_path + '/temp'}
                                                      ))
        
        self.apply_ops()
        axes = {'flex':'Flex', 'flex3':'Flex', 'horizontal':'Dil', 'vertical':'Dil'}
        d = axes.get(instr['devpath'], fields['measure_to_plot'])
        cf.pop('datascale')
        self.dict_toset(doc.resolveWidgetPath(None, report_path + '/temp/ax:' + d), cf)
        self.apply_ops()
        command_interpreter.run("MoveToLastPage()")


plugins.toolspluginregistry.append(ReportPlugin)

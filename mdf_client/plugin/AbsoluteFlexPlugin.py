#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Tools and plugins for Veusz, providing MDF Thermal Analysis functionality"""
import veusz.plugins as plugins
import numpy as np

from .utils import MisuraPluginDataset1D
from mdf_canon.circle import absolute_flex
from mdf_canon import csutil
from . import utils
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__file__)

class AbsoluteFlexPlugin(utils.CachedResultFragment, plugins.DatasetPlugin):

    """Re-calculate Absolute Fleximeter displacement from calibration factors and thrre-point displacements"""
    # tuple of strings to build position on menu
    menu = ('MDF', 'Absolute Flex')
    # internal name for reusing plugin later
    name = 'AbsoluteFlex'
    # string which appears in status bar
    description_short = 'Absolute Fleximeter'

    # string goes in dialog box
    description_full = (
        'Re-calculate Absolute Fleximeter displacement from calibration factors and thrre-point displacements')
    
    cached_dataset_fields = ['ds_middle','ds_right','ds_left']

    def __init__(self, ds_middle='', ds_right='', ds_left='', right_calibration=25000., 
                 left_calibration=25000., interaxis=72000, smooth=5, smode='Output',ds_out='absflex', ds_radius='absradius'):
        """Define input fields for plugin."""
        self.fields = [
            plugins.FieldDataset('ds_middle', 'Center displacement', default=ds_middle),
            plugins.FieldDataset('ds_right', 'Right displacment', default=ds_right),
            plugins.FieldDataset('ds_left', 'Left displacment', default=ds_left),
            
            plugins.FieldFloat('right_calibration', 'Right-center distance', default=right_calibration),
            plugins.FieldFloat('left_calibration', 'Left-center distance', default=left_calibration),
            plugins.FieldFloat('interaxis', 'Interaxis', default=interaxis),
            
            plugins.FieldInt('smooth', 'Smoothing Window', default=int(smooth)),
            plugins.FieldCombo('smode', descr='Apply Smoothing to', items=[
                               'Input', 'Output'], default=smode),
            plugins.FieldDataset(
                'ds_out', 'Output displacement name', default=ds_out),
            plugins.FieldDataset(
                'ds_radius', 'Output radius name', default=ds_radius),
        ]

    def getDatasets(self, fields):
        """Returns single output dataset (self.ds_out).
        This method should return a list of Dataset objects, which can include
        Dataset1D, Dataset2D and DatasetText
        """
        # raise DatasetPluginException if there are errors
        ds = set([fields['ds_middle'], fields['ds_right'], fields['ds_left'], 
                  fields['ds_out'], fields['ds_radius']])
        if len(ds)<5:
            raise plugins.DatasetPluginException(
                'All input/output datasets must differ.')
        if '' in ds:
            raise plugins.DatasetPluginException(
                'All input/output dataset fields must be initialized.')
        self.ds_out = MisuraPluginDataset1D(fields['ds_out'])
        self.ds_out.unit = 'micron'
        self.ds_out.old_unit = 'micron'
        self.ds_out.label = 'AbsFlex'
        self.ds_out.m_var = 'd'
        self.ds_radius = MisuraPluginDataset1D(fields['ds_radius'])
        self.ds_radius.unit = 'micron'
        self.ds_radius.old_unit = 'micron'
        self.ds_out.label = 'AbsFlexRadius'
        self.ds_out.m_var = 'r'
        # return list of datasets
        return [self.ds_out, self.ds_radius]
    
    def calculate_absolute_flex(self):
        smooth = self.cached_fields['smooth']
        smode = self.cached_fields['smode']
        
        M = self.cached_datasets['ds_middle']
        R = self.cached_datasets['ds_right']
        L = self.cached_datasets['ds_left']
        
        # Smooth input curves
        if smooth > 0 and smode == 'Input':
            M = csutil.smooth(M, smooth, 'hanning')
            R = csutil.smooth(R, smooth, 'hanning')
            L = csutil.smooth(L, smooth, 'hanning')
        
        #TODO: convert to array math
        out = []
        rad = []
        Rcal = self.cached_fields['right_calibration']
        Lcal = self.cached_fields['left_calibration']
        chord = self.cached_fields['interaxis']
        for i in range(len(M)):
            res = absolute_flex(M[i],R[i],L[i], Rcal, Lcal, chord)
            out.append(res['d'])
            rad.append(res['radius'])
        
        out = np.array(out)
        rad = np.array(rad)
            
        # Smooth output curve
        if smooth > 0 and smode == 'Output':
            out = csutil.smooth(out, smooth, 'hanning')
            rad = csutil.smooth(rad, smooth, 'hanning')
            
        return out,rad
            
    
    def updateDatasets(self, fields, helper):
        """Calculate coefficient
        """
        if self.is_cache_dirty(fields, helper):
            out, rad = self.calculate_absolute_flex()
            self.cached_result = [out,rad]
        else:
            out,rad = self.cached_result

        self.ds_out.update(data=out)
        self.ds_radius.update(data=rad)
        return [self.ds_out]

    


# add plugin classes to this list to get used
plugins.datasetpluginregistry.append(AbsoluteFlexPlugin)

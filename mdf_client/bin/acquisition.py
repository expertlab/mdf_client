#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
from mdf_canon.logger import get_module_logging
from mdf_client import acquisition, from_argv, address_from_argv, iutils, configure_logger
from mdf_client.network import normalize_address
import multiprocessing
from veusz import qtall
import setproctitle

logging = get_module_logging(__name__)

    
def run_acquisition_app():
    setproctitle.setthreadtitle(f"MdfClient: Acquisition")
    qtall.app = iutils.initApp("MDF Acquisition")
    o = iutils.getOpts()
    logging.debug('Passed options', o)
    app = iutils.app
    mw = acquisition.LiveAcquisitionWindow()
    block = acquisition.BlockadeFilter()
    app.installEventFilter(block)
    mw.show()
    if o['-h']:
        mw.set_addr(address_from_argv())
    configure_logger('acquisition.log')
    sys.exit(app.exec_())


if __name__ == '__main__':
    multiprocessing.freeze_support()
    run_acquisition_app()

#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from mdf_client import iutils, conf
from mdf_client.clientconf import confdb
import multiprocessing

    
def run_conf_app():
    logging.debug('initApp')
    iutils.initApp("MDF Conf")
    logging.debug('done initApp', confdb['recent_server'])
    o = iutils.getOpts()
    logging.debug('Passed options', o)
    fp = o['-o']
    mc = conf.MConf(fixed_path=fp)
    if o['-h']:
        mc.setAddr(o['-h'])
    mc.menu.show()
    mc.show()
    sys.exit(iutils.app.exec_())


if __name__ == '__main__':
    multiprocessing.freeze_support()
    run_conf_app()

#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
from mdf_client import iutils, configure_logger
from mdf_client import graphics
from mdf_client.clientconf import confdb, activate_plugins
#import veusz.utils.vzdbus as vzdbus 
#import veusz.utils.vzsamp as vzsamp
import multiprocessing

def run_graphics_app():
    activate_plugins(confdb)
    app = graphics.GraphicsApp()
# 	iutils.initApp(qapp=app)
# 	vzdbus.setup()
# 	vzsamp.setup()
# 	iutils.initNetwork()
    iutils.initTranslations(app)
    app.startup()
    iutils.initRegistry()
    configure_logger('graphics.log')
    app.exec_()
# 	csutil.stop_profiler(mw)

if __name__ == '__main__':
    multiprocessing.freeze_support()
    run_graphics_app()

#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
import os
import json
from time import time
from mdf_canon.csutil import rate_limit
from mdf_canon.logger import get_module_logging
from mdf_client import qt, single_application, live
import multiprocessing
from collections import defaultdict
from veusz import qtall
import setproctitle
from mdf_client.clientconf import open_confdb, confdb
from mdf_client import configure_logger, iutils, open_url, network, sync
from mdf_client.acquisition.acquisition import tray_menu_actions
logging = get_module_logging(__name__)

saved_set = set()


class IntervalNotExpired(BaseException):
    pass


def not_expired(s, *a, **k):
    raise IntervalNotExpired(str(s))


class Meridian(qt.QtWidgets.QMainWindow):
    download_thread = False
    iteration = 0
    interval = 1
    times = defaultdict(lambda: 0)
    _server = None

    def __init__(self):
        super().__init__()
        self.setWindowTitle('MDF Meridian')
        self.setWindowIcon(iutils.theme_icon('meridian'))
        self.sync = sync.SyncWidget()
        self.setCentralWidget(self.sync)
        
        self.pause = qt.QtWidgets.QPushButton('||')
        self.sync.setCornerWidget(self.pause)
        self.pause.setToolTip('Pause/Go')
        self.pause.toggled.connect(self.set_pause)
        self.pause.setCheckable(True)
        self.pause.setChecked(False)
        self.setWindowFlags(qt.Qt.WindowTitleHint | qt.Qt.WindowMinimizeButtonHint | qt.Qt.WindowMaximizeButtonHint);
        
        self.single = single_application.SingleApplication()
        self.single.check_new_instance(os.path.join(confdb['logdir'], 'meridian.lock'), ask=False)
        
        self.tray_icon = qt.QtWidgets.QSystemTrayIcon(self)
        self.tray_icon.setIcon(iutils.theme_icon('meridian'))
        tray_menu_actions(self, title='MDF Meridian')
        self.tray_icon.show()
        
        self.timer = qt.QtCore.QTimer()
        self.timer.setInterval(self.interval * 1000)
        self.timer.timeout.connect(self.check)
        self.show()
        self.timer.start()
       
    def enterEvent(self, ev):
        self.timer.stop()
        self.set_pause(1)
        
    def leaveEvent(self, ev):
        self.timer.start()
        self.set_pause(0)
        
    def set_pause(self, paused=-1):
        if paused < 0:  # invert current state
            paused = not self.pause.isChecked()
        self.pause.setChecked(paused)
        if paused:
            self.timer.stop()
        else:
            self.timer.start()

    def execute_remote(self, event):
        open_url(event)
    
    # @property
    def server(self) -> object:
        global confdb
        if self._server:
            try:
                self._server['name']
            except:
                logging.debug('Connection lost, retry...')
                self._server = None
        if not self._server:
            entry = confdb['recent_server'][-1]
            address, user, password = entry[:3]
            try:
                err, self._server = network.connection(address, user, password)
            except:
                return None
            if err < 0:
                return None
        if not self._server:
            return None
        if not self.sync.server or (self._server.conn_addr != self.sync.server.conn_addr): 
            self.sync.set_server(self._server)
        return self._server
        
    def check_instrument(self) -> bool:
        self.server()  # refresh
        if not confdb['databaseSync']:
            return False
        self.sync.check_for_new_downloads()
    
    @rate_limit(limit=1 / 600, sleep=not_expired)
    def check_remote_database(self) -> bool:
        self.times['check_remote_database'] = time()
        if not confdb['databaseSync']:
            return False
        if not self.server():
            logging.debug('Not connected')
            return False
        elif self.sync.server['isRunning']:
            logging.debug('Not checking remote database: still running')
            return False
        return self.sync.refresh_remote()
    
    def h_get(self, key, *a, **k):
        return self.times[key], 0
    
    @rate_limit(limit=1 / 600, sleep=not_expired)
    def check_confdb(self):
        global confdb
        confdb = open_confdb()
        self.times['call_check_confdb'] = time()
 
    def check(self) -> None:
        global confdb
        cmd = self.single.uptime()
        try:
            self.check_confdb()
        except IntervalNotExpired:
            # print('not expired check_confdb', self.times)
            pass
        try:
            self.check_instrument()
        except IntervalNotExpired:
            # print('not expired check_instrument', self.times)
            pass
        try:
            self.check_remote_database()
        except IntervalNotExpired:
            # print('not expired check_new_tests', self.times)
            pass
        if not cmd:
            return
        cmd = cmd.split(',')
        if len(cmd) > 1:
            event = ','.join(cmd[1:])
            cmd = cmd[0]
            logging.debug('Execute', cmd, event)
            getattr(self, 'execute_' + cmd, lambda ev: 0)(json.loads(event))


def run_meridian_app():
    setproctitle.setthreadtitle(f"MdfClient: Meridian")
    qtall.app = iutils.initApp("MDF Meridian")
    o = iutils.getOpts()
    logging.debug('Passed options', o)
    mw = Meridian()
    # mw.show()
    mw.showMinimized()
    # qt.QtCore.QTimer().singleShot(1000, mw.hide)
    configure_logger('meridian.log')
    app = iutils.app
    app.setQuitOnLastWindowClosed(False)
    sys.exit(app.exec_())


if __name__ == '__main__':
    multiprocessing.freeze_support()
    run_meridian_app()

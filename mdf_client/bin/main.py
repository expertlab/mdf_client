#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
import multiprocessing
from mdf_client.bin.acquisition import run_acquisition_app
from mdf_client.bin.graphics import run_graphics_app
from mdf_client.bin.conf import run_conf_app
from mdf_client.bin.browser import run_browser_app
from mdf_client.bin.conf import run_conf_app
from mdf_client.bin.meridian import run_meridian_app

import code
import numpy as np
from mdf_canon.csutil import dumps
from mdf_client import from_argv, default
connect = default
from time import sleep, time


def reconnect(m, limit=20):
    retry = 0
    while m['name'] is None and retry < limit:
        retry += 1
        sleep(3)
        print('RETRY', retry, 'of', limit)


def run_console(): 
    print('RUN CONSOLE')
    m = from_argv()
    
    wait = lambda: reconnect(m)
    
    msg = '################\n' * 3
    if not m:
        msg += '\nConnection to localhost failed. \nRun connect(host,port,user,password).\n'
    else:
        msg += 'Connection to MDF server at {} available as object "m".'.format(m.addr)
    msg += '################\n' * 3        
    glo = globals()
    glo.update(locals())
    print('interact...')
    code.interact(msg, local=glo)
    print('END CONSOLE')

    
def export_config():
    from mdf_client import iniconf
    m = from_argv()
    print('Exporting configuration to ./configuration.ini')
    iniconf.save(m, './configuration.ini')
    print('Exported to ./configuration.ini')
    

# TODO: add --console for command line environment
# TODO: migrate to getopts
app_map = {'--acquisition': run_acquisition_app,
           '--live': run_acquisition_app,
           '--graphics': run_graphics_app,
           '--browser': run_browser_app,
           '--conf': run_conf_app,
           '--cmd': run_console,
           '--export': export_config,
           '--meridian': run_meridian_app}


def run_misura_app():
    for a in sys.argv:
        if a in app_map:
            app = app_map[a]
            sys.argv.remove(a)
            app()
            return
    # if no argument can be recogize, just run the browser
    run_browser_app()

    
if __name__ == '__main__':
    multiprocessing.freeze_support()
    run_misura_app()

#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
import os

from mdf_client import iutils, browser, live, configure_logger
import multiprocessing


def list_files():
    a = filter(lambda e: len(e) > 4 and (e[:4] in ('http', 'mdf:') or e[-3:] in ('.h5', 'mdf')), sys.argv)
    a = list(a)
    a1 = []
    for f in a:
        # Handle multiple docs
        if f[:4] in ('http', 'mdf:'): 
            if f.count('|') > 0:
                f = f.split('|')
                for uid in f[1:]:
                    a1.append(f[0] + '|' + uid)
            else:
                a1.append(f)
        else:
            a1.append(os.path.abspath(f))
    return a1


def run_browser_app():
    a = list_files()
    iutils.initApp("MDF Browser")
    live.registry.toggle_run(False)
    app = iutils.app

    mw = browser.MainWindow()
    
    for f in a:
        mw.open_file(f)
    mw.show()
    configure_logger('browser.log')
    sys.exit(app.exec_())


if __name__ == '__main__':
    multiprocessing.freeze_support()
    run_browser_app()

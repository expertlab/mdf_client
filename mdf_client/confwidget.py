#!/usr/bin/python
# -*- coding: utf-8 -*-
import functools

from mdf_client.qt import QtWidgets, QtGui, QtCore

from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from mdf_canon.plugin import dataimport
from mdf_canon.indexer import indexer

from . import _
from . import iutils
from .clientconf import confdb, settings, default_misuradb_path


class Path(QtWidgets.QWidget):
    sig_newDb = QtCore.pyqtSignal()
    sig_save = QtCore.pyqtSignal()
    
    def __init__(self, path, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.lay = QtWidgets.QHBoxLayout()
        self.setLayout(self.lay)
        self.lay.addWidget(QtWidgets.QLabel(_('Configuration File:')))
        self.line = QtWidgets.QLineEdit(self)
        self.line.setText(path)
        self.lay.addWidget(self.line)
        self.button = QtWidgets.QPushButton(self)
        self.button.setText(_('Open'))
        self.lay.addWidget(self.button)
        self.button.clicked.connect(self.change)

        self.btn_reload = QtWidgets.QPushButton(self)
        self.btn_reload.setText(_('Reload'))
        self.lay.addWidget(self.btn_reload)
        self.btn_reload.clicked.connect(self.reload)

        self.btn_save = QtWidgets.QPushButton(self)
        self.btn_save.setText(_('Save'))
        self.lay.addWidget(self.btn_save)
        self.btn_save.clicked.connect(self.save)

    def reload(self):
        self.sig_newDb.emit()

    def save(self):
        check_default_database()
        confdb.save()

    def change(self):
        path = QtWidgets.QFileDialog.getOpenFileName(
            parent=self, caption=_("Client configuration path"))
        if not path:
            return
        self.line.setText(path[0])
        self.reload()


class ClientConf(QtWidgets.QWidget):

    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.lay = QtWidgets.QVBoxLayout()
        self.setLayout(self.lay)
        self.path = Path(confdb.path, self)
        self.lay.addWidget(self.path)
        confdb.load_license()
        from mdf_client import conf
        self.conf = conf.Interface(confdb, confdb, confdb.describe())
        self.lay.addWidget(self.conf)
        self.path.sig_newDb.connect(self.change)

    def change(self):
        path = str(self.path.line.text())
        r = confdb.load(path)
        self.conf.close()
        del self.conf
        from mdf_client import conf
        self.conf = conf.Interface(confdb, confdb, confdb.describe())
        self.lay.addWidget(self.conf)
        settings.setValue('/Configuration', path)


class RecentInterface(object):
    """Common functions for recent elements management"""
    open_new = QtCore.pyqtSignal(str)
    select = QtCore.pyqtSignal(str)
    convert = QtCore.pyqtSignal(str)

    def __init__(self, conf, category):
        super(RecentInterface, self).__init__()
        self.category = category
        self.conf = conf
        self.name = category
        if self.category == 'executed':
            self.name = 'Recently executed'
            self.label = self.name
        elif self.name == 'm3database':
            self.name = 'Misura3 database'
            self.label = self.name
        else:
            self.label = 'Recently opened {}s'.format(self.name.capitalize())
            
    def get_executed_list(self):
        nsl = []
        if not self.conf:
            return nsl
        if not self.conf.index:
            return nsl
        for t in self.conf.index.query(limit=self.conf['hfile']):
            name = '{} ({})'.format(t[indexer.col_name], t[indexer.col_instrument])
            uid = t[indexer.col_uid]
            f = t[indexer.col_file]
            nsl.append([name, f, map(lambda e: str(e), t)])
        
        return nsl

    def getNameSigList(self):
        if self.category == 'executed':
            return self.get_executed_list()
        tab = self.conf['recent_' + self.category]
        logging.debug('getNameSigList', self.category, tab)
        nsl = []
        for i, row in enumerate(reversed(tab)):
            sig = row[0] or ''
            name = row[0] or ''
            if self.category == 'file':
                if row[1]:
                    name = row[1] + ' (' + iutils.shorten(row[0] or '') + ')'
                else:
                    row[1] = ''
            if self.category == 'server':
                name0 = row[0].replace('//', '/').split('/')[1]
                name = row[1] + '@' + name
#               sig='https://%s:%s@%s/RPC' % (row[1],row[2],name0)
            nsl.append([name, sig, row])
        return nsl

    def clear_recent(self):
        assert self.category != 'executed'
        logging.debug('ConfWidget: Clearing recent entries')
        tname = 'recent_' + self.category
        self.conf[tname] = [self.conf[tname][0]] 
        self.conf.save()
        self.conf.sig_rem.emit()

    def new(self, *a):
        assert self.category != 'executed'
        if self.category in ['server']:
            path = QtWidgets.QInputDialog.getText(self, _('Specify a new server address'), _(
                'Address'), text='https://user:user@localhost:3880/RPC')
        else:
            d = self.conf.last_directory(self.category)
            path = QtWidgets.QFileDialog.getOpenFileName(
                self.parent(), _("Open a new ") + self.category, d)
        if not path:
            return
        self.open_new.emit(path[0])
        self.select.emit(path[0])

    def data_import(self, *a):
        assert self.category != 'executed'
        d = self.conf.last_directory(self.category)
        file_filter = ''
        for converter in dataimport.data_importers:
            file_filter += '{} ({});;'.format(_(converter.name), converter.file_pattern.replace(';', ' '))
            print('adding filter', file_filter)
        path = QtWidgets.QFileDialog.getOpenFileName(
            self.parent(), _("Data import"),
            d,
            file_filter)
        self.convert.emit(path[0])


def addNameSigList_to_menu(menu, nsl, emitter=False):
    r = []
    emitter = emitter or menu
    for name, sig, row in nsl:
        p = functools.partial(
            emitter.select.emit, sig)
        a = menu.addAction(name, p)
        a.setToolTip('\n'.join(row))
        r.append(a)
    return r
    

class RecentMenu(RecentInterface, QtWidgets.QMenu):
    """Recent objects menu"""
    open_new = QtCore.pyqtSignal(str)
    select = QtCore.pyqtSignal(str)
    convert = QtCore.pyqtSignal(str)
    server_disconnect = QtCore.pyqtSignal()
    server_shutdown = QtCore.pyqtSignal()
    server_restart = QtCore.pyqtSignal()

    _detached = False
    
    def __init__(self, conf, category, parent=None):
        QtWidgets.QMenu.__init__(self, parent=parent)
        RecentInterface.__init__(self, conf, category)
        self.setTitle(self.label)
        self.setWindowTitle(self.label)
        self.setTearOffEnabled(True)
        self.redraw()
        self.conf.sig_mem.connect(self.redraw)
        self.conf.sig_rem.connect(self.redraw)
        self.aboutToShow.connect(self.redraw)

    def redraw(self):
        self.clear()
        self.setTearOffEnabled(True)
        nsl = self.getNameSigList()
        addNameSigList_to_menu(self, nsl)
        self.addSeparator()
        if self.category != 'executed':
            self.addAction(_("Clear list"), self.clear_recent)
            self.addAction(_("Open") + '...', self.new)
        if self.name == 'file' and len(dataimport.data_importers) > 0:
            self.addAction(_("Import") + '...', self.data_import)
        if self.category == 'server':
            self.addAction(_('Disconnect'), self.server_disconnect.emit)
            self.addAction(_('Restart'), self.server_restart.emit)
            self.addAction(_('Shutdown'), self.server_shutdown.emit)
        self.addSeparator()
        self.addAction(_('Detach'), self.detach)
        
    def detach(self):
        if self._detached:
            self._detached.hide()
            self._detached.close()
        wg = RecentWidget(self.conf, self.category)
        wg.route_select = lambda *a: self.select.emit(*a)
        wg.route_convert = lambda *a: self.convert.emit(*a)
        # wg.select.connect(wg.route_select)
        wg.select.connect(self.select.emit)
        wg.convert.connect(self.convert.emit)
        wg.open_new.connect(self.open_new.emit)
        wg.setWindowTitle(self.windowTitle())
        wg.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        wg.show()
        self._detached = wg


class RecentWidget(RecentInterface, QtWidgets.QWidget):

    """Recent objects list widget"""
    open_new = QtCore.pyqtSignal(str)
    select = QtCore.pyqtSignal(str)
    add_to = QtCore.pyqtSignal(str, int)
    convert = QtCore.pyqtSignal(str)
    
    def __init__(self, conf, category, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        RecentInterface.__init__(self, conf, category)
        self.setWindowTitle(self.label + ':')
        self.lay = QtWidgets.QVBoxLayout()
        self.menu = QtWidgets.QMenu()
        self.lay.addWidget(QtWidgets.QLabel(self.windowTitle()))

        self.list = QtWidgets.QListWidget(self)
        self.list.itemActivated.connect(self.select_item)
        self.list.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.list.customContextMenuRequested.connect(self.show_context_menu)
        self.list.itemSelectionChanged.connect(self.pre_select_item)
        self.conf.sig_mem.connect(self.redraw)
        self.conf.sig_rem.connect(self.redraw)
        self.lay.addWidget(self.list)

        self.open_button = QtWidgets.QPushButton(_('Open Selected'), parent=self)
        self.open_button.setIcon(iutils.theme_icon('document-new'))
        self.open_button.setEnabled(False)
        self.open_button.clicked.connect(self.select_item)
        self.lay.addWidget(self.open_button)
        
        if self.category != 'executed':
            lbl = {'file': ' MDF file'}.get(category, '')
            self.add_button = QtWidgets.QPushButton(_('Open another') + lbl + ' ...', parent=self)
            if self.name == 'server':
                icon = iutils.theme_icon('connect')
            else:
                icon = iutils.theme_icon('system-file-manager')
            self.add_button.setIcon(icon)
            
            self.add_button.clicked.connect(self.new)
            self.lay.addWidget(self.add_button)
        
        if category == 'file' and len(dataimport.data_importers) > 0:
            self.import_button = QtWidgets.QPushButton(
                _('Import from') + '...', parent=self)
            self.import_button.clicked.connect(self.data_import)
            self.lay.addWidget(self.import_button)

        self.redraw()
        self.setLayout(self.lay)
        
    def __len__(self):
        return self.list.count()

    def redraw(self):
        """Updates the list"""
        self.list.clear()
        nsl = self.getNameSigList()
        for name, sig, row in nsl:
            item = QtWidgets.QListWidgetItem(name)
            # Assign to the item userdata the path of the object, which will be
            # emitted in select_item
            item.setData(QtCore.Qt.UserRole, sig)
            item.setToolTip('\n'.join(row))
            self.list.addItem(item)
            
    def pre_select_item(self, item=False):
        if not item:
            item = self.list.currentItem()
        print('Preselect', item)
        if not item:
            self.open_button.setEnabled(False)
            return False
        self.open_button.setEnabled(True)
        return item   
    
    def show_context_menu(self, pt):
        item = self.pre_select_item()
        if not item:
            logging.debug('No item selected: cannot show context menu')
        self.menu.clear()
        self.menu.addAction(_('Open'), self.select_item)
        if self.category != 'executed':
            self.menu.addAction(_('Remove recent'), self.remove_recent)
        if self.category == 'file':
            self.add_to_browser_windows(item)
        
        self.menu.exec_(self.list.mapToGlobal(pt))
        
    def add_to_browser_windows(self, item):
        browser = self.parent().parent().parent().parent().parent().parent().parent()
        self.menu.addSeparator()
        for i, tab in enumerate(browser.list_tabs()[1:]):
            open_function = functools.partial(self.add_to.emit,
                                              item.data(QtCore.Qt.UserRole),
                                              i + 1)
            self.menu.addAction(tab.title, open_function)
        
    def remove_recent(self, item=False):
        assert self.category != 'executed'
        item = self.pre_select_item(item)
        sig = item.data(QtCore.Qt.UserRole)
        logging.debug('Removing recent:', item.text(), sig)
        self.conf.rem(self.category, sig)

    def select_item(self, item=False):
        """Emit the 'select(QString)' signal with the path of the object"""
        item = self.pre_select_item(item)
        if item:
            self.select.emit(
                      item.data(QtCore.Qt.UserRole))
        

class Greeter(QtWidgets.QWidget):

    """Group of recent object widgets, for file, database and server items."""

    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.lay = QtWidgets.QHBoxLayout()
        self.setWindowTitle('Recent data sources')
        self.executed = RecentWidget(confdb, 'executed', self)
        self.lay.addWidget(self.executed)        
        self.file = RecentWidget(confdb, 'file', self)
        self.executed.select.connect(self.file.select)
        self.lay.addWidget(self.file)
        self.database = RecentWidget(confdb, 'database', self)
        self.lay.addWidget(self.database)
        if confdb['m3_enable']:
            self.m3database = RecentWidget(confdb, 'm3database', self)
            self.lay.addWidget(self.m3database)
        # self.server = RecentWidget(confdb, 'server', self)
#        self.lay.addWidget(self.server)

        self.setLayout(self.lay)

        
import os

empty_db_msg = _("""The default database was not configured.
Completed tests cannot be downloaded anywhere.
Please select now a new or existing database file (Ok), 
or Cancel to accept the default path:\n""") + default_misuradb_path
missing_db_msg = _("""The default database was not found in the configured path:\
{}
Please be sure any external/network drive is connected.
Please select now a new or existing database file (Ok), 
or (Cancel) to accept the default path:\n""") + default_misuradb_path
invalid_db_msg = _('The MDF database cannot reside within the installation folder:\
\n\n{}\n\t resides in: \n{}\n\n\
Please choose another file path, otherwise the database will be wiped every time MDF is updated.')


def check_default_database():
    from . import parameters as params
    db = confdb['database']
    ret = True
    if db.startswith(params.pathClient):
        QtWidgets.QMessageBox.critical(None, _('Invalid database path'),
                                   invalid_db_msg.format(db,
                                                        params.pathClient))
        db = ''   
    if not os.path.exists(db):
        if not db:
            r = QtWidgets.QMessageBox.warning(None, _('No default database was configured'), empty_db_msg,
                                          QtWidgets.QMessageBox.Cancel | QtWidgets.QMessageBox.Ok, QtWidgets.QMessageBox.Ok)
        else:
            r = QtWidgets.QMessageBox.warning(None, _('Default database was not found'), missing_db_msg.format(db),
                                          QtWidgets.QMessageBox.Cancel | QtWidgets.QMessageBox.Ok, QtWidgets.QMessageBox.Ok)
        
        if r == QtWidgets.QMessageBox.Cancel:
            confdb['database'] = default_misuradb_path
        else:
            fname = QtWidgets.QFileDialog.getSaveFileName(
                None, 'Choose the default database path',
                default_misuradb_path,
                "SQLite (*.sqlite);;Misuradb (misuradb)",
                options=QtWidgets.QFileDialog.DontConfirmOverwrite)
            if fname and fname[0].startswith(params.pathClient):
                QtWidgets.QMessageBox.critical(None, _('Invalid database path'),
                                           invalid_db_msg.format(fname[0],
                                                                 params.pathClient))
                # Reconfigure
                return check_default_database()
            if fname:
                confdb['database'] = fname[0]
            else:
                confdb['database'] = default_misuradb_path
        confdb.save()
        confdb.create_index(force=True)
    return ret

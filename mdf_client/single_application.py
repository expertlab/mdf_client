#!/usr/bin/python
# -*- coding: utf-8 -*-
from time import time, sleep
import os
import sys
from . import _
from mdf_client.qt import QtWidgets, QtCore
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)


class SingleApplication(QtCore.QObject):
    timepath = ''
    upfront = QtCore.pyqtSignal()

    def close_previous(self, tp):
        logging.debug('Closing previous instance')
        open(tp, 'w').write('close')
        sleep(3)
        if os.path.exists(tp):
            os.remove(tp)
            
    def set_timepath(self, tp):
        os.makedirs(os.path.dirname(tp), exist_ok=True)
        open(tp, 'w').close()
        self.timepath = tp
    
    def check_new_instance(self, tp, ask=True):
        if os.path.exists(tp):
            mt = os.stat(tp).st_mtime
            if time() - mt < 5:
                if not ask:
                    logging.debug('Another instance is running: closing...')
                    self.close_previous(tp)
                    self.set_timepath(tp)
                    return False
                btn = QtWidgets.QMessageBox.critical(None, _('Too many connected instances'),
                                           _(f'There seems to be another active instance'),
                                           QtWidgets.QMessageBox.Open | QtWidgets.QMessageBox.Close | QtWidgets.QMessageBox.Ignore)
                if btn == QtWidgets.QMessageBox.Ignore:
                    os.remove(tp)
                    logging.debug('Removing lock file and proceeding anyway.')
                elif btn == QtWidgets.QMessageBox.Close:
                    self.close_previous(tp)
                    return False
                else:
                    logging.debug('Showing previous instance')
                    open(tp, 'w').write('show')
                    QtWidgets.QApplication.quit()
                    self.close()
                    sys.exit(0)
        self.set_timepath(tp)
        return True
    
    def uptime(self):
        if not self.timepath:
            return
        if not os.path.exists(self.timepath):
            open(self.timepath, 'w').close()
            return
        f = open(self.timepath, 'r+')
        cmd = f.read().replace('\n', '')
        f.seek(0)
        f.truncate()
        if cmd == 'show':
            logging.debug('Received `show` command, raising...')
            self.upfront.emit()
        elif cmd == 'close':
            logging.debug('Received `close` command, exiting...')
            self.detach()
            QtWidgets.QApplication.quit()
            sys.exit(0)
        f.close()
        os.utime(self.timepath, None)
        return cmd
        
    def detach(self):
        if self.timepath and os.path.exists(self.timepath):
            os.remove(self.timepath)
        self.timepath = False


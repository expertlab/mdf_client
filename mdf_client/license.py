#!/usr/bin/python
# -*- coding: utf-8 -*-
"""License update dialog"""
import json
from traceback import format_exc
from . import widgets
from mdf_client.qt import QtWidgets, QtCore
from . import _
from .clientconf import confdb
from . import cloud

from mdf_canon.logger import get_module_logging

import urllib
logging = get_module_logging(__name__)

msg_failed = _("""Could not renew the license.
Please provide the following information to the support:

Hash: {}
Status code: {}
Message: {}""")

reply_errors = {400:'Hash identifier mismatch',
                       402:'Licensing contract expired',
                       403:'Not registered or invalid identifier',
                       500:'Unknown command'}


class License(QtWidgets.QDialog):
    server = False
    license = False
    prefix = ''
    klicense = 'license'
    kidentifier = 'identifier'

    def __init__(self, server=False, parent=None):
        QtWidgets.QDialog.__init__(self, parent=parent)
        self.server = server
        if server:
            self.license = server.license
        else:
            confdb.load_license()
        self.license = confdb if not server else server.license
        keys = ['hash', 'expiry', 'authentic', 'functionalities', 'hardwareDiff']
        if server: 
            keys.append('rates')
        else:
            keys = ['license_' + k for k in keys]
            self.prefix = 'license_'
        
        self.setWindowModality(QtCore.Qt.WindowModal)
        self.resize(500, 200)
        self.lay = QtWidgets.QVBoxLayout()
        
        self.label = QtWidgets.QLabel()
        self.widgets = {}
        for key in keys:
            if not self.license.has_key(key):
                logging.error('Cannot show key', key)
                continue
            val = self.license.gete(key)
            if key == 'hardwareDiff' and not val['current']:
                logging.debug('Hardware is unchanged')
                continue
            if key == 'authentic':
                msg = _('The license is valid') if val['current'] else _('The license is not valid')
                self.setWindowTitle(msg)
            wg = widgets.build(server, self.license, val)
            wg.layout().insertWidget(0, wg.label_widget)
            self.lay.addWidget(wg)
            self.widgets[key] = wg
        
        self.btn_renew = QtWidgets.QPushButton(_("Renew cloud license"), parent=self)
        self.btn_renew.clicked.connect(self.renew_license)
        self.lay.addWidget(self.btn_renew)
        
        self.btn_up = QtWidgets.QPushButton(_("Load a new license"), parent=self)
        self.btn_up.clicked.connect(self.upload_license)
        self.lay.addWidget(self.btn_up)
        
        self.btn_save = QtWidgets.QPushButton(_("Save instrument identifier"), parent=self)
        self.btn_save.clicked.connect(self.save_identifier)
        self.lay.addWidget(self.btn_save)
        
        self.setLayout(self.lay)
        
    def upload_license(self):
        sn = 'client' if not self.server else self.server['eq_sn'] 
        path = QtWidgets.QFileDialog.getOpenFileName(
                self, _("Select the license file"), sn + '.license')
        if not path:
            return False
        self.license[self.prefix + 'license'] = open(path[0], 'r').read()
        
        if self.server:
            st = self.license['status']
        else:
            st = confdb.load_license()
        if st:
            self.license.save()
            QtWidgets.QMessageBox.information(self, _('The new license was accepted.'),
                                          _("Thanks for updating your license.")
                                          )
            self.accept()
            return True
        QtWidgets.QMessageBox.error(self, _('The new license was not accepted.'),
                                _('Please provide a valid license.'))
        return False
    
    def save_identifier(self):
        sn = 'client' if not self.server else self.server['eq_sn'] 
        path = QtWidgets.QFileDialog.getSaveFileName(
                self, _("Save instrument identifier as:"), sn + '.identifier')
        if not path:
            return False
        open(path[0], 'w').write(self.license[self.prefix + 'identifier'])
    
    def renew_license(self):
        
        opt = {'hash':self.license[self.prefix + 'hash'],
               'ident': self.license[self.prefix + 'identifier'],
               'cmd':'renew_client_license'}
        
        if self.server:
            opt['serial'] = self.server['eq_sn']
            opt['v_server'] = self.server.support['versionString']
            opt['hardwareDiff'] = self.license['hardwareDiff']
            opt['cmd'] = 'renew_server_license'
        
        try: 
            content = cloud.query(opt).read()
        except urllib.error.HTTPError as err:
            logging.error(format_exc())
            if err.code not in reply_errors:
                raise err
            r = {'statusCode':err.code, 'compression':False, 'body':json.dumps(reply_errors[err.code])}
        except:
            logging.error(format_exc())
            QtWidgets.QMessageBox.warning(self, _('Failed connection'),
                                      _('Please check your internet connection to:\n') + confdb['license_url'])
            self.reject()
            return False
        
        try:
            print(content)
            r = json.loads(content)
            print(r)
            assert 'statusCode' in r
            assert 'body' in r
        except:
            if not r:
                logging.error(r, format_exc())
                QtWidgets.QMessageBox.warning(self, _('Invalid reply'),
                                      _('Please contact support.'))
        body0 = r['body'] 
        if r.get('compression', True):
            body0 = cloud.gzinflate(body0)
        body = json.loads(body0)
        if r['statusCode'] != 200:
            QtWidgets.QMessageBox.warning(self, _('License renewal failed'),
                    msg_failed.format(opt['hash'], repr(r['statusCode']), body0))
        else:
            oldlicense = self.license[self.prefix + 'license']
            self.license[self.prefix + 'license'] = body
            if not self.server:
                self.license.load_license()
            if self.license[self.prefix + 'status']:
                self.license.save('default')
                QtWidgets.QMessageBox.information(self, _('License renewal succeeded'),
                                              _('The license was successfully renewed'))
                self.accept()
                return True
            else:
                logging.debug('Renewal failed!', r)
                self.license[self.prefix + 'license'] = oldlicense
                msg = msg_failed.format(opt['hash'], r['statusCode'], body)
                msg = _('The license was not accepted.\n') + msg
                QtWidgets.QMessageBox.information(self, _('License rejected'), msg)
                self.reject()
                return False


def check_server_license(server):
    r = True
    opt = {'serial': server['eq_sn']}
    if not server.has_child('license'):
        logging.error('No license object!')
        r = False
        cloud.ping(opt)
    elif server.license['status']:
        logging.debug('License is valid.')
        r = True
        opt['hash'] = server.license['hash']
        cloud.ping(opt)
    else: 
        wg = License(server)
        wg.exec_()
        r = server.license['status']
    return r

        

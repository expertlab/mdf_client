#!/usr/bin/python
# -*- coding: utf-8 -*-
try:
    from PyQt5 import sip
except:
    import sip
import os
from time import sleep
from traceback import print_exc
from mdf_canon.csutil import isWindows
from . import network
from mdf_canon.logger import get_module_logging
from .network import normalize_address
from .parameters import determine_path
API_NAMES = ["QDate", "QDateTime", "QString",
             "QTextStream", "QTime", "QUrl", "QVariant"]
API_VERSION = 2
for name in API_NAMES:
    sip.setapi(name, API_VERSION)
from mdf_client.qt import QtCore, QtGui

logging = get_module_logging(__name__)


def _(text, disambiguation=None, context='misura'):
    """Veusz-based translatable messages tagging."""
    return QtCore.QCoreApplication.translate(context, text,
                                             disambiguation=disambiguation)

# CONNECTION SHORTCUTS


def open_url(url):
    logging.debug('Opening url:', url)
    oldpath = os.environ.pop('LD_LIBRARY_PATH', "")
    QtGui.QDesktopServices.openUrl(QtCore.QUrl(url))
    os.environ['LD_LIBRARY_PATH'] = oldpath


def default(host='127.0.0.1', port=3880, user='admin', password='admin', mac=''):
    if '.' not in host:
        if host == '127':
            host = '127.0.0.1'
        else:
            try:
                host = '192.168.0.' + int(host)
            except:
                pass
    addr = 'https://{}:{}/RPC'.format(host, port)
    ret, rem = network.getConnection(addr, user, password, mac=mac, smart=True)
    if not ret:
        return False
    return rem


def address_from_argv():
    import sys
    import getopt
    print('from argv %s', (sys.argv))
    opts, args = getopt.getopt(sys.argv[1:], 'h:p:u:w:r:')
    r = {'-h': '127.0.0.1', '-p': 3880,
         '-u': 'admin', '-w': 'admin', '-r': 1}
    for opt, val in opts:
        r[opt] = val
    host = normalize_address(r['-h'], r['-u'], r['-w'], r['-p'])[-4]
    return host


def from_argv():
    """Get connection from command line arguments"""
    import sys
    import getopt
    import logging
    logging.debug('from argv %s', (sys.argv))
    opts, args = getopt.getopt(sys.argv[1:], 'h:p:u:w:r:')
    r = {'-h': '127.0.0.1', '-p': 3880,
         '-u': 'admin', '-w': 'admin', '-r': 1}
    for opt, val in opts:
        r[opt] = val
    limit = int(r['-r'])
    retry = 0
    m = None
    while retry <= limit:
        try:
            m = default(host=r['-h'], port=r['-p'], user=r['-u'], password=r['-w'])
        except KeyboardInterrupt:
            raise
        except:
            pass
        if not m:
            retry += 1
            print('RETRY', retry, limit)
            print_exc()
            sleep(3)
            continue
        break
    print('from_argv', m)
    return m


def configure_logger(log_file_name=False, logdir=None, logsize=None, level=None):
    import logging
    import logging.handlers
    from mdf_client.clientconf import confdb
    from mdf_client.units import Converter
    from mdf_canon.logger import formatter

    class ClosedFileHandler(logging.handlers.RotatingFileHandler):
        """Close and re-open the output file for each record. If an error occurrs, 
        buffer the record and retry at next emit.
        Overcome Windows errors when logging from multiple threads or processes.
        """        

        def __init__(self, *a, **k):
            super(self.__class__, self).__init__(*a, **k)
            self.buffer = []
            
        def emit(self, record):
            if not self.stream:
                self.stream = self._open()
            self.buffer.append(record)
            try:
                while len(self.buffer):
                    super(self.__class__, self).emit(self.buffer.pop(0))
            except:
                print('Logging error', len(self.buffer), record)
            self.close()

    HandlerClass = ClosedFileHandler if isWindows else logging.handlers.RotatingFileHandler
    
    root = logging.getLogger()
    logdir = logdir or confdb['logdir']
    if not os.path.exists(logdir):
        os.makedirs(logdir)

    logsize = logsize or Converter.convert('kilobyte', 'byte', confdb['logsize'])
    
    if log_file_name:
        log_file = os.path.join(logdir, log_file_name)
        rotating_file_handler = HandlerClass(log_file, maxBytes=logsize,
                                                                     backupCount=confdb['lognumber'])
        root.addHandler(rotating_file_handler)
    
    for h in root.handlers:
        h.setFormatter(formatter)
    level = level or confdb['loglevel']
#     level = 0
    root.setLevel(level)

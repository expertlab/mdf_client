# -*- mode: python -*-
"""SPEC File for PyInstaller - Windows Binary"""
from glob import glob
import os.path
from mdf_client.parameters import pathClient
from veusz import utils as vutils
# Check cv is installed
try:
    import cv2 
except:
    print("OpenCV is not available! Video export is disabled.")

console = True
debug = False
cli = pathClient + '/'
bin = cli + 'bin/'
res = vutils.resourceDirectory + '/'
vzd = res + '../'

rthooks = [cli + '/installer/rthook_pyqt4.py']
# Hidden imports
him = ['mdf_canon', 'mdf_canon.csutil', 'mdf_client', 'mdf_imago','mdf_cloud',
       'cv2', 'scipy.special._ufuncs_cxx',
       'astropy', 'scipy.odr' , 'scipy.odr.odrpack', 'veusz.helpers.qtloops' , 'veusz.helpers.qtmml',
       'veusz.helpers.recordpaint', 'veusz.helpers._nc_cntr', 'veusz.helpers.threed','scipy._lib.messagestream',
       'scipy.special.cython_special',
       'pkg_resources.py2_warn', 'numpy.core._multiarray_umath']

# Excluded imports
exim = ['Tkinter','tkinter','thegram','pylab', 'matplotlib','PySide','PySide2','IPython','notebook','jedi','nbconvert','pydoc', 'astropy']

# VEUSZ ANALYSIS
a = Analysis([bin + 'main.py'],
             pathex=[bin],
             excludes=exim,
             hiddenimports=him,
             runtime_hooks=rthooks)

exename = os.path.join('build', 'mdf.exe')

print(('Building PYZ', exename))
pyz = PYZ(a.pure)
print(('Creating EXE', exename))
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=1,
          name=exename,
          debug=debug,
          strip=None,
          upx=False,
          console=console)#,
          #icon=os.path.join(cli,'art','mdf.ico'))

# remove unneeded plugins
for pdir in ('accessible', 'codecs', 'graphicssystems'):
    a.binaries = [b for b in a.binaries if b[1].find(os.path.join(vzd, 'plugins', pdir)) == -1]


# add necessary documentation, licence
for fn in ('VERSION', 'ChangeLog', 'AUTHORS', 'README', 'INSTALL', 'COPYING'):
    a.binaries += [ (os.path.join("resources", fn), os.path.join(cli, fn), 'DATA') ]

# add various required files to distribution
for name in ['icons/*.png', 'icons/*.ico', 'icons/*.svg', 'examples/*.vsz',
            'examples/*.dat', 'examples/*.csv', 'examples/*.py', 'ui/*.ui', 'widgets/data/*.dat']:
    basedir = os.path.dirname(name)
    for source_path in glob(os.path.join(res, name)):
        fname = os.path.basename(source_path)
        dirname = os.path.basename(basedir)
        parent = os.path.dirname(basedir)
        installed_path = os.path.join(parent, "resources", dirname, fname)
        a.binaries.append((installed_path, source_path, 'DATA'))

# misura4 specific data dirs


def add_binaries(fdir, ddir):
    for fname in os.listdir(fdir):
        source_path = os.path.join(fdir, fname)
        if ddir == "ui":
            parent = os.path.dirname(ddir)
            dirname = os.path.basename(ddir)
            installed_path = os.path.join(parent, "resources", dirname, fname)
        else:
            installed_path = os.path.join(ddir, fname)
        if os.path.isdir(source_path):
            add_binaries(source_path, installed_path)
        else:
            a.binaries.append((installed_path, source_path, 'DATA'))

        
for ddir in ['art', 'i18n', 'ui']:
    fdir = os.path.join(cli, ddir)
    add_binaries(fdir, ddir)

found = -1
for i,b in enumerate(a.datas):
    if b[1].endswith('qt.conf'):
        
        found = i
        break

if found>=0:
    print('Fixing qt.conf', a.datas[found])
    a.datas.pop(found)
    a.datas.append(('PyQt5/qt.conf', '/usr/lib/x86_64-linux-gnu/qt5/qt.conf', 'DATA'))

print(('BINARIES', a.binaries))
          
print(('Collecting', exename))
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=False,
               name=os.path.join('main'))

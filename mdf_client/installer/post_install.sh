#!/bin/bash
TARGET=/opt/mdf/client
LOGGED_USER=`whoami`
DESKTOP=`xdg-user-dir DESKTOP`
APPS="${HOME}/.local/share/applications"
MIME="${HOME}/.config/mimeapps.list"
MENU="${HOME}/.config/menus/applications-kmenuedit.menu"


function permissions {
	P="${1}"
	while [ "${P}" != "/" ]; do
		if [[ -d "${P}" ]]; then
			echo "Checking permissions on ${P}..."
		else
			echo "Target ${P} does not exist"
			P=`dirname "${P}"`
			continue 
		fi
		if [[ -w "${P}" ]]; then
			echo "Permissions on ${P} are ok"
		else
			echo "Fixing permissions for $LOGGED_USER on ${P}"
			sudo chown $LOGGED_USER:sudo "${P}"
			sudo chmod 775 "${P}"
		fi
		P=`dirname "${P}"`;
	done
}


permissions "${TARGET}"
rm -rfv "${TARGET}"
mkdir -pv "${TARGET}"
mv -fv * "${TARGET}"
chmod 777 "${TARGET}" "${TARGET}/mdf.exe"

mkdir -p "${APPS}"

function create_icon {
	MimeType=""
	if [ "${1}" == "graphics" ]; then MimeType="MimeType=application/x-veusz;x-scheme-handler/mdf;"; fi 
	if [ "${1}" == "browser" ]; then	MimeType="MimeType=application/x-hdf;";	fi
	echo Creating desktop icon for $1
	echo "[Desktop Entry]
	Type=Application
	Exec=${TARGET}/mdf.exe --$1 %U
	Name=MDF ${1^}
	GenericName=MDF ${1^}
	StartypNotify=true
	Terminal=false
	Categories=MDF
	Icon=${TARGET}/art/$2
	${MimeType}" > "$APPS/$1.desktop"
	
	chmod +x "$APPS/$1.desktop"
	rm -v "$DESKTOP/$1.desktop"
	ln -s "$APPS/$1.desktop" "$DESKTOP/$1.desktop"
}

echo '<?xml version="1.0"?>
<mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info">
  <mime-type type="x-scheme-handler/mdf">
    <comment>Measurement Development Framework Protocol</comment>
  </mime-type>
</mime-info>
' > els-mdf.xml

xdg-mime install els-mdf.xml

echo '<?xml version="1.0"?>
<mime-info xmlns="http://www.freedesktop.org/standards/shared-mime-info">
  <mime-type type="application/x-hdf">
    <comment>Measurement Development Framework Data</comment>
     	<glob pattern="*.h5"/>
    	<glob pattern="*.mdf"/>
  </mime-type>
</mime-info>
' > els-x-hdf.xml

xdg-mime install els-x-hdf.xml

create_icon acquisition icon.svg
create_icon browser browser.svg
create_icon graphics graphics.svg



ENTRY=" <Menu>
  <Name>Expert Lab Service</Name>
  <Include>
   <Filename>graphics.desktop</Filename>
   <Filename>browser.desktop</Filename>
   <Filename>acquisition.desktop</Filename>
  </Include>
 </Menu>"


CURRENT_MENU=`cat "${MENU}"`

if [ "${CURRENT_MENU}" == "" ]; then
	ENTRY="<Menu>
${ENTRY}
</Menu>"
else
	# Remove last </Menu> tag
	CURRENT_MENU="${CURRENT_MENU%</Menu>}"
	ENTRY="${CURRENT_MENU}${ENTRY}
</Menu>"	
fi  


grep "<Name>Expert Lab Service</Name>" "${MENU}"
if [ "$?" != "0" ]; then
	echo "Updating kmenuedit $?"
	echo $ENTRY
	echo "${ENTRY}" > "${MENU}"
	cat $MENU
else
	echo "No need to update kmenuedit"
fi

echo "Updating mime apps"
mimeapps=`cat "$MIME"|grep -v "\[Added Associations\]"|grep -v "application/x-hdf"|grep -v "application/x-veusz"`

echo "[Added Associations]
application/x-hdf=browser.desktop
application/x-veusz=graphics.desktop
${mimeapps}" > $MIME

cat $MIME


H=`hostname`
VAR="${LOGGED_USER} ${H} = (root) NOPASSWD: /usr/sbin/dmidecode
"
SUDOER=/etc/sudoers.d/$LOGGED_USER
if [ -e $SUDOER ]; then
	grep "${VAR}" $SUDOER
	if [ $?==0 ]; then SUDOER=0; fi
fi

if [ $SUDOER == 0 ]; then
	echo "Sudoers file is OK"
else
	echo "Adding LOGGED_USER to sudores file"
	sudo rm -v $SUDOER
	echo "${VAR}" > /tmp/sudomdf
	sudo chown root:root /tmp/sudomdf
	sudo mv /tmp/sudomdf $SUDOER
	sudo chmod 0444 $SUDOER
fi



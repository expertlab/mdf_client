#!/bin/bash

#set -e
COMMAND="$1"
git config --global --add safe.directory /opt/mdf/mdf_client

MVERSION="5.4"
if [ "$USERPROFILE" = "" ]; then
	CODE_BASE=/opt/mdf
	IS_WINDOWS=0
	PYINST=pyinstaller
else
	CODE_BASE=/opt/mdf
	IS_WINDOWS=1
	PYINST="wine pyinstaller"
fi

CLIENT_DIR=$CODE_BASE/mdf_client/mdf_client
CANON_DIR=$CODE_BASE/mdf_canon/mdf_canon
IMAGO_DIR=$CODE_BASE/mdf_imago/mdf_imago
CLOUD_DIR=$CODE_BASE/mdf_cloud/mdf_cloud
VEUSZ_DIR=$CODE_BASE/veusz

INSTALLER_DIR=$CLIENT_DIR/installer

MAKESELF=/makeself.sh
STARTUP_SCRIPT=$INSTALLER_DIR/post_install.sh
NIGHTLY_SCRIPT=$CANON_DIR/nighlty.sh

DISTRIBUTION_DIR=$INSTALLER_DIR/dist
OUTPUT_MDF_DIR=$DISTRIBUTION_DIR/main
BUILD_IN_PROGRESS_FILE=$INSTALLER_DIR/build_in_progress
LAST_BUILD_STATUS_FILE=$INSTALLER_DIR/last_build_status


if [ "$USERPROFILE" = "" ]; then
	SPEC_FILE=$INSTALLER_DIR/client_linux_pyinst.spec
else
	SPEC_FILE=$INSTALLER_DIR/client_windows_pyinst.spec
fi


if [ "$COMMAND" = 'help' ]; then
    echo Arguments: 
    echo  "compile" to compile without building the self-installer
    echo  "compress" to just compress the binary file and create the self-installer, without building
    echo  "build" to compile and build self-installer \(equals compile+compress\)
    echo  "run <app>" to just run the client `app` without building nor compressing
    echo No arguments will execute "build".
    exit 1
elif [ "$COMMAND" = 'run' ]; then 
    python3 /opt/mdf/mdf_client/mdf_client/bin/$2.py
    exit $? 
# Called by docker_build.sh
elif [ "$COMMAND" != "compress" ]; then
    touch $LAST_BUILD_STATUS_FILE
    if [ -a $BUILD_IN_PROGRESS_FILE ] && [ -z "$1" ]; then
        echo "Build already in progress. File exists: $BUILD_IN_PROGRESS_FILE" 
        exit 0
    fi
    touch $BUILD_IN_PROGRESS_FILE

    ##################
    cd "$INSTALLER_DIR"
    echo "Changes detected."
    echo "Removing old local build..."
    rm -rf "$DISTRIBUTION_DIR"
    echo "Done."
    
    echo "Let's start..."
    
    mkdir "$DISTRIBUTION_DIR"
    mkdir "$OUTPUT_MDF_DIR"
    
    
    ###################
    ###################
    
    $PYINST -y --clean --win-private-assemblies --console "$SPEC_FILE"
    
    if [ $? -ne 0 ]; then
        echo "Error building MDF package."
        rm -vf $BUILD_IN_PROGRESS_FILE
        echo "Pyinstaller error!" > $LAST_BUILD_STATUS_FILE
        exit 1
    fi

     
fi

###################
###################

# Copy the correct license
cp $CODE_BASE/mdf_client/LICENSE.txt $OUTPUT_MDF_DIR/LICENSE
rm -vf "$OUTPUT_MDF_DIR/VERSION"
# Create version stamps
VERSION="$OUTPUT_MDF_DIR/VERSION.txt"
GIT_CLIENT=`git -C "$CLIENT_DIR" log --pretty=format:'%h' -n 1`
GIT_CANON=`git -C "$CANON_DIR" log --pretty=format:'%h' -n 1`
#GIT_IMAGO=`git -C "$IMAGO_DIR" log --pretty=format:'%h' -n 1`
#GIT_CLOUD=`git -C "$CLOUD_DIR" log --pretty=format:'%h' -n 1`
GIT_VEUSZ=`git -C "$VEUSZ_DIR" log --pretty=format:'%h' -n 1`
echo "mdf_client = $GIT_CLIENT" > $VERSION
echo "mdf_canon = $GIT_CANON" >> $VERSION
#echo "mdf_imago = $GIT_IMAGO" > $VERSION
#echo "mdf_cloud = $GIT_CLOUD" >> $VERSION
echo "veusz = $GIT_VEUSZ" >> $VERSION
FSTAMP=`date "+%Y%m%d%H%M"`
STAMP=`date +"%F %T"`
echo "date = $STAMP" >> $VERSION

cp -v $VERSION "$OUTPUT_MDF_DIR/resources/VERSION"

# hack in case of Anaconda python distribution
CONDADIR=`which conda`
if [ $? = 1 ]; then
	CONDADIR=`which python3`
	CONDADIR=`dirname "$CONDADIR"`
	# hack to make svg icons work
	#cp -rv "$CONDADIR"/Lib/site-packages/PyQt5/Qt/plugins/* "$OUTPUT_MDF_DIR/qt5_plugins/"
else 
	CONDADIR=`dirname "$CONDADIR"`
	CONDADIR=`dirname "$CONDADIR"`
	# hack to make svg icons work
	cp -rv "$CONDADIR"/Library/plugins/* "$OUTPUT_MDF_DIR/qt4_plugins/"
	#cp -rv "$CONDADIR"/Lib/site-packages/PyQt5/Qt/plugins/* "$OUTPUT_MDF_DIR/qt5_plugins/"
fi 

cp -v "${CONDADIR}"/Library/bin/mkl_* "${OUTPUT_MDF_DIR}/"
# Allow 4GB memory - already done by pyinstaller
# python "$INSTALLER_DIR/large_address_aware.py" 

# Rename main destination folder to mdf
mv -Tv "${OUTPUT_MDF_DIR}" "${DISTRIBUTION_DIR}/mdf"

if [ $IS_WINDOWS = 0 ]; then
    source /etc/os-release  
    TARWIN="linux"
else 
    TARWIN="win"
    VERSION_ID="w10"
fi
TARNAME="${INSTALLER_DIR}/mdf-client-${MVERSION}-${VERSION_ID}-update-${FSTAMP}-${TARWIN}.tar.gz"
tar -vzcf "${TARNAME}" -C $DISTRIBUTION_DIR/mdf ./mdf.exe
echo "BUILT ${1} ${TARNAME}"

if [ "$COMMAND" = "compile" ]; then
   echo Compiled to "$DISTRIBUTION_DIR/mdf"
   exit 0
fi

if [ $IS_WINDOWS = 0 ]; then
		source /etc/os-release
		old=`pwd`
		cd "${DISTRIBUTION_DIR}/mdf"
		cp "${STARTUP_SCRIPT}" ./post_install.sh
		cp "${NIGHTLY_SCRIPT}" ./nightly.sh
		OUT="${INSTALLER_DIR}/mdf-client-${MVERSION}-${VERSION_ID}-${FSTAMP}.run"
		bash $MAKESELF --bzip2 ./ "${OUT}" "MDF Client Installer: ${MVERSION} ${FSTAMP} ${VERSION_ID}" "./post_install.sh"
		chmod u+x "${OUT}"
		cd $old
else
    $INSTALLER_DIR/sign.sh $OUTPUT_MDF_DIR /output/certificate.crt /output/private.key
	MAKENSIS="C:\Program Files (x86)\NSIS"
	if [ -d "$MAKENSIS" ]; then
		MAKENSIS="$MAKENSIS\makensis.exe"
	else
		MAKENSIS="C:\Program Files\NSIS\makensis.exe"
	fi
	
	rm -v "${INSTALLER_DIR}/installer.exe"
	cp "$INSTALLER_DIR/build/client_windows_pyinst/client_windows_pyinst.exe.manifest" "$INSTALLER_DIR/dist/mdf/"
	cp -r /root/.wine/drive_c/users/root/miniconda3/Lib/site-packages/tables.libs "$INSTALLER_DIR/dist/mdf/"
	cd "$INSTALLER_DIR"
	wine "$MAKENSIS" client_windows_setup.nsi
	# HACK FOR WIN DOCKER
	mkdir -p "/output"
	mv -v "${INSTALLER_DIR}/installer.exe" "/output/mdf-client-${MVERSION}-${VERSION_ID}-${FSTAMP}.exe"
	mv -v ${TARNAME} "/output"
fi



rm -vf $BUILD_IN_PROGRESS_FILE
echo "OK" > $LAST_BUILD_STATUS_FILE

echo "Done!"
echo

#!/usr/bin/env bash

set -e

OUTPUT_DIR="${1:-/output}"
CRT_FILE="${2:-certificate.crt}"
PRIVATE_KEY_FILE="${3:-private.key}"

cd "$OUTPUT_DIR"
EXE_TO_SIGN=$(ls -1 mdf*.exe)
mv "$EXE_TO_SIGN" "$EXE_TO_SIGN.todelete"

echo "Signing $EXE_TO_SIGN ..."
osslsigncode sign -certs "$CRT_FILE" -key "$PRIVATE_KEY_FILE" -n "MDF Client" -i "https://expertlabservice.it" -in "$EXE_TO_SIGN.todelete" -out "$EXE_TO_SIGN"
rm "$EXE_TO_SIGN.todelete"
cd -
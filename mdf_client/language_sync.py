#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Aggiornamento del progetto di traduzione in base al file runtime generato con l'opzione parameters.linguist=True"""
from mdf_canon.logger import Log as logging
import inspect
import re
from mdf_canon import option
from mdf_canon.csutil import iter_package
import os
from collections import defaultdict
from mdf_client.clientconf import default_desc
from mdf_canon import units

langs = ['it', 'de', 'fr', 'es', 'en']

header = """<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="%s">
"""

context_h = """
<context>
    <name>%s</name>
"""

context_f = "</context>"
footer = "</TS>"

entry = """
    <message>
        <source>%s</source>
	<translation>%s</translation>
	<translatorcomment>%s</translatorcomment>
    </message>
"""
u_entry = """
    <message>
        <source>%s</source>
	<translation type="unfinished">%s</translation>
	<translatorcomment>%s</translatorcomment>
    </message>
"""

pathClient = os.path.dirname(__file__)
pathLang = os.path.join(pathClient, 'i18n')


def mescape(s):
    s = escape(s)
# 	s=s.replace('"','&quot;')
    return s


def tag(g, line, o):
    if "<%s>" % g in line:
        return line.split(g + '>')[1][:-2]
    return o


def stats(ctx):
    ct = 0
    done = 0
    undone = 0
    for c, v in ctx.items():
        ct += 1
        for s, m in v.items():
            if m[0] == '':
                undone += 1
            else:
                done += 1
    return [ct, done, undone]


def update(lang, ctx, base='misura'):
    """Add already translated values to the context `ctx`."""
    c = False
    s = False
    t = False
    m = ''
    ctx = ctx.copy()
    filename = os.path.join(pathLang, base + '_' + lang + '.ts')
    # No previous translations: nothing to update!
    if not os.path.exists(filename):
        logging.debug(lang.upper(), '\tOriginal translation not found:', filename)
        return ctx
    # Update from found filename
    for line in open(filename, 'r'):
        if '<context>' in line:
            c = False
            s = False
            t = False
            m = ''
        if '<message>' in line:
            s = False
            t = False
            m = ''
        c = tag('name', line, c)
        s = tag('source', line, s)
        t = tag('translation', line, t)
        m = tag('translatorcomment', line, m)
        if '</message>' in line and c and s:
            if c not in ctx:
                ctx[c] = {}
            s = mescape(s)
            if not t:
                t = ''
            t = mescape(t)
# 			print '\tfound translation:',c,s,t
            ctx[c][s] = (t, m)
    return ctx


def write_ts(lang, ctx):
    """Output context to .ts formatted file"""
    filename = os.path.join(pathLang, 'misura_' + lang + '.ts')
    out = open(filename, 'w')
    out.write(header % lang)
    for c, ent in ctx.items():
        out.write(context_h % c)
        logging.debug('\tContext:', c)
        for s, e in ent.items():
            if e[0] == '':
                out.write(u_entry % (s, e[0], e[1]))
            else:
                out.write(entry % (s, e[0], e[1]))
        out.write(context_f)
    out.write(footer)

# this is the package we are inspecting -- for example 'email' from stdlib


autodoc_dir = '/tmp/mdf_autodoc'

base_options = {}


def render_value(opt, key, pad=0):
    val = opt[key]
    # if key in ['readLevel', 'writeLevel'] and (val or 0) < 3:
    #    return ''
    if isinstance(val, (list, tuple)):
        val = sorted([str(v) for v in val])
    elif isinstance(val, dict):
        keys = [str(k) + ': ' + str(val[k]) for k in val]
        val = '{' + ', '.join(sorted(keys)) + '}'
    if isinstance(val, (str, int, float)):
        r = str(val)
    else:
        r = repr(val)
    if not r:
        print('Cannot render ', key, opt)
        return r
    r = r.replace('*', '\\*').replace('`', '\\`').replace('|', '\|')
    if key in ('unit', 'csunit'):
        u = units.hsymbols.get(r, None)
        if u:
            return f'``{val}`` {u}'
    elif key == 'attr':
        v = [f":ref:`{a} <option_attribute_{a}>`" for a in sorted(val)]
        return ', '.join(v)
    elif key == 'current' and opt['type'] == 'Script':
        r1 = '..  code-block::\n    :caption: Script code\n    \n    '
        r1 += r.replace('\n', '\n    ')
        r1 += '\n\n'
        return r1
    elif key == 'current' and pad:
        r = r.lstrip('\n').rstrip('\n')
        r = r.replace('\n\n', '\n').replace('\n', '``' + ('\n' + ' ' * pad) * 2 + '``')
        r = r.rstrip('``') + '``'
        return '``' + r
    return '``' + r + '``'


levels = ['-', '^', '"', '~']

known_attributes = set()
known_types = set()
known_keys = set()


def render_model_elements(known, dpath, etype='type', definitions={}):
    rst = f'\n\n.. _option_{etype}:\n\nOption {etype.capitalize()}s\n-------------------\n\n'
    for element in sorted(known):
        rst += f'.. _option_{etype}_{element}:\n\n'
        title = f'``{element}``'
        if element in definitions:
            title += ': ' + definitions[element]
        rst += f'{title}\n{"^"*len(title)}\n\n'
        path = f'desc/{etype}_{element}.txt'
        rst += f'.. include:: {path}\n\n'
        open(dpath + '/' + path, 'w').close()
    return rst


def render_model(path):
    global known_attibutes, known_types, known_keys
    dpath = os.path.dirname(path)
    os.makedirs(dpath + '/desc', exist_ok=True)
    tag = "\n.. ---autodoc_start---\n\n"
    rst = tag
    rst += render_model_elements(known_keys, dpath, 'key', option.defined_keys)
    rst += render_model_elements(known_types, dpath, 'type', option.defined_types)
    rst += render_model_elements(known_attributes, dpath, 'attribute', option.defined_attr)
    
    with open(path, 'r') as f:
        cur = f.read()
    # Cut old and append new
    i = cur.find(tag)
    if i >= 0:
        cur = cur[:i]
    cur += rst
    with open(path, 'w') as f:
        f.write(cur)


def properties_table(opt, parent_opt={}):
    # Build a simple table out of properties
    table = ''
    space = '  '
    # table content
    keys = list(opt.keys())
    keys.sort()
    for e in ('factory_default', 'chron', 'header', 'handle', 'name', 'children', 'type'):
        if e in keys:
            keys.remove(e)
    for e in ['current']:
        if e not in keys:
            continue
        keys.remove(e)
        keys.insert(0, e)
    otype = opt.get('type', 0)
    if otype == 'Script' and 'current' in keys:
        keys.remove('current')
    # Find max length of fields for table headers
    len_key = 22
    len_val = 5
    for key, val in opt.items():
        if len(key) > len_key:
            len_key = len(key)
        val = repr(val)
        if len(val) > len_val:
            len_val = len(val)
    len_key = 22 + (len_key + 2) * 2
    len_val += 4  # includes also ``` engraving
    base = dict(parent_opt) or dict(base_options.get(otype, {}))
    dopt = dict(opt)
    for key in sorted(keys):
        if key not in opt:
            continue
        bopt = base.get(key, None)
        if dopt.get(key, None) == bopt and key != 'type':
            continue
        val = render_value(opt, key, pad=len_key + len(space))
        if val:
            table += f':ref:`{key} <option_key_{key}>`'.ljust(len_key) + space + val.ljust(len_val) + '\n'
    # Build header
    if table:
        header0 = '=' * len_key + space + '=' * len_key + '\n'
        header = 'Property'.ljust(len_key) + space + 'Value'.ljust(len_val) + '\n'
        # Compose table
        table = header0 + header + header0 + table + header0
        # text += table + '\n\n'
    return table


def as_rst(opt, anchor='', class_name='', level='-', inheritance='', parent_opt={}):
    """Format option entry dictionary `opt` as restructured text for documentation.
    """
    global known_attributes, known_types, base_options, known_keys
    known_attributes.update(opt.get('attr', []))
    otype = opt['type']
    known_keys.update(opt.keys())
    known_types.add(otype)
    if otype not in base_options:
        base_options[otype] = option.Option(handle='__foo__', type=otype)
    title = opt['name']
    len_name = len(title) + 2
    header = anchor + '\n\n'
    h0 = (level * len_name) + '\n'
    header += title + '\n' + h0
    handle = opt["handle"]
    header += f'\nHandle ``{handle}`` of type :ref:`{otype} <option_type_{otype}>`'
    if opt.get('parent', 0):
        p = escape_upper_case_with_1(opt["parent"])
        header += f' with parent option: :ref:`{opt["parent"]} <{docspace}_{class_name}_{p}>`'
    header += '. '
    if inheritance:
        header += '\n' + inheritance
    header += '\n\n'
    table = properties_table(opt, parent_opt)
    if table:
        table += '\n\n'
    if otype == 'Script' and opt['current']:
        scr = render_value(opt, 'current')
        if len(scr) > 1:
            table += scr
    desc = f'{autodoc_dir}/desc/{class_name}/{handle}.txt'
    os.makedirs(os.path.dirname(desc), exist_ok=True)
    if not os.path.exists(desc):
        include = 0
    else:
        f = open(desc, 'r')
        include = f.tell()
        f.close()
    if not table and not include:
        return ''
    open(desc, 'a').close()
    rst = header + table
    rst += f'.. include:: desc/{class_name}/{handle}.txt\n\n'
    return rst


docspace = 'mdf'

class_header = '''

.. _{0}_class_{1}:

{1}
'''


def optkey(el):
    el1 = el.copy()
    for k in ('factory_default', 'chron', 'header', 'priority', 'kid', 'children'):
        el1.pop(k, 0)
    
    return repr(el1)


def escape_upper_case_with_1(h):
    """Add `1` characters to mark a subsequent uppercase character,
    because ReST labels are case-insensitive."""
    return re.sub('(?=[A-Z])', '1', h)


rendered = set()


def render_conf_def(conf_def: dict, class_def: object,
                    parent_def: dict={}, rst: str='',
                    inherited: str='', level: str='-') -> (str, str):
    global rendered
    class_name = class_def.__name__
    for h, el in conf_def.items():
        # Check valid Option
        if not isinstance(el, dict):
            continue
        if 'type' not in el:
            continue
        if 'name' not in el:
            el['name'] = h.capitalize()
        if 'Hidden' in el.get('attr', []) or el['type'] in ('Hidden', 'Section'):
            print('Skip hidden', h)
            continue
        if h in ('log', 'style'):
            continue
        # If option label is already present in rst
        csh = escape_upper_case_with_1(h)
        ref = f'{docspace}_{class_name}_{csh}'
        assert ref not in rendered
        rendered.add(ref)
        anchor = f'.. _{ref}:'
        for k in ('factory_default', 'chron', 'priority', 'kid'):
            el.pop(k, 0)
        elkey = optkey(el)
        inheritance = ''
        parent = None
        found = None
        parents = parent_def.get(h, [])
        for cls in parents:
            sk = get_skeleton(cls)
            if not found:
                found = cls
            if optkey(sk[h]) != elkey:
                found = parent
                break
            parent = cls
        found_sk = get_skeleton(found)
        parent_opt = found_sk.get(h, {})
        if parents and not found:
            found = parents[0]
        if found:
            pname = found.__name__
            inheritance = f'Inherits :ref:`{docspace}_{pname}_{csh}` from :ref:`{pname} <{docspace}_class_{pname}>`\n'
        opt = option.Option(**el)
        opt.validate()
        opt = opt._entry
        children = el.pop('children', [])
        opt_rst = as_rst(opt, anchor, class_name, level=level,
                         inheritance=inheritance, parent_opt=parent_opt)
        if inheritance and not opt_rst:
            inherited += f'- {anchor}\n\n    {inheritance}'
        rst += opt_rst
        if children:
            i = levels.index(level) + 1
            print('Rendering children of', class_name, h, len(children), len(rst), len(inherited))
            rst, inherited = render_conf_def(children, class_def, parent_def, rst, inherited, levels[i])
            print('Done children', len(rst), len(inherited))
    return rst, inherited


def render_section(sections_dict, section_name, class_def, parent_def):
    section = sections_dict[section_name]
    if not section:
        print('empty section', section_name, section)
        return '', ''
    rst = ''
    inherited = ''
    header = ''
    level = 0
    if len(sections_dict) > 1 and section_name != 'Main':
        title = 'Section: ' + section_name.capitalize()
        header = '-' * (len(title) + 2)
        header = f"\n{title}\n{header}\n\n"
        level = 1
    prop_list = sections_dict[section_name]
    base, config, results, status = option.groups(prop_list)
    config.update(base)
    panels = [config, results, status]
    pnames = ['Configurations', 'Results', 'Status']
    multipanel = sum(map(lambda v: bool(len(v)), panels)) > 1
    if 'genome/options' in autodoc_dir:
        multipanel = 0
    multipanel = 0
    for i, panel in enumerate(panels):
        if not len(panel):
            continue
        add_level = multipanel
        h = ''
        rst1, inherited = render_conf_def(panel, class_def, parent_def,
                                          inherited=inherited,
                                          level=levels[level + add_level])
        if multipanel and len(rst1) > 5:
            h = f"\nPanel: {pnames[i]}\n"
            h += levels[level] * (len(h) + 2) + '\n\n'
            rst1 = h + rst1
        rst += rst1
    if header and rst:
        rst = header + rst
    return rst, inherited


skeleton_cache = {}


def get_skeleton(obj):
    global skeleton_cache
    if not hasattr(obj, '__name__'):
        return {}
    if not hasattr(obj, 'skeleton'):
        return {}
    if not hasattr(obj, '__mro__'):
        return {}
    if obj.__name__ in skeleton_cache:
        return skeleton_cache[obj.__name__]
    sk = getattr(obj, 'skeleton', lambda: {})()
    skeleton_cache[obj.__name__] = sk
    return sk


# Those are skipped anyway because they are empty - and a level of detail not needed
classes_skipped = set(['ConfigurationInterface', 'Node'])
classes_done = set([])
class_tree = {}


def _render_tree(tree: dict, tlevel: int=0, key: str="class") -> str:
    rst = ''
    for name, sub in tree.items():
        line = ' ' * tlevel + f'- :ref:`{name} <{docspace}_{key}_{name}>`'
        rst += line + '\n'
        add_rst = _render_tree(sub, tlevel + 1, key)
        if add_rst:
            rst += '\n' + add_rst + '\n'
    return rst


def render_tree() -> bool:
    global class_tree, docspace
    rst = _render_tree(class_tree)
    if not rst:
        return False
    out = f'.. _{docspace}_types:\n\nTypes Hierarchy\n=======================\n\n'
    out += rst + '\n\n'
    with open(f"{os.path.dirname(autodoc_dir)}/classes.rst", 'w') as f:
        f.write(out)
    class_tree = {}
    return True


def add_class_to_tree(mro: list):
    global class_tree, classes_skipped, classes_done
    tree = class_tree
    for cls in mro[::-1]:
        n = cls.__name__
        if n in classes_skipped:
            continue
        if not hasattr(cls, 'skeleton'):
            continue
        if n not in tree:
            tree[n] = {}
        tree = tree[n]


def docstring(cls: type) -> str:
    """Fix tabs. Resolve and escapes dosctring pointers:
- @ or @. -> :ref:`docspace_class_OwnClass`
- @.optName -> :ref:`docspace_OwnClass_opt1Name`
- @OtherClass -> :ref:`docspace_class_OtherClass`
- @OtherName.optName -> :ref:`docspace_OtherClass_opt1Name` 
"""
    global docspace
    if not cls.__doc__:
        return None
    # Remove tabs
    doc = cls.__doc__.replace('\n    ', '\n').replace('\n\t', '\n')
    pattern = r"@((\.)?\w+(\.\w+)?)"
    ref = f':ref:`{docspace}'
    prefix = f"{ref}_{cls.__name__}"

    def replacement(m):
        m = m.group(1)
        if not m:
            return
        # Own option
        own = m.startswith('.')
        if own and len(m) <= 1:
            r = f"{ref}_class_{cls.__name__}"
        elif own:
            # Own option
            m = escape_upper_case_with_1(m[1:])
            r = f"{prefix}_{m}"
        elif '.' in m:
            # Pointer to an option in another class
            m = m.replace('.', '_').split('_')
            class_name = m.pop(0)
            m = escape_upper_case_with_1('_'.join(m))
            r = ref + '_' + class_name + '_' + m
        else:
            # Pointer to another class definition
            r = ref + '_class_' + m
        
        print('REPL', m, r)
        return r + '`'

    new_s = re.sub(pattern, replacement, doc)
    return new_s


def autodoc(class_def: object):
    """Generate rst paragraphs out of list of options"""
    global docspace, classes_skipped, class_tree, classes_done
    class_name = class_def.__name__
    if class_name.startswith('Fake') or class_name.startswith('Dummy') or \
            class_name.startswith('Simulated') or class_name.startswith('Test') or\
            class_name in classes_skipped or class_name in classes_done:
        return False
    conf_def = get_skeleton(class_def)
    if not conf_def:
        print('Object does not have a conf_def nor skeleton. Skipping autodoc.', class_def)
        classes_skipped.add(class_name)
        return False
    # Recreate hierarchy
    parent_def = defaultdict(list)
    hierarchy = ''
    hlevel = 0
    mro = list(inspect.getmro(class_def))
    for cls in mro[::-1]:
        if cls == class_def:
            continue
        parent_conf_def = get_skeleton(cls) or {}
        if not len(parent_conf_def) or (cls.__name__ in classes_skipped):
            print('skip hierarchy', cls)
            continue
        print('parent_def', class_def.__name__, cls.__name__, len(parent_conf_def))
        hierarchy += ' ' * hlevel
        hierarchy += f'- :ref:`{cls.__name__} <{docspace}_class_{cls.__name__}>`\n\n'
        hlevel += 1
        for el in parent_conf_def.values():
            parent_def[el['handle']].append(cls)
        hierarchy += '\n'
    add_rst = ''
    inherited = ''
    sections_dict = option.sections(conf_def)
    for section_name in sections_dict:
        rst, inher = render_section(sections_dict, section_name, class_def, parent_def)
        add_rst += rst
        inherited += inher
    print('generated rst', class_name, len(add_rst), len(inherited))
    desc = f'{autodoc_dir}/desc/{class_name}.txt'
    if len(add_rst) or len(inherited):
        h = class_header.format(docspace, class_name)
        h += '=' * +(len(h)) + '\n\n'
        doc = docstring(class_def)
        if doc:
            h += doc + '\n\n'
        h += f'.. include:: desc/{class_name}.txt\n\n'
        with open(desc, 'a') as f:
            f.close()
        if not add_rst:
            add_rst = "This class does not define own options. All options are inherited and listed below.\n\n"
        if hierarchy:
            add_rst += 'Hierarchy\n-----------------\n\n'
            add_rst += hierarchy + '\n\n'
        if inherited:
            add_rst += 'Inherited options\n--------------------------\n\n'
            add_rst += inherited
        add_rst = h + '\n\n' + add_rst
    # Write the rst file
    path_rst = os.path.join(autodoc_dir, class_name + '.rst')
    # Write on output file
    if len(add_rst) > 0:
        classes_done.add(class_name)
        add_class_to_tree(mro)
        open(path_rst, 'w').write(add_rst)
        print('Written', path_rst, len(add_rst))
        return True
    elif os.path.exists(path_rst):
        os.remove(path_rst)
    print('No description for', class_name)
    return False


class ClientConfiguration(object):
    conf_def = default_desc


done = set([])
done_handle_name = set([])


def collect_conf(module, translations):
    """Scan a module for all classes defining a conf_def iterable attribute or a skeleton classmethod"""
    names = dir(module)
    missing = 0
    for name in names:
        obj = getattr(module, name, False)
        if obj is False:
            continue
        # Skip modules defining a conf_def list at their root.
        if getattr(obj, '__bases__', False) is False:
            continue
        conf_def = get_skeleton(obj)
        if conf_def is None:
            continue

        if obj in done:
            continue
        done.add(obj)
        logging.debug('Found conf_def', obj.__name__, conf_def)
        autodoc(obj)
        for h, el in conf_def.items():
            if not isinstance(el, dict):
                continue
            tr = el.get('name', False)
            if not tr:
                continue
            htr = h + '.' + tr
            if htr in done_handle_name:
                continue
            done_handle_name.add(htr)
            h = obj.__name__ + '.' + h
            logging.debug(obj, h, tr)
            translations[h] = tr
            # Get translatable option names
            opt = el.get('options', False)
            if not opt:
                continue
            if 'values' not in el:
                continue
            for i, o in enumerate(opt):
                h1 = h + '_opt{}'.format(i)
                translations[h1] = o
    return translations, missing


def rprint(m):
    for sub, isp in iter_package(m):
        print(sub)
        if isp:
            rprint(sub)
    return


exclude = ['mdf_client', 'mdf_canon', 'mdf_crystal', 'libvideodev', 'mdf_droid.utils', 'tests']

            
def iterpackage(package, translations=None):
    """Scan a package for all subpackages and all modules containing classes defining conf_def attribute.
    Accept an imported module as argument.
    Returns translations dictionary and missing count."""
    translations = translations if translations is not None else {}
    missing = 0
    for module, ispkg in iter_package(package, exclude):
        translations, ms = collect_conf(module, translations)
        missing += ms
        if ispkg:
            translations, ms = iterpackage(module, translations)
            missing += ms
    return translations, missing


def collect(*modules):
    """Collect translatable strings from static source code analysis.
    Returns all collected strings."""
    global autodoc_dir
    os.makedirs(autodoc_dir + '/desc', exist_ok=True)

    from mdf_canon.indexer import indexer
    translations = {}
    missing = 0
    print('Collect modules', modules)
    for mod in modules:
        print('Collect module:', mod)
        tr, miss = iterpackage(mod)
        missing += miss
        translations.update(tr)
    logging.debug('Stats', len(
        translations), len(set(translations)), missing)

    for column in indexer.columns_to_translate:
        translations["dbcol:" + column] = "dbcol:" + column

    out = open('static.txt', 'w')
    for h, tr in translations.items():
        out.write('{}\t{}\n'.format(h, tr))
    out.close()
    return translations


######################
# CLIENT CODE ANALYSIS - From Veusz pyqt_find_translatable
######################
import ast
import sys
import os


class Message(object):

    '''A translatable string.'''

    def __init__(self, string, filename=None, lineno=None, comment=None):
        self.string = string
        self.filename = filename
        self.lineno = lineno
        self.comment = comment


class PythonMessageVisitor(ast.NodeVisitor):

    '''A visitor which visits function calls and definitions in source.'''

    def __init__(self, filename, outmessages, verbose=True):
        '''filename is file being read
        If set, mapping of context to Messages will be returned in
        outmessages.'''

        self.filename = filename

        # map translation functions to contexts
        self.fn2context = {}
        # arguments for functions
        self.fnargs = {}

        self.messages = outmessages
        self.verbose = verbose

    def visit_Call(self, obj):
        '''Function call made.'''

        # this needs to be called to walk the tree
        self.generic_visit(obj)

        try:
            fn = obj.func.id
        except AttributeError:
            # no name known
            return

        if fn not in self.fn2context:
            return

        if len(obj.args) + len(obj.keywords) not in (1, 2, 3) or len(obj.args) < 1:
            sys.stderr.write(
                'WARNING: Translatable call to %s in %s:%i '
                'requires 1 to 3 parameters\n' % 
                (repr(fn), self.filename, obj.lineno))
            return

        # convert arguments to text
        try:
            args = [a.s for a in obj.args]
            keywords = dict([(a.arg, a.value.s) for a in obj.keywords])
        except AttributeError:
            sys.stderr.write(
                'WARNING: Parameter to translatable function '
                '%s in %s:%i is not string\n' % 
                (repr(fn), self.filename, obj.lineno))
            return

        # defaults
        text = args[0]
        context = self.fn2context[fn]
        comment = None

        # examine any unnamed arguments
        ctxidx = self.fnargs[fn].index('context')
        if len(args) > ctxidx:
            context = args[ctxidx]
        disidx = self.fnargs[fn].index('disambiguation')
        if len(args) > disidx:
            comment = args[disidx]

        # now look at named arguments which override defaults
        context = keywords.get('context', context)
        comment = keywords.get('disambiguation', comment)

        # create new message
        if context not in self.messages:
            self.messages[context] = []
        self.messages[context].append(
            Message(text, filename=self.filename, lineno=obj.lineno,
                    comment=comment))

        if self.verbose:
            sys.stdout.write(
                'Found text %s (context=%s, disambiguation=%s) in %s:%i\n' % 
                (repr(text), repr(context), repr(comment),
                 self.filename, obj.lineno))

    def visit_FunctionDef(self, obj):
        '''Function definition made.'''

        # this needs to be called to walk the tree
        self.generic_visit(obj)

        try:
            name = obj.name
        except AttributeError:
            return

        args = obj.args
        # want a three-parameter function with two default values
        if len(args.args) != 3 or len(args.defaults) != 2:
            return

        argids = [a.id.lower() for a in args.args]
        # only functions with disambiguation and context as optional arguments
        if 'disambiguation' not in argids or 'context' not in argids:
            return

        contextidx = argids.index('context')
        try:
            context = args.defaults[contextidx - 1].s
        except AttributeError:
            sys.stderr.write(
                "WARNING: Translation function definition %s in "
                "%s:%i does not have default string for 'context'\n" % 
                (repr(name), self.filename, obj.lineno))
            return

        if name in self.fn2context:
            sys.stderr.write(
                'WARNING: Duplicate translation function %s '
                'in %s:%i\n' % (repr(name), self.filename, obj.lineno))
            return

        if self.verbose:
            sys.stdout.write(
                'Found translation function %s with default '
                'context %s in %s:%i\n' % 
                (repr(name), repr(context), self.filename, obj.lineno))

        # map function name to default context
        self.fn2context[name] = context
        self.fnargs[name] = argids


def python_find_strings(filename, retn, verbose=True,
                        gcontext={'_': 'misura'},
                        gargs={'_': ('text', 'disambiguation', 'context')}):
    '''Update output in retn with strings in filename.'''

    if verbose:
        sys.stdout.write('Examining file %s\n' % repr(filename))
    with open(filename) as f:
        source = f.read()

    tree = ast.parse(source, filename)

    v = PythonMessageVisitor(filename, retn, verbose=verbose)
    v.fn2context = gcontext.copy()
    v.fnargs = gargs.copy()
    v.visit(tree)


def scan_client_source(path, out=False):
    retn = {}
    for root, dirs, files in os.walk(path):
        for fn in files:
            if not fn.endswith('.py'):
                continue
            fp = os.path.join(root, fn)
            logging.debug('Scanning', fp)
            python_find_strings(fp, retn)
    # Simplify output
    if not out:
        out = {}
    for ctx, msgs in retn.items():
        if ctx not in out:
            out[ctx] = {}
        for msg in msgs:
            v = mescape(msg.string)
            out[ctx][v] = ('', '')
#         out[ctx] += [msg.string for msg in msgs]
    return out


######################
# END OF CLIENT CODE ANALYSIS
######################
from mdf_client.parameters import pathClient


def language_sync(translations={}):
    """Merge all translatable strings from runtime requests, code analysis, already translated code."""
    # Translation contexts
    contexts = {'Option': {}}
    for v in translations.values():
        v = mescape(v)
        contexts['Option'][v] = ('', '')
    
    # Collect from client code analysis
    contexts = scan_client_source(pathClient, out=contexts)

    statistics = {}
    for l in langs:
        logging.debug('LANGUAGE:', l)
        ctx = update(l, contexts.copy(), base='misura')
        ctx = update(l, ctx)
        write_ts(l, ctx)
        statistics[l] = stats(ctx)
        # cancello tutte le traduzioni, mantenendo però le chiavi
        contexts = {}
        for c, v in ctx.items():
            if c not in contexts:
                contexts[c] = {}
            for k, e in v.items():
                contexts[c][k] = ('', '')

    logging.debug('Completeness:')
    for l in langs:
        s = statistics[l]
        logging.debug('%s: %.2f %% (missing: %i)' % 
                      (l.upper(), 100. * s[1] / (s[1] + s[2]), s[2]))


def main():
    global docspace, autodoc_dir, exclude, class_tree
    import mdf_server
    import mdf_droid
    docspace = 'mdf'
    autodoc_dir = "/opt/mdf/ceramics_genome_doc/mdf/options"
    autodoc(ClientConfiguration)
    translations = collect(mdf_droid, mdf_server)
    
    render_tree()
    
    # render_model("/opt/mdf/ceramics_genome_doc/options.rst"); return
    import mdf_crystal
    exclude.remove('mdf_crystal')
    exclude += ['mdf_server', 'mdf_droid']
    autodoc_dir = "/opt/mdf/ceramics_genome_doc/genome/options"
    docspace = 'genome'
    translations.update(collect(mdf_crystal))
    render_tree()
    
    render_model("/opt/mdf/ceramics_genome_doc/options.rst")
    # language_sync(translations)
    
    
if __name__ == '__main__':
    main()

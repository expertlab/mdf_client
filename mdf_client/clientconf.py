#!/usr/bin/python
# -*- coding: utf-8 -*-
"""MDF Configuration Manager"""
import os
import multiprocessing
from mdf_canon.logger import get_module_logging
import sqlite3
from traceback import format_exc, print_exc
from mdf_client.qt import QtCore
import mdf_canon
from mdf_canon import option, indexer, dashboard
from mdf_canon import csutil
from mdf_canon.csutil import configparser, unicode_func, unicode_decode
from mdf_canon.option import ao, mkheader
from mdf_canon.indexer import Indexer
from mdf_canon.plugin import clientconf_update_functions
from . import units
from . import parameters as params
from copy import deepcopy

logging = get_module_logging(__name__)
RulesTable = dashboard.RulesTable
default_misuradb_path = os.path.expanduser("~/MisuraData/misuradb.sqlite")

logsound = os.path.join(params.pathArt, 'sounds', 'warning.ogg')

default_desc = {}

ao(default_desc, 'lang', **{'name': "Client Language",
                            'current': 'sys',
                            'type': 'Chooser',
                            'options': ['sys', 'en', 'it', 'fr', 'es', 'ru']
                            })
ao(default_desc, 'refresh', **{'name': 'Remote Server Refresh Rate (ms)',
                               'current': 1000, 'max': 20000, 'min': 0, 'type': 'Integer'})

ao(default_desc, 'database', **
   {'name': 'Default Database', 'current': '', 'type': 'FilePath'})

ao(default_desc, 'hserver', **
   {'name': 'Recent Servers', 'current': 5, 'max': 20, 'min': 0, 'type': 'Integer'})
ao(default_desc, 'saveLogin', **
   {'name': 'Save User/Password by Default', 'current': True, 'type': 'Boolean'})
ao(default_desc, 'autoConnect', **
   {'name': 'Auto-connect to last server',
    'current': True, 'type': 'Boolean'})

ao(default_desc, 'databaseSync', 'Boolean', True, 'Check for new tests upon connection')

ao(default_desc, 'authLevel', 'Chooser', 2, 'Authorization level when opening test files',
   values=list(range(6)), options=['guest', 'basic', 'user', 'tech', 'maint', 'admin'])

ao(default_desc, 'hdatabase', **
   {'name': 'Recent Database Files', 'current': 10, 'max': 100, 'min': 1, 'type': 'Integer'})
ao(default_desc, 'hfile', **{'name': 'Recent Test Files',
                             'current': 15, 'max': 100, 'min': 1, 'type': 'Integer'})
ao(default_desc, 'hm3database', **
   {'name': 'Recent Misura3 Databases', 'current': 10, 'max': 100, 'min': 1, 'type': 'Integer'})

ao(default_desc, 'logdir', **{'name': 'Log files directory',
                              'current': os.path.expanduser("~/MisuraData/log"),
                              'type': 'FilePath'})
ao(default_desc, 'loglevel', **{'name': 'Logging Level', 'current': 30,
                                'max': 50, 'min':-1, 'step': 10, 'type': 'Integer', 'parent': 'logdir'})
ao(default_desc, 'lognotify', **{'name': 'Popup notification level', 'current': 40,
                                 'max': 50, 'min':-1, 'step': 10, 'type': 'Integer', 'parent': 'logdir'})
ao(default_desc, 'logsize', **{'name': 'Size of each log file', 'current':
                               20000, 'min': 0, 'unit': 'kilobyte', 'type': 'Integer', 'parent': 'logdir'})
ao(default_desc, 'lognumber', **{'name': 'Max number of logfiles to be kept',
                                 'current': 5, 'min': 0, 'type': 'Integer', 'parent': 'logdir'})
ao(default_desc, 'logsound', **{'name': 'Alarm sound to play', 'current': logsound,
                                 'type': 'FilePath', 'parent': 'logdir'})
ao(default_desc, 'logsoundlevel', **{'name': 'Play alarm on log level',
                                 'current': 51, 'min': 0, 'max': 51, 'type': 'Integer', 'parent': 'logdir'})

ao(default_desc, 'templates', **{'name': 'Templates directory',
                                 'current': os.path.expanduser("~/MisuraData/templates"),
                                 'type': 'FilePath'})

cpu = multiprocessing.cpu_count()
ao(default_desc, 'maxcpu', **{'name': 'Maximum number of CPU', 'current': cpu,
                              'max': cpu, 'min': 1, 'step': 1, 'type': 'Integer'})
##############
# Autoupdate
ao(default_desc, 'updateAuto', 'Boolean', True, 'Check for updates')
ao(default_desc, 'updateUrl', 'String',
   'https://www.expertlabservice.it/packages/mdf',
   'AutoUpdate site address', parent='updateAuto')
ao(default_desc, 'updateLastClient', 'Time', 0, 'Last checked for client updates',
   parent='updateAuto')
ao(default_desc, 'updateLastServer', 'Time', 0, 'Last checked for server updates',
   parent='updateAuto')

ao(default_desc, 'remoteSupport', 'String',
   'serveo.net',
   'Remote support domain')

###############
# Rules
u = [[k, v] for k, v in units.user_defaults.items()]
ao(default_desc, 'units', 'Table', u, 'Measurement units',
   header=mkheader('dimension', 'unit'))

ao(default_desc, 'rule', 'Section', 'Dataset Rules', 'Dataset Rules')

rule_exc = r'''/beholder/
/hydra/
/analyzer/
/autoroi/
/iA$
/iB$
/iC$
/iD$'''
ao(default_desc, 'rule_exc', 'TextArea', rule_exc, 'Ignore datasets')

rule_inc = ''
ao(default_desc, 'rule_inc', 'TextArea', rule_inc, 'Force inclusion')

rule_load = r'''hsm/sample\d/h$
hsm/sample\d/Vol$
/sample0/d$
/sample0/cte\d+$
/sample0/dL\d+$
/sample0/thickness$'''
ao(default_desc, 'rule_load', 'TextArea', rule_load, 'Force loading')

rule_unit = [
    [r'^t$', 'second'],
    [r'/hsm/sample\d/h$', 'percent'],
    [r'/hsm/sample\d/Vol$', 'percent'],
    [r'/vertical/sample\d/d$', 'percent'],
    [r'/horizontal/sample\d/d$', 'percent']
]
ao(default_desc, 'rule_unit', 'Table', rule_unit, 'Dataset units',
   header=mkheader('rule', 'unit'))

rule_plot = r'''hsm/sample\d/Vol$
/sample0/d$
dta/sample0/deltaT$
/kiln/T$'''
ao(default_desc, 'rule_plot', 'TextArea', rule_plot, 'Auto Plot')

rule_axis = [
    [r'kiln/P$', 'Power'],
    [r'kiln/pid/o(p|P|I|D|L)?$', 'Power'],
    [r'kiln/T(s|k|e|r)?$', 'Temperature'],
    [r'sample\d/T$', 'Temperature'],
    [r'flex/sample\d/d(Center|Right|Left)?$', 'Flex'],
    [r'(horizontal|vertical)/sample\d/d(Right|Left|Base|Height)?$', 'Dil'],
    [r'hsm/sample\d/w(\d)?$', 'Width'],
    # Calibration datasets
    [r'/sample\d/d/[^/]*_d$', 'Dil'],
    [r'/sample\d/d/[^/]*_fit$', 'Dil'],
    [r'/sample\d/d/[^/]*_cal$', 'Dil'],
    # Misura3 compat
    [r'kiln/p(p|P|I|D)?$', 'Power'],  # old Misura4
    [r'flex/d$', 'Flex'],
    [r'flex/camA$', 'Flex'],
    [r'odlt/sample0/camA', 'Dil'],
    [r'odlt/sample0/camB', 'Dil'],
    # Gas
    [r'kiln/gas/flow.*', 'Gas'],
]
ao(default_desc, 'rule_axis', 'Table', rule_axis, 'Axes',
   header=mkheader('rule', 'axis'))

rule_nav_hide = r'''\w+/\w+/t$
\w+/\w+_t$
\w+/\w+_T$
\w+/\w+Error$'''
ao(default_desc, 'rule_nav_hide', 'TextArea', rule_nav_hide, 'Hide from Navigator')

rule_style = [['/T$', '', 1, 'red', '', '']]
ao(default_desc, 'rule_style', 'Table', rule_style, 'Formatting',
   header=mkheader('rule', 'range', ('scale', 'Float'), 'color', 'line', 'marker'))

ao(default_desc, 'rule_autoformat', 'Chooser', 'Line Color',
   'Mark curves sharing same axis by:', options=['Line Style', 'Line Color'])

ao(default_desc, 'rule_logo', **
   {'name': 'Plot Logo', 'current': '', 'type': 'FilePath'})

ao(default_desc, 'rule_title', 'Chooser', 'Measure', 'Show test title in plot',
   options=['None', 'Measure', 'Comment'])

ao(default_desc, 'rule_structuredFormat', **
   {'name': 'Structured test name', 'current': '<%Y_%m_%d>_<test>_<material>_<source>_<instrument>', 'type': 'String'})

ao(default_desc, 'rule_structuredSample', **
   {'name': 'Structured test name', 'current': '<test>_<material>_<source>', 'type': 'String'})

ao(default_desc, 'rule_structuredGroups', **
   {'name': 'Structured name groups', 'type': 'Table',
    "header": mkheader('code', 'group', 'name'),
    "current": [['TENS', 'test', 'Tension Flex'],
                ['PYR', 'test', 'Pyroplasticity Flex'],
                ['COE', 'test', 'Thermal Expansion Dil'],
                ['SIN', 'test', 'Sintering Dil'],
                ['HSM', 'test', 'Microscope'],
                ['BOD', 'material', 'Body'],
                ['GLZ', 'material', 'Glaze'],
                ['ENG', 'material', 'Engobe'],
                ['GRT', 'material', 'Grit'],
                ['ENG-GLZ', 'material', 'Engobe+Glaze'],
                ['ENG-BOD', 'material', 'Engobe+Body'],
                ['FULL', 'material', 'Body+Engobe+Glaze+...'],
                ['0001', 'source', 'Customer 1'],
                ['0002', 'source', 'Customer 2'],
                ['0003', 'source', 'Customer 3'],
                ['A', 'instrument', 'Lab. A'],
                ['B', 'instrument', 'Lab. B'],
                ], })

ao(default_desc, 'rule_smoothMax', 'Integer', 30, name='Smooth max', min=5)
ao(default_desc, 'rule_smoothDiv', 'Integer', 20, name='Smooth divisor', min=0)

##########
# Plugins
ao(default_desc, 'm3', 'Section', 'Data import', 'Data import')
ao(default_desc, 'm3_enable', 'Boolean', False, 'Enable Misura 3 database interface')
ao(default_desc, 'm3_open', 'Boolean',
   True, 'Open recent M3 db in Browser')
ao(default_desc, 'm3_plugins', 'TextArea', '', 'Import plugins by name')

default_desc.update(deepcopy(dashboard.conf))

############
# Recent tables
ao(default_desc, 'recent_server', 'Table', attr=['Hidden'],
   header=mkheader('address', 'user', 'password', 'MAC',
                   'serial', 'name'))

rf = mkheader('path', 'name')
ao(default_desc, 'recent_database', 'Table', attr=[
   'Hidden'], header=rf)
ao(default_desc, 'recent_file', 'Table', attr=[
   'Hidden'], header=rf)
ao(default_desc, 'recent_m3database', 'Table', attr=[
   'Hidden'], header=rf)

# Windows installer options
ao(default_desc, 'inst_m3', **{'name': 'Component: M3', 'current': False, 'type': 'Hidden'})
ao(default_desc, 'inst_flash', **{'name': 'Component: Flash', 'current': False, 'type': 'Hidden'})
ao(default_desc, 'inst_adv', **{'name': 'Component: Advanced', 'current': False, 'type': 'Hidden'})

recent_tables = 'server,database,file,m3database'.split(',')

##########
# Client license panel
ao(default_desc, 'license', 'Section', 'License', 'License')
ao(default_desc, 'license_status', 'Boolean', False, 'Valid license')
# List of deprecated license urls
old_license_url = set(['https://kn41i5ww73.execute-api.eu-central-1.amazonaws.com/prod',
                       'https://www.expertlabcloud.it/rpc/rpc.php'])
license_url = 'https://app.ceramics-genome.ai/rpc/rpc.php'
ao(default_desc, 'license_url', 'String', license_url, 'License renewal address')
ao(default_desc, 'license_expiry', 'Time', 0, 'Expiry', attr=['ReadOnly', 'Runtime', 'Config'])
ao(default_desc, 'license_authentic', 'Boolean', '', 'Authentic', attr=['ReadOnly', 'Runtime', 'Config'])
ao(default_desc, 'license_hash', 'String', '', 'Client hash', attr=['ReadOnly', 'Config'])
ao(default_desc, 'license_license', 'TextArea', '', 'Client license')
ao(default_desc, 'license_functionalities', 'String', '', 'Client functionalities', attr=['ReadOnly', 'Runtime', 'Config'])
ao(default_desc, 'license_identifier', 'TextArea', '', 'Client identifier', attr=['ReadOnly', 'Runtime', 'Config'])

##########
# Cloud panel
ao(default_desc, 'cloud', 'Section', 'Cloud', 'Cloud')
ao(default_desc, 'cloud_enable', 'Boolean', False, 'Cloud services')
ao(default_desc, 'cloud_user', 'String', "", 'Account user')
ao(default_desc, 'cloud_password', 'String', "", 'Account password', attr=["Password"])
ao(default_desc, 'cloud_versioning', 'Boolean', False, 'Local versioning')
ao(default_desc, 'cloud_remote', 'String', '', 'Remote ID', attr=['ReadOnly'])
ao(default_desc, 'cloud_remoteNick', 'String', '', 'Remote name', attr=['ReadOnly'])
ao(default_desc, 'cloud_localNick', 'String', '', 'Local name', attr=['ReadOnly'])
ao(default_desc, 'cloud_local', 'String', '', 'Local ID', attr=['ReadOnly'])
ao(default_desc, 'cloud_relay', 'String', 'sync.expertlabservice.it', 'Remote name')

ao(default_desc, 'cloud_folders', 'Table', name='Additional backup folders',
   header=mkheader(('path', 'FilePath')))

ao(default_desc, 'cloud_additional', 'Table', name='Additional backup devices',
   header=mkheader('label', 'DeviceID', 'type'))

func_dict = {'backup': u'Cloud Backup',
    'post': u'Post Analysis',  # mdf_imago
    'coupling': u'Coupling Temperature',  # future
    } 


def get_installer_opts():
    r = {}
    current = os.path.join(params.pathClient, 'installer_options.ini')
    if not os.path.exists(current):
        logging.debug('No installer options found', current)
        return r
    logging.info('Reading installer configuration', current)
    conf = configparser.SafeConfigParser()
    conf.optionxform = str
    conf.read(current)
    for opt, val in conf.items('main'):
        r[opt] = int(val)
    bak = os.path.join(params.pathClient, 'installer_options_backup.ini')
    if os.path.exists(bak):
        os.remove(bak)
    os.rename(current, bak)
    return r


def tabname(name):
    if name in recent_tables:
        return 'recent_' + name
    return name


rule_suffixes = ['exc', 'inc', 'load', 'plot']


def update_url(url):
    if ('webshare' in url) or \
            ('bitbucket' in url) or \
            ('packages.expertlabservice.it' in url) or \
            ('.s3.' in url):
        logging.debug('Resetting updateUrl', url, default_desc['updateUrl']['current'])
        return default_desc['updateUrl']['current']
    return url


class ConfDb(option.ConfigurationProxy, QtCore.QObject):
    _Method__name = 'CONF'
    conn = False
    path = ''
    index = False
    RULE_RANGE = 0
    RULE_SCALE = 1
    RULE_COLOR = 2
    RULE_LINE = 3
    RULE_MARKER = 4
    time_loaded = 1e32
    
    sig_load = QtCore.pyqtSignal()
    sig_mem = QtCore.pyqtSignal()
    sig_rem = QtCore.pyqtSignal()
    
    def __init__(self, path=False):
        QtCore.QObject.__init__(self)
        option.ConfigurationProxy.__init__(self)
        self.store = option.SqlStore()
        self._lock = indexer.FileSystemLock()
        self._lock.copy_on_lock = True
        if not path:
            return None
        # Load/create
        self.known_uids = {}
        self.load(path)
        
    def __getitem__(self, *a, **k):
        self.check_time_loaded()
        return super(ConfDb, self).__getitem__(*a, **k)
    
    def __setitem__(self, *a, **k):
        self.check_time_loaded()
        return super(ConfDb, self).__setitem__(*a, **k)
    
    def smooth_window(self, N):
        d = self['rule_smoothDiv']
        m = self['rule_smoothMax']
        if d < 1:
            return min((N, m))
        smooth = max(5, N // d)
        return min((smooth, m, N))
    
    def check_time_loaded(self):
        if not self.path:
            return True
        last_saved = os.stat(self.path).st_mtime
        if last_saved > self.time_loaded:
            logging.error('Stale configuration', last_saved, self.time_loaded)
            self.load(self.path)
            return False
        return True

    _rule_style = False

    @property
    def rule_style(self):
        """A RulesTable for styles"""
        if not self._rule_style:
            self._rule_style = RulesTable(self['rule_style'])
        return self._rule_style

    _rule_dataset = False

    @property
    def rule_dataset(self):
        """A special RulesTable collecting dataset loading behaviors."""
        if not self._rule_dataset:
            tab = [[('header placeholder',), ('retn',)],
                   [self['rule_plot'], 4],  # plot
                   [self['rule_load'], 3],  # load
                   [self['rule_inc'], 2],  # create
                   [self['rule_exc'], 1],  # exclude
                   ]
            self._rule_dataset = RulesTable(tab)
        return self._rule_dataset

    _rule_unit = False

    @property
    def rule_unit(self):
        if not self._rule_unit:
            self._rule_unit = RulesTable(self['rule_unit'])
        return self._rule_unit
    
    _rule_axis = False

    @property
    def rule_axis(self):
        if not self._rule_axis:
            self._rule_axis = RulesTable(self['rule_axis'])
        return self._rule_axis
    
    _rule_nav_hide = False
    
    @property
    def rule_nav_hide(self):
        if not self._rule_nav_hide:
            tab = [[('t',), ('retn',)],
                   [self['rule_nav_hide'], 1],
                   ]
            self._rule_nav_hide = RulesTable(tab)
        return self._rule_nav_hide
    
    _rule_opt_hide = False

    @property
    def rule_opt_hide(self):
        if not self._rule_opt_hide:
            tab = [[('t',), ('retn',)],
                   [self['opt_hide'], 1],
                   ]
            self._rule_opt_hide = RulesTable(tab)
        return self._rule_opt_hide
    
    _rule_opt_status = False

    @property
    def rule_opt_status(self):
        if not self._rule_opt_status:
            self._rule_opt_status = RulesTable(self['opt_status'])
        return self._rule_opt_status
    
    _rule_opt_tc = False

    @property
    def rule_opt_tc(self):
        if not self._rule_opt_tc:
            tab = [[('t',), ('retn',)],
                   [self['opt_tc'], 1],
                   ]
            self._rule_opt_tc = RulesTable(tab)
        return self._rule_opt_tc

    def reset_rules(self):
        self._rule_style = False
        self._rule_dataset = False
        self._rule_unit = False
        self._rule_opt_hide = False
        self._rule_opt_status = False
        self._rule_opt_tc = False
        self._rule_nav_hide = False

    def load_configuration(self):
        # Configuration table
        self.default_desc = deepcopy(default_desc)
        loaded = False
        self.conn = sqlite3.connect(self.path,
                    detect_types=sqlite3.PARSE_DECLTYPES)
        self.conn.text_factory = unicode_func
        cursor = self.conn.cursor()
        conf_table_exists = cursor.execute(
            "select 1 from sqlite_master where type='table' and name='conf'").fetchone()
        if conf_table_exists:
            stored_desc = self.store.read_table(cursor, 'conf')
            desc = deepcopy(default_desc)
            desc.update(stored_desc)
            self.desc = desc
            loaded = True
            
        if not loaded:
            logging.debug('Recreating client configuration')
            for key, val in self.default_desc.items():
                self.store.desc[key] = option.Option(**val)
            self.desc = self.store.desc
            self.store.write_table(cursor, "conf")
            
        self.time_loaded = os.stat(self.path).st_mtime
        # Apply authorization level
        option.ConfigurationProxy._readLevel = self['authLevel']
        option.ConfigurationProxy._writeLevel = self['authLevel']
        if os.path.exists(self['database']):
            logging.debug('Found default database', self['database'])
            self.mem_database(self['database'], 'Default', save=False)
        self.setattr('m3_enable', 'attr', [])
        self['updateUrl'] = update_url(self['updateUrl'])
        if self['license_url'] in old_license_url:
            logging.debug('Updating license url', self['license_url'])
            self['license_url'] = license_url
        if not os.path.exists(self['logsound']):
            self['logsound'] = logsound
        cursor.close()
        self.conn.commit()
        return loaded

    def migrate_desc(self):
        """Migrate saved newdesc to current hard-coded configuration structure default_desc"""
        desc_ret = {}
        for key, val in self.desc.items():
            if key in self.default_desc:
                saved_opt = option.Option(**val)
                coded_opt = option.Option(**self.default_desc[key])
                saved_opt.migrate_from(coded_opt)
                desc_ret[key] = saved_opt
        self.desc = desc_ret

    def add_option(self, *a, **k):
        """When a new option is defined, add also to default_desc definition"""
        out = option.ConfigurationProxy.add_option(self, *a, **k)
        self.default_desc[out['handle']] = out.entry
        return out

    def load(self, path=False):
        """Load an existent client configuration database, or create a new one."""
        logging.debug('LOAD', path)
        self.index = False
        self.close()
        if path:
            self.path = path
            csutil.ensure_directory_existence(path)
        self._lock.set_path(self.path)
        self._lock.acquire(timeout=3)
        if not os.path.exists(self.path):
            self._lock.recover_backup()
        loaded = False
        try:
            loaded = self.load_configuration()
        except:
            logging.critical('Cannot load client configuration', self.path, format_exc())
            os.remove(self.path)
        if (not loaded) and self._lock.recover_backup():
            logging.debug('Loading configuration backup')
            self.load_configuration()
        self._lock.release()

        # Forget recent tables defined with old headers
        for tname in recent_tables:
            break
            # FIXME
            tname = 'recent_' + tname
            if self.desc[tname].get('header', []) != self.default_desc[tname]['header']:
                cur = self.desc[tname]['current']
                cur.pop(0)
                self.desc[tname] = self.default_desc[tname]
                self.desc[tname]['current'] = cur

        self.sig_load.emit()
        
        for key in ['logdir', 'templates']:
            if not os.path.exists(self[key]):
                logging.debug('Creating configured directory', key, self[key])
                try:
                    os.makedirs(self[key])
                except:
                    logging.error(format_exc())

        self.reset_rules()
        self.create_index()
        
        self.load_installer_options()
        
    def load_license(self):
        mod_license = mdf_canon.mod_license
        identifier0 = mod_license.calculate_clientid(date=False)
        hsh = mod_license.identifier_hash(identifier0.encode('utf8'))
        identifier = mod_license.calculate_clientid(date=True)
        enc_ident = mod_license.encrypt_identifier(identifier)
        self.desc['license_identifier']['current'] = enc_ident
        self.desc['license_hash']['current'] = hsh
        r = mod_license.verify_license(self['license_license'], identifier0, func_dict)
        t, valid, funcs, rates, msg, signature = r
        logging.debug('Client license:', valid, t, funcs)
        self.desc['license_authentic']['current'] = valid
        self.desc['license_status']['current'] = valid
        self.desc['license_expiry']['current'] = t
        self.desc['license_functionalities']['current'] = ', '.join(list(funcs))
        if valid:
            for k in rates:
                logging.debug('Found cloud license', k, rates[k])
                self.desc['cloud_' + k]['current'] = rates[k]
            
        return valid
        
    def load_installer_options(self):
        r = get_installer_opts()
        logging.debug('load_installer_options', r)
        if not r:
            return False
        for k, v in r.items():
            k = 'inst_' + k
            if k not in self:
                logging.error('Missing installer option', k, v)
                continue
            self[k] = v
        # M3
        self['m3_enable'] = self['inst_m3']
        
        # FLASH
        m3p = self['m3_plugins']
        if self['inst_flash']:
            if 'mdf_client.flash' not in m3p:
                if m3p and not m3p.endswith('\n'):
                    m3p += '\n'
                self['m3_plugins'] = m3p + 'mdf_client.flash'
        else: 
            # Disable plugin
            self['m3_plugin'] = m3p.replace('mdf_client.flash', '').replace('\n\n', '\n')
            
        # ADV
        if self['inst_adv']:
            self['authLevel'] = 5
        else:
            self['authLevel'] = 2
            
        self.save()

    def create_index(self, force=False):
        self.index = False
        path = self['database']
        force = force and path and (not (os.path.exists(path))) and \
                os.path.exists(os.path.dirname(path)) 
        if force:
            open(path, 'w').close()
        if path and os.path.exists(path):
            logging.debug('Creating indexer at', path)
            self.index = Indexer(path)
            return True
        else:
            logging.debug('Default database not found:', path)
            return False

    def clean_rules(self):
        for rule in ['rule_load', 'rule_exc', 'rule_inc']:
            self[rule] = self[rule].strip()

    def save(self, *a, **k):
        """Save to an existent client configuration database."""
        logging.debug('SAVING', self.path)
        last_saved = os.stat(self.path).st_mtime
        if last_saved > self.time_loaded:
            logging.error('Cannot save over more recent configuration!', last_saved, self.time_loaded)
            self.load()
            return False
        self.clean_rules()
        self._lock.acquire(timeout=3)
        cursor = self.conn.cursor()
        self.store.write_table(cursor, 'conf', desc=self.desc)
        cursor.close()
        self.conn.commit()
        self.time_loaded = os.stat(self.path).st_mtime
        self._lock.release()
        self.reset_rules()
        self.create_index()
        return True

    def mem(self, name, *arg, **kwarg):
        """Memoize a recent datum"""
        logging.debug("mem ", name, arg)
        # Avoid saving duplicate values
        arg = list(unicode_func(a) for a in arg)
        self.rem(name, arg[0], save=False)
        tname = tabname(name)
        tab = list(self[tname])
        logging.debug('mem', name, tab)
        tab.append(arg)
        lim = self.desc['h' + name]['current']
        if len(tab) > lim:
            tab.pop(0)
        
        self[tname] = tab
        self.sig_mem.emit()
        if kwarg.get('save', True):
            self.save()
        return True

    def rem(self, name, key, save=True):
        """Forget a recent datum"""
        key = unicode_func(key)
        tname = tabname(name)
        tab = self[tname]
        done = 0
        while True:
            v = [r[0] for r in tab]
            if not key in v:
                break
            i = v.index(key)
            tab.pop(i)  # preserve the header
            logging.debug('rem', name, key, i)
            done += 1
        if not done:
            return False
        self[tname] = tab
        if save:
            self.sig_rem.emit()
            self.save()
        return True

    def close(self):
        if self.conn:
            self.conn.close()
        try:
            self._lock.release()
        except:
            pass
        option.ConfigurationProxy.close(self)

    def mem_file(self, path, name='', save=True):
        self.mem('file', path, name, save=save)

    def rem_file(self, path, save=True):
        self.rem('file', path, save=save)

    def mem_database(self, path, name='', save=True):
        self.mem('database', path, name, save=save)

    def rem_database(self, path, save=True):
        self.rem('database', path, save=save)

    def mem_m3database(self, path, name='', save=True):
        self.mem('m3database', path, name, save=save)

    def rem_m3database(self, path, save=True):
        self.rem('m3database', path, save=save)

    def found_server(self, addr):
        addr = str(addr)
        v = [r[0] for r in self['recent_server']]
        # Check if the found server was already saved with its own user and
        # password
        print('found_server', addr, r)
        if addr in v:
            return False
        # Otherwise, save it with empty user and password
        self.mem('server', addr, '', '')
        return True

    def logout(self, addr):
        addr = str(addr)
        v = [r[0] for r in self['recent_server']]
        if addr not in v:
            return False
        # Change order so that it becomes the most recent
        i = v.index(addr)
        entry = self['recent_server'][i]
        # TODO: this looses password!!!
        self.rem('server', addr)
        self.mem('server', *entry)
        return True

    # TODO: accettare serial e name e altro...
    def mem_server(self, addr, user='', password='', mac='', name='', serial='', save=True):
        # Remove entries with empty user/password
        addr, user, password, mac, name, serial = unicode_decode(addr), unicode_decode(
            user), unicode_decode(password), unicode_decode(mac), unicode_decode(name), unicode_decode(serial)
        if not save:
            password = ''
        logging.debug('mem_server', addr, user, password, mac, name, serial, save)
        return self.mem('server', addr, user, password, mac, serial, name)

    def rem_server(self, addr):
        self.rem('server', addr)

    def get_from_key(self, table_name, key):
        """Returns username and passwords used to login"""
        for entry in self[table_name]:
            if entry[0] == key:
                return entry
        return [''] * len(self.desc[table_name]['header'])

    def resolve_uid(self, uid):
        """Search file path corresponding to uid across default database and recent databases"""
        if not uid:
            logging.debug('no uid')
            return False
        known = self.known_uids.get(uid, False)
        if known:
            logging.debug('UID was known %s', known)
            return known, False
        else:
            logging.debug('Uid not previously known', uid)
        if self.index:
            dbPath = self.index.dbPath
            file_path = self.index.searchUID(uid)
            if file_path:
                logging.debug('uid found in default db', uid, file_path)
                return file_path, dbPath
        else:
            dbPath = False
        file_path = False
        recent = self['recent_database']
        # Add also implicit m3 databases
        if self['m3_enable']:
            for r in self['recent_m3database']:
                path = r[0]
                path = os.path.dirname(path)
                path = os.path.join(path, 'm4', 'database.sqlite')
                if not os.path.exists(path):
                    continue
                recent.append((path, 0))
        # Scan all recent db
        for path in recent:
            path = path[0]
            if path == dbPath:
                continue
            if not os.path.exists(path):
                logging.debug('Skip db:', path)
                continue
            try:
                db = Indexer(path)
            except:
                logging.info('Db open error: \n', format_exc())
                continue
            file_path = db.searchUID(uid)
            if file_path:
                dbPath = path
                break
            logging.debug('UID not found:', uid, path)
        if not file_path:
            return False
        self.known_uids[self.uid] = file_path
        return file_path, dbPath

    def last_directory(self, category):
        """Return most recently used directory for files in `category`"""
        tab = self['recent_' + category]
        logging.debug('new: tab', category, tab)
        d = ''
        if len(tab) > 0:
            print(tab)
            d = os.path.dirname(tab[-1][0])
        return d


import importlib


def activate_plugins(confdb):
    plugins = confdb['m3_plugins'].splitlines()
    for plug in plugins:
        plug = plug.replace('\n', '')
        print('Plugging: ', plug)
        try:
            importlib.import_module(plug)
        except:
            print_exc()
    # Add client configurations defined in 3rd parties
    print('updating clientconf', clientconf_update_functions)
    for update_func in clientconf_update_functions:
        update_func(confdb)
    confdb.migrate_desc()


settings = QtCore.QSettings(
    QtCore.QSettings.NativeFormat, QtCore.QSettings.UserScope, 'Expert Lab Service', 'MDF')


# Set the configuration db
def open_confdb():
    confdb = None
    cf = str(settings.value('/Configuration'))
    if cf == '' or not os.path.exists(cf):
        confdb = ConfDb(params.pathConf)
    elif os.path.exists(cf):
        params.set_pathConf(cf)
        confdb = ConfDb(path=cf)
    return confdb


confdb = open_confdb()
settings.setValue('/Configuration', confdb.path)
# activate_plugins(confdb)

from veusz import setting
setting.settingdb['ask_tutorial'] = True
setting.settingdb['feedback_asked_user'] = True
setting.settingdb['feedback_disabled'] = True
setting.settingdb['vercheck_asked_user'] = True
setting.settingdb['vercheck_disabled'] = True
if not os.path.exists(setting.settingdb['stylesheet_default']):
    setting.settingdb['stylesheet_default'] = os.path.join(params.pathArt, 'plot.vst')
    

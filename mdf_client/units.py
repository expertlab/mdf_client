# -*- coding: utf-8 -*-
"""Unit conversion"""
from math import *
from mdf_canon.units import *  # TODO: fix relay
from mdf_canon.units import known_units, Converter
import veusz.plugins as plugins
from copy import copy
import numpy as np
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)


def get_from_unit(ds, to_unit):
    """Get starting conversion unit for dataset `ds0` to convert it to `to_unit`"""
#     ds = getattr(ds0, 'pluginds', ds0)
    from_unit = getattr(ds, 'unit', False)
    if not from_unit or to_unit in ['None', '', None, False]:
        raise plugins.DatasetPluginException(
            'Selected dataset does not have a measurement unit.')
    # Implicit To-From percentage conversion
    from_group = known_units[from_unit]
    to_group = known_units[to_unit]
    if from_group != to_group:
        if 'part' not in (from_group, to_group):
            raise plugins.DatasetPluginException(
                'Incompatible conversion: from {} to {}'.format(from_unit, to_unit))
        if to_group == 'part':
            from_unit = 'percent'
        elif from_group == 'part':
            # Guess default unit for destination dimension
            from_unit = getattr(ds, 'old_unit', user_defaults[to_group])

    return from_unit, to_unit, from_group, to_group


def convert_func(ds, to_unit):
    """Returns conversion function which can be applied over an array or value
    to convert `ds0` to `to_unit`"""
#     ds = getattr(ds0, 'pluginds', ds0)
    from_unit, to_unit, from_group, to_group = get_from_unit(ds, to_unit)
    func = Converter.convert_func(from_unit, to_unit)
    ret = func
    # If groups differ, concatenate a percentage conversion to func
    if from_group != to_group:
        action = percent_action(ds, 'Invert')
        pfunc = percent_func(ds, action)
        ret = lambda out: func(pfunc(out))
    return ret


def convert(ds, to_unit, return_copy=True):
    """Convert dataset `ds` to `to_unit`.
    Returns a new dataset."""
    # In case ds derived from a plugin, return the original plugin dataset
#     ds = getattr(ds0, 'pluginds', ds0)
    from_unit, to_unit, from_group, to_group = get_from_unit(ds, to_unit)
    logging.debug('Convert', from_unit, to_unit, ds)
    func = convert_func(ds, to_unit)
    ds1 = ds
    if return_copy:
        ds1 = copy(ds)
    out = func(np.array(ds1.data))
    ds1.data = plugins.numpyCopyOrNone(out)
    ds1.unit = to_unit
    return ds1


def percent_action(ds, action='Invert'):
    """Autodetect percentage conversion action to be performed"""
    cur = getattr(ds, 'm_percent', False)
    # invert action
    if action == 'Invert':
        if cur:
            action = 'To Absolute'
        else:
            action = 'To Percent'
    logging.debug('percent_action', action)
    return action


def percent_func(ds, action='To Absolute', auto=True):
    """Returns the function used to convert dataset `ds` to percent or back to absolute"""
    ini = getattr(ds, 'm_initialDimension', False)
    # Auto initial dimension
    if not ini:
        if not auto or action != 'To Percent':
            raise plugins.DatasetPluginException('Selected dataset does not have an initial dimension set. \
        Please first run "Initial dimension..." tool. {}{}'.format(action, ds))
        ini = np.array(ds.data[:5]).mean() or 100
        logging.debug('calculated initial dimension:', ini, ds)
        ds.m_initialDimension = ini
    
    if action == 'To Absolute':
        # If current dataset unit is not percent, convert to
        u = getattr(ds, 'unit', 'percent')
        convert_func = Converter.convert_func(u, 'percent')
        func = lambda out: convert_func(out * ini / 100.)
    elif action == 'To Percent':
        func = lambda out: 100. * out / ini
    return func


def percent_conversion(ds, action='Invert', auto=True):
    ds = copy(ds)
    action = percent_action(ds, action)
    func = percent_func(ds, action, auto)
    out = func(np.array(ds.data))

    # Evaluate if the conversion is needed
    # based on the current status and the action requested by the user
    if action == 'To Absolute':
        u = getattr(ds, 'unit', 'percent')
        ds.unit = getattr(ds, 'old_unit', False)
        ds.old_unit = u
    elif action == 'To Percent':
        ds.old_unit = getattr(ds, 'unit', False)
        ds.unit = 'percent'
    ds.data = plugins.numpyCopyOrNone(out)
    return ds

#!/bin/bash

T=`readlink -m "${BASH_SOURCE[0]}"`
CLIENTROOT="$( cd "$( dirname "${T}" )"/.. && pwd )"
CLIENTDIR=$CLIENTROOT/mdf_client
export PYTHONPATH=$CLIENTROOT:$CLIENTROOT/../mdf_babel:$PYTHONPATH

####
# Client startup functions
####

function maq {
    reset;clear;
	python3 -m mdf_client.bin.main --live $@
}
function mcf {
    reset;clear;
	python3 -m mdf_client.bin.main --conf $@
}
function mgr {
    reset;clear;
	python3 -m mdf_client.bin.main --graphics $@
}

function mbr {
    reset;clear;
	python3 -m mdf_client.bin.main --browser $@
}

function mar {
    reset;clear;
	mbr
}

function mer {
    reset;clear;
    python3 -m mdf_client.bin.main --meridian $@
}


####
# Interactive prompt
####
function msc {
	oldir=`pwd`
	cd $CLIENTDIR
	python3 -m mdf_client.bin.main --cmd -r99 $@
	cd "$oldir"
}

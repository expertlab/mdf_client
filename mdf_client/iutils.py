#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Standard interface construction utilities"""
import os
from functools import cmp_to_key
try:
    import psutil
except:
    psutil = False
import sys
import collections
from collections import OrderedDict, defaultdict
from time import sleep
import signal
from pickle import loads, dumps
from mdf_canon.csutil import isWindows
from veusz.veusz_main import excepthook
import veusz.dialogs.exceptiondialog

from mdf_client.qt import QtWidgets, QtGui, QtCore

from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)

from . import parameters as params
from . import network
from .live import registry  # needed for initialization
from .clientconf import confdb, settings, activate_plugins
signal.signal(signal.SIGINT, signal.SIG_DFL)  # cattura i segnali
import veusz.utils
from veusz import document
import ssl 
from mdf_canon import csutil

from veusz.utils import searchFirstOccurrence, iter_widgets
from numpy import sqrt

veusz.dialogs.exceptiondialog._emailUrl = None
app = None
translators = []

network.manager.sig_connection.connect(confdb.mem_server)
network.manager.sig_found.connect(confdb.found_server)
network.manager.sig_lost.connect(confdb.rem_server)

net = network.manager


def initClient():
    global translators, app, confdb, registry
    app = QtWidgets.QApplication.instance()
    app.aboutToQuit.connect(closeApp)
    app.lastWindowClosed.connect(closeApp)
    initTranslations(app)
    initNetwork()
    initRegistry()
    activate_plugins(confdb)

    
if psutil:
    process = psutil.Process(os.getpid())
else:
    process = -1
limit = 4e9
security_limit = 2.5e9
last_ram = 1e9


def memory_check(warn=False):
    """This is a palliative message box which will warn the user whenever approaching the 
    maximum addressable memory by a 32bit process."""
    global last_ram
    if process == -1:
        return (False, 0)
    ram = process.memory_info().rss
    r = False
    if ram > security_limit:
        r = True
        print('Running out of RAM:', ram, last_ram)
        if warn and ram > last_ram:
            msg = '{:.1f}GB < {:.1f}GB'.format(ram / 1e9, limit / 1e9)
            msg = 'Approaching maximum RAM usage:\n\t{}'.format(msg)
            msg += '\nPlease some close tests or unload some data.'
            QtWidgets.QMessageBox.critical(None, 'Memory Limit', msg)
    last_ram = ram
    return (r, ram)


def initTranslations(app):
    appTranslator = QtCore.QTranslator()
    pathLang = os.path.join(params.determine_path(), 'i18n')
    logging.debug('initClient: pathLang', pathLang)
    lang = confdb['lang']
    if lang == 'sys':
        lang = params.locale
    logging.debug("misura_" + lang)
    if appTranslator.load("misura_" + lang, pathLang):
        logging.debug('installing translator')
        app.installTranslator(appTranslator)
        # devo creare un riferimento permanente onde evitare che il QTranslator
        # vada distrutto
        translators.append(appTranslator)
    else:
        logging.debug('translations not available')


def initNetwork():
    """Start zeroconf network scanner - deprecated"""
    # network.manager.start()
    pass


def initRegistry():
    # Avvio il registro
    registry.set_manager(network.manager)
    registry.toggle_run(True)


def closeRegistry():
    logging.debug('iutils.closeRegistry')
    registry.toggle_run(False)
    sleep(1)
    registry.quit()
    registry.wait()


stylesheet = ""

defaultexcepthook = sys.excepthook


def initApp(name='mdf', org="Expert Lab Service", domain="expertlabservice.it", client=True, qapp=False):
    """Inzializzazione generale di una applicazione misura Client.
    Creazione QApplication, installazione dei traduttori"""
    global translators, app
    app = qapp
    if not app:
        app = QtWidgets.QApplication(sys.argv)
    app.aboutToQuit.connect(closeApp)
    app.setOrganizationName(org)
    if domain:
        app.setOrganizationDomain(domain)
    app.setApplicationName(name)
    app.setStyleSheet(stylesheet)
    if client:
        initClient()
    app.setAttribute(QtCore.Qt.AA_DontShowIconsInMenus, False)
    global defaultexcepthook
    defaultexcepthook = sys.excepthook
    sys.excepthook = excepthook
    return app


app_closed = False


def closeApp():
    """Connected to quit and last window closed signals."""
    # Avoid closing multiple times if multiple signals are sent!
    global app_closed
    if app_closed:
        return
    app_closed = True
    logging.debug('Closing App')
    # Save configuration
    confdb.save()
    closeRegistry()
    network.manager.scan = False
    sleep(0.1)
    network.manager.terminate()
    network.closeConnection()
    app.quit()


def xcombinations(items, n):
    # Origine:
    # http://code.activestate.com/recipes/190465/
    if n == 0:
        yield []
    else:
        for i in range(len(items)):
            for cc in xcombinations(items[:i] + items[i + 1:], n - 1):
                yield [items[i]] + cc


def plotPalette(n):
    """Generatore di tavolozza massimo contrasto per i grafici."""
    l = n ** (1 / 3.) + 2  # arrotondamento per eccesso
    l = int(l)
    v = range(255, 0, int(-255 / (l)))
    v.append(0)
    cc = []
    for c in xcombinations(v, 3):
        cc.append(c)
    r = []
    for i in range(2 * n):
        if i % 2 == 0:
            i = i // 2
        else:
            i = -i
        r.append(cc[i])
    return r


def isProxy(obj):
    r = repr(obj)
    return ('AutoProxy' in r) or ('MisuraProxy' in r)


def guessNextName(name):
    """Add +1 to a name composed of string+integer"""
    v = list(name)
    if len(v) < 1:
        return '', 0, '0'
    dg = set('0123456789')
    if v[-1] not in dg:
        return name, 1, name + '1'
    n = []
    while len(v) > 0:
        c = v.pop()
        if c in dg:
            n.append(c)
        else:
            v.append(c)
            break
    n.reverse()
    n = int(''.join(n))
    name = ''.join(v)
    return name, n, name + str(n + 1)


def num_to_string(val):
    if type(val) == type(''):
        return val
    a = abs(val)
    if 0.01 < a < 1000 or a < 10 ** -14:
        s = '%.2f' % val
    elif 1000 < a < 10000:
        s = '%.1f' % val
    else:
        s = '%.2E' % val
    pt = QtCore.QLocale().decimalPoint()
    return s.replace('.', pt)


def getOpts():
    """-h Address
    -o Object
    Returns a dictionary with defaults or expressed variables.
    """
    import sys
    import getopt
    opts, args = getopt.getopt(sys.argv[1:], 'h:o:')
    logging.debug(opts, args)
    h = 'https://admin:admin@localhost:3880/RPC'
    o = '/'
    r = {'-h': False, '-o': False}
    for opt, val in opts:
        if opt == 'o':
            if not val.startswith('/'):
                val = '/' + val
            if not val.endswith('/'):
                val += '/'
        if opt == 'h':
            if not h.startswith('https://'):
                val = 'https://' + val
            if not h.endswith('/RPC'):
                val += '/RPC'
        r[opt] = val
    return r


def get_custom(doc, name):
    for ctype, cname, val in doc.customs:
        if cname == name:
            return val
    return False


def most_involved_node(involved_plots, doc, exclude=':kiln'):
    # Collect all involved datasets
    involved = []
    for inp in involved_plots:
        # 'plot' entry is created by get_plotted_tree
        # Add to involved all datasets involved in all curves plotted
        # in the involved_plots
        involved += doc.model.plots['plot'].get(inp, [])
    # Find the common ancestor
    involved = [inv.split('/') for inv in involved]
    best_involved = involved
    lengths = [len(inv) for inv in involved]
    max_len = max(lengths)
    best_count = -1
    crumbs = []
    # Groups of most common names sorted by depth
    most_commons = [] 
    best = False
    for i in range(max_len):
        # Exclude the last element
        if len(crumbs) >= max_len:  # -1:
            break
        # Keep only longer than current depth (i)
        best_involved = list(filter(lambda inv: len(inv) > i + 1,
                               best_involved))
        # Then keep only common anchestors
        if crumbs:
            best_involved = list(filter(lambda inv: inv[:len(crumbs)] == crumbs,
                                   best_involved))
        
        # Names of the current depth (i) across all involved datasets
        level = [inv[i] for inv in best_involved]
        level = list(filter(lambda el: exclude not in el, level))
        
        # Find the most common name for the current depth (i)
        j = -1
        mc = []
        best = False
        for m, count in collections.Counter(level).most_common():
            if count > j:
                best = m
            j = count
            mc.append(m)
        if not best:
            break
        most_commons.append(mc)
        # Stop if the count decreases
        if count < best_count:
            break
        best_count = count
        crumbs.append(best)
        
    print('most_common', involved_plots, involved, crumbs, most_commons)
    return crumbs, most_commons


def calc_plot_hierarchy(doc, page_obj, exclude=':kiln/'):
    pages = doc.model.plots['page']

    hierarchy = defaultdict(list)

    for page, page_plots in pages.items():
        crumbs = most_involved_node(page_plots, doc, exclude=exclude)[0]
        notes = doc.resolveWidgetPath(None, page).settings.notes
        hierarchy[len(crumbs)].append((page, page_plots, crumbs, notes))
    
    hierarchy = sorted(list(hierarchy.items()), key=cmp_to_key(lambda a, b: a[0] - b[0]))
    hierarchy = [sorted(h[1], key=lambda a: '/'.join(a[2]).lower())
                 for h in hierarchy]
    inpage = False
    level = -1
    page_idx = -1
    for level, pages in enumerate(hierarchy):
        for page_idx, (page_name, page_plots, crumbs, notes) in enumerate(pages):
            if page_name == page_obj.name:
                inpage = True
                break
        if inpage:
            break
    if not inpage:
        level = -1
        page_idx = -1

    return hierarchy, level, page_idx


def getUsedPrefixes(doc):
    p = {}
    for name, ds in doc.data.items():
        lf = ds.linked
        if lf is None:
            logging.debug('no linked file for ', name)
            continue
        p[lf.filename] = lf
    logging.debug('getUsedPrefixes', p, list(doc.data.keys()))
    return p


def get_plotted_tree(base, m=False):
    """Builds a dictionary for the base graph:
            m => {'plot': {plotpath: dsname,...},
                      'dataset': {dsname: plotpath,...},
                      'axis':{axispath:[ds0,ds1,...]},
                      'xaxis':{axispath:[ds0, ds1, ...]},
                      'sample':[smp0,smp1,...]. 
                      'page': [plot names...}"""
    if m is False:
        m = {'plot': OrderedDict(),
             'dataset': OrderedDict(),
             'xdataset': OrderedDict(),
             'axis': OrderedDict(),
             'xaxis': OrderedDict(),
             'sample': [],
             'page': defaultdict(list)}
    for wg in base.children:
        # Recurse until I find an xy object
        if wg.typename in ('page', 'grid'):
            m = get_plotted_tree(wg, m)
        elif wg.typename == 'graph':
            m = get_plotted_tree(wg, m)
        elif wg.typename == 'xy':
            dsn = wg.settings.yData
            xdsn = wg.settings.xData
            ds = wg.document.data.get(dsn, False)
            if ds is False:
                continue
            
            if wg.path not in m['plot']:
                m['plot'][wg.path] = []
            m['plot'][wg.path].append(dsn)
            
            if dsn not in m['dataset']:
                m['dataset'][dsn] = []
            m['dataset'][dsn].append(wg.path)
            
            if xdsn not in m['xdataset']:
                m['xdataset'][xdsn] = []
            m['xdataset'][xdsn].append(wg.path)
            
            # Fill page: plots map
            page = searchFirstOccurrence(wg, 'page', -1)
            m['page'][page.name].append(wg.path)

            # Save the dataset under its axis key
            axpath = wg.parent.path + '/' + wg.settings.yAxis
            if axpath not in m['axis']:
                m['axis'][axpath] = []
            m['axis'][axpath].append(dsn)
            
            # Save also x axis
            xaxpath = wg.parent.path + '/' + wg.settings.xAxis
            if xaxpath not in m['xaxis']:
                m['xaxis'][xaxpath] = []
            m['xaxis'][xaxpath].append(wg.settings.xData)

            # Fill sample map
            if not '/sample' in dsn:
                continue
            smp = getattr(ds, 'm_smp', False)
            if not smp:
                continue
            if smp.ref:
                continue
            m['sample'].append(smp['fullpath'])

        elif wg.typename in ('axis', 'axis-function'):
            if wg.settings.direction != 'vertical':
                if wg.path not in m['xaxis']:
                    m['xaxis'][wg.path] = []
            elif wg.path not in m['axis']:
                m['axis'][wg.path] = []
    # Persistent sorting
    for k0 in list(m.keys()):
        if k0 in ('sample', 'changeset'):
            continue
        for k1 in m[k0].keys():
            m[k0][k1] = sorted(m[k0][k1])
    return m


def shorten(name, number_of_chars_to_show=30):
    if len(name) <= number_of_chars_to_show:
        return name
    n2 = int(number_of_chars_to_show // 2)
    return name[0:n2] + "..." + name[-n2:]

# Caricamento icone


def loadIcons():
    """Icons loading. Must be called after qapplication init."""
    # d=list(os.path.split(veusz.utils.utilfuncs.resourceDirectory))[:-1]+['misura','client','art']
    # artdir=os.path.join(*tuple(d))

    for key in ['m4.connect', 'm4.conf', 'm4.navigator', 'm4.db', 'm4.open', 'm4.sintering',
                'm4.softening', 'm4.sphere', 'm4.halfSphere',
                'm4.melting', 'm4.single-ramp', 'm4.icon']:
        n = key.split('.')[1] + '.svg'
        n = os.path.join(params.pathArt, n)
        logging.debug(n)
        if not os.path.exists(n):
            continue
        veusz.utils.action._iconcache[key] = QtGui.QIcon(n)

        
def theme_icon(name, ext='.svg', missing=QtGui.QIcon()):
    if QtGui.QIcon.hasThemeIcon(name):
        return QtGui.QIcon.fromTheme(name)
    p = os.path.join(params.pathIcons, name + ext)
    if not os.path.exists(p):
        p = os.path.join(params.pathArt, name + ext)
    if not os.path.exists(p):
        if missing is not None:
            logging.debug('ICON not found', name, p)
        return missing
    return QtGui.QIcon(p)


def with_waiting_mouse_cursor(function_to_call):
    QtWidgets.QApplication.setOverrideCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))
    try:
        function_to_call()
    finally:
        QtWidgets.QApplication.restoreOverrideCursor()
        
        
class QMenuWithToolTips(QtWidgets.QMenu):

    def event(self, ev):
        """Tooltip preview handling"""
        if ev.type() == QtCore.QEvent.ToolTip and self.activeAction():
            QtWidgets.QToolTip.showText(
                ev.globalPos(), self.activeAction().toolTip())
        else:
            QtWidgets.QToolTip.hideText()
        return QtWidgets.QMenu.event(self, ev)

    
from . import _


def informative_message_box(r, parent=None, limit=200, title=_('Operation Result')):
    r1 = r
    if isinstance(r, bytes):
        r = r.decode('utf8')
    more = False
    if len(str(r)) > limit:
        more = True
        r = r[:limit - 10] + '...'
    msg = QtWidgets.QMessageBox(parent=parent)
    msg.setWindowTitle(title)
    msg.setWindowFlags(QtCore.Qt.Dialog)
    msg.setInformativeText(str(r))
    if more:
        msg.setDetailedText(r1)
    return msg  

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Repeat image analysis"""
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from mdf_canon.reference import get_reference, get_node_reference
# TODO: move to morphometrix package
try:
    from mdf_imago import postanalysis
    enabled = True
except:
    enabled = False
    logging.debug('Post-analysis disabled: no morphometrix package.')


def deferred_postanalysis(*a, **k):
    from mdf_client.qt import QtCore
    from mdf_client import widgets
    aborted = [False]
    def abort():
        aborted[0]=True
    thread = widgets.RunMethod(postanalysis.postanalyze, *a, **k)
    thread.step =100
    thread.kwargs['callback'] = thread.job
    thread.pid = 'Post-analysis'
    thread.abort = abort
    QtCore.QThreadPool.globalInstance().start(thread)
    return thread

if __name__ == '__main__':
    #test_path = '/home/daniele/MisuraData/hsm/BORAX powder 10 C min.h5'
    #data_path = '/hsm/sample0'

    test_path = '/home/daniele/MisuraData/horizontal/profiles/System Interbau 80 1400.h5'
    data_path = '/horizontal/sample0/Right'
    from mdf_canon.indexer import SharedFile
    f = SharedFile(test_path)
    f.load_conf()
    smp = f.conf.toPath(data_path)
    postanalysis(f, smp.analyzer, smp)

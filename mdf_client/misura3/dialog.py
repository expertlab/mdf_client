# -*- coding: utf-8 -*-
import os
from traceback import format_exc

from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)

from .preview import ImagePreview
from .table import TestsTable
from . import convert
from .m3db import getImageCode, get_mdb_interface, etp, validate_tabname

from ..live import registry
from mdf_client import widgets

from mdf_client.qt import QtWidgets, QtGui, QtCore

settings = QtCore.QSettings(
    QtCore.QSettings.NativeFormat, QtCore.QSettings.UserScope, 'Expert Lab Service', 'MDF')
    

class TestDialog(QtWidgets.QWidget):

    """Dialogo per la visualizzazione delle prove contenute in un database Misura3."""
    format = 'ImageM3'  # Do not do image format decompression while converting
    img = True  # Require images in the produced file
    keep_img = True  # Keep images in the produced file
    force = True  # Update existing files
    converter = False # File converter
    progress_timer = False
    imported = QtCore.pyqtSignal(str)
    batch = 0
    batches = 0
    
    def __init__(self, path=False, importOptions=True, parent=None):
        global settings
        QtWidgets.QWidget.__init__(self, parent)
        self.setMinimumSize(600, 0)
        self.lay = QtWidgets.QVBoxLayout()
        self.setLayout(self.lay)
        self.importAllFields = False
        self.dbif = False
        
        self.table = TestsTable(parent=self)
        self.lay.addWidget(self.table)

        # Ricerca per tipo
        self.filterType = QtWidgets.QComboBox(self)
        for lbl in dir(etp):
            if '_' in lbl:
                continue
            i = getattr(etp, lbl)
            if i in [1,2,4,16]:
                continue
            self.filterType.addItem(str(lbl), i)
        self.filterType.currentIndexChanged.connect(self.setFilterType)

        # Ricerca per stringa
        self.searchText = QtWidgets.QLineEdit(self)
        self.searchText.returnPressed.connect(self.setSearchText)
        
        self.btn_previous = QtWidgets.QPushButton('<')
        self.btn_previous.clicked.connect(self.previous_batch)
        self.btn_next = QtWidgets.QPushButton('>')
        self.btn_next.clicked.connect(self.next_batch)  
        
        self.do = QtWidgets.QPushButton('Import')
        self.do.clicked.connect(self.select)

        # Import Options
        self.doPreview = QtWidgets.QCheckBox('Preview images below', self)
        self.doPreview.clicked.connect(self.preview)
        self.doPreview.setCheckState(0)

        self.doForceData = QtWidgets.QCheckBox('Force data update', self)
        self.doForceData.setCheckState(0)
        
        self.doOpen = QtWidgets.QCheckBox('Open after import', self)
        self.doOpen.setCheckState(2)        

        self.doForceImages = QtWidgets.QComboBox(self)
        self.doForceImages.addItem('Require images')
        self.doForceImages.addItem('Do not require images')
        self.doForceImages.addItem('Force new image conversion')
        if not self.img:
            u = 1
        if self.img and self.keep_img:
            u = 0
        if self.img and not self.keep_img:
            u = 2
        self.doForceImages.setCurrentIndex(u)

        grid = QtWidgets.QWidget(self)
        glay = QtWidgets.QGridLayout()
        grid.setLayout(glay)

        j = 0
        glay.addWidget(self.filterType, j, 0)
        glay.addWidget(self.searchText, j, 1)
        glay.addWidget(self.btn_previous, j, 2)
        glay.addWidget(self.btn_next, j, 3)

        # Show import options
        if importOptions:
            j += 1
            glay.addWidget(self.doPreview, j, 0)
            glay.addWidget(self.doForceImages, j, 1)
            j += 1
            glay.addWidget(self.doForceData, j, 0)
            glay.addWidget(self.doOpen, j, 1)
            j += 1
            glay.addWidget(self.do, j, 1)
        else:
            self.doPreview.hide()
            self.doForceData.hide()
            self.doForceImages.hide()
            self.doOpen.hide()
        self.lay.addWidget(grid)

        self.strip = ImagePreview(self)
        self.strip.hide()
        self.lay.addWidget(self.strip)
        # Option to execute a full import to dictionary or only a path|id
        # import:
        self.fullImport = False
        self.table.doubleClicked.connect(self.select)
        self.table.clicked.connect(self.preview)
        
        # Connessione DB
        self.path = path
        if path:
            self.setDB(self.path)

    def setDB(self, path):
        """Open the database path"""
        if self.dbif:
            del self.dbif
        try:
            self.setWindowTitle('M3: '+path)
            self.dbif = get_mdb_interface(path)
            self.table.path = path
            self.resetList()
        except:
            logging.error('setDB: ',format_exc())
        

    def resetList(self, *args):
        tests = self.dbif.list_tests()
        logging.debug('Retrieved tests:', len(tests), self.path)
        self.table.curveModel.setTests(tests)
        self.table.resizeRowsToContents()
        self.batch = 0
        self.batches = len(tests)//self.table.curveModel.limit 
        self.btn_previous.setEnabled(False)
        self.btn_next.setEnabled(self.batches>0)
        
    @property
    def batch_start(self):
        return self.batch*self.table.curveModel.limit 
        
    def next_batch(self):
        self.batch+=1
        if self.batch >= self.batches:
            self.btn_next.setEnabled(False)
        self.btn_previous.setEnabled(True)
        self.table.curveModel.set_batch(self.batch)
    
    def previous_batch(self):
        self.batch-=1
        if self.batch==0:
            self.btn_previous.setEnabled(False)
        self.btn_next.setEnabled(True)
        self.table.curveModel.set_batch(self.batch)
            

    def setFilterType(self, idx):
        tt = self.filterType.itemData(idx)
        if tt>=0:
            logging.debug('Selecting', tt)
            rl = {110: "R",111:"L"}.get(tt, "")
            tests = self.dbif.filter_tests(tt, rl)
        else:
            tests = self.dbif.list_tests()
        self.table.curveModel.setTests(tests)

    def setSearchText(self):
        t = str(self.searchText.text()).lower()
        if len(t) < 1:
            self.resetList()
            return
        tests = self.table.curveModel.tests
        ntests = []
        for row in tests:
            if t in row[2].lower():
                ntests.append(row)
        self.table.curveModel.setTests(ntests)

    def getCode(self, row):
        prova = self.table.curveModel.tests[row]
        return getImageCode(prova[0])
        
    def done(self, pid, failed=False):
        logging.debug('Setting converted done', pid)
        if not self.converters:
            if self.progress_timer:
                self.progress_timer.stop()
                self.progress_timer = False
            return
        converter = False
        for c in self.converters:
            if pid==c.pid:
                converter = c
                break
        if not converter:
            return
            
        self.converters.remove(c)
        if failed:
            registry.tasks.failed(converter.pid, failed)
            return 
            
        registry.tasks.done(c.pid)
        outpath = converter.outpath
        
        if converter.progress < 100:
            logging.error('Conversion aborted', outpath)
            converter.interrupt = True
            return
        logging.debug('Conversion ended, exported to:', outpath)
        self.done_paths.append(outpath)

    def convert(self, path):
        path = str(path)
        dbpath, idprove = path.split('|')
        outdir = os.path.dirname(dbpath)
        outdir = os.path.join(outdir, 'm4')
        if not os.path.exists(outdir):
            os.mkdir(outdir)
        self.outdir = outdir
        
        self.force = self.doForceData.isChecked()
        fi = self.doForceImages.currentIndex()

        if fi == 0:
            self.keep_img = True
            self.img = True
        elif fi == 1:
            self.img = False
            self.keep_img = True
        elif fi == 2:
            self.img = True
            self.keep_img = False
        
        converter = convert.Converter(dbpath, self.outdir)
        outpath = converter.get_outpath(idprove, img=self.img,
                                             keep_img=self.keep_img,
                                             force=self.force)
        if not converter.outpath:
            logging.error('Could not convert empty test', path)
            return False
        if os.path.exists(converter.outpath) and not outpath:
            outpath = converter.outpath
            logging.debug('Skipping the export process, file found:', outpath)
            if self.doOpen.isChecked():
                self.imported.emit(outpath)
            if converter.outFile:
                converter.outFile.close(all_handlers=True)
            return False
        converter.pid = 'Converting to misura format: ' + idprove
        registry.tasks.jobs(100, converter.pid)
        self.converters.append(converter)
        try:
            converter.convert(frm=self.format)   
            self.done(converter.pid)
            return True
        except:
            e = format_exc()
            logging.error('Conversion failed:', e)
            self.done(converter.pid, failed=e)
            return False
        
    conversion_started = 0
    def update_progress(self):
        if self.converters and self.progress_timer and not self.conversion_started:
            self.conversion_started = 1
            return 
        if self.conversion_started and not self.converters and self.progress_timer:
            logging.debug('Ending progress_timer')
            self.progress_timer.stop()
            return
        for c in self.converters:
            registry.tasks.job(c.progress, c.pid)
        QtWidgets.qApp.processEvents()
        
    def do_conversion(self):
        logging.debug('do_conversion start',len(self.target_paths))
        for i,path in enumerate(self.target_paths):
            if self.conversion_task.error:
                logging.info('Conversion task was interrupted at {} of {}'.format(i,len(self.target_paths)))
                break
            registry.tasks.job(i, self.conversion_task.pid, path)
            self.convert(path)
        logging.debug('do_conversion end')
            
    def after_conversion(self):
        if self.doOpen.isChecked():
            for p in self.done_paths:
                self.imported.emit(p)
            
    def select(self, idx=False):
        """Import selected test/tests"""
        sel = self.table.selectedIndexes()
        sel = set([idx.row()+self.batch_start for idx in sel])
        self.converters = []
        self.target_paths = []
        self.done_paths = []
        N = len(sel)
        self.continue_task = True
        logging.debug('importing tests:', N)
        # Never implicitly open if massive
        if N>10:
            self.doOpen.setCheckState(0)
        for i in sel:
            prova = self.table.curveModel.tests[i]
            logging.debug('importing test: ', prova)
            self.tname = validate_tabname(prova[2])
            imported = self.path + '|' + str(prova[0])
            self.target_paths.append(imported)
        self.conversion_task = widgets.RunMethod(self.do_conversion)
        self.conversion_task.step = N     
        self.conversion_task.pid = 'Converting {} tests'.format(N)
        self.conversion_task.notifier.ended.connect(self.after_conversion)
        self.conversion_task.init_run()
        self.conversion_task.do()
        
        self.progress_timer = QtCore.QTimer(self)
        self.progress_timer.setInterval(500)
        self.progress_timer.timeout.connect(self.update_progress)
        self.progress_timer_started = 0
        self.progress_timer.start()
        
    
    def preview(self, idx=False):
        if not self.doPreview.isChecked():
            self.strip.hide()
            return
        print(idx, type(idx))
        idx = self.table.selectedIndexes()[0]
        if not idx:
            return
        i = idx.row()
        prova = self.table.curveModel.tests[i]
        if prova[3] not in [etp.ProvinoSingolo, etp.ProvinoDoppioCompleto, etp.ProvinoDoppioParziale, etp.SoloImmagini]:
            self.strip.hide()
            return
        code = self.getCode(i)
        image_dir = os.path.dirname(str(self.path)) + '/' + code + '/' + code + 'H'
        rows = self.dbif.get_data(code)
        self.strip.dmodel.setPathData(image_dir, rows)
        self.strip.show()
        logging.debug('Showing images from',len(rows),image_dir)


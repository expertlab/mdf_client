#!/usr/bin/python2.6
# -*- coding: utf-8 -*-
"""Converts from Misura3 to MDF database formats"""
import tables
import numpy as np
from copy import deepcopy
import os
from traceback import format_exc
from datetime import datetime
from time import time, mktime
import string
import hashlib

from PIL import Image

from mdf_canon import bitmap, option, reference, indexer
from mdf_canon.option import ao
from mdf_canon.csutil import basestring, StringIO
from mdf_canon.plugin import dataimport

from . import m3db

try:
    from mdf_imago import postanalysis
    from mdf_imago import std_m3, std_mdf, edge
except:
    print(format_exc())
    postanalysis = False
    

logging = dataimport.SelectLogging()

valid_chars = "-_. %s%s" % (string.ascii_letters, string.digits)

# Heating curve point
HeatingCyclePoint = {
    't': tables.Float32Col(pos=1),
    'T': tables.Float32Col(pos=2),
    'chk': tables.BoolCol(pos=3)
}

base_dict = dataimport.base_dict()

measure_dict = dataimport.measure_dict()


measure_scripts = [
    # end (End of test)
    {"handle": 'end', "name": 'End of the test',
        "type": 'Meta',
     },
    {"handle": 'scrEnd', "name": 'End of test',
        "current": 'mi.Point(idx=-1)', "parent": 'end',
        "flags": {'period': -1}, "type": 'Script', "readLevel": 3, "writeLevel": 3,
     },

    {"handle": 'endStatus', "name": "End status", "attr": ['ReadOnly', 'Result'],
        "current": '', "writeLevel": 5, "type": 'TextArea'},

    #------

    # maxT (Maximum Temperature)
    {"handle": 'maxT', "name": 'Maximum Temperature', "type": 'Meta'},
    {"handle": 'scrMaxT', "name": 'Maximum Temperature',
     "current": """
i,t,T=mi.Max('T')
mi.t(t)
mi.T(T)
""", "parent": 'maxT',
        "flags": {'period': -1}, "type": 'Script', "readLevel": 3, "writeLevel": 3,
     },

    # maxHeatingRate (Maximum Heating Rate)
    {"handle": 'maxHeatingRate',
        "name": 'Maximum Heating Rate', "type": 'Meta', "unit":{'value':u'celsius/min'}},
    {"handle": 'scrMaxHeatingRate', "name": 'Maximum Heating Rate',
        "current": """
T1=kiln.TimeDerivative('T')
if len(T1)<10: mi.Exit()
rate=max(T1)
w=mi.Where(T1==rate)
if w<0: mi.Exit()
mi.Point(idx=w+1)
mi.Value(rate*60)

""", "parent": 'maxHeatingRate', "readLevel": 3, "writeLevel": 3,
        "flags": {'period': -1}, "type": 'Script',
     },

    # coolingDuration (Total cooling duration)
    {"handle": 'coolingDuration',
        "name": 'Total cooling duration', "type": 'Meta'},
    {"handle": 'scrCoolingDuration', "name": 'Total cooling duration',
     "current": """
ret = mi.GetCoolingTimeAndIndex()
if not ret:
    mi.Exit()
cooling_time =ret[0]
t, T = mi.AtTime('T', cooling_time)
end_t, end_T = mi.AtIndex('T', -1)
mi.T(T - end_T)
mi.t(end_t - t)
""", "parent": 'coolingDuration', "readLevel": 3, "writeLevel": 3,
        "flags": {'period': -1}, "type": 'Script',
     },

    # maxCoolingRate (Maximum Cooling Rate)
    {"handle": 'maxCoolingRate',
        "name": 'Maximum Cooling Rate', "type": 'Meta',"unit":{'value':u'celsius/min'}},
    {"handle": 'scrMaxCoolingRate', "name": 'Maximum Cooling Rate',
     "current": """
ret = mi.GetCoolingTimeAndIndex()
if not ret:
    mi.Exit()
cooling_time, cooling_index = ret
T1=mi.TimeDerivative('/kiln/T', cooling_time)
if len(T1)<10: mi.Exit()
rate=min(T1)
w = mi.Where(T1 == rate) - 1
if w<0: mi.Exit()
mi.Value(rate/60)
mi.Point(idx=w)
""", "parent": 'maxCoolingRate', "readLevel": 3, "writeLevel": 3,
        "flags": {'period': -1}, "type": 'Script',
     },
]

for scr in measure_scripts:
        ao(measure_dict, **scr)

smp_dict = dataimport.base_dict()
smp_dict['name']['current'] = 'Sample'
ao(smp_dict, 'idx', 'Integer', attr=['Hidden'])
ao(smp_dict, 'ii', 'Integer', attr=['Hidden'])
ao(smp_dict, 'initialDimension', 'Float', 3000., name='Initial dimension')
ao(smp_dict, 'initialWidth', 'Float', 2000., name='Initial width')
ao(smp_dict, 'roi', 'Rect', [0, 0, 640, 480], attr=['Hidden'])
ao(smp_dict, 'profile', 'Profile', attr=['Hidden'])
ao(smp_dict, 'frame', 'Image', attr=['Hidden'])
ao(smp_dict, 'startTemp', 'Float',-1000,'Characterization start temperature')
ao(smp_dict, 'anStartTemp', 'Float',-1000,'Image analysis start temperature',attr=['Hidden'])
ao(smp_dict, 'recFrame', 'Boolean',True,'Record frames', attr=['Hidden'])
ao(smp_dict, 'recProfile', 'Boolean',True,'Record profiles',attr=['Hidden'])
# frame?

hsm_smp_dict = deepcopy(smp_dict)

for opt in std_mdf.sample_conf+std_mdf.conf+std_m3.conf:
    ao(hsm_smp_dict, **opt)

dil_smp_dict = deepcopy(smp_dict)
for opt in edge.sample_definition:
    ao(dil_smp_dict, **opt)

flex_smp_dict = deepcopy(dil_smp_dict)

for opt in edge.dilatometer_definition:
    ao(dil_smp_dict, **opt)

kiln_dict = dataimport.kiln_dict()
kiln_dict['name']['current'] = 'Kiln'
ao(kiln_dict, 'Regulation_Kp', 'Float', 0, 'Proportional Factor')
ao(kiln_dict, 'Regulation_Ki', 'Float', 0, 'Integral Factor')
ao(kiln_dict, 'Regulation_Kd', 'Float', 0, 'Derivative Factor')

instr_dict = dataimport.instr_dict()
ao(instr_dict, 'camera', 'Role', ['camerapath', 'default'])

camera_dict = dataimport.instr_dict()
camera_dict['name']['current'] = 'camera'
camera_dict['devpath']['current'] = 'camerapath'
camera_dict['dev']['current'] = 'camera'
ao(camera_dict, 'last_frame', 'Image', [], attr=['Hidden'])
ao(camera_dict, 'size', 'List', [640, 480], attr=['Hidden'])
ao(camera_dict, 'Analysis_umpx', 'Integer', 1, name='Micron to pixel conversion')
ao(camera_dict, 'nSamples', 'Integer', 1, attr=['Hidden'])
ao(camera_dict, 'Analysis_instrument', 'String', 'hsm', attr=['Hidden'])
ao(camera_dict, 'Analysis_Simulation', 'Boolean', True, attr=['Hidden'])


anl_dict = dataimport.base_dict()
ao(anl_dict, 'blackWhite', 'Boolean', True, attr=['Hidden'])
ao(anl_dict, 'adaptiveThreshold', 'Boolean', False, attr=['Hidden'])
ao(anl_dict, 'autoregion', 'Boolean', False, attr=['Hidden'])

server_dict = dataimport.server_dict()

empty = {'self': deepcopy(camera_dict)}

# Tree for generical sample
smp_tree = {'self': smp_dict}

# Tree for hsm sample
hsm_smp_tree = {'self': hsm_smp_dict}

# Tree for hsm sample
dil_smp_tree = {'self': dil_smp_dict}
flex_smp_tree = {'self': flex_smp_dict}

# Tree for generical instrument
instr_tree = {'self': instr_dict,
                     'measure': {'self': measure_dict},
}

# Main tree
tree_dict = {'self': server_dict,
             'kiln': {'self': kiln_dict},
             'beholder': {'self': deepcopy(dataimport.base_dict()),
                          'idx0': {'self': camera_dict}
                          }

             }

def create_tree(outFile, tree, path='/', t=None):
    """Recursive tree structure creation"""
    for key, foo in tree.list():
        if outFile.has_node(path, key):
            logging.debug('Path already found:', path, key)
            continue
        logging.debug('Creating group:', path, key)
        outFile.create_group(path, key, key)
        if t:
            outFile.update_last_edit_time(path, name=key, t=t)
        dest = path + key + '/'
        if outFile.has_node(dest):
            continue
        create_tree(outFile, tree.child(key), dest, t=t)

def filter_cell(e):
    e=e.replace(',','.')
    if '#' in e:
        e = '0'
    return e
                
class Converter(dataimport.Converter):
    name = 'Misura3'
    file_pattern = '*.mdb'
    outpath = ''
    interrupt = False
    progress = 0
    outFile = False

    def __init__(self, dbpath, outdir=False):
        self.dbpath = dbpath

        if not outdir:
            outdir = os.path.dirname(dbpath)
            outdir = os.path.join(outdir, 'm4')
            if not os.path.exists(outdir):
                os.mkdir(outdir)
        self.outdir = outdir
        self.m4db = os.path.join(outdir, 'database.sqlite')

    def get_outpath(self, tcode=False, img=True, force=True, keep_img=True):
        # Open DB and import test data
        if not tcode:
            self.dbpath, tcode = self.dbpath.split('|')
        self.outFile = False
        self.outpath = False
        self.original_version = ''
        self.tcode = tcode
        dbif = m3db.get_mdb_interface(self.dbpath)
        tests = dbif.get_test(tcode)
        if len(tests) != 1:
            logging.debug( 'Wrong number of tests found', tests)
            return False
        test = tests[0]
        self.test = test
        self.icode = m3db.getImageCode(tcode)
        self.rows = dbif.get_data(self.icode)
        del dbif
        logging.debug('CONVERT GOT', len(self.rows))
        if len(self.rows) < 1:
            logging.debug('No points', self.rows)
            
            return False

        # Open Output File
        safeName = ''.join(
            c for c in self.test[m3db.fprv.Desc_Prova] if c in valid_chars)
        self.outpath = os.path.join(self.outdir, safeName + '_' + tcode + '.h5')
        self.img = True
        # Manage overwriting options
        if os.path.exists(self.outpath):
            # If I am forcing overwriting and not caring about current images:
            # create new file
            self.outFile = False
            try: 
                self.outFile = indexer.SharedFile(self.outpath, mode='r')
                # Keep images only if they exist
                keep_img = keep_img and self.outFile.has_node('/hsm/sample0/frame') \
                    and self.outFile.has_node('/ver_1/hsm/sample0/profile')
                self.outFile.close(all_handlers=True)
                self.outFile = indexer.SharedFile(self.outpath, mode='a')
                self.original_version = self.outFile.get_version()
            except:
                logging.error('Error opening existing file:', format_exc())
                force = True
                keep_img = False
                
            if self.outFile and not self.outFile.has_node('/conf'):
                force = True
                keep_img = False
                
            if not force:
                logging.debug('Already exported path:', self.outpath)
                self.outFile.close(all_handlers=True)
                return False
            elif force and not keep_img:
                if self.outFile:
                    self.outFile.close(all_handlers=True)
                os.remove(self.outpath)
                self.outFile = indexer.SharedFile(self.outpath, mode='w')
            # Keep current images
            elif keep_img:
                self.img = False
        # If it does not exist, create!
        else:
            self.outFile = indexer.SharedFile(self.outpath, mode='w')
        if self.outFile.get_version()!='':
            self.outFile.set_version('')
        self.keep_img = keep_img
        self.force = force
        return self.outpath


    def convert(self, frm='ImageM3', max_num_images=-1):
        """Extract a Misura 3 test and export into a MDF test file"""
        outFile = self.outFile
        zt = time()
        log_ref = reference.Log(outFile, '/', server_dict['log'])

        def log(msg, priority=10):
            # TODO: check zerotime
            logging.debug(msg)
            log_ref.commit([[time() - zt, (priority, msg)]])

        log('Importing from %s, id %s' % (self.dbpath, self.tcode))
        log('Conversion Started at ' + 
            datetime.now().strftime("%H:%M:%S, %d/%m/%Y"))
        log('Conversion Parameters: \n\tImages: %r, \n\tUpdate: %r, \n\tKeep Images: %r, \n\tImage Format: %r' % (
            self.img, self.force, self.keep_img, frm))

        self.progress = 11
        # Heating cycle table
        cycle = m3db.getHeatingCycle(self.test)
        self.progress = 12

        # ##
        # CONFIGURATION
        serial = hashlib.md5(self.dbpath.encode('utf8')).hexdigest()[:8]
        tree = deepcopy(tree_dict)
        # Create instrument dict
        instr = m3db.getInstrumentName(self.test[m3db.fprv.Tipo_Prova])
        if not instr:
            logging.error('Unknown instrument:',m3db.fprv.Tipo_Prova)
            self.cancel()
            return False
        tree[instr] = deepcopy(instr_tree)
        if instr == 'hsm':
            tree[instr]['sample0'] = deepcopy(hsm_smp_tree)
        elif instr in ('horizontal','vertical'):
            tree[instr]['sample0'] = deepcopy(dil_smp_tree)
        elif instr=='flex':
            tree[instr]['sample0'] = deepcopy(flex_smp_tree)
        else:
            tree[instr]['sample0'] = deepcopy(smp_tree)
        # Get a configuration proxy
        tree = option.ConfigurationProxy(tree, readLevel=5, writeLevel=5)
        instrobj = getattr(tree, instr)
        tree['runningInstrument'] = instr
        tree['lastInstrument'] = instr
        tree['eq_sn'] = serial
        instrobj['name'] = instr
        # Sample
        smp = instrobj.sample0
        smp['name'] = self.test[m3db.fprv.Desc_Prova]
        if self.tcode[-1] in 'RL':
            smp['name']+= ' '+self.tcode[-1]
        

        # Set ROI
        roi = [0, 0, 640, 480]
        if self.tcode.endswith('L'):
            roi[2] = 320.
        elif self.tcode.endswith('R'):
            roi[0] = 320.
            roi[2] = 320.
        smp['roi'] = roi

        # Measure
        tid = self.dbpath + '|' + self.tcode
        try:
            tdate0 = self.test[m3db.fprv.Data]
            zerotime = mktime(tdate0.timetuple())
        except:
            logging.error('Invalid date found', tdate0)
            tdate0 = datetime.now()
            zerotime = mktime(tdate0.timetuple())
        
        tdate = tdate0.strftime("%H:%M:%S, %d/%m/%Y")
        logging.debug(tdate)
        instrobj.measure['zerotime'] = zerotime
        instrobj.measure['name'] = self.test[m3db.fprv.Desc_Prova]
        c = self.test[m3db.fprv.Note]
        c = self.tcode + (' - '+c if c else '')
        instrobj.measure['comment'] = c 
        instrobj.measure['date'] = tdate
        instrobj.measure['id'] = tid
        uid = self.dbpath + '|' + self.tcode
        uid = hashlib.md5(uid.encode('utf8')).hexdigest()
        instrobj.measure['uid'] = uid
        instrobj['zerotime'] = zerotime

        # Kiln
        tree.kiln['curve'] = cycle
        tree.kiln['Regulation_Kp'] = self.test[m3db.fprv.Pb]
        tree.kiln['Regulation_Ki'] = self.test[m3db.fprv.ti]
        tree.kiln['Regulation_Kd'] = self.test[m3db.fprv.td]
        tree.kiln['ksn'] = serial
        # Create the hierarchy
        create_tree(outFile, tree, t=zerotime)

        # ##
        # GET THE ACTUAL DATA
        header, columns = m3db.getHeaderCols(self.test[m3db.fprv.Tipo_Prova], self.tcode)
        rows = np.array(self.rows)
        self.rows = rows
        logging.debug(header, columns, len(rows[0]), instr)
        instr_path = '/' + instr
        smp_path = instr_path + '/sample0'
        smp_path_T = smp_path+'/T'

        self.progress = 13

        arrayRef = {}
        timecol = rows[:, m3db.fimg.Tempo].astype('float')
        timecol, unidx = np.unique(timecol, return_index=True)
        # Set first point as zero time
        timecol -= timecol[0]
        ini_area = 0.0
        dim = set(['d', 'h', 'w', 'camA', 'camB']).intersection(
            set(header))
        ini0 = self.test[m3db.fprv.Inizio_Sint]
        initialDimension = 0
        # Convert data points
        for i, col in enumerate(header):
            if self.interrupt:
                self.cancel()
                return False
            if col == 't':
                data = timecol
                continue
            if columns[i] >= rows.shape[1]:
                logging.debug("Skipping undefined column. Old db?", columns[i])
                continue
            data = rows[:, columns[i]][unidx]
            # Uncommon , separators                
            if isinstance(data[0],basestring):
                data = list(map(filter_cell, data))
                data = np.array(data)
            
            data = data.astype('float')
            # Skip invalid data
            if np.isnan(data).all():
                continue
            data[~np.isfinite(data)] = 0.0
             
            # Unit and client-side unit
            if col == 'h':
                ini1 = 3000.
            elif col == 'A':
                ini1 = -data[0]
            elif col == 'w':
                ini1 = 2000.
            elif col in ['d', 'camA', 'camB']:
                if ini0 is None:
                    logging.error('Invalid initial dimension')
                    self.cancel()
                    return False
                ini1 = ini0 * 100.                

            unit = False
            csunit = False
            if col == 'd' or 'Percorso' in col:
                data = ini1*data / 1000./100.
                unit = 'micron'
            elif 'Percorso' in col:
                data = data / 1000.
                unit = 'percent'
            elif col == 'h':
                data = ini1*data / 100./100.
                unit = 'micron'
            elif col == 'soft':
                data = data / 100.
                unit = 'percent'
            elif col == 'A':
                data = -data
                unit = 'micron^2'
            elif col == 'P':
                data = data / 10.
                unit = 'micron'
            elif col == 'w':
                logging.debug(data)
                data = data / 200.
                unit = 'micron'
            # Detect delayed start
            elif col == 'T':
                smp['startTemp'] = data[0]
                smp['anStartTemp'] = data[0]
            if col in ['T', 'P', 'S']:    
                arrayRef[col] = reference.Array(outFile, '/summary/kiln', kiln_dict[col])
            else:
                opt = ao({}, col, 'Float', 0, col, attr=['History', 'Hidden'])[col]
                if col in dim:
                    opt['percent'] = col not in ['camA', 'camB', 'w']
                if col in ['d', 'h']:
                    opt['initialDimension'] = ini1
                    initialDimension = ini1
                if unit:
                    opt['unit'] = unit
                if csunit:
                    opt['csunit'] = csunit
                instrobj.sample0.sete(col, opt)
                arrayRef[col] = reference.Array(outFile, '/summary' + smp_path, opt)
            # Recreate the reference so the data is clean
            arrayRef[col].dump()
            path = arrayRef[col].path
            base_path = path[8:]
            # Create hard links
            if not outFile.has_node(base_path):
                outFile.link(base_path, path)
            ref = arrayRef[col]
            ref.append(np.array([timecol, data]).transpose())
            
        # Create sample0/T links
        for pre in ['','/summary']:
            if not outFile.has_node(pre+smp_path_T):
                outFile.link(pre+smp_path_T, '/kiln/T')
        
        outFile.flush()

        self.progress = 20
        # ##
        # ASSIGN INITIAL DIMENSION
        smp['initialDimension'] = initialDimension
        outFile.flush()

        self.progress = 21
        log('Converted Points: %i\n' % (len(rows) - 1) * len(header))
        ######
        # Final configuration adjustment and writeout
        ######
        elapsed = float(timecol[-1])
        instrobj.measure['elapsed'] = elapsed
        # Get characteristic shapes
        if instr == 'hsm':
            sh = m3db.getCharacteristicShapes(self.test, rows.transpose())
            logging.debug('got characteristic shapes', sh)
            instrobj.sample0.desc.update(sh)
        tree_dump = tree.tree()
        
        # Write conf tree
        outFile.save_conf(tree.tree())
        outFile.set_attributes('/conf', attrs={'version': '3.0.0',
                                'zerotime': zerotime,
                                'elapsed': elapsed,
                                'instrument': instr,
                                'date': tdate,
                                'serial': serial,
                                'uid': uid})
        os.utime(self.outpath,(zerotime, zerotime))
        # ##
        # IMAGES
        appended_images = False
        imgdir = os.path.join(os.path.dirname(self.dbpath), self.icode)
        if os.path.exists(imgdir) and self.img and (instr in ['hsm', 'drop', 'post']):
            logging.debug('Appending images')
            appended_images = self.append_images(imgdir, frm, max_num_images)
            self.progress = 80
            if appended_images and postanalysis:
                def callback(p):
                    self.progress = 80+p/11.
                log('Postanalysis...')
                postanalysis.postanalyze(outFile,callback=callback)
            else:
                logging.debug('No postanalysis',appended_images, postanalysis)
            if not appended_images:
                if self.interrupt:
                    logging.debug('Interrupted')
                    return False
                else:
                    log('ERROR Appending images')
        else:
            logging.debug('Instrument does not have images to append',instr,imgdir,os.path.exists(imgdir), self.img)
                    
        # Standards
        r = outFile.run_scripts(getattr(outFile.conf, instr))
        logging.debug('Done running scripts:',instr, r)
        if instr=='hsm':
            outFile.conf.hsm.sample0['m3_enable'] = True
        
        # Save again after post analyis and scripts
        outFile.save_conf()
        if self.original_version!='':
            tree_dump = outFile.conf.tree()
            outFile.set_version(self.original_version)
            outFile.save_conf(tree_dump)
        
        self.progress = 99
        log('Appending to MDF database: '+ self.outpath)
        outFile.close(all_handlers=True)
        
        indexer.Indexer.append_file_to_database(self.m4db, self.outpath)
        log('Conversion ended: '+ self.outpath)
        self.progress = 100
        logging.debug('Setting time back to', zerotime)
        os.utime(self.outpath,(zerotime, zerotime))
        return self.outpath



    def append_images(self, imgdir, frm, max_num_images=-1):
        side=self.tcode[-1]
        refClass = getattr(reference, frm)
        ref = refClass(self.outFile, '/hsm/sample0', smp_dict['frame'])
        a = self.outFile.get_attributes(ref.path)
        self.outFile.set_attributes(ref.path, attrs=a)
        sjob = self.progress
        job = (79. - sjob) / len(self.rows)
        oi = 0
        esz = 0
        t0 = float(self.rows[0, m3db.fimg.Tempo])
        for i, r in enumerate(self.rows):
            if self.interrupt:
                self.cancel()
                break
            if i >= max_num_images and max_num_images >= 0:
                break
            nj = sjob + i * job
            self.progress = int(nj)

            num = int(r[m3db.fimg.Numero_Immagine])
            img = '%sH.%03i' % (self.icode, int(num))
            img = os.path.join(imgdir, img)
            if not os.path.exists(img):
                logging.debug('Skipping non-existent image', img)
                continue

            im, size = decompress(img, frm, side)
            
            sz = len(im)
            esz += sz
            t = float(self.rows[i, m3db.fimg.Tempo]) - t0
#            print 'append_images', t, t0, float(self.rows[i, m3db.fimg.Tempo])
            ref.commit([[t, im]])
            oi += 1
            self.outFile.flush()
        logging.debug('Included images:', oi)
        logging.debug('Expected size:', esz / 1000000.)
        return True


def frame_side(fr, side='S'):
    im = Image.frombytes('L', (640, 480), fr[::-1]).convert('L')
    im = np.asarray(im)
    if side=='L':
        im = im[:,320:]
    else:
        im = im[:,:320]
    return im

def decompress(img, frm, side='S'):
    """Available formats (Storage):
            'Image': misura compression algorithm (Image)
            'ImageM3': legacy Misura3 compression algorithm (Binary)
            other: any PIL supported format (Binary)
    """
    fr = open(img, 'rb').read()
    if frm == 'ImageM3':
        if side in 'RL':
            fr = bitmap.decompress(fr)
            im = frame_side(fr, side)
            fr = bitmap.compress(im)
        return (fr, (640, 480))
    try:
        fr = bitmap.decompress(fr)
        if frm == 'ImageBMP':
            return fr
        im = frame_side(fr, side)
        if frm == 'Image':
            return (np.asarray(im), im.size)
        else:
            sio = StringIO.StringIO()
            im.save(sio, frm)
            im.save('debug.' + frm, frm)
            sio.seek(0)
            r = sio.read()
            return (r, im.size)
    except:
        logging.debug(format_exc())
        logging.debug('Error reading image', img)
        r = ('', (0, 0))
    return r


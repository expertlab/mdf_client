# -*- coding: utf-8 -*-
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
import datetime
from mdf_client.qt import QtWidgets, QtGui, QtCore

from .m3db import fields_PROVE

class TestListModel(QtCore.QAbstractTableModel):
    """Modello di dati per la tabella PROVE, contenente la lista di tutte le prove presenti nel database."""
    limit = 500
    start = 0
    
    def __init__(self):
        QtCore.QAbstractTableModel.__init__(self)
        self.tests = []
        self.header = fields_PROVE

    def rowCount(self, index=QtCore.QModelIndex()):
        return min(len(self.tests)-self.start, self.limit)

    def columnCount(self, index=QtCore.QModelIndex()):
        if not len(self.tests):
            return 0
        return len(self.tests[0])

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if not index.isValid() or not (0 <= index.row() <= self.rowCount()):
            return 0
        if role == QtCore.Qt.DisplayRole:
            row = self.tests[index.row()+self.start]
            if len(row) <= index.column():
                return 0
            obj = row[index.column()]
            # Converto in stringa se l'oggetto è datetime
            if type(obj) == type(datetime.datetime(1, 2, 3)):
                if obj.year > 1900:
                    obj = obj.strftime('%d %b %Y')
            #if index.column()==0:
            #    logging.debug('data:',index.row()+self.start, row)
            return obj

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if orientation != QtCore.Qt.Horizontal:
            return None
        if role == QtCore.Qt.DisplayRole:
            return self.header[section]

    def setTests(self, tests):
        self.start = 0
        self.tests = tests
        self.modelReset.emit()
        
    def set_batch(self, n):
        self.start = self.limit*n
        logging.debug('set_batch', n, self.start)
        self.modelReset.emit()

class TestsTable(QtWidgets.QTableView):

    """Visualizzazione tabellare delle prove contenute nel database."""

    def __init__(self, path='', parent=None):
        QtWidgets.QTableView.__init__(self, parent)
        self.path = path
        self.curveModel = TestListModel()
        self.setModel(self.curveModel)
        self.selection = QtCore.QItemSelectionModel(self.model())
        self.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.setSelectionModel(self.selection)
        self.setColumnWidth(0, 45)
        self.setColumnWidth(1, 50)
        self.setColumnWidth(2, 350)
        



#!/usr/bin/python2.6
# -*- coding: utf-8 -*-
import platform
import os
from traceback import print_exc, format_exc

pyodbc = False
try:
    import pyodbc
except:
    print_exc()
    print('Misura3 import is not available. Install pyodbc.')

import numpy

from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from mdf_canon.option import ao
from mdf_canon.csutil import unicode_func

# apt install unixodbc unixodbc-dev mdbtools odbc-mdbtools
# pip3 install pyodbc

badchars = ['_']


def validate_tabname(fn):
    """Purifica un table name da tutti i caratteri potenzialmente invalidi"""
    if getattr(fn, 'toUtf8', False):
        fn = fn.toUtf8()
    fn = unicode_func(fn)
    # scarta tutti i caratteri non alfanumerici
    fn = "".join(
        [x for x in fn if x.isalpha() or x.isdigit() or x not in badchars])
    fn = fn.encode('ascii', 'ignore')
    return fn


# Subdefinizioni
fields_Heat = ''  # Ciclo di cottura
for i in range(1, 9):
    fields_Heat += 'Salita%i, TempMax%i, Stasi%i, ' % (i, i, i)

# intervalli
fields_Intervalli = 'Tipo_Salita, Inizio, Intervallo, Fine, Inizio1, Intervallo1, Fine1, '
fields_Intervalli += 'Intervallo2, Fine2, Intervallo3, Fine3, '

fields_Data = 'Posizione, '  # dati acquisiti
for p in '_SX', '_DX':
    fields_Data += 'Sint%s, Angolo%s, Rapporto%s, Area%s, ' % (p, p, p, p)
fields_Data += 'Temp, Tempo, Larghezza, Larghezza2,Colonna01,Colonna02'

fields_Termodil = ''
for i in range(1, 4):
    fields_Termodil += 'Termodil%i, ' % (i)
for i in range(1, 4):
    fields_Termodil += 'Termodil%i_Exp, ' % (i)

# Definizione di una riga della tabella PROVE (impostazioni della prova)
fields_PROVE = 'IDProve, Data, Desc_Prova, Tipo_Prova, Classe, ' + fields_Intervalli + \
    'Tempo_Totale, Tempo_Parziale, Note, Temp_Sint, Temp_Rammoll, Temp_Sfera, ' + \
    'Temp_Mezza_Sfera, Temp_Fusione, ' + fields_Termodil + \
    'Inizio_Sint , Indice_Rammoll, ' + fields_Heat + \
    'Pb, ti, td, Fattore, Left_Margin, Right_Margin, Top_Margin, Bottom_Margin, FireFree'
fields_PROVE = fields_PROVE.replace(' ', '').split(',')
# Definizione di una riga della tabella IMMAGINI (dati acquisiti)
fields_IMMAGINI = 'IDProve, IDImmagini, Numero_Immagine, ' + fields_Data
fields_IMMAGINI = fields_IMMAGINI.replace(' ', '').split(',')


class enumTipoProva:
    All = -1
    ProvinoSingolo = 0
    ProvinoDoppioCompleto = 1
    ProvinoDoppioParziale = 2
    SoloImmagini = 3
    Impasto = 4
    CurvaCorrezione = 5
    Dilatometro = 6
    CorrezioneDilatometro = 7
    DilatometroVerticale = 8
    DilatometroOrizzontale = 9
    Calibrazione = 10
    DropAnalysis = 11
    Flessimetro = 12
    Rodaggio = 13
    DTA = 15
    FSE = 16  # ?
    ProvinoR = 110
    ProvinoL = 111


etp = enumTipoProva()


def getInstrumentName(tp):
    """Converts Misura3 test type into misura instrument type name"""
    if tp in [0, 1, 2, 3, 4, 110, 111]:
        return 'hsm'
    if tp in [6, 7, 9, 10]:
        return 'horizontal'
    if tp == etp.DilatometroVerticale:
        return 'vertical'
    if tp in [5, 12]:
        return 'flex'
    if tp == etp.Rodaggio:
        return 'kiln'
    if tp == etp.DTA:
        return 'dta'
    return False


class fieldIMG:

    def __init__(self):
        for i, c in enumerate(fields_IMMAGINI):
            setattr(self, c, i)

    def __call__(self, n):
        return fields_IMMAGINI[n]


fimg = fieldIMG()


class fieldPRV:

    def __init__(self):
        for i, c in enumerate(fields_PROVE):
            setattr(self, c, i)

    def __call__(self, n):
        return fields_PROVE[n]


fprv = fieldPRV()


def getHeaderCols(tt, tcode='', all=False):
    """Selects and returns the appropriate columns and their headers"""
    global etp, fimg
    logging.debug('getHeaderCols', tt, tcode)
    # DILATOMETRY
    h = ['t', 'T']
    c = [fimg.Tempo, fimg.Temp]
    if tt in [etp.DilatometroVerticale, etp.DilatometroOrizzontale, etp.Calibrazione]:
        h += ['d', 'S', 'camA', 'camB', 'P', 'Mov']
        c += [fimg.Sint_SX, fimg.Rapporto_DX, fimg.Angolo_SX,
              fimg.Rapporto_SX, fimg.Angolo_DX, fimg.Posizione]
    elif tt in [etp.Dilatometro, etp.CorrezioneDilatometro, etp.Impasto, etp.CurvaCorrezione]:
        h += ['d']
        c += [fimg.Sint_SX]

    # FLEX
    elif tt in [etp.Flessimetro]:
        h += ['d', 'S', 'camA', 'P', 'Mov']
        c += [fimg.Sint_SX, fimg.Rapporto_DX,
              fimg.Sint_DX, fimg.Angolo_DX, fimg.Posizione]

    # HSM
    elif tt in [etp.ProvinoSingolo, etp.DropAnalysis]:
        h += ['h', 'ang', 'eqhw', 'A', 'S', 'P', 'w', 'soft']
        c += [fimg.Sint_SX, fimg.Angolo_SX, fimg.Rapporto_SX, fimg.Area_SX,
              fimg.Rapporto_DX, fimg.Angolo_DX, fimg.Larghezza, fimg.Colonna01]
    elif tt == etp.ProvinoR or (tt == etp.ProvinoDoppioCompleto and tcode.endswith('R')):
        h += ['h', 'ang', 'eqhw', 'A', 'w', 'soft']
        c += [fimg.Sint_DX, fimg.Angolo_DX, fimg.Rapporto_DX,
              fimg.Area_DX, fimg.Larghezza2, fimg.Colonna02]
    elif tt == etp.ProvinoL or (tt == etp.ProvinoDoppioCompleto and tcode.endswith('L')):
        h += ['h', 'ang', 'eqhw', 'A', 'w', 'soft']
        c += [fimg.Sint_SX, fimg.Angolo_SX, fimg.Rapporto_SX,
              fimg.Area_SX, fimg.Larghezza, fimg.Colonna01]

    # OTHER
    elif tt in [etp.Rodaggio]:
        h += ['S', 'P']
        c += [fimg.Angolo_DX, fimg.Rapporto_DX]
    elif tt == etp.FSE:
        h = ['t', 'W', 'Flex', 'Disp']
        c += [fimg.Rapporto_SX, fimg.Sint_SX]
    elif tt == etp.DTA:
        h += ['DTA', 'S', 'P']
        c += [fimg.Sint_SX, fimg.Rapporto_DX, fimg.Angolo_DX]
    if all:
        h1 = []
        for i, f in enumerate(fields_IMMAGINI):
            if i not in c:
                h1.append(f)
            else:
                j = c.index(i)
                h1.append(h[j])
        c = range(len(fields_IMMAGINI))
        h = h1
    return h, c


def getPlotCols(tt):
    cols = (3,)
    if tt in [etp.Rodaggio]:
        cols = (3, 4)
    elif tt == etp.FSE:
        cols = (2, 3, 4)
    return cols


def getHeatingCycle(entry):
    """Translate heating cycle expressed as Ramp, Temp, Stasis in a PROVE entry,
    into misura [t,T,S] list format"""
    out = [[0, 0]]
    addt = 0
    for i in range(1, 9):
        s = 'Salita%i,TempMax%i,Stasi%i' % (i, i, i)
        R, T, S = s.split(',')
        R = entry[getattr(fprv, R)]
        T = entry[getattr(fprv, T)]
        S = entry[getattr(fprv, S)]
        if None in [R, T]:
            break
        t0, T0 = out[-1]
        R = R * numpy.sign(T - T0)
        if R == 0:
            logging.error('Zero rate:', i, R, T, S)
            break
        t = float(t0 + 60. * (T - T0) / R)
        if i == 1 and t == 0:
            addt = 1
        t += addt
        out.append([t, T])
        if S == None:
            continue
        if S > 0:
            out.append([t + 60. * S, T])
    return out


shm4 = {'Temp_Sint': 'Sintering',
        'Temp_Rammoll': 'Softening',
        'Temp_Sfera': 'Sphere',
        'Temp_Mezza_Sfera': 'HalfSphere',
        'Temp_Fusione': 'Melting'}
shm4d = {'Sintering': 'Sintering point',
         'Softening': 'Softening point',
         'Sphere': 'Sphere point',
         'HalfSphere': 'Half sphere point',
         'Melting': 'Melting point'}


def getCharacteristicShapes(test, cols):
    """Returns the characteristic shapes of a test supporting them"""
    sh = {}
    if test[fprv.Tipo_Prova] not in [etp.ProvinoSingolo, etp.ProvinoR, etp.ProvinoL, etp.ProvinoDoppioCompleto]:
        return sh
    vt = numpy.array(cols[fimg.Tempo]).astype('float')
    vT = numpy.array(cols[fimg.Temp]).astype('float')
    logging.debug(vT)
    for i, name in enumerate('Temp_Sint,Temp_Rammoll,Temp_Sfera,Temp_Mezza_Sfera,Temp_Fusione'.split(',')):
        r = test[getattr(fprv, name)]
        if r == None:
            continue
        # Point as temperature
        if r > 0:
            v = vT  # Search on temp. vector
        # Point as -time
        else:
            r = -r  # Turn positive
            v = vt  # Search on time vector
        d = numpy.abs(v - r)
        idx = numpy.where(d == d.min())[0]
        if not len(idx):
            continue
        idx = int(idx[0])
        logging.debug('Where:', r, idx)
        if idx == 0:
            t = 'None'
            T = 'None'
            point = 'None'
            idx = 'None'
        else:
            t = float(vt[idx])
            T = float(vT[idx])
        hnd = shm4[name]
        d = shm4d[hnd]
        ao(sh, 'm3_' + hnd, 'Meta', {
           'time': t, 'temp': T, 'value': 'None'}, d, priority=100 + i)
    return sh


def getConnectionCursor(path):
    if 'Linux' in platform.platform():
        # FIXME: Molti valori completamente sballati dalla lettura odbc.
        # Passare a SQLite3?
        driver = "{MDBTools}"
        cs = "DRIVER=%s;DBQ=%s" % (driver, path)
        logging.debug(cs)
        conn = pyodbc.connect(cs)
    else:
        driver = "{Microsoft Access Driver (*.mdb, *.accdb)}"
        cs = "DRIVER=%s;DBQ=%s" % (driver, path)
        logging.debug(cs)
        try:
            conn = pyodbc.connect(cs)
        except:
            driver = "{Microsoft Access Driver (*.mdb)}"
            cs = "DRIVER=%s;DBQ=%s" % (driver, path)
            logging.debug(cs)
            conn = pyodbc.connect(cs)
    cursor = conn.cursor()
    return conn, cursor
    
    
class PyODBCInterface(object):

    def __init__(self, path):
        self.path = path
        self.conn, self.cursor = getConnectionCursor(path)
        
    def list_tests(self):
        self.cursor.execute("select * from Prove order by Data desc")
        return self.cursor.fetchall()
    
    def filter_tests(self, tt=None, rl=None):
        q = []
        par = []
        if rl is not None:
            par.append("%" + rl)
            q.append("[IDProve] like ?")
        if tt is not None:
            par.append(tt)
            q.append("[Tipo Prova] = ?")
        if not par:
            return self.list_tests()
        cmd = "select * from Prove where" + (' AND '.join(q))
        cmd +=' order by Data desc' 
        self.cursor.execute(cmd, tuple(par))
        return self.cursor.fetchall()
    
    def get_test(self, tcode):
        self.cursor.execute("select * from Prove where IDProve = '%s'" % tcode)
        return self.cursor.fetchall()
    
    def get_data(self, icode):
        self.cursor.execute(
            "select * from Immagini where [IDProve] = '%s' order by Tempo" % icode)
        return self.cursor.fetchall()
    
    def __del__(self):
        self.conn.close()
    
    
from mdf_canon.csutil import go
from datetime import datetime
import hashlib
from mdf_client import _
from PyQt5 import QtWidgets


def default_cvt(s):
    if len(s) == 0:
        return 0
    return float(s) if not s.isdigit() else int(s)


def date_cvt(s):
    return datetime.strptime(s[1:-1], "%m/%d/%y %H:%M:%S")


def utf_cvt(s):
    r = s[1:-1]
    return r


test_converters = {
    0:lambda s: s[1:-1],  # code
    1:lambda s: date_cvt(s),  # date
    2: utf_cvt,  # title
    3: default_cvt,  # type
    4:utf_cvt,  # class
    18:utf_cvt,  # notes
    }

data_converters = {
    0:lambda s: s[1:-1],  # code
    }


class MdbToolsMissingException(BaseException):

    def __init__(self, msg=''):
        msg = 'Could not find mdb-export tool. Please apt install mdbtools.' + msg
        super(MdbToolsMissingException, self).__init__(msg)



class MdbToolsInterface(object):
    delimiter = " $&$ "
    newline = ' <-~->\n'
    cachedir = False
    version = 0
    
    def __init__(self, path, cachedir=True):
        st, out = go('which mdb-export')
        if st:
            raise MdbToolsMissingException()
        self.path = path
        if cachedir:
            from mdf_client.parameters import pathTmp
            cachedir = os.path.join(pathTmp, 'mdb')
            fold = hashlib.shake_128(os.path.dirname(path).encode('utf8')).hexdigest(8)
            cachedir = os.path.join(cachedir, os.path.basename(path), fold, '')
            if not os.path.exists(cachedir):
                os.makedirs(cachedir)
            logging.log('MdbToolsInterface with cachedir', cachedir, self.path)
        self.cachedir = cachedir
        self.headers = {}
        
    def clear(self, tab):
        if self.cachedir:
            base = self.cachedir
        else:
            base = os.path.dirname(self.path)
        end = '-{}.export'.format(tab)
        for f in os.listdir(base):
            if f.endswith(end):
                logging.debug('MdbToolsInterface.export: removing', f)
                os.remove(os.path.join(base, f))
        
    def export(self, tab, dest):
        self.clear(tab)
        cmd = 'mdb-export "{}" {} -d "{}" -R "{}" > "{}.temp"'.format(self.path, tab, self.delimiter, self.newline, dest)
        logging.debug(cmd)
        go(cmd)
        go('mv "{0}.temp" "{0}"'.format(dest))
        
    def dest(self, tab):
        p = self.cachedir if self.cachedir else self.path
        t = int(os.stat(self.path).st_mtime)
        if self.cachedir:
            return '{}{}-{}-{}.export'.format(p, t, self.version, tab)
        return '{}-{}-{}-{}.export'.format(p, t, self.version, tab)
        
    def check_time(self, tab):
        dest = self.dest(tab)
        if os.path.exists(dest):
            return dest 
        self.export(tab, dest)
        return dest
    
    def load_tests(self, converters, csv, query=False):
        cvt = {}
        cvt.update(converters)
        
        tab = open(query or csv, 'r').read().split(self.newline)
        header = []
        N = 0
        out = []
        for i, line in enumerate(tab[:-1]):
            line = line.split(self.delimiter)
            if i==0:
                header = line
                N = len(header)
                if not query:
                    continue
            if len(line)!=N:
                logging.debug('Malformed line', i, len(line), N, line)
                continue
            for col, val in enumerate(line):
                line[col] = cvt.get(col, default_cvt)(val)
                
            out.append(line)
        out.sort(key=lambda k: k[1], reverse=True)
        return out
    
    def list_tests(self):
        csv = self.check_time('Prove')
        tab = self.load_tests(test_converters, csv)
        
        return [list(t) for t in tab]
    
    def filter_tests(self, tt=None, rl=False):
        csv = self.check_time('Prove')
        q = '{}.{}{}.mdbquery'.format(csv, tt, rl)
        print('SELECTING', tt, rl, q)
        cmd = []
        if rl:
            cmd.append('grep -E \'^\\"[0-9]{5}%s\\"\' "%s"' % (rl, csv))
        if tt is not None:
            d = self.delimiter.replace('$', '\$').replace('&', '\&')[1:-1]
            cmd.append('grep -E \'^([^(%s)]+ %s ){3}%s %s\' "%s"' % (d, d, tt, d, csv))
        if not len(cmd):
            return self.list_tests()
        cmd = '|'.join(cmd)
        cmd += ' >  "{}"'.format(q)
        logging.debug(cmd)
        st, out = go(cmd)
        tab = []
        if st:
            return tab
        try:
            tab = self.load_tests(test_converters, csv, q)
            logging.error(format_exc())
        finally:
            pass
            # os.remove(q)
        return tab
        
    def get_test(self, tcode):
        csv = self.check_time('Prove')
        q = '{}.{}.mdbquery'.format(csv, tcode)
        cmd = 'grep ^\\"{} "{}" > "{}"'.format(tcode, csv, q)
        logging.debug(cmd)
        go(cmd)
        tab = []
        try:
            tab = self.load_tests(test_converters, csv, q)
        finally: 
            pass
            # os.remove(q)
        return tab
    
    def get_data(self, icode):
        csv = self.check_time('Immagini')
        q = '{}.{}.mdbquery'.format(csv, icode)
        cmd = 'grep ^\\"{}\\" "{}" > "{}"'.format(icode, csv, q)
        logging.debug(cmd)
        go(cmd)
        tab = self.load_tests(data_converters, csv, q)
        os.remove(q)
        return [list(t) for t in tab]
    
    
fail_msg = _("""Cannot access Misura3 files because of missing "mdbtools" utility.
Please install it with:
    sudo apt install mdbtools""")


def get_mdb_interface(path):
    if 'Linux' in platform.platform():
        try:
            return MdbToolsInterface(path)
        except:
            QtWidgets.QMessageBox.critical(None, _('Misura3 failure: missing component'), fail_msg)
            return False
    return PyODBCInterface(path)


def getImageCode(code):
    # code = code.decode('ascii', 'replace')
    code = code.split('\x00')[0].split('?')[0]
    code = str(code)
    try:
        int(code[-1])
    except:
        code = code[:-1]
    return code


if __name__ == '__main__':
    base = '/home/daniele/Sviluppo/m3db/ELS2019.mdb'
    base = '/home/daniele/MisuraData/lea/db/LEA.mdb'
    base = '/home/daniele/MisuraData/mrz/micro/dbMISURA3 2009.MDB'
    dbif = get_mdb_interface(base)
    r = dbif.list_tests()
    print(len(r))
    print(r[0])
    # print(dbif.get_test("00098F"))
    # data = dbif.get_data("00098")
    # print(len(data), data[0])
    # print(dbif.filter_tests(12))


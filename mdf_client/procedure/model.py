#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Designer per il ciclo termico."""
import collections
import json
from mdf_canon.logger import get_module_logging
from mdf_client.procedure.row import colTEMP
from .. import _
from mdf_canon.csutil import basestring
from mdf_canon.csutil import next_point, decode_cool_event, power_event_duration, move_event_duration, event_duration
from mdf_client.qt import QtGui, QtCore
from . import row
from . import flags

logging = get_module_logging(__name__)


def clean_curve(dat, events=True):
    """Convert `dat` model thermal cycle to a (time,Temp) list of points.
    Include `events` or try to convert them to numerical values."""
    crv = []
    # Event-based time correction
    time_correction = 0
    for index_row, ent in enumerate(dat):
        t, T = ent[:2]
        if None in ent:
            logging.debug('Skipping row', index_row)
            continue
        if isinstance(T, basestring):
            print('Basestring', T)
            logging.debug('EVENT', index_row)
            T = str(T)
            if events:
                t += time_correction
                crv.append([t * 60, T])
                continue
            ev = T.split(',')
            timeout = -1
            if ev[0] == '>cool':
                T = float(ev[1])
                if len(ev) > 2:
                    timeout = float(ev[2]) / 60.
                # assume a 50°C/min ramp
                if timeout < 0 and len(crv) > 1:
                    T0 = crv[-1][1]
                    timeout = (T0 - T) / 50.
            elif ev[0] == '>checkpoint':
                dT = float(ev[1])
                T = crv[-1][1]
                # assume a 10 min dwell time / delta
                timeout = 10 / dT
            elif ev[0] == '>power':
                T = crv[-1][1]
                timeout = power_event_duration(ev) / 60.
            elif ev[0] == '>set':
                T = crv[-1][1]
                timeout = 0
            elif ev[0] == '>move':
                T = 0
                timeout = move_event_duration(ev) / 60.
            else:
                logging.debug('Skipping EVENT', index_row)
                continue
            if timeout < 0:
                logging.debug('Cannot render event', index_row, ev)
                continue
            time_correction += timeout
        t += time_correction
        crv.append([t * 60, T])
    return crv


def render_event(r):
    r = r.split(',')
    segment = r.pop(0)
    if segment == '>error':
        r.insert(0, '_/!\_')
    elif segment == '>gas':
        tab = json.loads(','.join(r[1:]))
        g = [e[0] + ':' + f'{e[1]:.0f}' for e in tab]
        r = ['gas:', ', '.join(g)]
    else:
        r.insert(0, segment[1:])
    r[0] = r[0].capitalize()
    return ' '.join(r)

    r = r.replace('>error', 'ERR:').replace('>', 'Event: ')
    
    
class ThermalCurveModel(QtCore.QAbstractTableModel):

    """Data model for thermal cycle editing"""
    sigModeChanged = QtCore.pyqtSignal()

    def __init__(self, remote={}, crv=None, is_live=False):
        QtCore.QAbstractTableModel.__init__(self)
        self.remote = remote
        self.dat = []
        self.row_modes = []
        header = []
        for s in ['Time (min)', u'Temperature (°C)', u'Heating Rate (°C/min)', 'Duration (min)']:
            header.append(_(s))
        self.header = header
        self.is_live = is_live

    def rowCount(self, index=QtCore.QModelIndex()):
        return len(self.dat)

    def columnCount(self, index=QtCore.QModelIndex()):
        return len(self.header)

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if not index.isValid() or not (0 <= index.row() < self.rowCount()):
            return 0
        row_entry = self.dat[index.row()]
        col = index.column()
        r = row_entry[index.column()]
        is_event = col == row.colTEMP and isinstance(r, basestring)
        if role == QtCore.Qt.DisplayRole:
            if is_event:
                r = render_event(r)
            else:
                r = round(r, 1)
            return r

        if role == QtCore.Qt.ForegroundRole:
            if is_event:
                return None
            current_row_mode = self.row_modes[index.row()]
            current_column_mode = self.mode_of_column(col)
            has_to_be_highligthed = index.row() > 0 and current_row_mode == current_column_mode
            
            if has_to_be_highligthed:
                return QtGui.QBrush(QtCore.Qt.darkRed)
            return None
        
        if (role == QtCore.Qt.BackgroundRole):
            if is_event and r.startswith('>error'):
                return QtGui.QBrush(QtCore.Qt.yellow)
            return None
                    
    def flags(self, index):
        return flags.execute(self, index, is_live=self.is_live)

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        index_row = index.row()
        index_column = index.column()
        if not index.isValid() or index_row < 0 or index_row > self.rowCount() or index_column < 0 or index_column > self.columnCount():
            logging.debug('setData: invalid line', index_row, index_column)
            return False
        if isinstance(value, basestring) and (not value.startswith('>')):
            value = float(value)
        row_entry = self.dat[index_row]
        logging.debug('setData:', index_row, index_column, value, row_entry[index_column])
        row_entry[index_column] = value
        self.dat[index_row] = row_entry
        self.update_rows_from(index_row)
        self.dataChanged.emit(self.index(index_row, 0), self.index(self.rowCount(), self.columnCount()))
        return True

    def insertRows(self, position, rows_number=1, index=QtCore.QModelIndex(), values=False):
        logging.debug('insertRows', position, rows_number, index.row())
        self.beginInsertRows(
            QtCore.QModelIndex(), position, position + rows_number - 1)
        if not values:
            values = [0] * self.columnCount()
        for current_row_index in range(rows_number):
            self.dat.insert(position + current_row_index, values)
            self.row_modes.insert(position + current_row_index, 'ramp')
        self.endInsertRows()

        return True

    def removeRows(self, position, rows=1, index=QtCore.QModelIndex()):
        self.beginRemoveRows(
            QtCore.QModelIndex(), position, position + rows - 1)
        self.dat = self.dat[:position] + self.dat[position + rows:]
        self.endRemoveRows()
        return True

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if orientation != QtCore.Qt.Horizontal:
            return
        if role == QtCore.Qt.DisplayRole:
            return self.header[section]
        elif role == QtCore.Qt.BackgroundRole:
            return QtGui.QBrush(QtGui.QColor(10, 200, 10))

    def mode_to(self, modename, row):
        self.row_modes[row] = modename
        self.headerDataChanged.emit(QtCore.Qt.Horizontal, 0, self.columnCount() - 1)
        self.sigModeChanged.emit()

    def mode_points(self, row):
        self.mode_to('points', row)

    def mode_ramp(self, row):
        self.mode_to('ramp', row)

    def mode_dwell(self, row):
        self.mode_to('dwell', row)

    def update_mode_of_row_with_mode_of_column(self, current_row, column):
        self.mode_to(self.mode_of_column(column), current_row)

    def mode_of_column(self, column):
        modes_dict = collections.defaultdict(bool)

        modes_dict[row.colTIME] = 'points'
        modes_dict[row.colRATE] = 'ramp'
        modes_dict[row.colDUR] = 'dwell'

        return modes_dict[column]

    def setCurve(self, crv, progressBar=False):
        self.removeRows(0, self.rowCount())
        self.insertRows(0, len(crv))
        for i, row in enumerate(crv):
            t, T = row
            # Detect TCEv
            if isinstance(T, basestring):
                D = event_duration(T) / 60.
                R = 0
                self.dat[i] = [t / 60., T, R, D]
                continue
            D = 0
            R = 0
            # Read previous time and setpoint to determine rate and duration
            if i > 0:
                idx, ent = next_point(crv, i - 1, -1, events=True)
                if isinstance(ent[1], basestring):
                    ev = ent[1].split(',')[0][1:]
                    if ev == 'cool':
                        cT, to = decode_cool_event(ent[1])
                        ent[1] = cT
                    elif ev in ['checkpoint', 'set', 'gas']:
                        idx, ent = next_point(crv, i - 1, -1, events=False)
                    elif ev == 'move':
                        ent[1] = crv[0][colTEMP]
                    else:
                        ent = False
                logging.debug('setCurve', i, idx, ent)
                if ent is False:
                    ent = row
                t0, T0 = ent
                D = (t - t0) / 60.
                if T == T0 or D == 0:
                    R = 0
                else:
                    R = (T - T0) / D
            self.dat[i] = [t / 60., T, R, D]
            if progressBar:
                progressBar.setValue(i)
                # QtWidgets.QApplication.processEvents()
        # Signal the entire table changed
        logging.debug('dataChanged')
        self.dataChanged.emit(self.index(0, 0),
                              self.index(self.rowCount(),
                                         self.columnCount()))
        print('setData', crv, '\nout:', self.dat)

    def update_rows_from(self, row_index=0):
        """Update all model rows starting from `row_index`"""
        maxHeatingRate = 80
        if 'maxHeatingRate' in self.remote:
            maxHeatingRate = self.remote['maxHeatingRate']
        rateLimit = []
        if 'rateLimit' in self.remote:
            rateLimit = self.remote['rateLimit']
        time_correction = 0
        for ir in range(row_index, self.rowCount()):
            # Update row
            new_row, time_correction = row.update_row(
                self.dat, ir, self.row_modes[ir], time_correction, maxHeatingRate, rateLimit)
            logging.debug('updated row:', ir, self.dat[ir], new_row)
            # Save row and time correction for next iter
            self.dat[ir] = new_row

    def curve(self, events=True):
        """Format table for plotting or transmission"""
        return clean_curve(self.dat, events)

#!/usr/bin/python
# -*- coding: utf-8 -*-

from mdf_client.qt import QtCore
from . import row
not_editable = QtCore.Qt.ItemFlags(QtCore.Qt.ItemIsEnabled)
editable = QtCore.Qt.ItemFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled)
selectable = QtCore.Qt.ItemFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
enabled = QtCore.Qt.ItemFlags(QtCore.Qt.ItemIsEnabled)
disabled = QtCore.Qt.ItemFlags()


def execute(thermal_curve_model, index, is_live=True):
    if not index.isValid() or not is_live:
        return not_editable
    
    row_index = index.row()
    column_index = index.column()
    if row_index == 0:
        if column_index != row.colTEMP:
            return disabled
    
    current_row_mode = thermal_curve_model.row_modes[row_index]
    current_column_mode = thermal_curve_model.mode_of_column(column_index)
    # Is event?
    if isinstance(thermal_curve_model.dat[row_index][row.colTEMP], str):
        return selectable
    # colTEMP always editable
    if column_index == row.colTEMP:
        # Except if rate==0 (must be equal to previous
        # if row_index>0 and thermal_curve_model.dat[row_index][row.colRATE] == 0:
        #    return enabled
        return editable
    # ??
    if (thermal_curve_model.dat[row_index][row.colTIME] < 0):
        return enabled
    
    if (current_row_mode != current_column_mode and column_index != row.colTEMP):
        return selectable

    return editable

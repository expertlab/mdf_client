#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Designer per il ciclo termico."""
import json
from mdf_canon.logger import get_module_logging
from .. import _
from mdf_canon import option
from mdf_canon.csutil import next_point, decode_cool_event, basestring, power_event_duration, move_event_duration, mancool_event_duration
from .. import conf
from .. import units
from mdf_client.qt import QtWidgets, QtGui, QtCore
from . import row as Trow
from .model import ThermalCurveModel
from .delegate import ThermalPointDelegate
from traceback import print_exc

logging = get_module_logging(__name__)


def shockHeat_is_available(kiln):
    return ('shockHeat' in kiln) and kiln['shockHeat']


def shockCool_is_available(kiln):
    return ('shockCool' in kiln) and kiln['shockCool']


def gas_is_available(kiln):
    try:
        r = kiln.gas['controlEnabled']
        print('gas_is_available', r)
        return r
    except:
        print_exc()
        return False


def functionality_is_available(kiln, name):
    license = kiln.parent().license
    if not license['functionalities']:
        license.check_license()
    return name in license['functionalities']


def motor_is_available(kiln):
    return (kiln["motorStatus"] >= 0)


class ThermalCurveTable(QtWidgets.QTableView):

    """Table view of a thermal cycle."""
    shockHeat_is_available = 0
    shockCool_is_available = 0
    motor_is_available = 0
    gas_is_available = 0
    
    def __init__(self, remote, parent=None, is_live=True):
        QtWidgets.QTableView.__init__(self, parent)
        self.setWordWrap(True)
        self.remote = remote
        self.curveModel = ThermalCurveModel(remote, is_live=is_live)
        self.setModel(self.curveModel)
        self.resizeRowsToContents()
        self.setItemDelegate(ThermalPointDelegate(remote, self))
        self.selection = QtCore.QItemSelectionModel(self.model())
        self.setSelectionModel(self.selection)
        
        if is_live:
            self.shockHeat_is_available = shockHeat_is_available(remote)
            self.shockCool_is_available = shockCool_is_available(remote)
            self.mancool_is_available = functionality_is_available(remote, 'mancool')
            self.power_is_available = functionality_is_available(remote, 'power')
            self.motor_is_available = (self.shockHeat_is_available or self.shockCool_is_available) and motor_is_available(remote)
            self.gas_is_available = gas_is_available(remote)
            self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
            self.customContextMenuRequested.connect(self.showMenu)
            
        self.resizeColumnsToContents()
        self.edit_enabled = True
        
    def enable(self, enabled):
        self.edit_enabled = enabled
        edit_trigger = QtWidgets.QTableView.AllEditTriggers if enabled else QtWidgets.QTableView.NoEditTriggers
        self.setEditTriggers(edit_trigger)

    def build_menu(self, m=False):
        if not self.edit_enabled:
            return False
        if not m:
            m = QtWidgets.QMenu(self)
        m.addAction(_('Insert point'), self.new_row)
        m.addAction(_('Insert checkpoint'), self.edit_checkpoint)
        m.addAction(_('Insert natural cooling'), self.edit_cool)
        # m.addAction(_('Insert set option'), self.edit_set)
        if self.power_is_available:
            m.addAction(_('Insert power modulation'), self.edit_power)
        if self.motor_is_available:
            m.addAction(_('Insert movement'), self.edit_move)
        if self.shockHeat_is_available:
            m.addAction(_('Insert control transition'), self.edit_tctrans)
        if self.mancool_is_available:
            m.addAction(_('Insert manual flash cooling'), self.edit_mancool)
        if self.gas_is_available:
            m.addAction(_('Insert gas control'), self.edit_gas)
        m.addAction(_('Remove current row'), self.del_row)
        m.addSeparator()
        
        crow = self.selection.currentIndex().row()
        if crow > 0:
            event = self.model().dat[crow][1]
            if isinstance(event, str) and event.startswith('>'):
                n = event.split(',')[0][1:].capitalize()
                m.addAction(_('Edit {} event').format(n), self.edit_event)
            
        return m
        
    def showMenu(self, pt):
        m = self.build_menu()
        m.popup(self.mapToGlobal(pt))

    def setCurve(self, crv, progressBar=False):
        self.model().setCurve(crv, progressBar)
        self.adjust_rows()
        
    def adjust_rows(self):
        for i, row in enumerate(self.model().dat):
            if isinstance(row[1], str):
                self.setSpan(i, Trow.colTEMP, 1, 2)
        self.resizeRowsToContents()

    def curve(self):
        return self.model().curve()

    def edit_event(self):
        row = self.selection.currentIndex().row()
        values = self.model().dat[row][1].split(',')
        event = values.pop(0)[1:]
        if event == 'error':
            values = values[1:]
            event = values.pop(0)
        logging.debug('edit_event', row, event, values)
        f = getattr(self, 'edit_' + event, False)
        if not f:
            logging.error('Cannot find edit function for event', event)
            return False
        return f(row, *values)
        
    ### ADD/DEL ####
    def new_row(self):
        crow = self.selection.currentIndex().row()
        if crow < 0:
            crow = self.model().rowCount() - 1
        row_to_copy = Trow.previous_not_event_row_index(
            crow, self.model().dat)
        values = self.model().dat[row_to_copy][:]
        values[0] = self.model().dat[crow][0]
        self.model().insertRows(crow + 1, values=values)
        
        self.selection.setCurrentIndex(self.model().index(crow + 1, 0),
                                       QtCore.QItemSelectionModel.ClearAndSelect)

    def insert_event(self, event, row=None):
        """Insert new `event` at current row"""
        crow = self.selection.currentIndex().row()
        if crow < 0:
            crow = self.model().rowCount()
        # Find latest valid time from crow
        t = 0
        idx, ent = next_point(self.model().dat, crow, -1)
        timeout = 0
        if ent is not False:
            t = ent[0]
        # TODO: rewrite to use simply event_duration()
        if event.startswith('>cool'):
            T, timeout = decode_cool_event(event)
            logging.debug('insert_event cool', t, timeout)
            if timeout > 0:
                t += timeout / 60.
        elif event.startswith('>power'):
            t += power_event_duration(event) / 60.
        elif event.startswith('>move'):
            t += move_event_duration(event) / 60.
        elif event.startswith('>mancool'):
            t += mancool_event_duration(event) / 60.
        if row:
            logging.debug('insert_event modify', self.model().dat[row], t, event)
            self.model().dat[row] = [t, event, 0, 0]
            crow = row
        else:
            logging.debug('insert_event new')
            row = crow + 1
            self.model().insertRows(row, values=[t, event, 0, 0])
        
        self.selection.setCurrentIndex(self.model().index(row, 0),
                                       QtCore.QItemSelectionModel.ClearAndSelect)
        self.model().update_rows_from(row)
        self.adjust_rows()
        
    def edit_move(self, row=None, move='openclose', timeout=60):
        desc = {}
        option.ao(desc, 'move', 'Chooser', name=_("Furnace movement"),
                  current=move, options=['open', 'close', 'openclose'])
        option.ao(desc, 'timeout', 'Float', name=_("Wait after movement"),
                  unit='minute', current=float(timeout) / 60, min=1, step=0.1)
        cp = option.ConfigurationProxy({'self': desc})
        chk = conf.InterfaceDialog(cp, cp, desc, parent=self)
        chk.setWindowTitle(_("Checkpoint configuration"))
        ok = chk.exec_()
        if ok:
            timeout = units.Converter.convert('minute', 'second', cp['timeout'])
            event = '>move,{},{}'.format(cp['move'], timeout)
            self.insert_event(event, row)
        return ok

    def edit_checkpoint(self, row=None, deltaST=3, timeout=120 * 60):
        if row is None:
            crow = self.selection.currentIndex().row()
            if crow <= 0:
                QtWidgets.QMessageBox.warning(self, _('Impossible event requested'),
                                          _('Cannot insert a checkpoint event as first row'))
                return False
            elif isinstance(self.model().dat[crow][1], basestring):
                # Currently unsupported
                QtWidgets.QMessageBox.warning(self, _('Impossible event requested'),
                                          _('Cannot insert a checkpoint event after another event'))
                return False
        desc = {}
        option.ao(desc, 'deltaST', 'Float', name=_("Temperature-Setpoint tolerance"),
                  unit='celsius', current=float(deltaST), min=0, max=100, step=0.1)
        option.ao(desc, 'timeout', 'Float', name=_("Timeout"),
                  unit='minute', current=float(timeout) / 60, min=0, max=1e3, step=0.1)
        cp = option.ConfigurationProxy({'self': desc})
        chk = conf.InterfaceDialog(cp, cp, desc, parent=self)
        chk.setWindowTitle(_("Checkpoint configuration"))
        ok = chk.exec_()
        if ok:
            timeout = units.Converter.convert('minute', 'second', cp['timeout'])
            event = '>checkpoint,{:.1f},{:.1f}'.format(cp['deltaST'], timeout)
            self.insert_event(event, row)
        return ok

    def edit_cool(self, row=None, target=50, timeout=-60):
        desc = {}
        
        current_row = self.selection.currentIndex().row()
        previous_row = Trow.previous_not_event_row_index(current_row + 1, self.model().dat)
        previous_temperature = self.model().dat[previous_row][1]

        option.ao(desc, 'target', 'Float', name=_("Target cooling temperature"),
                  unit='celsius', current=float(target), min=0, max=previous_temperature, step=0.1)
        option.ao(desc, 'timeout', 'Float', name=_("Timeout (<=0 means forever)"),
                  unit='minute', current=float(timeout) / 60, min=-1, max=1e3, step=0.1)
        cp = option.ConfigurationProxy({'self': desc})
        chk = conf.InterfaceDialog(cp, cp, desc, parent=self)
        chk.setWindowTitle(_("Natural cooling configuration"))
        ok = chk.exec_()
        if ok:
            timeout = units.Converter.convert('minute', 'second', cp['timeout'])
            if timeout < 0:
                timeout = -1
            event = '>cool,{:.1f},{:.1f}'.format(cp['target'], timeout)
            self.insert_event(event, row)
        return ok
    
    def edit_power(self, row=None, amplitude=5, period=180, cycles=5, wait=10 * 60, base=0, final=0):
        crow = self.selection.currentIndex().row()
        if crow <= 0:
            QtWidgets.QMessageBox.warning(self, _('Impossible event requested'),
                                      _('Cannot insert a power modulation event as first row'))
            return False
        desc = {}
        option.ao(desc, 'amplitude', 'Float', name=_("Amplitude"),
                  unit='percent', current=float(amplitude), min=0, max=50, step=0.1)
        option.ao(desc, 'period', 'Float', name=_("Period"),
                  unit='second', current=float(period), min=0.1, max=600, step=0.1)
        option.ao(desc, 'cycles', 'Integer', name=_("Cycles"),
                  current=int(cycles), min=0, max=100, step=1)
        option.ao(desc, 'wait', 'Float', name=_("Stabilize"),
                  unit='minute', current=float(wait) / 60, min=0, max=60, step=0.1)
        option.ao(desc, 'base', 'Float', name=_("Base power (0=Auto)"),
                  unit='percent', current=float(base), min=0, max=100, step=0.1)
        option.ao(desc, 'final', 'Float', name=_("Final power (0=fixed)"),
                  unit='percent', current=float(final), min=0, max=100, step=0.1)
        cp = option.ConfigurationProxy({'self': desc})
        chk = conf.InterfaceDialog(cp, cp, desc, parent=self)
        chk.setWindowTitle(_("Power modulation configuration"))
        ok = chk.exec_()
        if ok:
            wait = units.Converter.convert('minute', 'second', cp['wait'])
            event = '>power,{:.1f},{:.1f},{},{:.1f},{:.1f},{:.1f}'.format(cp['amplitude'],
                                                                   cp['period'], cp['cycles'],
                                                                   wait, cp['base'], cp['final'])
            self.insert_event(event, row)
        return ok

    def edit_tctrans(self, row=None, target=1, rate=5):
        desc = {}
        option.ao(desc, 'target', 'Float', name=_("Target Sample Thermocouple Weight"),
                  current=float(target), min=0, max=1, step=0.01)
        option.ao(desc, 'rate', 'Float', name=_("Control temperature switching rate (0=sudden)"),
                  unit='celsius/min', current=float(rate), min=0, max=30, step=0.1)
        cp = option.ConfigurationProxy({'self': desc})
        chk = conf.InterfaceDialog(cp, cp, desc, parent=self)
        chk.setWindowTitle(_("Thermocouple Control Transition Configuration"))
        ok = chk.exec_()
        if ok:
            event = '>tctrans,{:.2f},{:.1f}'.format(cp['target'], cp['rate'])
            self.insert_event(event, row)
        return ok
    
    def edit_mancool(self, row=None, timeout=300):
        desc = {}
        option.ao(desc, 'timeout', 'Integer', name=_("Timeout"), unit='second',
                  current=float(timeout), min=0, max=3600, step=1)
        cp = option.ConfigurationProxy({'self': desc})
        chk = conf.InterfaceDialog(cp, cp, desc, parent=self)
        chk.setWindowTitle(_("Manual Shock Cooling Configuration"))
        ok = chk.exec_()
        if ok:
            event = '>mancool,{:.0f}'.format(cp['timeout'])
            self.insert_event(event, row)
        return ok
    
    def edit_set(self, row=None, target='/kiln/pid/Kp', value='4'):
        desc = {}
        option.ao(desc, 'target', 'String', target, name=_("Target option"))
        option.ao(desc, 'value', 'String', json.loads(value), name=_("New value"))
        cp = option.ConfigurationProxy({'self': desc})
        chk = conf.InterfaceDialog(cp, cp, desc, parent=self)
        chk.setWindowTitle(_("Set option to new value"))
        ok = chk.exec_()
        if ok:
            path = cp['target'].strip('/').split('/')
            value = cp['value']
            opt = path.pop(-1)
            obj = self.remote.parent().toPath(path)
            if obj is None:
                logging.error('edit_set: cannot find target object', path)
                return False
            opt = obj.gete(opt)
            # If not a number, encode in json
            try:
                float(value)
            except:
                value = json.dumps(value)
            event = '>set,{},{}'.format(cp['target'], value)
            self.insert_event(event, row)
        return ok
    
    def find_previous_gas(self):
        crow = self.selection.currentIndex().row()
        for row in self.model().dat[0:crow][::-1]:
            if isinstance(row[1], str) and row[1].startswith('>gas'):
                r = json.loads(','.join(row[1].split(',')[2:]))
                return r
        return None
    
    def edit_gas(self, row=None, *table):
        if len(table) > 1:
            table = json.loads(','.join(table[1:]))
        else:
            table = self.find_previous_gas()
        opt = self.remote.gas.gete('gas')
        current = opt['current']
        opt['kid'] = '**local**'
        desc = {'gas': opt}
        if table:
            if set([r[0] for r in current]).symmetric_difference(set([r[0] for r in table])):
                logging.error('Gas definition mismatch', table, current)
            else:
                opt['current'] = table
        cp = option.ConfigurationProxy({'self': desc})
        chk = conf.InterfaceDialog(cp, cp, desc, parent=self)
        chk.setWindowTitle(_("Set gas flow table"))
        ok = chk.exec_()
        totals = {r[0]:r[1] for r in cp['gas']}
        tot = sum(totals.values())
        # vap = totals.pop('H2O', 0)
        # maxvap = sum(totals.values()) * self.remote.gas['maxVapourContent'] / 100
        if tot > self.remote.gas['maxTotalSetpoint']:  # or vap > maxvap:
            QtWidgets.QMessageBox.warning(self, 'Gas flow is too high',
                                          'Overall gas flow exceeds 100%. It will be reproportioned on Apply/Save.',
                                          QtWidgets.QMessageBox.Ok)
        if ok:
            event = '>gas,/kiln/gas/gas,{}'.format(json.dumps(cp['gas']))
            self.insert_event(event, row)
        return ok
        
    def del_row(self):
        crow = self.selection.currentIndex().row()
        if crow <= 1:
            crow = 1
        self.model().removeRows(crow)

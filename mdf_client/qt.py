#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
py = sys.version_info[0]

if py == 2:
    from PyQt4 import QtCore, QtGui, QtSvg, uic
    Qt = QtCore.Qt
    QtWidgets = QtGui
    
else:
    # PY3
    from PyQt5 import QtCore, QtWidgets, QtSvg, uic, QtGui
    Qt = QtCore.Qt

    
def uconnect(signal, slot, conntype=QtCore.Qt.AutoConnection):
    """Connect uniquely, if possible."""
    try:
        print('signal already connected', signal, slot)
        return signal.connect(slot, conntype | QtCore.Qt.UniqueConnection)
    except TypeError:
        return False


def udisconnect(signal, slot):
    try:
        return signal.disconnect(slot)
    except TypeError:
        print('signal already disconnected', signal, slot)
        return False

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Applicazione di grafica basata su Veusz"""
# Librerie generali
import os

import sip
sip.setapi('QString', 2)
from mdf_client.qt import QtWidgets, QtGui, QtCore

# Veusz libraries
import veusz.utils
from veusz.windows import mainwindow 

import veusz.document as document
import veusz.setting as setting
from veusz import veusz_main
import veusz.windows.plotwindow as plotwindow

from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from mdf_canon.plugin import dataimport
from mdf_canon.csutil import unicode_func

from .. import helpmenu

from ..filedata import MisuraDocument
from .. import _
from .. import misura3
from .. import filedata
from .. import plugin
from .. import parameters as params
from ..plugin import *  # ??? devo importarlo per pyinstaller...!
from .. import navigator
from ..clientconf import confdb
from ..database import getDatabaseWidget, getRemoteDatabaseWidget

from ..confwidget import RecentMenu, ClientConf, addNameSigList_to_menu
from .. import iutils
from ..live import Tasks
from . import plot
from . import veuszplot

setting.transient_settings['unsafe_mode'] = True


def set_tearoff(m):
    for a in m.actions():
        if a.menu():
            a.menu().setTearOffEnabled(True)
            set_tearoff(a.menu())


class CustomInterface(object):

    def __init__(self, main_window, name):
        self.mw = main_window
        self.menu = QtWidgets.QMenu(name)
        menu_bar = self.mw.menuBar()
        help_action = menu_bar.actions()[-1]
        menu_bar.insertMenu(help_action, self.menu)
        self.name = name

    def buildMenus(self, actions):
        # Build menus
        menu = list(actions.keys())
        mmenus = [[self.name.lower(), '&' + self.name, menu]]
        veusz.utils.constructMenus(
            self.mw.menuBar(), {self.name.lower(): self.menu}, mmenus, actions)
        
        set_tearoff(self.menu)

    def buildToolbars(self, actions):
        # Build the toolbars
        tb = QtWidgets.QToolBar(self.name + " Toolbar", self.mw)
        tb.setObjectName(self.name + 'Toolbar')
        self.mw.addToolBar(QtCore.Qt.TopToolBarArea, tb)
        veusz.utils.addToolbarActions(tb, actions, tuple(actions.keys()))

    def defaultPlot(self, dataset_names, instrument_name):
        plugin_class, plot_rule_func = plot.get_default_plot_plugin_class(instrument_name)
        logging.debug('defaultPlot', dataset_names, instrument_name, plugin_class)
        linked = self.mw.document.data[list(dataset_names.keys())[0]].linked
        plot_rule = plot_rule_func(confdb, linked.conf)
        logging.debug('defaultPlot', plugin_class, plot_rule)
        p = plugin_class()
        result = p.apply(self.mw.cmd, {'dsn': dataset_names,
                                       'rule': plot_rule})
        self.mw.document.enableUpdates()
        self.mw.plot.actionForceUpdate()
        self.mw.m4.openedFiles.refresh_model()
        self.mw.m4.openedFiles.expand_plotted_nodes()
        return result


def misura_import(self, filename, **options):
    """Import misura data from HDF file"""
    defaults = {'rule_exc': confdb['rule_exc'],
                'rule_inc': confdb['rule_inc'],
                'rule_load': confdb['rule_plot'],
                'rule_unit': confdb['rule_unit'],
                'gen_rule_load': True}
    if not filename.startswith('mdf://'):
        defaults['rule_load'] += '\n' + confdb['rule_load']
    for k, v in defaults.items():
        if k in options:
            continue
        options[k] = v
    # lookup filename
    filename = unicode_func(filename)
    realfilename = self.findFileOnImportPath(filename)
    logging.debug('open_file:', filename, realfilename)
    p = filedata.ImportParamsMisura(
        filename=realfilename, **options)
    op = filedata.OperationMisuraImport(p)
    self.document.applyOperation(op)
    confdb.mem_file(realfilename, op.measurename)
    dsnames = op.outdatasets
    logging.debug('Imported datasets %s' % (' '.join(dsnames),))
    return dsnames, op.instrument


# Add the ImportMisura command to the CommandInterface class
imp = 'ImportMisura'
safe = list(document.CommandInterface.safe_commands)
safe.append(imp)
document.CommandInterface.safe_commands = safe
document.CommandInterface.ImportMisura = misura_import


def set_data_val(self, dsname, column, row, val):
    """Set dataset `dsname` to `val` in range `row`. row can be a slice in case val is array."""
    op = document.OperationDatasetSetVal(dsname, column, row, val)
    self.document.applyOperation(op)


# Add the SetDataVal command to the CommandInterface class
imp = 'SetDataVal'
safe = list(document.CommandInterface.safe_commands)
safe.append(imp)
document.CommandInterface.safe_commands = safe
document.CommandInterface.SetDataVal = set_data_val


class MisuraInterface(CustomInterface, QtCore.QObject):
    """MainWindow methods useful for misura specific widgets and actions"""
    
    def __init__(self, main_window):
        CustomInterface.__init__(self, main_window, 'MDF')
        QtCore.QObject.__init__(self, parent=main_window)
        self.recent_actions = []

        # Navigator
        self.openedFilesDock = QtWidgets.QDockWidget(self.mw.centralWidget())
        self.openedFilesDock.setWindowTitle(_('MDF Navigator'))
        self.openedFiles = navigator.Navigator(
            parent=self.openedFilesDock, doc=self.mw.document, mainwindow=self.mw)
        self.openedFilesDock.setWidget(self.openedFiles)
        self.openedFilesDock.setObjectName('misuranavigator')
        self.mw.addDockWidget(
            QtCore.Qt.RightDockWidgetArea, self.openedFilesDock)

        self.mw.plot.sigUpdatePage.connect(self.update_page)
        self.openedFiles.sig_select_plot.connect(self.nav_select)
        
        # Database dock
        self.databaseDock = QtWidgets.QDockWidget(self.mw.centralWidget())
        self.databaseDock.setWindowTitle(_('MDF Database'))
        self.databaseTabs = QtWidgets.QTabWidget()
        self.databaseTabs.setTabBarAutoHide(True)
        self.databaseDock.setWidget(self.databaseTabs)
        self.databaseDock.setObjectName('mdfdatabase')
        self.databaseDock.hide()
        self.databaseTabs.tabCloseRequested.connect(self.databaseTabs.removeTab)
        
        self.mw.addDockWidget(
            QtCore.Qt.RightDockWidgetArea, self.databaseDock)

        # Recent Files Menus
        self.recentExe = RecentMenu(confdb, 'executed', self.mw)
        self.recentExe.select.connect(self.liveImport)
        self.menu.addMenu(self.recentExe)
        self.recentFile = RecentMenu(confdb, 'file', self.mw)
        self.recentFile.select.connect(self.liveImport)
        self.recentFile.convert.connect(self.convert_file)
        self.menu.addMenu(self.recentFile)
        self.recentDatabase = RecentMenu(confdb, 'database', self.mw)
        self.recentDatabase.select.connect(self.open_database)
        self.menu.addMenu(self.recentDatabase)
        self.menu.aboutToShow.connect(self.populate_recent)

#        ACTIONS
        a = veusz.utils.makeAction
        self.actions = {
            'm4.open':
                a(self, 'Open Local Test File', 'Open MDF File',
              self.recentFile.new, icon='m4.open'),
            'm4.mdfnav':
                a(self, 'Navigator', 'Opened Tests Navigator',
              self.hs_navigator, icon='m4.navigator', checkable=True),
            'm4.conf':
                a(self, 'Client configuration', 'Preferences',
              self.open_conf, icon='m4.conf'),
            'm4.compare':
                a(self, 'Compare samples', 'Compare samples',
              self.compare_samples),
        }
        self.actions['m4.mdfnav'].setChecked(True)
        
        def slotfn(klass):
            return lambda: self.mw.treeedit.slotMakeWidgetButton(klass)

        # TODO: Find better place for those!
        # ('datapoint', 'intercept', 'synaxis', 'imagereference'):
        for widgettype in ['label', 'key']:
            wc = document.thefactory.getWidgetClass(widgettype)
            slot = slotfn(wc)
            self.mw.treeedit.addslots[wc] = slot
            actionname = 'add.' + widgettype
            self.actions[actionname] = veusz.utils.makeAction(
                self.mw.treeedit,
                wc.description, 'Add %s' % widgettype,
                slot,
                icon='button_%s' % widgettype)
        self.mw.treeedit.vzactions.update(self.actions)
        self.buildMenus(self.actions)
        self.buildToolbars(self.actions)

        self.mw.ci.addCommand('DefaultPlot', self.defaultPlot)

        self.mw.vzactions['help.about'].setText('About Veusz')
        self.mw.vzactions.update(self.actions)
        
        hmenu = self.mw.menuBar().actions()[-1].menu()
        hmenu.removeAction(self.mw.vzactions['help.home'])
        hmenu.removeAction(self.mw.vzactions['help.bug'])
        hmenu.removeAction(self.mw.vzactions['help.tutorial'])
        hmenu.removeAction(self.mw.menus["help.examples"].menuAction())
        # hmenu.removeAction(self.mw.vzactions['help.examples']) 
        
        about_mdf_action = veusz.utils.makeAction(self,
                                                     _('Displays information about MDF'),
                                                     _('About MDF'),
                                                     helpmenu.showAbout)
        about_mdf_action.setIcon(
            QtGui.QIcon(os.path.join(params.pathArt, 'about.png')))

        hmenu.addAction(about_mdf_action)
        
        file_menu = self.mw.menuBar().actions()[0].menu()
        file_menu.insertAction(file_menu.actions()[1],
                               self.actions['m4.open'])
        
        windows_menu = self.mw.menuBar().actions()[2].menu().actions()[0].menu()
        windows_menu.insertAction(windows_menu.actions()[0],
                                  self.actions['m4.mdfnav'])
        
    def populate_recent(self):
        r = self.recent_actions
        for ac in r:
            self.menu.removeAction(ac)
        r = []
        # Recent files
        r.append(self.menu.addSeparator())
        nsl = self.recentFile.getNameSigList()
        r += addNameSigList_to_menu(self.menu, nsl, emitter=self.recentFile)
        
        # Recent databases
        r.append(self.menu.addSeparator())
        nsl = self.recentDatabase.getNameSigList()
        r += addNameSigList_to_menu(self.menu, nsl, emitter=self.recentDatabase)
        
        self.recent_actions = r
        
    def open_conf(self):
        self.cc = ClientConf()
        self.cc.show()

    def update_page(self, *foo):
        """Update the navigator view in order to show colours and styles
        effectively present in the current page"""
        n = self.mw.plot.getPageNumber()
        page = self.mw.document.basewidget.getPage(n)
        if page is None:
            logging.debug('NO PAGE FOUND', n)
            return
        if self.openedFiles.model().page.startswith(page.path):
            logging.debug('Not updating page', page.path)
            return
        logging.debug(
            'MisuraInterface.update_page', self.openedFiles.model().page, page.path)
        self.openedFiles.model().set_page(page.path)
        logging.debug('done model.set_page')
        self.mw.plot.sigUpdatePage.connect(self.update_page)

    def nav_select(self, path):
        """Collect selection signals from the navigators and route them to the widgets tree"""
        view = self.mw.treeedit.treeview
        model = view.model()
        wdg = self.mw.document.resolveWidgetPath(None, path)
        idx = model.getWidgetIndex(wdg)
        if idx:
            view.setCurrentIndex(idx)

    def hs_navigator(self):
        """Hide/show the MDF Navigator for opened tests and files"""
        if self.openedFilesDock.isVisible():
            self.openedFilesDock.hide()
        else:
            self.openedFilesDock.show()

    def open_database(self, path=False):
        if not path:
            path = confdb['database']
        idb = getDatabaseWidget(path)
        
        if not idb:
            logging.error('Could not open database', path)
            return False
        # idb.show()
        idb.sig_selectedFile.connect(self.liveImport)
        # Keep a reference
        # self.idb = idb
        confdb.mem_database(path)
        
        self.databaseDock.show()
        idb.menu.hide()
        idb.doClose.setChecked(False)
        idb.doClose.hide()
        self.databaseTabs.setTabsClosable(True)
        t = self.databaseTabs.addTab(idb, os.path.basename(path))
        self.databaseTabs.setTabToolTip(t, path)
        logging.debug('open_database', path)
        
    def clear_databases(self):
        while self.databaseTabs.count():
            self.databaseTabs.removeTab(0)

    def open_server(self, path):
        idb = getRemoteDatabaseWidget(path)
        if not idb:
            return False
        idb.show()

    def listImport(self):
        """Import data from a list of misura HDF files."""
        if not getattr(self, 'imd', False):
            return
        for f in self.imd.ls:
            f = str(f)
            self.liveImport(f, options=self.imd.options)
        if self.imd.doClose.isChecked():
            self.imd.close()

    def liveImport(self, filename, options={}):
        """Import misura data and do the default plotting"""
        print('liveImport', filename)
        self.mw.document.suspendUpdates()
        try:
            dataset_names, instrument_name = self.open_file(filename, **options)
            self.defaultPlot(dataset_names, instrument_name)
        finally:
            if len(self.mw.document.suspendupdates) > 0:
                self.mw.document.enableUpdates()

    def convert_file(self, path):
        outpath = dataimport.convert_file(path)
        self.liveImport(outpath)

    def open_file(self, filename, **options):
        """Import misura data from HDF file"""
        # lookup filename
        filename = unicode_func(filename)
        logging.debug('importing misura with defaults', filename)
        dsnames, instrument_name = self.mw.cmd.ImportMisura(
            filename, **options)
        self.openedFiles.refresh_model()
        Tasks.instance().hide()
        return dsnames, instrument_name
    
    def compare_samples(self):
        from mdf_client.fileui.compare_samples import CompareMicroscopeSamples
        self.comparewin = CompareMicroscopeSamples()
        self.comparewin.document = self.mw.document
        self.comparewin.show()


class Misura3Interface(CustomInterface, QtCore.QObject):

    """MainWindow methods useful for Misura3 specific widgets and actions"""

    def __init__(self, main_window):
        CustomInterface.__init__(self, main_window, 'Misura3')
        QtCore.QObject.__init__(self, parent=main_window)
        self.recent_actions = []
        self.recentDatabase = RecentMenu(confdb, 'm3database', self.mw)
        self.recentDatabase.select.connect(self.open_database)
        self.menu.addMenu(self.recentDatabase)
        self.menu.aboutToShow.connect(self.populate_recent)

    m3db = False

    def open_database(self, path):
        if self.m3db:
            self.m3db.hide()
            self.m3db.close()
            self.m3db = False
        db = misura3.TestDialog(path=path)
        db.keep_img = True
        db.img = True
        db.force = False
        db.imported.connect(self.liveImport)
        confdb.mem_m3database(path)
        db.show()
        self.m3db = db

    def liveImport(self, filename):
        """Import and plot default datasets. Called upon user request"""
        self.mw.document.suspendUpdates()
        dataset_names, instrument_name = self.open_file(filename)
        self.defaultPlot(dataset_names, instrument_name)

    def open_file(self, filename, **options):
        """Import misura data from HDF file"""
        # lookup filename
        filename = unicode_func(filename)
        dsnames, instrument_name = self.mw.cmd.ImportMisura(
            filename, **options)
        self.mw.m4.openedFiles.refresh_model()
        return dsnames, instrument_name
        
    def populate_recent(self):
        r = self.recent_actions
        for ac in r:
            self.menu.removeAction(ac)
        r = []
       
        # Recent databases
        r.append(self.menu.addSeparator())
        nsl = self.recentDatabase.getNameSigList()
        r += addNameSigList_to_menu(self.menu, nsl, emitter=self.recentDatabase)
        
        self.recent_actions = r


class Graphics(mainwindow.MainWindow):

    """Main Graphics window, derived directly from Veusz"""

    # FIXME: patch to Veusz for dynamic document substitution!
    @property
    def document(self):
        return self._document

    @document.setter
    def document(self, ndoc):
        """Block overwrites! (hack!)"""
        return

    def __init__(self, *a):
        iutils.loadIcons()
        logging.debug('Load Icons OK')
        self._document = MisuraDocument()
        # Shortcuts to command interpreter and interface
        
        mainwindow.PlotWindow = veuszplot.VeuszPlotWindow
        mainwindow.MainWindow.__init__(self, *a)
        logging.debug('MainWindow init')
        self.ci = self.console.interpreter
        self.cmd = self.ci.interface
        # misura Interface
        logging.debug('misura Interface')
        self.m4 = MisuraInterface(self)
        # Misura3 Interface
        # print 'Misura3 Interface'
        self.m3 = Misura3Interface(self)
        self.datadock.hide()
        
        self.setWindowIcon(QtGui.QIcon(os.path.join(params.pathArt, 'graphics.svg')))
        
        from ..navigator import NavigatorToolbar
        self.navtoolbar = NavigatorToolbar(self.m4.openedFiles, self)
        self.navtoolbar.show()
        self.addToolBar(QtCore.Qt.TopToolBarArea, self.navtoolbar)
        self.plot.update_page()
        QtCore.QTimer.singleShot(500, self.m4.open_database)
        
        for bar in (self.datatoolbar, self.treeedit.edittoolbar, self.treeedit.addtoolbar):
            self.removeToolBarBreak(bar)
            self.removeToolBar(bar)
            self.addToolBar(bar)
            bar.hide()

    def setupDefaultDoc(self, mode):
        """Make default temperature/time pages"""
        if self.documentsetup:
            return
        plugin.makeDefaultDoc(self.cmd)
        self.loadDefaultStylesheet()
        self.loadDefaultCustomDefinitions()
        self.documentsetup = True

    def updateTitlebar(self):
        filename = 'Untitled' if self.filename == '' else os.path.basename(
            self.filename)
        self.setWindowTitle(_('%s - MDF') % filename)


class GraphicsApp(veusz_main.VeuszApp):

    def __init__(self):
        
        super(GraphicsApp, self).__init__()
        if 'python' in self.args:
            self.args.remove('python')

    def openMainWindow(self, docs):
        """Open the main window with any loaded files."""
        MainWindow = Graphics

        emptywins = []
        for w in self.topLevelWidgets():
            if isinstance(w, MainWindow) and w.document.isBlank():
                emptywins.append(w)
        created = 0
        # load in filenames given
        v_import = []
        for filename in docs:
            if filename.endswith('.py') or filename.endswith('.pyc') or filename.endswith('.pyd'):
                continue
            if filename.endswith('.h5'):
                v_import.append(filename)
                continue
            if filename[:4] in ('http', 'mdf:'):
                v = filename.split('|')
                # Handle multiple docs
                for f in v[1:]:
                    v_import.append(v[0] + '|' + f)
                continue
            created += 1
            if not emptywins:
                MainWindow.CreateWindow(filename)
            else:
                emptywins[0].openFile(filename)
        if not created:
            # create blank window
            logging.debug('creating blank window')
            mw = MainWindow.CreateWindow()
            for filename in v_import:
                mw.m4.liveImport(filename)

    def startup(self):
        self.slotStartApplication()

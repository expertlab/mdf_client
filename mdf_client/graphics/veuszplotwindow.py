#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Simple plotting for browser and live acquisition."""
import os
from functools import partial
from traceback import format_exc
from mdf_canon.logger import get_module_logging
from veusz import qtall as qt4
from mdf_client.qt import QtWidgets, QtCore
from .. import plugin
from ..iutils import searchFirstOccurrence, get_plotted_tree
import veusz.setting.settingdb as setdb
import veusz.windows.treeeditwindow as treeeditwindow
import veusz.document as document
from veusz.document import registerImportCommand, operations
import veusz.windows.plotwindow as plotwindow
from veusz.windows import consolewindow
from veusz.document import OperationWidgetDelete
from veusz.document.export import printPages

MAX = 10 ** 5
MIN = -10 ** 5
logging = get_module_logging(__name__)


def _(text, disambiguation=None, context='PlotWindow'):
    """Translate text."""
    return qt4.QCoreApplication.translate(context, text, disambiguation)


def get_current_graph(plot_window):
    p = plot_window.getPageNumber()
    page = plot_window.document.basewidget.children[p]
    graph = searchFirstOccurrence(page, 'graph')
    cmd = document.CommandInterface(plot_window.document)
    return graph, cmd


def get_configuration_id(doc, cid):
    found = False, False
    for name, ds in doc.data.items():
        if not getattr(ds, 'linked', False):
            continue
        if not getattr(ds.linked, 'conf'):
            continue
        if str(id(ds.linked.conf)) == cid:
            found = name, ds
            break
    return found


def with_busy_cursor(function_to_decorate):

    def wrapper(*a, **k):
        qt4.QApplication.setOverrideCursor(qt4.QCursor(qt4.Qt.WaitCursor))

        try:
            function_to_decorate(*a, **k)
        finally:
            qt4.QApplication.restoreOverrideCursor()

    return wrapper


def get_datapoint_var(var0, label):
    if var0 == 'y':
        search = '%(ylabel)s'
        var = '{}s=%(y).3g'.format(search)
        
    else:
        search = '%({0})'.format(var0)
        var = '{}={}.0f'.format(var0, search)
    if search not in label:
        if label:
            label += r'\\' + var
        else: 
            label += var
    else: 
        label1 = list(filter(lambda e: search not in e, label.split(r'\\')))
        label = r'\\'.join(label1)
    return var, search, label

    
def process_dragMoveEvent(event):
    # logging.debug('dragMoveEvent', event.mimeData())
    accept = event.mimeData().hasFormat("image/png")
    accept += event.mimeData().hasFormat("text/option")
    if accept:
        event.acceptProposedAction()  

        
def process_dropEvent(plot_window, drop_event):
    if drop_event.mimeData().hasFormat("mdf/option"):
        return process_option_dropEvent(plot_window, drop_event)
    
    elif drop_event.mimeData().hasFormat("image/png"):
        return process_image_dropEvent(plot_window, drop_event)
    
    elif drop_event.mimeData().hasFormat("text/plain"):
        return process_text_dropEvent(plot_window, drop_event)

            
def process_image_dropEvent(plot_window, drop_event):
    graph, cmd = get_current_graph(plot_window)
    pix = drop_event.mimeData().data("image/png")
    name = bytes(drop_event.mimeData().data("text/plain")).decode('utf8')
    name = name.replace(':/', ':').replace('/', '_')
    
    cmd.To(graph.path)
    try:
        cmd.Remove(name)
    except:
        pass
    encoded = bytes(pix.toBase64()).decode('ascii')
    cmd.Add('imagefile', name=name, autoadd=False,
            filename='{embedded}',
            embeddedImageData=encoded)

    
def process_option_dropEvent(plot_window, drop_event):
    graph, cmd = get_current_graph(plot_window)
    data = bytes(drop_event.mimeData().data("mdf/option")).decode('utf8')
    cid, opt_path = data.split(':')
    dsname, ds = get_configuration_id(plot_window.document, cid)
    if not dsname:
        logging.error('No dataset associated with conf id:', cid, opt_path)
    name = opt_path.replace(':/', ':').replace('/', '_')
    cmd.To(graph.path)
    try:
        cmd.Remove(name)
    except:
        pass
    cmd.Add('optionlabel', name=name, autoadd=False, showName=True,
            option=opt_path, dataset=dsname)

    
def process_text_dropEvent(plot_window, drop_event):
    graph, cmd = get_current_graph(plot_window)
    text = bytes(drop_event.mimeData().data("text/plain")).decode('utf8')
    cmd.To(graph.path)
    cmd.Add('label', autoadd=False, label=text)

    
class VeuszPlotWindow(plotwindow.PlotWindow):
    sigNearestWidget = QtCore.pyqtSignal(object)
    intercept_dialog = False
    
    def __init__(self, document, parent=None, **kw):
        plotwindow.PlotWindow.__init__(self, document, parent=parent, **kw)
        self.contextmenu = QtWidgets.QMenu(self)
        self.sigUpdatePage.connect(self.update_page)
        self.navigator = False
        registerImportCommand('MoveToLastPage', self.moveToLastPage)
        self.setAcceptDrops(True)
        self.sigPointPicked.connect(self.intercept_on_pick)
        
    def close(self):
        self.document.signalModified.disconnect(self.slotDocModified)
        
    def intercept_on_pick(self, pickinfo):
        logging.debug('intercept_on_pick', pickinfo.widget.path)
        if not self.parent():
            logging.debug('Cannot locate mainwindow')
            return False
        if self.intercept_dialog:
            logging.debug('An intercept dialog is already opened', self.intercept_dialog)
            return False
        d = plugin.InterceptPlugin.clicked_curve(pickinfo, self.parent())
        d.dialogFinished.connect(self.clear_intercept_dialog)
        self.intercept_dialog = True 
        return True
    
    def place_datapoint_action(self, widget, pos):
        info = widget.pickPoint(pos.x(), pos.y(), self.painthelper.widgetBounds(widget))
        return self.intercept_on_pick(info)
            
    def clear_intercept_dialog(self):
        self.intercept_dialog = False
        
    def update_page(self, *foo, **kw):
        """Update the navigator view in order to show colours and styles
        effectively present in the current page"""
        new_page = kw.get('new_page', -1)
        n = new_page if new_page >= 0 else self.getPageNumber() 
        page = self.document.basewidget.getPage(n)
        if page is None:
            logging.debug('NO PAGE FOUND', n, id(self))
            return
            
        self.docchangeset = -100  # force plot update for page resizing
        self.document.model.set_page(page.path)
        g = searchFirstOccurrence(page, 'graph', 0) or page
        sel = self.parent().treeedit.selwidgets
        if sel and (not sel[0].path.startswith(page.path)):
            self.sigWidgetClicked.emit(g, 'new')
        
    def setPageNumber(self, pageno):
        r = plotwindow.PlotWindow.setPageNumber(self, pageno)
        print('setPageNumber', pageno)
        self.update_page(new_page=pageno)
        return r
        
    def dragMoveEvent(self, event):
        process_dragMoveEvent(event)

    def dropEvent(self, drop_event):
        process_dropEvent(self, drop_event)

    def moveToLastPage(self):
        number_of_pages = self.document.getNumberPages()
        self.setPageNumber(number_of_pages - 1)
        
    def set_navigator(self, navigator):
        self.navigator = navigator
        self.sigWidgetClicked.connect(self.navigator.sync_currentwidget)
        
    def build_datapoint_menu(self, menu, widget):
        label = widget.settings.labelText
        act = menu.addAction(_('Show value'), partial(self.datapoint_toggle, widget, 'y'))
        r = get_datapoint_var('y', label)[-1]
        act.setCheckable(True)
        act.setChecked(len(r) < len(label))
        act = menu.addAction(_('Show temperature'), partial(self.datapoint_toggle, widget, 'T'))
        r = get_datapoint_var('T', label)[-1]
        act.setCheckable(True)
        act.setChecked(len(r) < len(label))
        act = menu.addAction(_('Show time'), partial(self.datapoint_toggle, widget, 't'))
        r = get_datapoint_var('t', label)[-1]
        act.setCheckable(True)
        act.setChecked(len(r) < len(label))
        
        act = menu.addAction(_('Tangent line'), partial(self.boolean_toggle, widget, 'showTangent'))
        act.setCheckable(True)
        act.setChecked(widget.settings.showTangent)
        act = menu.addAction(_('Perpendicular line'), partial(self.boolean_toggle, widget, 'showPerpendicular'))
        act.setCheckable(True)
        act.setChecked(widget.settings.showPerpendicular)
        
        m = menu.addMenu(_('Pass through'))
        m.aboutToShow.connect(partial(self.show_style_menu, widget, m, False, 'secondPoint'))
        
        act = menu.addAction(_('Remove nearby jump'), partial(widget.removeGaps))
        act = menu.addAction(_('Update'), partial(widget.actionUp))
        
    def build_text_menu(self, menu, widget, subopt='Text'):
        menu.addSeparator()
        for label, opt in [(_('Text color'), 'color'),
                          (_('Text size'), 'size'),
                          (_('Text font'), 'font'), ]:
            m = menu.addMenu(label)
            m.aboutToShow.connect(partial(self.show_style_menu, widget, m, subopt, opt))
            
    def build_option_edit_menu(self, menu, widget):
        return menu.addAction(_('Edit and recalculate'), widget.edit_option_dialog)
        
    def widget_menu(self, menu, pos):
        widget = self.identify_widget(pos)
        if widget is None:
            return
        if widget.typename == 'optionlabel':
            self.build_option_edit_menu(menu, widget)
        if widget.typename in ('datapointlabel', 'curvelabel', 'optionlabel', 'label', 'linkedfileslabel'):
            self.build_text_menu(menu, widget)
        if widget.parent and widget.parent.typename == 'datapoint':
            widget = widget.parent
        line = False
        marker = False
        
        if widget.typename == 'datapoint':
            menu.addSeparator()
            marker = 'marker'
            self.build_datapoint_menu(menu, widget)
        elif widget.typename == 'thermalcyclelabel':
            menu.addSeparator()
            for label, opt in [(_('Show title'), 'showTitle'),
                         (_('Show headers'), 'header'),
                         (_('Show units'), 'units'), ]:
                act = menu.addAction(label, partial(self.boolean_toggle, widget, opt))
                act.setCheckable(True)
                act.setChecked(widget.settings[opt])
            self.build_text_menu(menu, widget)
            return
            
        elif widget.typename == 'xy':
            marker = 'marker'
            line = 'PlotLine'
        elif widget.typename in ('axis', 'axis-function'):
            line = 'Line'
            self.build_text_menu(menu, widget, 'Label')
            for label, opt in [(_('Show major grid'), 'GridLines/hide'),
                              (_('Show minor grid'), 'MinorGridLines/hide'),
                              ]:
                act = menu.addAction(label, partial(self.boolean_toggle, widget, opt))
                act.setCheckable(True)
                act.setChecked(not self.document.resolveSettingPath(widget, opt).val)
        else:
            return
        
        menu.addSeparator()
        
        if line:
            m = menu.addMenu(_('Style'))
            m.aboutToShow.connect(partial(self.show_style_menu, widget, m, line, 'style'))
            m = menu.addMenu(_('Color'))
            m.aboutToShow.connect(partial(self.show_style_menu, widget, m, line, 'color'))
            m = menu.addMenu(_('Width'))
            m.aboutToShow.connect(partial(self.show_style_menu, widget, m, line, 'width'))
        
        if marker:
            m = menu.addMenu(_('Marker'))
            m.aboutToShow.connect(partial(self.show_style_menu, widget, m, False, marker))
        
        if widget.typename != 'xy':
            return
        
        m = menu.addMenu(_('Assign to axis'))
        m.aboutToShow.connect(partial(self.show_style_menu, widget, m, False, 'yAxis'))
        
        m = menu.addAction(_('Place datapoint'), partial(self.place_datapoint_action, widget, pos))
        
        act = menu.addAction(_('Visual translator'), partial(self.boolean_toggle, widget, 'visualTranslator'))
        act.setCheckable(True)
        act.setChecked(widget.settings.visualTranslator)
        
        dsn = widget.settings.yData
        node = self.document.model.tree.traverse(dsn)
        if not node:
            return
        
        m = menu.addMenu(_('X Unit'))
        m.aboutToShow.connect(partial(self.show_units_menu, m, widget.settings.xData))
        m = menu.addMenu(_('Y Unit'))
        m.aboutToShow.connect(partial(self.show_units_menu, m, dsn))
        
        self.dataset_menu = self.navigator.buildContextMenu(node)
        self.dataset_menu.aboutToShow.connect(partial(self.navigator.hovered_node, node))
        menu.addMenu(self.dataset_menu)
        
        self.group_menu = QtWidgets.QMenu(node.parent.name().capitalize())
        sub_menu_func = partial(self.navigator.build_recursive_menu,
                                node.parent,
                                self.group_menu)
        self.group_menu.aboutToShow.connect(sub_menu_func)
        menu.addMenu(self.group_menu)
        
    def show_units_menu(self, menu, dataset):
        menu.clear()
        node = self.document.model.tree.traverse(dataset)
        dom = self.navigator.domainsMap['MeasurementUnitsNavigatorDomain']
        dom.add_unit(menu, node)
        
    def build_style_menu(self, setting, widget, menu):
        """Build a specific section of the style menu"""
        ctrl = setting.makeControl(menu)
        ctrl.hide()
        ctrl = getattr(ctrl, 'combo', ctrl)
        for i in range(ctrl.count()):
            txt = ctrl.itemText(i)
            if not txt:
                continue
            val = setting.fromUIText(txt)
            # Skip myself if listing a pass-through point
            if val == widget.name and setting.name == 'secondPoint':
                continue
            func = partial(self.set_from_menu, setting, widget, val)
            a = menu.addAction(ctrl.itemIcon(i), txt, func)
            a.setCheckable(True)
            if i == ctrl.currentIndex():
                a.setChecked(True)
        self._ctrl = ctrl
        menu.addSeparator()
        
    def uniform_axis_setting(self, widget, name, val):
        for group in ('TickLabels', 'MajorTicks', 'MinorTicks', 'Label'):
            cs = widget.settings[group].get(name)
            self.document.applyOperation(operations.OperationSettingSet(cs, val))
            
    def delete_unused_axis(self, parent, path):
        ax = self.document.resolvePath(parent, path)
        path = ax.path
        logging.debug('delete_unused_axis', path)
        tree = get_plotted_tree(parent)
        print(tree['axis'][path], tree)
        if tree['axis'][path]:
            return False
        logging.debug('delete_unused_axis unused ax:', path)
        
        self.document.applyOperation(operations.OperationWidgetDelete(ax))
        return True

    def set_from_menu(self, setting, widget, val):
        old = setting.get()
        logging.debug('set_from_menu', widget.typename, setting.path, setting.name, old, 'to', val)
        self.document.applyOperation(operations.OperationSettingSet(setting, val))
        if widget.typename == 'xy' and setting.name == 'yAxis':
            self.delete_unused_axis(widget.parent, old)
        if widget.typename in ('axis', 'axis-function') and setting.name == 'color':
            self.uniform_axis_color(widget, setting.name, val)
                
    def datapoint_toggle(self, widget, var0):
        label = widget.settings.labelText
        var, search, label = get_datapoint_var(var0, label)
        self.document.applyOperation(operations.OperationSettingSet(widget.settings.get('labelText'), label))
        widget.actionUp()
        
    def boolean_toggle(self, widget, opt):
        s = self.document.resolveSettingPath(widget, opt)
        newval = not s.val
        self.document.applyOperation(operations.OperationSettingSet(s, newval))
        
    def show_style_menu(self, widget, menu, subopt, opt, *a):
        """Populate the style menu with line styles and colors"""
        print('show_style_menu', widget.typename, widget.path, subopt, opt)
        menu.clear()
        if subopt:
            s = widget.settings[subopt].get(opt)
        else:
            s = widget.settings.get(opt)
        self.build_style_menu(s, widget, menu)
        
    def mouseMoveEvent(self, event):
        ret = plotwindow.PlotWindow.mouseMoveEvent(self, event)
        pos = self.mapToScene(event.pos())
        widget = self.identify_widget(pos)
        if widget:
            self.sigNearestWidget.emit(widget)
        return ret
        
    def contextMenuEvent(self, event):
        """Show context menu."""
        menu = QtWidgets.QMenu()
        for act in self.contextmenu.actions():
            menu.addAction(act)
        pos = self.mapToScene(event.pos())
        self.f = partial(self.edit_properties, pos, False)
        menu.addAction('Properties', self.f)
        self.f1 = partial(self.edit_properties, pos, True)
        menu.addAction('Formatting', self.f1)
        self.fdel = partial(self.delete_widget, pos)
        menu.addAction('Delete selected', self.fdel)
        
        self.widget_menu(menu, pos)
        
        menu.addSeparator()
        # add some useful entries
        menu.addAction(self.vzactions['view.zoommenu'])
        menu.addSeparator()
        menu.addAction(self.vzactions['view.prevpage'])
        menu.addAction(self.vzactions['view.nextpage'])
        menu.addSeparator()

        # force an update now menu item
        menu.addAction('Force update', self.actionForceUpdate)

        if self.isfullscreen:
            menu.addAction('Close full screen', self.slotFullScreen)
        else:
            menu.addAction(self.vzactions['view.fullscreen'])

        # Update policy submenu
        submenu = menu.addMenu('Updates')
        intgrp = qt4.QActionGroup(self)

        def slotfn(v):
            return lambda: self.actionSetTimeout(v, False)

        # bind interval options to actions
        for intv, text in self.updateintervals:
            act = intgrp.addAction(text)
            act.setCheckable(True)
            fn = slotfn(intv)
#             fn = veusz.utils.BoundCaller(self.actionSetTimeout, intv)
            act.triggered.connect(fn)
            if intv == self.interval:
                act.setChecked(True)
            submenu.addAction(act)

        # antialias
        menu.addSeparator()
        act = menu.addAction('Antialias', self.actionAntialias)
        act.setCheckable(True)
        act.setChecked(self.antialias)

        # Selected object specific actions
        # ...
        menu.addSeparator()
        # Export to PDF
        menu.addAction(_('Export Page'), self.slotPageExport)
        menu.addAction(_('Export All'), self.slotDocExport)
        menu.addAction(_('Print Page'), self.slotFilePrint)
        # menu.addAction('Save', self.save_to_file)
        menu.exec_(qt4.QCursor.pos())

    def save_to_file(self):
        name = QtWidgets.QFileDialog.getSaveFileName(self, _('Save this plot to file'),
                                                 _(
                                                     'runtime.vsz'),
                                                 filter='Veusz (*.vsz);;Images (*.png *.jpg);;Vector (*svg *pdf *eps)')
        name = unicode_func(name)
        if len(name) == 0:
            return
        f = open(name[0], 'w')
        self.document.saveToFile(f)
        f.close()
        
    def identify_widget(self, pos):
        if not len(self.document.basewidget.children):
            return None
        widget = self.painthelper.identifyWidgetAtPoint(
            pos.x(), pos.y(), antialias=self.antialias)
        if widget is None:
            # select page if nothing clicked
            widget = self.document.basewidget.getPage(self.pagenumber) 
        return widget      

    def edit_properties(self, pos, frm=False):
        widget = self.identify_widget(pos)
        
        self.w = treeeditwindow.PropertyList(
            self.document, showformatsettings=frm)
        self.w.getConsole = lambda *a: consolewindow.ConsoleWindow(self.document)
        s = treeeditwindow.SettingsProxySingle(self.document, widget.settings, widget.actions)
        self.w.updateProperties(s)
        self.w.setWindowTitle('{} for {} at {}'.format({False:'Properties', True:'Formatting'}[frm],
                                                 widget.typename, widget.path))
        
        # Show line tab:
        if frm and widget.typename == 'xy':
            tabbed = self.w.childlist[0]
            tabbed.setCurrentIndex(1)
        elif not frm:
            self.w._addActions(s, len(self.w.childlist))
        self.w.show()
        
    def delete_widget(self, pos):
        widget = self.identify_widget(pos)
        op = OperationWidgetDelete(widget)
        self.document.applyOperation(op)

    dirname_export = False
    filename = ''
    exdialog = False

    def slotDocExport(self):
        """Export the graph."""
        if self.exdialog:
            try:
                self.exdialog.hide()
                self.exdialog.close()
            except:
                pass
            del self.exdialog
        from veusz.dialogs.export import ExportDialog
        setdb['export_page'] = 'all'
        dialog = ExportDialog(self, self.document, self.filename)
        dialog.formatClicked('pdf')
        self.exdialog = dialog
        dialog.show()

    def slotPageExport(self, page_num=-1):
        """Copied from MainWindow. TODO: make this more modular in veusz!"""
        # check there is a page
        if self.document.getNumberPages() == 0:
            qt4.QMessageBox.warning(self, _("Error - Veusz"),
                                    _("No pages to export"))
            return

        # File types we can export to in the form ([extensions], Name)
        fd = qt4.QFileDialog(self, _('Export page'))
        if self.dirname_export:
            fd.setDirectory(self.dirname_export)

        fd.setFileMode(qt4.QFileDialog.AnyFile)
        fd.setAcceptMode(qt4.QFileDialog.AcceptSave)

        # Create a mapping between a format string and extensions
        filtertoext = {}
        # convert extensions to filter
        exttofilter = {}
        filters = []
        # a list of extensions which are allowed
        validextns = []
        formats = document.AsyncExport.getFormats()
        for extns, name in formats:
            extensions = " ".join(["*." + item for item in extns])
            # join eveything together to make a filter string
            filterstr = '%s (%s)' % (name, extensions)
            filtertoext[filterstr] = extns
            for e in extns:
                exttofilter[e] = filterstr
            filters.append(filterstr)
            validextns += extns
            
        fd.setNameFilters(filters)

        # restore last format if possible
        try:
            filt = setdb['export_format']
            fd.selectNameFilter(exttofilter[filt])
        except (KeyError, IndexError, ValueError):
            filt = 'pdf'
            fd.selectNameFilter(exttofilter[filt])
            logging.debug(format_exc())
        
        if self.filename:
            # try to convert current filename to export name
            filename = os.path.basename(self.filename)
            filename = os.path.splitext(filename)[0] + '.' + filt
            fd.selectFile(filename)
        
        if fd.exec_() == qt4.QDialog.Accepted:
            # save directory for next time
            self.dirname_export = fd.directory().absolutePath()

            filterused = str(fd.selectedNameFilter())
            setdb['export_format'] = filtertoext[filterused][0]

            chosenextns = filtertoext[filterused]

            filename = fd.selectedFiles()[0]
            logging.debug('Exported', filename)
            self.doExport(filename, validextns, chosenextns, page_num)

    @with_busy_cursor
    def doExport(self, filename, validextns, chosenextns, page_num=-1):
        ext = os.path.splitext(filename)[1][1:]
        if (ext not in validextns) and (ext not in chosenextns):
            filename += "." + chosenextns[0]
        
        if page_num == -1:
            page_num = [self.getPageNumber()]
            
        export = document.AsyncExport(
            self.document,
            bitmapdpi=setdb['export_DPI'],
            pdfdpi=setdb['export_DPI_PDF'],
            antialias=setdb['export_antialias'],
            color=setdb['export_color'],
            quality=setdb['export_quality'],
            backcolor=setdb['export_background'],
            svgtextastext=setdb['export_SVG_text_as_text'],
            svgdpi=setdb['export_DPI_SVG'],
        )
        
        export.add(filename, page_num)
        export.finish()

    def slotFilePrint(self, pageNum=-1):
        """Open a print dialog and print document."""
        parentwindow = self
        document = self.document
        if document.getNumberPages() == 0:
            qt4.QMessageBox.warning(parentwindow, _("Error - Veusz"),
                                    _("No pages to print"))
            return
        
        prnt = qt4.QPrinter(qt4.QPrinter.HighResolution)
        prnt.setColorMode(qt4.QPrinter.Color)
        from veusz import utils, compat
        prnt.setCreator(_('Veusz %s') % utils.version())
        if self.filename:
            prnt.setDocName(self.filename)
        if pageNum < 1:
            self.checkPlotUpdate()
            pageNum = self.getPageNumber() + 1
            logging.debug('slotFilePrint', pageNum)
        dialog = qt4.QPrintDialog(prnt, parentwindow)
        dialog.setMinMax(1, document.getNumberPages())
        dialog.setPrintRange(qt4.QAbstractPrintDialog.PageRange)
        dialog.setFromTo(pageNum, pageNum)
        if dialog.exec_():
            # get page range
            if dialog.printRange() == qt4.QAbstractPrintDialog.PageRange:
                # page range
                minval, maxval = dialog.fromPage(), dialog.toPage()
            elif dialog.printRange() == qt4.QAbstractPrintDialog.CurrentPage:
                minval, maxval = pageNum, pageNum
            else:
                # all pages
                minval, maxval = 1, document.getNumberPages()
    
            # pages are relative to zero
            minval -= 1
            maxval -= 1
    
            # reverse or forward order
            if prnt.pageOrder() == qt4.QPrinter.FirstPageFirst:
                pages = list(compat.crange(minval, maxval + 1))
            else:
                pages = list(compat.crange(maxval, minval - 1, -1))
    
            # if more copies are requested
            pages *= prnt.copyCount()
            # do the printing
            printPages(document, prnt, pages)


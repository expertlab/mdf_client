#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Simple plotting for browser and live acquisition."""
import os
from traceback import format_exc
from mdf_canon.logger import get_module_logging
from veusz import qtall as qt4
from mdf_client.qt import QtWidgets, QtCore
from .. import filedata, plugin
from ..iutils import searchFirstOccurrence
import veusz.setting.settingdb as setdb
import veusz.windows.treeeditwindow as treeeditwindow
import veusz.document as document
from veusz.dialogs import dataeditdialog
import veusz.setting as setting
from veusz.compat import cstrerror
from .veuszplotwindow import VeuszPlotWindow, _

logging = get_module_logging(__name__)
MAX = 10 ** 5
MIN = -10 ** 5


class VeuszPlot(QtWidgets.QWidget):

    """Simple Veusz graph for live plotting"""
    vzactions = {}
    documentOpened = QtCore.pyqtSignal()
    dialogShown = QtCore.pyqtSignal(QtWidgets.QWidget)
    cmd = False
    ci = False

    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.menus = {'edit.select': QtWidgets.QMenu()}  # fake
        self._menuBar = QtWidgets.QMenuBar()
        self._menuBar.hide()
        self.lay = QtWidgets.QVBoxLayout()
        self.setLayout(self.lay)
        self.plot = QtWidgets.QWidget()
        self.treeedit = QtWidgets.QWidget()
        self.lay.addWidget(self.plot)
        self.setMinimumSize(180, 180)
        
    def clear(self):
        try:
            self.disconnect()
            self.plot.disconnect()
        except:
            logging.debug('Could not disconnect all signals')
        self.doc = False
        self.lay.removeWidget(self.plot)
        
        self.plot.deleteLater()
        self.plot = False
        self.treeedit.deleteLater()
        self.treeedit = False
        self.cmd = False
        self.ci = False
        
    def close(self):
        self.clear()
        r = super().close()
        self.deleteLater()
        return r

    def menuBar(self):  # fake
        return self._menuBar

    def addToolBar(self, *args):
        """This function is required during self.plot instantiation."""
        args[1].hide()
        pass

    def showDialog(self, dialog):
        """Show dialog given."""
        dialog.show()
        self.dialogShown.emit(dialog)

    def addToolBarBreak(self, *a, **k):
        pass

    def enable_shortcuts(self):
        for name, action in self.plot.vzactions.items():
            try:
                self.addAction(action)
            except:
                logging.debug(format_exc(), name, action)

    def set_doc(self, doc=False):
        self.clear()
        # Adding Veusz objects
        if not doc:
            logging.debug('Creating new document', doc)
            doc = filedata.MisuraDocument()
        self.document = doc
        self._document = self.document
        self.cmd = document.CommandInterface(self.document)
        self.ci = document.CommandInterpreter(self.document)
        self.plot = VeuszPlotWindow(self.document, self)
        self.lay.addWidget(self.plot)

        self.enable_shortcuts()

        # Faking mainwindow
        self.treeedit = treeeditwindow.TreeEditDock(doc, self)
        self.treeedit.hide()
        self.vinit = 0

        plugin.makeDefaultDoc(self.cmd)
        self.loadDefaultStylesheet()

        # Override the zoompage action in order to fit the plot into widget
        # dimension.
        zp = self.plot.vzactions['view.zoompage']
        zp.triggered.disconnect(self.plot.slotViewZoomPage)
        zp.triggered.connect(self.fitSize)
        self.delayed = QtCore.QTimer(self)
        self.zoompageAct = zp
        self.plot.sigWidgetClicked.connect(self.treeedit.selectWidget)
        self.treeedit.widgetsSelected.connect(self.plot.selectedWidgets)
        self.treeedit.sigPageChanged.connect(self.plot.setPageNumber)
        self.document.model.sigPageChanged.connect(self.sync_page)
        
    def loadDefaultStylesheet(self):
        """Loads the default stylesheet for the new document."""
        filename = setdb['stylesheet_default']
        if filename:
            try:
                self.document.applyOperation(
                    document.OperationLoadStyleSheet(filename))
            except EnvironmentError as e:
                qt4.QMessageBox.warning(
                    self, _("Error - Veusz"),
                    _("Unable to load default stylesheet '%s'\n\n%s") % 
                    (filename, cstrerror(e)))
            else:
                # reset any modified flag
                self.document.setModified(False)
                self.document.changeset = 0

    def sync_page(self, page=-1):
        self.plot.update_page()
        self.fitSize()

    def pauseUpdate(self):
        self.updatePolicy = setting.settingdb['plot_updatepolicy']
        setting.settingdb['plot_updatepolicy'] = 0

    def restoreUpdate(self):
        setting.settingdb['plot_updatepolicy'] = self.updatePolicy

    def delayedUpdate(self):
        self.plot.actionForceUpdate()
        if not self.vinit:
            self.vinit = True
            self.fitSize()

    def showEvent(self, e):
        logging.debug('show event')
        self.fitSize()
# 		self.plot.delayed.singleShot(100, self.delayedUpdate)

    def resizeEvent(self, e):
        if not self.cmd:
            # No plot/document defined
            return
        self.fitSize(zoom=True)
        self.plot.actionForceUpdate()
        
    def set_if_differs(self, setpath, value):
        if self.cmd.Get(setpath) != value:
            self.cmd.Set(setpath, value)
            return True
        return False
    
    def fitSize(self, zoom=False):
        """Fit the plot into the widget size.
        `zoom` asks for zooming factor preservation."""
        if not isinstance(self.plot, VeuszPlotWindow):
            logging.debug('Cannot fitSize on widget')
            return
        changeset = self.document.changeset
        if not zoom and self.plot.zoomfactor != 1.:
            self.plot.slotViewZoom11()
        w = self.plot.width()
        h = self.plot.height()
#         if self.plot.viewtoolbar.isVisible():
#             h -= (self.plot.viewtoolbar.height() + 30)
        w = 1. * w / self.plot.dpi[0] - .2
        h = 1. * h / self.plot.dpi[1] - .2
        self.cmd.To('/')
        
        # If the plot dimension is not sufficient, hide the axes.
        page = self.document.basewidget.getPage(self.plot.getPageNumber())
        if not len(self.document.basewidget.children):
            return
        if page is None:
            page = self.document.basewidget.children[0]
        g = page.path
        if g.endswith('_report'):
            return
        
        sw = '{:.1f}in'.format(w)
        sh = '{:.1f}in'.format(h)
        r1 = self.set_if_differs(page.path + '/width', sw)
        r2 = self.set_if_differs(page.path + '/height', sh)
        if not r1 and not r2:
            logging.debug('fitSize: nothing to do')
            self.document.changeset_ignore += self.document.changeset - changeset
            return
        logging.debug('fitSize', w, h)
        wg = searchFirstOccurrence(page, 'grid', 1)
        if wg is None:
            wg = searchFirstOccurrence(page, 'graph', 1)
        g = wg.path 
        
        if h < 2 or w < 4:
            self.set_if_differs(g + '/leftMargin', '0.1cm')
            self.set_if_differs(g + '/bottomMargin', '0.1cm')
            self.set_if_differs(g + '/rightMargin', '0.1cm')
            try:
                self.set_if_differs(g + '/x/hide', True)
                self.set_if_differs(g + '/y/hide', True)
            except:
                pass
        else:
            print('settings for', g)
            self.set_if_differs(g + '/leftMargin', '1.5cm')
            self.set_if_differs(g + '/bottomMargin', '1.1cm')
            self.set_if_differs(g + '/rightMargin', '1.4cm')
            try:
                self.set_if_differs(g + '/x/hide', False)
                self.set_if_differs(g + '/y/hide', False)
            except:
                pass
        logging.debug('endfitsize')
        self.document.changeset_ignore += self.document.changeset - changeset

    def slotDataEdit(self, editdataset=None):
        """Edit existing datasets.
        If editdataset is set to a dataset name, edit this dataset
        """
        dialog = dataeditdialog.DataEditDialog(self, self.document)
        dialog.show()
        if editdataset is not None:
            dialog.selectDataset(editdataset)
        return dialog

    def get_range_of_axis(self, full_axis_name):
        r = self.document.resolveWidgetPath(None, full_axis_name)
        r.computePlottedRange(force=True)
        return r.getPlottedRange()


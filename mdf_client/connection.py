#!/usr/bin/python
# -*- coding: utf-8 -*-
# CHIARIRE QUESTI IMPORT!!!
from mdf_client.qt import QtWidgets, QtGui, QtCore
from mdf_client import _
from . import network
from .clientconf import confdb

from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from .network import wake_on_lan
from .livelog import LiveLog


def auto_address(addr):
    if not addr.startswith('https://'):
        addr = 'https://' + addr
        if not addr.endswith('/RPC'):
            if not addr.endswith(':3880'):
                addr += ':3880'
            addr += '/RPC'
    return addr


def addrConnection(addr, user=False, password=False, mac=False, globalconn=False, signal=True, ask=True):
    addr = auto_address(addr)
    if False in [user, password]:
        user, password = confdb.get_from_key('recent_server', addr)[1:3]
    lw = LoginWindow(addr, user, password, globalconn=globalconn, signal=signal)
    try:
        login = lw.tryLogin(user, password, mac)
    except:
        login = False
    if not login and ask:
        lw.exec_()
    return lw.obj


class Inc(object):
    n = 0

    def i(self):
        self.n += 1
        return self.n


fail = _('Login Failed.')


class BaseLoginWindow(QtWidgets.QDialog):
    obj = False
    connection_failed = QtCore.pyqtSignal()
    login_failed = QtCore.pyqtSignal()
    login_succeeded = QtCore.pyqtSignal()

    def __init__(self, user='username', password='password', parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        self.setWindowTitle('Login Required')
        self.lay = QtWidgets.QGridLayout(self)
        self.setLayout(self.lay)
        
        self.userLbl = QtWidgets.QLabel(_('User Name') + ':')
        self.pwdLbl = QtWidgets.QLabel(_('Password') + ':')
        self.user = QtWidgets.QLineEdit(user)
        self.password = QtWidgets.QLineEdit(password)
        self.password.setEchoMode(QtWidgets.QLineEdit.Password)
        self.ckSave = QtWidgets.QCheckBox(_('Save login'), self)
        self.ckSave.setCheckState(2 * confdb['saveLogin'])
        self.ok = QtWidgets.QPushButton('Ok')
        self.ko = QtWidgets.QPushButton(_('Cancel'))
        self.ok.clicked.connect(self.tryLogin)
        self.ko.clicked.connect(self.reject)
        self.lay.addWidget(self.userLbl, 2, 0)
        self.lay.addWidget(self.user, 2, 1)
        self.lay.addWidget(self.pwdLbl, 3, 0)
        self.lay.addWidget(self.password, 3, 1)
        self.lay.addWidget(self.ok, 4, 0)
        self.lay.addWidget(self.ko, 4, 1)
        self.lay.addWidget(self.ckSave, 5, 1)
        self.user.setFocus()
    
    def set_default_if_empty(self, user='user', password='user'):
        if not self.user.text():
            self.user.setText(user)
        if not self.password.text():
            self.password.setText(password)
            
    def credentials(self):
        return str(self.user.text()), str(self.password.text())
     
    def tryLogin(self):
        self.accept()
        

class LoginWindow(BaseLoginWindow):

    def __init__(self, addr, user='username', password='password', mac='', globalconn=False, signal=True, parent=None, context='Local'):
        self.globalconn = globalconn
        self.mac = mac
        self.addr = addr
        super().__init__(user=user, password=password, parent=None)
        self.signal = signal
        if not signal:
            self.ckSave.hide()
        self.obj = False
        self.lay.addWidget(QtWidgets.QLabel('Destination:'), 1, 0)
        self.lay.addWidget(QtWidgets.QLabel(self.addr), 1, 1)
        
    def tryLogin(self, user='', password='', mac=False, ignore=False):
        if not user:
            user = str(self.user.text())
        if not password:
            password = str(self.password.text())
        if mac:
            self.mac = mac
        logging.debug('WAKE ON LAN?', repr(self.mac))
        if self.mac:
            tuple(map(wake_on_lan, self.mac.split('\n')))
        save = bool(self.ckSave.checkState())
        self.obj = False
        if self.globalconn:
            st, obj = network.getConnection(self.addr, user, password, self.mac, save)
        else:
            st, obj = network.simpleConnection(self.addr, user, password, self.mac, save, signal=self.signal)
        self.ignore = ignore
        
        if st is True:
            self.obj = obj
            self.login_succeeded.emit()
            self.done(0)
            return self.obj
        elif st == network.CONN_ERROR_LOGIN:
            self.msg = _('Login error')
            if obj:
                self.msg = obj._error
            logging.error(self.msg)
            self.reject()
            self.login_failed.emit()
        else:
            self.msg = _('Connection error')
            logging.error(self.msg)
            self.reject()
            self.connection_failed.emit()
        return False


class ServerWidget(QtWidgets.QWidget):

    def __init__(self, info, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.info = info
        self.lay = QtWidgets.QVBoxLayout(self)
        self.setLayout(self.lay)
# 		self.setMaximumHeight(200)
# 		self.setMinimumHeight(200)
# 		self.setMinimumWidth(600)
        self.grid = QtWidgets.QWidget(self)
        self.glay = QtWidgets.QGridLayout(self.grid)
        self.grid.setLayout(self.glay)
        self.label = info.name + ' @' + info.addr
        lbl = lambda txt, x, y: self.glay.addWidget(QtWidgets.QLabel(txt), x, y)
        r = Inc()
        cap = ', '.join(info.cap)
        lbl('Name:', r.i(), 0)
        lbl(info.name, r.n, 1)
        lbl('Serial:', r.n, 2)
        lbl(info.serial, r.n, 3)
        lbl('Address:', r.i(), 0)
        lbl(info.addr, r.n, 1)
        lbl('IP:', r.n, 2)
        lbl(str(info.ip), r.n, 3)
        lbl('Host:', r.i(), 0)
        lbl(info.host, r.n, 1)
        lbl('Capabilities:', r.n, 2)
        lbl(cap, r.n, 3)
        lbl('Port:', r.i(), 0)
        lbl(str(info.port), r.n, 1)
        lbl('User:', r.n, 2)
        lbl(info.user, r.n, 3)
        self.lay.addWidget(self.grid)
        self.button = QtWidgets.QPushButton(_('Connect'), self)
        self.lay.addWidget(self.button)
        
        self.button.pressed.connect(self.doConnection)
        self.menu = QtWidgets.QMenu(self)
        self.menu.addAction('Connect', self.doConnection)
        self.customContextMenuRequested.connect(self.showMenu)

    def doConnection(self):
        obj = addrConnection(self.info.addr)
        if not obj:
            return
        network.setRemote(obj)

    def showMenu(self, pt):
        self.menu.popup(self.mapToGlobal(pt))


class ServerSelector(QtWidgets.QToolBox):

    """Presents and keeps updated a list of available servers"""

    def __init__(self, parent=None, context='Local'):
        QtWidgets.QToolBox.__init__(self, parent)
        network.manager.sig_found.connect(self.redraw, QtCore.Qt.QueuedConnection)
        network.manager.sig_lost.connect(self.redraw, QtCore.Qt.QueuedConnection)
        self.label = _('Server Selector')
        self.redraw()

    def redraw(self):
        logging.debug('redraw')
        while True:
            wg = self.currentWidget()
            idx = self.currentIndex()
            if wg in [0, None]:
                break
            wg.close()
            del wg
            self.removeItem(idx)

        for key, srv in network.manager.servers.items():
            wg = ServerWidget(srv, self)
            self.addItem(wg, wg.label)


class ConnectionStatus(QtWidgets.QWidget):

    def __init__(self, parent=None, context='Local'):
        QtWidgets.QWidget.__init__(self, parent)
        self.label = _('Connection Status')
        self.setWindowTitle(self.label)
        self.lay = QtWidgets.QVBoxLayout()
        self.setLayout(self.lay)
        network.manager.sig_connected.connect(self.displayServerInfo)
        self.addr = QtWidgets.QLabel()
        self.lay.addWidget(self.addr)
        self.displayServerInfo()

        self.log = LiveLog(self)
        self.lay.addWidget(self.log)

        self.echo = QtWidgets.QLineEdit(self)
        self.echo.setPlaceholderText(_("Test Echo Logging"))
        self.echo.setMaxLength(50)
        self.lay.addWidget(self.echo)
        self.echo.returnPressed.connect(self.sendEcho)

    def sendEcho(self):
        r = network.manager.remote.send_log(str(self.echo.text()))
        self.log.update()
        self.echo.setText('')

    def displayServerInfo(self):
        if network.manager.addr == '':
            self.addr.setText('No server connected')
            return
        else:
            self.addr.setText('Connected Server: ' + str(network.manager.addr))


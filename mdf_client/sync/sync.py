#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Database synchronization service and widget"""
import os
import datetime
from time import mktime, time
from traceback import format_exc

from mdf_canon.logger import get_module_logging
from functools import partial
logging = get_module_logging(__name__)

from .. import _
from ..clientconf import confdb

from mdf_canon import indexer
from mdf_canon.csutil import unicode_func
from ..network import TransferThread, remote_dbdir
from .sync_table_model import SyncTableModel
from .. import database

from mdf_client.qt import QtWidgets, QtCore

record_length = len(indexer.indexer.testColumn)


class StorageSync(object):

    """Storage synchronization utilities"""
    chunk = 25
    """Max UID collect request"""
    server = False
    serial = False
    start = 0
    tot = 0
    downloading_record = False
    
    def __init__(self, transfer=False):
        self.transfer = transfer
        if self.transfer:
            self.transfer.dlFinished.connect(self.confirm_downloaded_record)
        """Object implementing a download_url(url,outfile) method"""

    def set_server(self, server):
        """Sets remote server towards which to run the synchronization service"""
        print('StorageSync.set_server', id(self), server)
        self.server = server
        self.remote_dbdir = remote_dbdir(server)
        """Remote db directory"""
        self.serial = self.server['eq_sn']
        """Server serial number"""
        self.tot = self.server.storage.get_len()
        if self.tot is None:
            self.tot = 0
        self.start = self.tot - self.chunk
        if self.start < 0:
            self.start = 0
        return True

    def set_dbpath(self, dbpath):
        """Set the local database path for storage synchronization"""
        if not dbpath:
            dbpath = confdb['database']
        self.dbpath = dbpath
        if not os.path.exists(self.dbpath):
            logging.debug('Database path does not exist!', self.dbpath)
            return False
        logging.debug('StorageSync set_dbpath', self.dbpath)
        self.maindb = indexer.Indexer(self.dbpath)
        self.db = indexer.Indexer(self.dbpath + '.sync')
        self.limit_table('sync_done')
        self.limit_table('sync_exclude')
        self.limit_table('sync_error')
        return True

    def prepare(self, dbpath=False, server=False):
        """Open database and prepare operations"""
        print('StorageSync.prepare', id(self), dbpath, server)
        if not self.set_dbpath(dbpath):
            return False
        if server:
            self.set_server(server)
        return True

    def has_uid(self, uid, tname):
        """Check if `uid` is in table `tname`"""
        q = f"SELECT 1 from {tname} where uid='{uid}'"
        r = self.db.execute_fetchall(q)
        return len(r)

    def rem_uid(self, uid, tname):
        """Remove `uid` from table `tname`"""
        self.db.execute("DELETE from {} where uid='{}'".format(tname, uid))
        logging.debug('removed uid', tname, uid)

    def add_record(self, record, tname):
        """Adds `record` list to table `tname`"""
        logging.debug('add_record', tname, record)
        if not record:
            return False
        v = ('?,' * len(record))[:-1]
        self.db.execute("INSERT INTO {} VALUES ({})".format(tname, v), record)
        return True

    def download_record(self, record):
        uid = record[2]
        if self.has_uid(uid, 'sync_approve'):
            self.rem_uid(uid, 'sync_approve')

        if self.has_uid(uid, 'sync_exclude'):
            logging.debug('Record was excluded. Enabling.', record)
            self.rem_uid(uid, 'sync_exclude')

        if self.has_uid(uid, 'sync_error'):
            logging.debug('Retrying: ', record)
            self.rem_uid(uid, 'sync_error')

        if len(record) > record_length:
            record = record[:record_length]

        uid = record[indexer.indexer.col_uid]
        self.transfer.dbpath = self.dbpath
        self.transfer.uid = uid
        self.transfer.server = self.server
        self.transfer.outfile = False
        self.downloading_record = record
        self.transfer.start()
        return True
    
    def confirm_downloaded_record(self, *a):
        logging.info('confirm_downloaded_record', self.downloading_record, self.transfer.outfile, a)
        if not self.downloading_record:
            return False
        if not self.transfer.outfile:
            return False
        # FIXME: cannot set mod time to test time
        zerotime = self.downloading_record[indexer.indexer.col_zerotime]
        zerotime = datetime.datetime.strptime(zerotime, '%Y-%m-%d %H:%M:%S')
        zerotime = mktime(zerotime.timetuple())
        os.utime(self.transfer.outfile, (zerotime, zerotime))
        logging.info('Finished download', self.downloading_record, self.transfer.outfile)
        self.add_record(self.downloading_record, 'sync_done')
        self.downloading_record = False
        return True
        
    def limit_table(self, tab, lim=1000):
        c = self.db.execute_fetchall('SELECT COUNT(*) FROM {};'.format(tab))[0][0]
        if c < lim + 100:
            return False
        c -= lim
        logging.debug('Deleting excessive rows', tab, c)
        r = self.db.execute('DELETE FROM {} ORDER BY zerotime LIMIT {}'.format(tab, c))
        logging.debug('Deleted', tab, c, r)
        return c

    def exclude_record(self, record):
        """Exclude `record` for download."""
        # Check if previously approved (remove!) or already excluded (exit)
        uid = record[2]
        if self.has_uid(uid, 'sync_queue'):
            logging.debug('Record was queued. Enabling.', record)
            self.rem_uid(uid, 'sync_queue')

        if self.has_uid(uid, 'sync_approve'):
            self.rem_uid(uid, 'sync_approve')

        if self.has_uid(uid, 'sync_error'):
            self.rem_uid(uid, 'sync_error')
            
        if self.has_uid(uid, 'sync_done'):
            self.rem_uid(uid, 'sync_done')

        if self.has_uid(uid, 'sync_exclude'):
            logging.debug('Record already excluded', record)
            return False
        if len(record) > record_length:
            record = record[:record_length]
        self.add_record(record, 'sync_exclude')
        return True

    def delete_record(self, record):
        """Permanently remove data file corresponding to `record` from remote server"""
        uid = record[indexer.indexer.col_uid]
        r = self.server.storage.remove_uid(uid)
        logging.debug('Remove file result', uid, r)
        if self.has_uid(uid, 'sync_queue'):
            logging.debug('Record was queued. Enabling.', record)
            self.rem_uid(uid, 'sync_queue')

        if self.has_uid(uid, 'sync_approve'):
            self.rem_uid(uid, 'sync_approve')

        if self.has_uid(uid, 'sync_error'):
            self.rem_uid(uid, 'sync_error')

        if self.has_uid(uid, 'sync_exclude'):
            self.rem_uid(uid, 'sync_exclude')
            
        if self.has_uid(uid, 'sync_done'):
            self.rem_uid(uid, 'sync_done')
            
    def clear_record(self, tab, record):
        uid = record[indexer.indexer.col_uid]
        if self.has_uid(uid, tab):
            self.rem_uid(uid, tab)

    def collect(self, server=False):
        # return  # XXX
        if not server:
            server = self.server
        if not server:
            logging.debug('StorageSync.collect: No remote storage defined')
            return 0
        if server['isRunning']:
            logging.debug('Cannot check for new downloads while a test is running')
            return 0
        all_tests = server.storage.list_tests()
        if not all_tests:
            logging.error('StorageSync.collect: Impossible to retrieve remote storage list.')
            return 0
        serial = server['eq_sn']
        not_downloaded_tests = [
            test for test in all_tests if not self.already_downloaded(test[2])
        ]
        
        # Purge queue and approve tables
        r = self.db.execute('DELETE FROM sync_approve')
        logging.debug('Purged sync_approve', r)
        
        not_processed_tests = [
            test for test in not_downloaded_tests
            if test[1] == serial
            and not self.has_uid(test[2], 'sync_queue')
            and not self.has_uid(test[2], 'sync_exclude')
            and not self.has_uid(test[2], 'sync_error')
            and not self.has_uid(test[2], 'sync_done')
            and not self.has_uid(test[2], 'sync_approve')
        ]
        tuple(map(lambda record: self.add_record(record, 'sync_approve'),
              not_processed_tests))
        
        return len(not_processed_tests)

    def already_downloaded(self, uid):
        r = self.maindb.searchUID(uid, full=True)
        return bool(r)

    def __len__(self):
        """Returns the length of the approval queue"""
        if not self.server:
            return 0
        return self.db.tab_len('sync_approve')


class SyncTable(QtWidgets.QTableView):

    """Table showing queued sync files, allowing the user to interact with them"""
    length = 0
    downloadRecord = QtCore.pyqtSignal(object)
    startDownload = QtCore.pyqtSignal()
    excludeRecord = QtCore.pyqtSignal(object)
    deleteRecord = QtCore.pyqtSignal(object)
    clearRecord = QtCore.pyqtSignal(str, object)

    def __init__(self, dbpath, table_name, parent=None):
        super(SyncTable, self).__init__(parent)
        self.model_reference = SyncTableModel(
            indexer.Indexer(dbpath + '.sync'), table_name)
        self.setModel(self.model_reference)
        for i in (0, 6, 9, 11):
            self.hideColumn(i)
        self.selection = QtCore.QItemSelectionModel(self.model())
        self.setSelectionModel(self.selection)
        self.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.menu = QtWidgets.QMenu(self)
        if table_name.endswith('_approve'):
            self.menu.addAction(_('Download'), self.download)
            self.menu.addAction(_('Ignore'), self.exclude)
            self.menu.addAction(_('Delete record'), self.delete)
        elif table_name.endswith('_queue'):
            self.menu.addAction(_('Ignore'), self.exclude)
        elif table_name.endswith('_exclude'):
            self.menu.addAction(_('Download'), self.download)
        elif table_name.endswith('_error'):
            self.menu.addAction(_('Retry'), self.download)
            self.menu.addAction(_('Ignore'), self.exclude)
            self.menu.addAction(_('Clear record'), self.clear)
        elif table_name.endswith('_done'):
            self.menu.addAction(_('Download again'), self.download)
            self.menu.addAction(_('Clear record'), self.clear)
        self.customContextMenuRequested.connect(self.showMenu)

    def showMenu(self, pt):
        self.menu.popup(self.mapToGlobal(pt))

    def showEvent(self, ev):
        """Automatically switch to appropriate queue when showed"""
        self.model().select()
        return super(SyncTable, self).showEvent(ev)

    def __len__(self):
        """Returns the number of rows of the current table for the connected server serial"""
        n = self.model().rowCount()
        if n != self.length:
            self.model().select()
            self.length = n
        return n

    def download(self):
        for record in database.iter_selected(self):
            self.downloadRecord.emit(record)
        self.model().select()
        self.startDownload.emit()
        
    def download_all(self):
        self.model().select()
        for record in self.model()._data:
            self.downloadRecord.emit(record)
        self.startDownload.emit()
        
    def exclude(self):
        """Move selection to sync_exclude table"""
        for record in database.iter_selected(self):
            self.excludeRecord.emit(record)
        self.model().select()
        
    def exclude_all(self):
        self.model().select()
        for record in self.model()._data:
            self.excludeRecord.emit(record)

    def delete(self):
        """Delete selected records from remote server"""
        for record in database.get_delete_selection(self):
            self.deleteRecord.emit(record)
        self.model().select()

    def clear(self):
        """Delete selected records from remote server"""
        for record in database.iter_selected(self):
            self.clearRecord.emit(self.model().table_name, record)
        self.model().select()


class SyncWidget(QtWidgets.QTabWidget):

    """Allows the user to control sync behavior."""
    ch = QtCore.pyqtSignal()
    dbpath = False
    ask_download_all = 0

    def __init__(self, parent=None):
        super(SyncWidget, self).__init__(parent)
        self.transfer = TransferThread(parent=self)
        # Set local tasks manager
        if parent:
            self.transfer.set_tasks(parent.tasks)
        
        self.transfer.dlFinished.connect(self.download_next)
        self.transfer.dlAborted.connect(self.download_next)
        self.storage_sync = StorageSync(self.transfer)

    def connect_database(self):
        dbpath = confdb['database']
        self.dbpath = dbpath
        self.tab_approve = self.add_sync_table('sync_approve',
                                               _('Waiting approval'))
        self.tab_approve.menu.addAction(_('Check for new downloads'), self.check_for_new_downloads)
        self.tab_approve.menu.addAction(_('Refresh remote database'), self.refresh_remote)
        self.act_download_all = self.tab_approve.menu.addAction(_('Download all pending'), self.tab_approve.download_all)
        self.act_download_all.setEnabled(False)
        self.act_exclude_all = self.tab_approve.menu.addAction(_('Exclude all pending'), self.tab_approve.exclude_all)
        self.act_exclude_all.setEnabled(False)
        self.tab_exclude = self.add_sync_table('sync_exclude', _('Ignored'))
        self.tab_done = self.add_sync_table('sync_done', _('Done'))
        self.tab_error = self.add_sync_table('sync_error', _('Errors'))

    def check_for_new_downloads(self):
        # return  # XXX
        logging.debug('Check for new downloads')
        self.storage_sync.collect()
        n = len(self.tab_approve)
        self.tab_approve.model().select()
        t0 = time()
        self.act_download_all.setEnabled(n > 0)
        self.act_exclude_all.setEnabled(n > 0)
        print('check_for_new_downloads', n, 'was', self.ask_download_all)
        if n > self.ask_download_all:
            self.ask_download_all = n
            msg = _('New test files were found on the instrument ({}). \nWould you like to download them to the local database?').format(n)
            btn = QtWidgets.QMessageBox.question(None, _('Download new tests'), msg,
                                                 QtWidgets.QMessageBox.YesToAll | QtWidgets.QMessageBox.Discard | QtWidgets.QMessageBox.Abort)
            # Check again if more than 1s passes between question and reply
            if time() - t0 > 1:
                n = len(self.tab_approve)
                if n < self.ask_download_all:
                    logging.debug('No download needed', n, self.ask_download_all)
                    return False
            if btn == QtWidgets.QMessageBox.YesToAll:
                self.tab_approve.download_all()
                return True
            elif btn == QtWidgets.QMessageBox.Discard:
                logging.debug('New files were ignored')
                self.tab_approve.exclude_all()
                return False
            elif btn == QtWidgets.QMessageBox.Abort:
                logging.debug('No action taken on files')
                return False
        print('NO NEW FILES')
        return False
        
    def refresh_remote(self, auto=False):
        # return  # XXX
        print('refresh_remote', self.storage_sync.server)
        if not self.storage_sync.server:
            logging.debug('Not connected')
        if auto and not confdb['databaseSync']:
            logging.debug('Database sync disabled by config option')
            return False
        if self.storage_sync.server['isRunning']:
            logging.debug('Cannot refresh remote database while it is running a test')
            return False
        self.storage_sync.server.storage.refresh()
        self.check_for_new_downloads()

    def add_sync_table(self, table_name, title):
        self.download_queue = []
        obj = SyncTable(self.dbpath, table_name, parent=self)
        self.addTab(obj, title)
        obj.downloadRecord.connect(self.queue_record)
        obj.startDownload.connect(self.download_next)
        obj.excludeRecord.connect(self.storage_sync.exclude_record)
        obj.deleteRecord.connect(self.storage_sync.delete_record)
        obj.clearRecord.connect(self.storage_sync.clear_record)
        return obj
    
    def queue_record(self, *a):
        self.download_queue.append(a)
        
    def download_next(self, *a):
        """Manage the download queue"""
        if not self.download_queue:
            self.check_for_new_downloads()
            return False
        logging.debug('download_next on', a)
        if not self.transfer.isRunning:
            v = self.download_queue.pop(0)
            logging.debug('download_next', v)
            self.storage_sync.download_record(*v)
            return True
        else:
            logging.debug('download_next waiting')
            return False

    def set_server(self, server):
        # return  # XXXX
        if not server:
            return False
        print('SyncWidget.set_server', id(self), server)
        self.connect_database()
        if not self.dbpath:
            return False
        self.storage_sync.prepare(self.dbpath, server)
        serial = "serial='{}'".format(server['eq_sn'])
        self.tab_approve.model().setFilter(serial)
        self.tab_error.model().setFilter(serial)
        self.tab_exclude.model().setFilter(serial)
        self.tab_done.model().setFilter(serial)
        QtCore.QTimer.singleShot(2500, partial(self.refresh_remote, auto=True))
        return True
        
    @property
    def server(self):
        return self.storage_sync.server
    
    def __len__(self):
        """Returns the length of the approval queue"""
        if not self.dbpath:
            return 0
        n = len(self.tab_approve)
        return n

    def showEvent(self, ev):
        """Automatically switch to appropriate queue when showed"""
        r = super(SyncWidget, self).showEvent(ev)
        if not self.dbpath:
            return r
        if len(self.tab_approve):
            if self.currentIndex() != 0:
                self.setCurrentIndex(0)
        elif len(self.tab_error):
            if self.currentIndex() != 2:
                self.setCurrentIndex(2)
        return r

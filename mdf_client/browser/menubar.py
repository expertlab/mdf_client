#!/usr/bin/python
# -*- coding: utf-8 -*-
from mdf_client.qt import QtWidgets, QtGui, QtCore
from .. import _
from ..clientconf import confdb
from ..confwidget import RecentMenu
from mdf_client.helpmenu import HelpMenu

from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)

class BrowserMenuBar(QtWidgets.QMenuBar):
    """Browser menus"""
    sig_new_database = QtCore.pyqtSignal(str)
    sig_re_standard = QtCore.pyqtSignal()
    
    def __init__(self, server=False, parent=None):
        QtWidgets.QMenuBar.__init__(self, parent)

        self.lstActions = []
        self.func = []
        self.file = self.addMenu(_('File'))
        
        self.recentExe = RecentMenu(confdb, 'executed', self)
        act = self.addMenu(self.recentExe)

        self.recentFile = RecentMenu(confdb, 'file', self)
        self.actFile = self.file.addAction(_('Open File'), self.recentFile.new)
        act = self.addMenu(self.recentFile)

        self.recentDatabase = RecentMenu(confdb, 'database', self)
        self.actDb = self.file.addAction(
            _('Open Database'), self.recentDatabase.new)
        self.addMenu(self.recentDatabase)

        self.actNewDb = self.file.addAction(_('New Database'), self.new_database)

        if confdb['m3_enable']:
            self.recentM3db = RecentMenu(confdb, 'm3database', self)
#           self.recentM3db.new.connect(self.open_database)
            self.file.addMenu(self.recentM3db)

        self.currents = self.addMenu(_('View Tests'))
        self.databases = self.addMenu(_('View Databases'))

        self.help_menu = HelpMenu()
        self.help_menu.add_help_menu(self)




    def new_database(self, path=False):
        if not path:
            path = QtWidgets.QFileDialog.getSaveFileName(
                self, "Choose a name for the new database", "C:\\")
        if not path:
            return
        self.sig_new_database.emit(path[0])

    def eval_standard(self):
        self.sig_re_standard.emit()

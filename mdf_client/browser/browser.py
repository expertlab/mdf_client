
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from mdf_client.qt import QtWidgets, QtGui, QtCore
import os

from .. import _
from .. import confwidget
from .. import filedata

from ..clientconf import confdb
from .. import iutils

from ..database import getDatabaseWidget, getRemoteDatabaseWidget
from .. import parameters

from . import menubar
from . import testwindow

from traceback import print_exc, format_exc

from mdf_canon.csutil import unicode_func

try:
    from .. import misura3
except:
    print_exc()
    misura3 = False




def decode_drop_event(drop_event):
    urls = drop_event.mimeData().urls()
    logging.debug('dropEvent', urls)
    for url in urls:
        # on windows, remove also the first "/"
        if os.name.lower() == 'nt':
            url = url.toString()
            if url.startswith('file:///'):
                url = url[8:]
            elif url.startswith('file:'):
                url = url[5:]
        else:
            url = url.toString().replace('file://', '')
        return url
    return False

class DatabasesArea(QtWidgets.QMdiArea):
    convert = QtCore.pyqtSignal(str)

    def __init__(self, parent=None):
        super(DatabasesArea, self).__init__(parent)
        self.setAcceptDrops(True)
        self.pix_size = QtCore.QSize(500, 100)
        self.pix = iutils.theme_icon("browser_drag_drop").pixmap(self.pix_size)
        self.gray = self.background()
        
    def resizeEvent(self, event):
        """Add the drag-n-drop artwork"""
        size = event.size()
        new_back = QtGui.QImage(size, QtGui.QImage.Format_ARGB32_Premultiplied)
        paint = QtGui.QPainter(new_back)
        paint.fillRect(0, 0, size.width(), size.height(), self.gray)
        paint.drawPixmap(size.width()-self.pix_size.width(), 
                         size.height()-self.pix_size.height(), 
                         self.pix, 0, 0, 0 , 0)
        paint.end()
        self.setBackground(QtGui.QBrush(new_back))
        return QtWidgets.QMdiArea.resizeEvent(self, event)

    def dragEnterEvent(self, event):
        logging.debug('dragEnterEvent', event.mimeData())
        if event.mimeData().hasUrls():
            event.acceptProposedAction()

    def dropEvent(self, drop_event):
        url = decode_drop_event(drop_event)
        if url:
            self.convert.emit(url)

class TestsTabBar(QtWidgets.QTabBar):
    dropped = QtCore.pyqtSignal(str)
    
    def __init__(self, *a, **k):
        super(TestsTabBar, self).__init__(*a, **k)
        self.setAcceptDrops(True)
    
    def dragEnterEvent(self, event):
        logging.debug('TestsTabBar.dragEnterEvent', event.mimeData())
        if event.mimeData().hasUrls():
            event.acceptProposedAction()    
    
    def dropEvent(self, drop_event):
        url = decode_drop_event(drop_event)
        if url:
            self.dropped.emit(url)        

class MainWindow(QtWidgets.QMainWindow):

    """Open single files, local databases, remote databases."""
    sig_do_open = QtCore.pyqtSignal(str)
    
    def __init__(self, parent=None):
        super(QtWidgets.QMainWindow, self).__init__(parent)
        self.opened_windows = []
        self.tab = QtWidgets.QTabWidget()
        self.tab_bar = TestsTabBar()
        self.tab_bar.dropped.connect(self.convert_file)
        self.tab.setTabBar(self.tab_bar)
        self.setAcceptDrops(True)
        self.area = DatabasesArea(self)
        self.tab.addTab(self.area, _('Databases'))
        self.tab.setTabsClosable(True)
        self.tab.setDocumentMode(True)
        self.memory_timer = QtCore.QTimer(self)
        self.memory_timer.timeout.connect(iutils.memory_check)
        self.memory_timer.start(2000)

        
        database_tab_index = 0
        self.remove_close_button_from_tab(database_tab_index)
        self.setCentralWidget(self.tab)
        self.setMinimumSize(800, 600)
        self.setWindowTitle(_('MDF Browser'))
        self.myMenuBar = menubar.BrowserMenuBar(parent=self)
        self.setMenuWidget(self.myMenuBar)
        
        self.tab.tabCloseRequested.connect(self.close_tab)
        self.tab.currentChanged.connect(self.current_tab_changed)

        self.sig_do_open.connect(self.open_file)
        
        self.myMenuBar.recentExe.select.connect(self.open_file)
        self.myMenuBar.recentFile.select.connect(self.open_file)
        self.myMenuBar.recentFile.convert.connect(self.convert_file)

        self.area.convert.connect(self.convert_file)

        self.myMenuBar.recentDatabase.select.connect(self.open_database)
        
        self.myMenuBar.sig_new_database.connect(self.new_database)

        # Recent objects greeter window:
        greeter = confwidget.Greeter(parent=self)
        greeter.file.select.connect(self.open_file)
        greeter.file.add_to.connect(self.open_file)
        greeter.file.convert.connect(self.convert_file)
        greeter.database.select.connect(self.open_database)

        if confdb['m3_enable'] and misura3:
            
            greeter.m3database.select.connect(self.open_m3db)
            self.myMenuBar.recentM3db.select.connect(self.open_m3db)
            if confdb['m3_open'] and len(greeter.m3database):
                item = greeter.m3database.list.item(0)
                greeter.m3database.select_item(item)
                

        win = self.area.addSubWindow(greeter,
                                     QtCore.Qt.CustomizeWindowHint |
                                     QtCore.Qt.WindowTitleHint |
                                     QtCore.Qt.WindowMinMaxButtonsHint)

        self.setWindowIcon(
            QtGui.QIcon(os.path.join(parameters.pathArt, 'browser.svg')))
        
        confwidget.check_default_database()
        self.open_database(confdb['database'])
        

    def closeEvent(self, event):
        logging.debug('closeEvent')
        i = 0
        while self.tab.count()>1:
            logging.debug('Closing tab:', i)
            if not self.close_tab(1):
                event.ignore()
                return True
            i += 1
            
        try:
            confdb._lock.acquire(block=True, timeout=5)
            confdb._lock.release()
        except:
            logging.debug(format_exc())
        ret = QtWidgets.QMainWindow.closeEvent(self, event)
        iutils.app.quit()
        return ret
        
    def convert_file(self, path):
        filedata.convert_file(self,path)
        
    def _open_converted(self):
        test_window = self.open_file(self.converter.outpath)
        self.converter.post_open_file(test_window.navigator)

    def _failed_conversion(self, error):
        QtWidgets.QMessageBox.warning(self, _("Failed conversion"), error)
        
    def find_tab_by_path(self, term):
        for i in range(1, self.tab.count()):
            p = self.tab.widget(i).fixedDoc.proxy.get_path()
            found = os.path.realpath(os.path.abspath(unicode_func(p)))
            print('find_tab_by_path', i,found,term)
            if found == term:
                return i
        return -1
        
    def find_tab_by_uid(self, term):
        term = str(term)
        for i in range(1, self.tab.count()):
            found = str(self.tab.widget(i).fixedDoc.proxy.get_uid())
            print('find_tab_by_uid', i,found,term)
            if found == term:
                return i
        return -1
    
    def close_by_uid(self, uid):
        """Close tab by file uid"""
        i = self.find_tab_by_uid(uid)
        
        if i>0:
            logging.debug('Closing a deleted tab', uid,i)
            self.close_tab(i, check_save=False)
        else:
            logging.debug('Not closing any tab upon deletion of',uid)
            
    
    def open_file(self, path, tab_index=-1, **kw):
        if path[:4] not in ('http','mdf:'):
            path = os.path.realpath(os.path.abspath(unicode_func(path)))
        logging.debug('Browser MainWindow.open_file', path, tab_index)
        if tab_index<0:
            i = self.find_tab_by_path(path)
            if i>0:
                logging.debug('File is already opened: switching to tab', i)
                self.tab.setCurrentIndex(i)
                return False    
        if tab_index>0:
            tw = self.tab.widget(tab_index)
            t = self.tab.tabText(tab_index).replace('+&','+')
            t = t.split('+')
            if len(t)==1:
                t = t[0]+ '+1'
            else:
                print(t)
                t[-1]=str(int(t[-1])+1)
                t = '+'.join(t)
            self.tab.setTabText(tab_index,t)
            tw.doc._proxy=False
            tw.doc.proxy_filename=False
            tw.measureTab.results.navigator.open_file(path)
            self.update_tooltip(tw)
            print("ADDED TO TAB",tab_index,path,tw.doc.proxies)
            return False
        
        try:
            doc = filedata.MisuraDocument(path)
        except Exception as error:
            logging.error(format_exc())
            self.myMenuBar.recentFile.conf.rem_file(path)
            QtWidgets.QMessageBox.warning(self, 'Error', str(error))
            return False
        tw = testwindow.TestWindow(doc)
        tw.fixedDoc.model.sigPageChanged.connect(self.manage_caches)
        instrument = doc.proxy.conf['runningInstrument']
        cw = self.centralWidget()
        icon = QtGui.QIcon(os.path.join(parameters.pathArt, 'small_' + instrument + '.svg'))
        idx = cw.addTab(tw, icon, tw.title)
        self.tab_bar.setTabToolTip(idx, tw.toolTip())
        tw.loaded_version.connect(self.update_tooltip)
        confdb.mem_file(path, tw.remote.measure['name'])
        self.opened_windows.append(tw)
        cw.setCurrentIndex(idx)
        self.update_tooltip(tw)
        return tw
    
    def current_tab_changed(self, idx):
        """Refresh accessed tests queue"""
        if idx==0:
            return
        w = self.tab.widget(idx)
        if w in self.opened_windows:
            self.opened_windows.remove(w)
        self.opened_windows.append(w)
    
    def manage_caches(self, *a):
        logging.debug('manage_caches', self.opened_windows)
        for w in self.opened_windows:
            logging.debug('manage_caches for window:', w.windowTitle())
            w.slot_manage_cache()
    
    def update_tooltip(self, test_window):
        idx = self.tab.indexOf(test_window)
        self.tab_bar.setTabToolTip(idx, test_window.get_tooltip())

    def open_database(self, path, new=False):
        idb = getDatabaseWidget(path, new=new, browser=self)
        if not idb:
            logging.error('Failed opening database widget at', path)
            return False
        idb.table.deleted_uid.connect(self.close_by_uid)
        win = self.area.addSubWindow(idb)
        win.show()
        confdb.mem_database(path)
        idb.sig_selectedFile.connect(self.open_file)
        idb.sig_selectedFileTab.connect(self.open_file)
        return idb

    def new_database(self, path):
        self.open_database(path, new=True)

    def open_server(self, addr):
        idb = getRemoteDatabaseWidget(addr)
        if not idb:
            return False
        win = self.area.addSubWindow(idb)
        win.show()
        idb.selectedRemoteUid.connect(self.open_remote_uid)
        return True

    def open_remote_uid(self, addr, uid):
        logging.debug('Open remote uid',uid)
        addr = str(addr)
        uid = str(uid)
        path = addr + '|' + uid
        self.open_file(path)

    def open_m3db(self, path):
        m3db = misura3.TestDialog(path=path)
        m3db.img = True
        m3db.keep_img = True
        m3db.force = False
        m3db.imported.connect(self.open_file)
        confdb.mem_m3database(path)
        win = self.area.addSubWindow(m3db)
        win.show()
        
    def list_tabs(self):
        return [self.tab.widget(i) for i in range(self.tab.count())]
            

    def close_tab(self, idx, check_save=True):
        logging.debug('Tab close requested', idx)
        if idx == 0:
            return False
        w = self.tab.widget(idx)
        if not w:
            return False
        if (not check_save) or w.check_save(nosync=False):
            if w in self.opened_windows:
                self.opened_windows.remove(w)
            self.tab.removeTab(idx)
            w.close()
            return True
        # Failed to close the tab
        return False

    def remove_close_button_from_tab(self, tab_index):
        self.tab.tabBar().tabButton(
            tab_index, QtWidgets.QTabBar.RightSide).resize(0, 0)

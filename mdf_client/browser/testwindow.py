#!/usr/bin/python
# -*- coding: utf-8 -*-
from functools import partial
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)

from mdf_client.qt import QtWidgets, QtGui, QtCore

import veusz.utils
from ..clientconf import confdb
from .. import _, iutils
from .. import filedata
from .. import fileui
from .. import acquisition
from ..live import registry
from ..graphics import Breadcrumb, PlotsBoard
from ..navigator import NavigatorToolbar
from mdf_client.iutils import calc_plot_hierarchy, most_involved_node
from mdf_client.fileui.compare_samples import CompareMicroscopeSamples
message_busy = 'The selected window cannot be closed, \
because one or more tasks are in progress.\n\
Please retry later.'


class TestWindow(acquisition.AbstractTestWindow):

    """View of a single test file"""
    loaded_version = QtCore.pyqtSignal(str)
    
    def __init__(self, doc, parent=None):
        acquisition.AbstractTestWindow.__init__(self, doc=doc, parent=parent)
        self.play = filedata.FilePlayer(self)
        
        self.load_version(-1)

    vtoolbar = False
    breadcrumb = False
    breadbar = False
    plotboard = False
    menuVersions = False
    navtoolbar = False
    initial_veusz_doc_changeset = -1
    sigDocPageChanged = QtCore.pyqtSignal()
    
    def load_version(self, v=-1):
        self.fixedDoc.paused = True
        logging.debug("SETTING VERSION", v)
        self.plot_page = False
        if not self.fixedDoc.proxy.isopen():
            self.fixedDoc.proxy.reopen()
            
        self.fixedDoc.proxy.set_version(v, load_conf=True)
        
        if self.fixedDoc.proxy.conf is False:
            logging.debug('load_conf')
            self.fixedDoc.proxy.load_conf()
        self.fixedDoc.proxy.conf.filename = self.fixedDoc.proxy.path
        self.fixedDoc.proxy.conf.doc = self.fixedDoc
        
        self.set_server(self.fixedDoc.proxy.conf)
        
        self.fixedDoc.proxy.conf._navigator = self.navigator
        self.name = self.fixedDoc.proxy.get_node_attr('/conf', 'instrument')
        self.title = self.remote.measure['name']
        self.setWindowTitle('Test: ' + self.remote.measure['name'])
        
        self.add_graphWin()
        if self.name == 'hsm':
            self.add_snapshotsDock()
            self.imageSlider.slider.choice()
            if self.imageSlider.strip.decoder:
                self.imageSlider.strip.step = 1 + len(self.imageSlider.strip.decoder) // 1000
            self.imageSlider.strip.set_idx()
        
        # Menu Bar mod
        self.create_version_plot_menus()
        
        if self.vtoolbar:
            self.vtoolbar.hide()
        self.vtoolbar = self.summaryPlot.plot.createToolbar(self)
        veusz.utils.addToolbarActions(self.vtoolbar,
                                      self.summaryPlot.treeedit.vzactions,
                                      ('add.key', 'add.label', 'add.shapemenu'))
        self.vtoolbar.show()
        
        self.navtoolbar = NavigatorToolbar(self.navigator, self)
        self.navtoolbar.show()
        self.addToolBar(QtCore.Qt.TopToolBarArea, self.navtoolbar)
        self.navtoolbar.versionSaved.connect(self.reset_changeset)
        
        self.add_breadcrumb()
        self.add_plotboard()
        self.doc.model.sigPageChanged.connect(self.slot_page_changed)
        self.fixedDoc.paused = False
        self.reset_changeset()
        self.loaded_version.emit(self.fixedDoc.proxy.get_version())
        self.navigator.expand_plotted_nodes()
        v = list(self.navigator.model().tree.children.values())
        if v:
            root = v[0]
            self.navigator.expand_node_path(root, select=True)
        
        # TODO: cleanup this! Should pass through some sort of plugin or config
        # mechanism...
        getattr(self, 'init_instrument_' + self.name, self.init_instrument)()
        print('DONE load_version')
        
    def add_logDock(self):
        r = super().add_logDock()
        self.logDock.setWidget(
                fileui.OfflineLog(self.fixedDoc.proxy, self.logDock))
        return r

    def updateInstrumentInterface(self, remote=False):
        if not super().updateInstrumentInterface(remote=remote):
            return False
        if self.name == 'hsm':
            self.add_compare()
        self.tasks.done(self.instrument_pid())
        return True
    
    def resetFileProxy(self, *a, **k):
        self.doc = self.fixedDoc
        return super().resetFileProxy()
    
    def closeEvent(self, ev):
        self.fixedDoc.close()
        return super().closeEvent(ev)
    
    def add_compare(self):
        self.compareSamples = CompareMicroscopeSamples()
        self.compareSamples.testwindow = self
        self.compareWin = self.centralWidget().addSubWindow(
            self.compareSamples, acquisition.subWinFlags)
        self.compareWin.hide()
        self.myMenuBar.create_window_show_action('compareWin', 'Compare Samples')
        return True
        
    def init_instrument(self):
        """Generic instrument initialization"""
        self.plotboardDock.hide()
        self.breadbar.hide()
        self.graphWin.showMaximized()
        
    def init_instrument_flash(self):
        self.navigator.status.add(filedata.dstats.outline)
        self.measureTab.setCurrentIndex(1)
        try:
            self.summaryPlot.cmd.Remove('/time')
        except:
            pass
        onOpen = confdb['flash_open']
        if onOpen == 'Ask':
            m = QtWidgets.QMessageBox(self)
            m.setWindowTitle(_('Select how to open Flash tests'))
            m.setText(_('Choose if to create a default plot or start the Flash Wizard'))
            m.setInformativeText(_('You can save your preference under \nHelp->Client Configuration->Flash->Show on opening'))
            m.addButton(_('Start the Wizard'), QtWidgets.QMessageBox.AcceptRole)
            m.addButton(_('Default Plot'), QtWidgets.QMessageBox.RejectRole)
            m.exec_()
            
            if m.result() == QtWidgets.QMessageBox.AcceptRole:
                onOpen = 'Wizard'
        if onOpen == 'Wizard':
            self.graphWin.showMinimized()
            dom = self.navigator.domainsMap['FlashNavigatorDomain']
            dom.wizard()
        else:
            self.graphWin.showMaximized()
        
    def get_tooltip(self):
        r = []
        for fp in self.fixedDoc.proxies.values(): 
            r.append(filedata.get_file_tooltip(fp.conf.instrument_obj, fp))
        return '\n'.join(r)
        
    def reset_changeset(self, *foo):
        self.initial_veusz_doc_changeset = self.doc.changeset - self.doc.changeset_ignore
        self.initial_server_changeset = self.server.recursive_changeset()
        logging.debug('Reset changeset', self.initial_veusz_doc_changeset,
                      self.initial_server_changeset)
        
    def add_playback(self):
        """FIXME: DISABLED"""
        return
        self.play = filedata.FilePlayer(self)
        self.play.set_doc(self.fixedDoc)
        self.play.sleep = 0.1
        self.controls.remote = self.play
        for pic, win in self.cameras.values():
            pic.setSampleProcessor(self.play)
#             d=self.doc.decoders['/dat/'+pic.role]
#             pic.setFrameProcessor(d)       
        
    def add_breadcrumb(self):
        if self.breadbar:
            self.removeToolBar(self.breadbar)
            self.breadcrumb.hide()
            self.breadcrumb.deleteLater()
        self.breadbar = QtWidgets.QToolBar(_('Breadcrumb'), self)
        self.breadcrumb = Breadcrumb(self.breadbar)
        self.breadcrumb.set_plot(self.summaryPlot)
        self.breadbar.addWidget(self.breadcrumb)
        self.addToolBar(QtCore.Qt.TopToolBarArea, self.breadbar)
        self.breadbar.show() 
        
    def add_plotboard(self):
        logging.warning('DISABLED PLOTSBOARD')
        if self.plotboard:
            self.plotboard.blockSignals(True)
            self.rem('plotboardDock', 'plotboard')            
        self.plotboardDock = QtWidgets.QDockWidget(self.centralWidget())
        self.plotboardDock.setWindowTitle('Plots Board')
        # self.plotboard = PlotsBoard(self)
        # self.plotboard.set_plot(self.summaryPlot)
        # self.plotboardDock.setWidget(self.plotboard)
        self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, self.plotboardDock) 
        self.myMenuBar.add_view_plotboard()
        
    def create_version_plot_menus(self):
        self.actStandard = self.myMenuBar.measure.addAction(
            _('Re-calculate metadata'), self.re_standard)
        if not self.menuVersions:
            self.menuVersions = fileui.VersionMenu(self.fixedDoc, self.fixedDoc.proxy)
            self.menuVersions.versionChanged.connect(partial(self.slot_version_changed))
            self.menuVersions.versionRemoved.connect(partial(self.slot_version_removed))
            self.menuVersions.versionSaved.connect(self.reset_changeset)
        self.myMenuBar.measure.addMenu(self.menuVersions)
        
    def slot_version_changed(self, new=None):
        """Proxy version changed. Need to refresh all ConfigurationInterface objects"""
        logging.debug('slot_version_changed', repr(new))
                
        self.server = self.fixedDoc.proxy.conf
        self.remote = self.server.instrument_obj
        self.add_measure()
        self.add_graphWin()
        self.add_tableWin()
        self.add_menubar()
        
        self.create_version_plot_menus()
        
        self.navigator.set_doc(self.doc)
        self.measureTab.results.set_doc(self.doc)
        if self.name == 'flash':
            self.navigator.status.add(filedata.dstats.outline)
        self.navigator.expand_plotted_nodes()
        
    def slot_version_removed(self, removed_version, new_version):
        """If the current version was removed and the file reverts to its original version,
        the entire plot must be wiped."""
        logging.debug('slot_version_removed', repr(removed_version), repr(new_version))
        if not new_version:
            self.navigator.reset_document()

    def slot_page_changed(self):
        p = self.summaryPlot.plot.getPageNumber()
        page = self.doc.basewidget.children[p]
        if page == self.plot_page:
            return False
        self.plot_page = page
        logging.debug('slot_page_changed', page)
        hierarchy, level, page_idx = calc_plot_hierarchy(self.fixedDoc, page)
        if level < 0:
            return False
        plots = hierarchy[level][page_idx][1]
        crumbs, most_commons = most_involved_node(plots, self.doc)
        get_path = lambda crumbs: '/' + \
            '/'.join([c.split(':')[-1] for c in crumbs])
        paths = []
        if len(crumbs) > 0:
            if len(crumbs) > 1:
                paths.append(get_path(crumbs))
            if len(crumbs) > len(most_commons):
                self.measureTab.refresh_nodes(paths)
                return True
            
            if len(crumbs) == len(most_commons):
                crumbs.pop(-1)

            p = get_path(crumbs)
            for c in sorted(most_commons[-1]):
                paths.append(p + '/' + c)
        self.measureTab.refresh_nodes(paths)
        self.sigDocPageChanged.emit()
        return True
    
    def slot_manage_cache(self):
        p = self.summaryPlot.plot.getPageNumber()
        page = self.doc.basewidget.children[p]
        logging.debug('slot_manage_cache', page.name)
        self.doc.manage_page_cache(page.name)
    
    def check_save(self, nosync=True):
        """Check if changes occurred to the Veusz document or the configuration proxy,
        and ask to save on current or new version.
        Returns true if saved or discarded, false if action aborted."""
        if not self.navigator.isEnabled():
            QtWidgets.QMessageBox.information(self,
                                          _('Cannot close test: ' + self.windowTitle()),
                                          _(message_busy))
            return False
        
        ret = True
        effective_changeset = self.doc.changeset - self.doc.changeset_ignore
        # FIXME: here I should check all server versions from all proxies
        conf_changeset = self.server.recursive_changeset()
        logging.debug('Checking changesets', effective_changeset, self.doc.changeset,
                      self.initial_veusz_doc_changeset, conf_changeset, self.initial_server_changeset)
        if effective_changeset > self.initial_veusz_doc_changeset or conf_changeset > self.initial_server_changeset:
            first = self.doc.proxies.values().__iter__().__next__()
            ver = first.get_version()
            logging.debug('got version', repr(ver))
            if not ver:
                ver = _('a new version')
            else:
                ver = _('on version: ') + ver             
            r = QtWidgets.QMessageBox.question(self, _('Save changes to ' + self.windowTitle()),
                                       _('Some changes were detected. \nWould you like to save ') + ver + ' ?',
                                       QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Discard | QtWidgets.QMessageBox.Abort)
            if r == QtWidgets.QMessageBox.Abort:
                logging.debug('Aborting close action.')
                return False
            elif r == QtWidgets.QMessageBox.Ok:
                logging.debug('Saving a version on close', ver)
                v = fileui.VersionMenu(self.doc, first)
                ret = v.save_version(nosync=nosync)
                if not nosync:
                    self.reset_changeset()
        return ret
    
    def close(self): 
        r = acquisition.AbstractTestWindow.close(self)
        logging.debug('TestWindow.close')
        self.play.close()
        if self.fixedDoc:
            self.fixedDoc.close()

    def set_idx(self, idx):
        logging.debug('TestWindow.set_idx', self.play.isRunning(), idx)
        if not self.play.isRunning():
            self.play.set_idx(idx)
        else:
            if idx == self.imageSlider.value():
                return
            self.imageSlider.set_idx(idx)

    def re_standard(self):
        """Re-evaluate the meta-data generating scripts (standards)."""
        fp = self.fixedDoc.proxy
        if fp.__class__.__name__ != 'SharedFile':
            logging.debug('Error: restandard is only possible on local files')
            return
        # Overwrite
        from mdf_canon import logger
        instr = getattr(fp.conf, fp.instrument_name(), None)
        if not instr:
            return
        instr.clean_log_buffer()
        logger.message_buffer = 1000
        r = fp.run_scripts()
        logger.message_buffer = 10
        if r:
            # Update every ActiveWidget connected to the registry
            registry.force_redraw()
            self.fixedDoc.signalModified.emit(True)
            self.summaryPlot.resize(self.summaryPlot.size())
            logs = instr.collect_logs()
            r = '\n'.join([' '.join(map(str, e[0])) for e in logs])
            msg = iutils.informative_message_box(r, self)
            msg.setText(_('Metadata recalculated.\n\nDetails:\n'))
            msg.exec_()
        else:
            QtWidgets.QMessageBox.critical(None, 'Error', 'Metadata calculation failed!')


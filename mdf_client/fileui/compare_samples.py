#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Compare multiple storyboards"""
from mdf_client.qt import QtWidgets, QtGui, QtCore
from mdf_client.qt import Qt
from mdf_client import fileui
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)

#TODO: sincronizzare in base temperatura

class CompareMicroscopeSamples(QtWidgets.QWidget):
    testwindow = False
    document = False
    def __init__(self, parent=None):
        super(CompareMicroscopeSamples, self).__init__(parent=parent)
        self.lay = QtWidgets.QVBoxLayout()
        self.setLayout(self.lay)
        self.boards = []
    
    @property
    def doc(self):
        if self.document:
            return self.document
        return self.testwindow.doc
    
    def showEvent(self, *args, **kwargs):
        for b in self.boards:
            self.lay.removeWidget(b)
            b.hide()
            b.close()
        if not self.doc.decoders:
            self.doc.create_decoders()
        self.boards = []
        for i, k in enumerate(list(self.doc.decoders.keys())):
            sli = fileui.ImageSlider()
            sli.set_doc(self.doc)
            sli.slider.sig_datapathChanged.emit(k)
            sli.slider.cbPath.setEnabled(False)
            sli.strip.sig_meta_changed.connect(self.route_meta_changed)
            if i>0:
                self.boards[0].slider.slider.valueChanged.connect(sli.set_idx)
            
            self.boards.append(sli)
            self.lay.addWidget(sli)
        logging.debug('Comparing',len(self.boards))
        return QtWidgets.QWidget.showEvent(self, *args, **kwargs)
    
    #@QtCore.pyqtSlot(float)
    def route_meta_changed(self, keys):
        for b in self.boards:
            
            b.strip.route_meta_changed(keys, signal=False)
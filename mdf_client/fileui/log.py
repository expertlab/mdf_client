#!/usr/bin/python
# -*- coding: utf-8 -*-
import functools
import numpy as np
from traceback import format_exc
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from mdf_canon import reference, csutil

from mdf_client.qt import QtWidgets, QtGui, QtCore

from mdf_client import _
from mdf_client.livelog import AbstractLogModel

class ReferenceLogModel(AbstractLogModel):
    def __init__(self, proxy, max_rows=1e4, parent=None):
        AbstractLogModel.__init__(self, max_rows)
        #super(ReferenceLogModel, self).__init__(max_rows, parent=None)
        self.proxy = proxy
        self.path = ''
        
    def flags(self, *args, **kwargs):
        r= QtCore.QAbstractTableModel.flags(self, *args, **kwargs)
        return r | QtCore.Qt.ItemIsEditable
        
    def get_log_paths(self):
        return self.proxy.header(['Log'])
    
    def set_log_path(self, path):
        self._data = reference.get_node_reference(self.proxy, path)
        n = self.proxy._get_node(path)
        self._levels = n.cols.priority[:].astype(int)
        self._filtered = np.where(self._levels>self.level)[0]
        self.modelReset.emit()
        self.path = path
        
    def decode_row(self, row):
        return [float(row[0]), int(row[1][0]), csutil.unicode_decode(row[1][1])]
        


class OfflineLog(QtWidgets.QTableView):
    iter = 0     
    def __init__(self, proxy, parent=None):
        QtWidgets.QTableView.__init__(self, parent)
        self.log_model = ReferenceLogModel(proxy)
        self.setModel(self.log_model)
        
        self.label = _('Log')
        self.menu = QtWidgets.QMenu(self)
        self.build_menu()
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.setWordWrap(True)
        self.setTextElideMode(QtCore.Qt.ElideLeft)
        self.customContextMenuRequested.connect(self.showMenu)
        self.setFont(QtGui.QFont('TypeWriter',  7, 50, False))
        self.horizontalHeader().setStretchLastSection(True)
    
    def showEvent(self, e):
        try:
            self.log_model.set_log_path('/support/log')
        except:
            logging.warning('File might be damaged.',format_exc())
        return super(OfflineLog, self).showEvent(e)
        
    def build_menu(self):
        self.menu.clear()
        for p in self.log_model.get_log_paths():
            func = functools.partial(self.log_model.set_log_path, p)
            a = self.menu.addAction(p, func)
            a.setCheckable(True)
        self.menu.addSeparator()
        self.log_model.build_menu(self.menu)     

    def showMenu(self, pt):
        for a in self.menu.actions():
            a.setChecked(a.text().replace('&','')==self.log_model.path)
        self.menu.popup(self.mapToGlobal(pt))
        

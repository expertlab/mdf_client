#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Tests summary table"""
import unittest
import os

from mdf_client.tests import iutils_testing
from mdf_client.fileui import SummaryView
from mdf_client import filedata, plugin  # neeeded for correct veusz init!

from mdf_client.qt import QtWidgets, QtGui

nativem4 = os.path.join(iutils_testing.data_dir, 'test_video.h5')


class TestSummary(unittest.TestCase):

    def setUp(self):
        self.s = SummaryView()

    def test(self):
        doc = filedata.MisuraDocument(nativem4)
        doc.reloadData()
        self.s.set_doc(doc)

        iutils_testing.show(self.s, __name__)


if __name__ == "__main__":
    unittest.main(verbosity=2)

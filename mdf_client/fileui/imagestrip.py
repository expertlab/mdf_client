#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from mdf_client.qt import QtWidgets, QtCore
from .minimage import MiniImage
from mdf_client.fileui import htmlreport
from mdf_client import _
from mdf_client.widgets import RunMethod
from mdf_client.live import registry
from mdf_client import conf

from mdf_canon import option

standards = ('MDF', 'Misura3', 'CEN/TS')


def get_shapes(sample, standard):
    all_shapes = {
        'MDF': {
            'Sintering': 'Sintering',
            'Softening': 'Softening',
            'Sphere': 'Sphere',
            'Halfsphere': 'HalfSphere',
            'Melting': 'Melting',
        },
        'Misura3': {
            'Sintering': 'm3_Sintering',
            'Softening': 'm3_Softening',
            'Sphere': 'm3_Sphere',
            'Halfsphere': 'm3_HalfSphere',
            'Melting': 'm3_Melting',
        },
        'CEN/TS': {
            'Sintering': 'cen_Sintering',
            'Deformation': 'cen_Deformation',
            'Emisphere': 'cen_Emisphere',
            'Flow': 'cen_Flow',
        }
    }
    void = {'time': 'None', 'temp': 'None', 'value': 'None'}
    ret = {}
    for shape_key, sample_key in all_shapes[standard].items():
        if sample_key in sample:
            ret[shape_key] = sample[sample_key]
        else:
            ret[shape_key] = void.copy()
    return ret


def export_images_option_dialog(parent, max_temp):
    opts = {}
    option.ao(
        opts, 'standard', 'Chooser', standards[0], name=_("Standard for characteristic shapes"),
              options=list(standards) + ['None'])
    option.ao(
        opts, 'start', 'Integer', name=_("Discard images below temperature"),
              unit='celsius', current=0, min=0, max=max_temp + 1, step=1)
    option.ao(
        opts, 'step', 'Float', name=_("Minimum Temperature stepping"),
              unit='celsius', current=1, min=0, max=50, step=0.1)
    option.ao(
        opts, 'timeStep', 'Float', name=_("Minimum Time stepping"),
              unit='second', current=0, min=0, max=600, step=0.1)
    option.ao(opts, 'h', 'Boolean', False, name=_("Show height"))
    option.ao(opts, 'w', 'Boolean', False, name=_("Show width"))
    option.ao(opts, 'Vol', 'Boolean', False, name=_("Show volume"))
    option.ao(opts, 'pot', 'Boolean', False, name=_("Show potential"))
    option.ao(opts, 'cohe', 'Boolean', False, name=_("Show cohesion"))
    
    option.ao(opts, 'outputDir', 'Boolean', False, name=_("Export PNG to sibling directory"))
    
    configuration_proxy = option.ConfigurationProxy(
        {'self': opts})
    temperature_dialog = conf.InterfaceDialog(
        configuration_proxy, configuration_proxy, opts, parent=parent)
    temperature_dialog.setWindowTitle(_('Image export options'))
    if temperature_dialog.exec_():
        return configuration_proxy
    return False


class ImageStrip(QtWidgets.QWidget):

    """Image strip"""
    t = 0
    idx = 0
    step = 10
    n = 5
    bytime = False
    decoder = False
    
    sig_set_time = QtCore.pyqtSignal(float)
    sig_set_idx = QtCore.pyqtSignal(int)
    sig_set_n = QtCore.pyqtSignal(int)
    sig_meta_changed = QtCore.pyqtSignal(object)

    def __init__(self, n=5, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.setAcceptDrops(True)
        self._extrusion = []
        self.lay = QtWidgets.QGridLayout()
        self.setLayout(self.lay)
        self.n = n
        self.labels = []
        self.menu = QtWidgets.QMenu(self)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.showMenu)
        self.menu.addAction(_('Change length'), self.chLen)
        self.menu.addAction(_('Change rows'), self.chRows)
        self.menu.addAction(_('Style'), self.chStyle)
        self.uniform_zoom = self.menu.addAction(_('Uniform zoom'), self.slot_zoom_changed)
        self.uniform_zoom.setCheckable(True)
        self.uniform_zoom.setChecked(True)
        self.actIndex = self.menu.addAction(_('Step by index'), self.by_index)
        self.actIndex.setCheckable(True)
        self.actTime = self.menu.addAction(_('Step by time'), self.by_time)
        self.actTime.setCheckable(True)
        self.menu.addAction(_("Export Images"), self.export_images)
        from mdf_client import video, extrusion
        if video.cv:
            self.menu.addAction(_("Render video"), self.render_video)
        if extrusion.pg:
            self.menu.addAction(_("3D Extrusion"), self.render_extrusion)
        self.setStyleSheet("background-color:gray;");
            
    def dragEnterEvent(self, event):
        logging.debug('Slider.dragEnterEvent', event.mimeData())
        if event.mimeData().hasFormat("application/json"):
            event.acceptProposedAction() 
        
    def dropEvent(self, event):
        j = event.mimeData().data("application/json").data().decode("utf-8")
        opt = json.loads(j)
        if opt['type'] != "Meta":
            event.reject()
            return False
        t = opt['current']['time']
        logging.debug('dropEvent: set_time', t)
        self.set_time(t)
        event.accept()
    
    def export_images(self):
        output_filename = QtWidgets.QFileDialog.getSaveFileName(self,
                                                            _('Save Report'),
                                                            '',
                                                            'HTML (*.html)')
        if not output_filename:
            return
        output_filename = output_filename[0]
        if not output_filename.endswith('.html'):
            output_filename += '.html'
        
        self.decoder.proxy.load_conf()
        dp = self.decoder.datapath.split('/')
        while dp[0].startswith('ver') or dp[0] in ('summary', ''):
            dp.pop(0)
        sample = self.decoder.proxy.conf
        for sample_name in dp[:-1]:
            sample = getattr(sample, sample_name)
        instrument_name = self.decoder.proxy.conf['runningInstrument']
        instrument = getattr(self.decoder.proxy.conf, instrument_name)
        
        opts = export_images_option_dialog(self.parent(), max(self.doc.data.get('0:kiln/T').data))
        if not opts:
            return
        
        characteristic_shapes = {}
        if opts['standard'] != 'None':
            characteristic_shapes = get_shapes(sample, opts['standard'])
        self.export_aborted = False
        values = []
        for v in ('h', 'w', 'Vol', 'pot', 'cohe'):
            if opts[v]:
                values.append(v)
        print('AAAAAAAA', instrument_name, instrument, instrument.measure, dp, sample_name, sample)
        thread = RunMethod(htmlreport.create_images_report,
            self.decoder,
            instrument.measure,
            sample,
            characteristic_shapes,
            startTemp=opts['start'],
            step=opts['step'],
            timeStep=opts['timeStep'],
            values=values,
            output=output_filename,
            output_dir=opts['outputDir'],
            jobs=registry.tasks.jobs,
            job=registry.tasks.job,
            done=registry.tasks.done,
            check_abort=self.check_abort_export,
            do_abort=self.do_abort_export,
        )
        thread.pid = 'Creating images report'
        thread.abort = self.do_abort_export
        QtCore.QThreadPool.globalInstance().start(thread)
        return thread
    
    def do_abort_export(self):
        self.export_aborted = True

    def check_abort_export(self):
        return self.export_aborted

    def render_video(self):
        # TODO: use time/index stepping
        from mdf_client import video
        v = video.VideoExporter(self.decoder.proxy, self.decoder.datapath)
        v.exec_()
        
    def render_extrusion(self):
        from mdf_client import extrusion
        w = extrusion.ExtrusionRender(self.decoder.proxy, self.decoder.datapath)
        w.wg.widgetsMap['startTime'].set(self.labels[0].t)
        w.show()
        self._extrusion.append(w)

    def set_doc(self, doc, datapath=False):
        logging.debug('ImageStrip.set_doc', doc, datapath)
        self.doc = doc
        self.idx = 0
        self.t = 0
        self.rows = 1
        self.decoder = doc.decoders.get(datapath, False)
        
        if self.decoder:
            self.calc_len(self.n)
            self.set_idx(0)
            self.decoder.sig_reset.connect(self.calc_len)
            return True
        return False

    def showMenu(self, pt):
        self.menu.popup(self.mapToGlobal(pt))
        self.actTime.setChecked(self.bytime)
        self.actIndex.setChecked(not self.bytime)

    def chLen(self):
        n = QtWidgets.QInputDialog.getInt(
            self, "New length", "Change number of images:", self.n, 1, 50)
        if n[1]:
            self.calc_len(n[0])

    def chRows(self):
        n = QtWidgets.QInputDialog.getInt(
            self, "Set rows", "Change number of rows displayed:", self.rows, 1, 10)
        if n[1]:
            self.rows = n[0]
            self.calc_len(self.n)
            
    def chStyle(self):
        w = self.decoder.contour_width
        c = self.decoder.contour_only
        
        dia = QtWidgets.QDialog()
        lay = QtWidgets.QFormLayout()
        # w = QtWidgets.QWidget()
        # w.setLayout(lay)
        
        wg_only = QtWidgets.QCheckBox()
        wg_only.setChecked(c)
        wg_width = QtWidgets.QSpinBox()
        wg_width.setRange(0, 100)
        wg_width.setValue(w)
        wg_closed = QtWidgets.QCheckBox()
        wg_closed.setChecked(self.decoder.contour_closed)
        ok = QtWidgets.QPushButton(_("Ok"))
        canc = QtWidgets.QPushButton(_("Cancel"))
        lay.addRow(_("Contour only: "), wg_only)
        lay.addRow(_("Contour width: "), wg_width)
        lay.addRow(_("Draw sample holder: "), wg_closed)
        lay.addRow(canc, ok)
        ok.pressed.connect(dia.accept)
        canc.pressed.connect(dia.reject)
        
        dia.setLayout(lay)
        r = dia.exec_()
        if r == QtWidgets.QDialog.Rejected:
            return False
        self.decoder.contour_only = bool(wg_only.checkState())
        self.decoder.contour_width = wg_width.value()
        self.decoder.contour_closed = bool(wg_closed.checkState())
        self.decoder.cached_profiles = {}
        return True

    def by_index(self):
        val, st = QtWidgets.QInputDialog.getInt(
            self, "Step by index", "Display one image every N:", value=self.step, min=1, step=1)
        if not st:
            return
        self.step = val
        self.bytime = False
        self.set_idx()

    def by_time(self):
        # TODO: by_time stepping
        val, st = QtWidgets.QInputDialog.getInt(
            self, "Step by time", "Display one image every N seconds:", value=self.step, min=1, step=1)
        if not st:
            return
        self.step = val
        self.bytime = True
        self.set_idx()

    def route_meta_changed(self, keys, signal=True):
        logging.debug('routing meta keys', keys)
        for lbl in self.labels:
            lbl.sync_meta_keys(keys)
        if signal:
            self.sig_meta_changed.emit(keys)

    def calc_len(self, n=-1):
        """Changes the number of visible images"""
        if n < 0:
            n = self.n
        self.n = n
        for lbl in self.labels:
            lbl.hide()
            lbl.deleteLater()
            del lbl
        self.labels = []
        if self.decoder:
            datapath = self.decoder.prefix + self.decoder.datapath[1:]
        else:
            datapath = False
        for i in range(n):
            row = int(i % self.rows)
            col = int(i / self.rows)
            lbl = MiniImage(self.doc, datapath, parent=self)
            lbl.zoomChanged.connect(self.slot_zoom_changed)
            self.lay.addWidget(lbl, row, col)
            self.labels.append(lbl)
            lbl.metaChanged.connect(self.route_meta_changed)
        # The first label holds the current idx, and emits current time signal
        
        self.labels[0].sig_set_time.connect(self.emitSetTime)
        self.sig_set_n.emit(self.n)
        self.set_idx()
        return True
    
    def slot_zoom_changed(self, width=0):
        if not self.uniform_zoom.isChecked() and width != 0:
            return False
        if width == 0:
            width = self.labels[0].curWidth
        for lbl in self.labels:
            lbl.zoom(width)
        return True

    def emitSetTime(self, t):
        """Route setTime signals received by the last MiniImage"""
        self.t = t
        logging.debug('ImageStrip.emitSetTime', t)
        self.sig_set_time.emit(t)

    def set_time(self, t):
        """Find the nearest index to time `t` and set myself on that idx"""
        logging.debug('ImageStrip.setTime', t)
        idx = self.decoder.get_time(t)
        self.t = t
        logging.debug('ImageStrip.setTime: idx', idx)
        return self.set_idx(idx)

    def set_idx(self, idx=-1):
        """Sets the current start index."""
        if len(self.labels) < self.n:
            return
        if self.n == 0:
            return
        logging.debug('strip idx', idx)
        if idx < 0:
            idx = self.idx
        
        batch = []
        
        for label_index in range(self.n):
            index_with_step = max(0, idx + (label_index * self.step))
            batch.append(index_with_step)
            
        self.decoder.get_batch(batch)
        
        for label_index in range(self.n):
            self.labels[label_index].set_idx(batch[label_index])
        
        self.idx = idx
        self.sig_set_idx.emit(idx)


class Slider(QtWidgets.QWidget):
    decoder = False
    sliderReleased = QtCore.pyqtSignal()
    sig_set_idx = QtCore.pyqtSignal(int)
    sig_datapathChanged = QtCore.pyqtSignal(str)
    
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent=parent)
        self.setAcceptDrops(True)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.slider = QtWidgets.QScrollBar(parent=self)
        self.slider.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.slider.setTracking(False)
        self.slider.setOrientation(QtCore.Qt.Horizontal)

        self.cbPath = QtWidgets.QComboBox(self)

        self.menuButton = QtWidgets.QPushButton('...', parent=self)
        self.menuButton.setMaximumWidth(50)

        self.lay = QtWidgets.QHBoxLayout()
        self.lay.setContentsMargins(0, 0, 0, 0)
        self.lay.setSpacing(0)
        self.lay.addWidget(self.slider)
        self.lay.addWidget(self.cbPath)
        self.lay.addWidget(self.menuButton)
        self.setLayout(self.lay)
        self.cbPath.currentIndexChanged.connect(self.choice)
        self.slider.sliderReleased.connect(self.sliderReleased.emit)
        
    def dragEnterEvent(self, event):
        logging.debug('Slider.dragEnterEvent', event.mimeData())
        if event.mimeData().hasFormat("application/json"):
            event.acceptProposedAction() 
        
    def dropEvent(self, event):
        j = event.mimeData().data("application/json").data().decode("utf-8")
        opt = json.loads(j)
        if opt['type'] != "Meta":
            event.reject()
            return False
        t = opt['current']['time']
        logging.debug('dropEvent: set_time', t)
        self.parent().set_time(t)

    def value(self):
        return self.slider.value()

    def set_doc(self, doc):
        self.doc = doc
        self.reset(True)
        self.doc.sig_updated.connect(self.calc_len)
        self.retry = 0

    def calc_len(self):
        if not self.isVisible():
            logging.debug('Slider.calc_len: not visible.')
            return False

        L = 0
        if not self.decoder:
            logging.debug('Slider.calc_len no decoder')
            self.retry += 1
            if self.retry % 15 == 0:
                self.reset()
            return 
        if self.decoder:
            L = len(self.decoder)
            logging.debug('calc_len', L)
        self.slider.setMaximum(L)
        logging.debug('Slider.calc_len', L)
        
        strip = self.parent().strip
        M = (strip.n) * (strip.step)
        follow = self.slider.value() >= L - M * 1.1
        if follow and L > M:
            logging.debug('Slider.calc_len autofollow', L, M)
            self.set_idx(L - M)
            self.slider.valueChanged.emit(self.slider.value())
        elif L < M:
            self.set_idx(self.slider.value())
            self.slider.valueChanged.emit(self.slider.value())
        return L

    def choice(self, foo=0):
        self.reset(choice=True)

    def reset(self, choice=False):
        self.cbPath.currentIndexChanged.disconnect(self.choice)

        # Get dat group
        i = self.cbPath.currentIndex()
        cgr = str(self.cbPath.itemData(i))
        logging.debug('current group', i, cgr)
        
        # Update group combo
        self.cbPath.clear()
        gr = []
        self.doc.create_decoders()
        for j, g in enumerate(self.doc.decoders.keys()):
            self.cbPath.addItem(g, g)
            gr.append(g)
            if self.decoder and g == self.decoder.prefix + self.decoder.datapath[1:]:
                i = j
        if i < 0 and len(gr) > 0:
            cgr = gr[0]
            i = 0
            choice = True
        self.cbPath.setCurrentIndex(i)
        self.cbPath.currentIndexChanged.connect(self.choice)

        # Reset decoder
        if choice:
            logging.debug('resetting to', cgr)
            self.decoder = self.doc.decoders.get(cgr, False)
            n = getattr(self.decoder, 'datapath', False)
            logging.debug('resetted to', n)
            if n:
                self.sig_datapathChanged.emit(cgr)
        m = 0
        if self.decoder:
            m = len(self.decoder)
        self.slider.setMaximum(m)
        self.slider.setMinimum(0)
        self.slider.setValue(0)
        logging.debug('done')

    def set_idx(self, idx):
        logging.debug('Slider.set_idx', idx)
        if self.slider.maximum() == 0:
            return
        self.slider.setValue(idx)
        self.sig_set_idx.emit(idx)


class ImageSlider(QtWidgets.QWidget):
    sig_set_time = QtCore.pyqtSignal(float)
    sliderReleased = QtCore.pyqtSignal()
    sig_set_idx = QtCore.pyqtSignal(int)
    
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent=parent)
        self.setWindowTitle(_('Storyboard'))
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.strip = ImageStrip(parent=self)
        
        self.strip.sig_set_time.connect(self.emitSetTime)
        
        self.customContextMenuRequested.connect(self.strip.showMenu)

        self.stripArea = QtWidgets.QScrollArea(self)
        self.stripArea.setWidgetResizable(True)
        self.stripArea.setWidget(self.strip)
        self.stripArea.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.stripArea.customContextMenuRequested.connect(self.strip.showMenu)

        # Slider for image navigation
        self.slider = Slider(self)
        self.slider.slider.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.slider.slider.customContextMenuRequested.connect(self.strip.showMenu)
        self.slider.menuButton.setMenu(self.strip.menu)
        self.slider.slider.valueChanged.connect(self.strip.set_idx)
        self.slider.slider.sliderMoved.connect(self.strip.set_idx)
        self.slider.sig_datapathChanged.connect(self.setPath)
        self.lay = QtWidgets.QVBoxLayout()
        self.lay.setContentsMargins(0, 0, 0, 0)
        self.lay.setSpacing(0)
        self.lay.addWidget(self.slider)
        self.lay.addWidget(self.stripArea)
        self.setLayout(self.lay)

        self.customContextMenuRequested.connect(self.strip.showMenu)
        self.slider.sliderReleased.connect(self.sliderReleased.emit)
        
    def close(self):
        self.strip.close()
        self.slider.close()
        return super().close()

    def emitSetTime(self, t):
        """Route setTime signals received by the ImageStrip"""
        logging.debug('ImageSlider.emitSetTime', t, self.slider.decoder.prefix + self.slider.decoder.datapath)
        self.sig_set_time.emit(t)

    def value(self):
        return self.slider.value()
    
    @property
    def idx(self):
        return self.value()

    @property
    def t(self):
        return self.strip.t

    def set_doc(self, doc):
        self.doc = doc
        self.slider.set_doc(doc)

    def setPath(self, path):
        self.strip.set_doc(self.doc, str(path))
        self.slider.decoder = self.strip.decoder
        self.slider.reset(False)
        return (self.strip.decoder is not False)

    def set_idx(self, idx):
        if self.slider.slider.maximum() == 0:
            return
        try:
            self.slider.sig_set_idx.disconnect(self.set_idx)
            self.strip.sig_set_idx.disconnect(self.set_idx)
        except:
            pass
        print('ImageSlider.set_idx emit sig_set_idx with', idx, self.slider.decoder.prefix + self.slider.decoder.datapath)
        self.sig_set_idx.emit(idx)
        self.slider.set_idx(idx)
        self.strip.set_idx(idx)
        self.slider.sig_set_idx.connect(self.set_idx)
        self.strip.sig_set_idx.connect(self.set_idx)

    def set_time(self, t):
        idx = None
        if self.slider.decoder:
            idx = self.slider.decoder.get_time(t)
        if idx is None:
            idx = int(t)

        self.set_idx(idx)

#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import base64
from mdf_canon.logger import get_module_logging
from mdf_canon import csutil
from mdf_client.qt import QtCore
from mdf_canon.reference.iterate_images import iterate_images
import shutil
logging = get_module_logging(__name__)


def byte_array_from(qimage):
    image_data = QtCore.QByteArray()
    buf = QtCore.QBuffer(image_data)
    buf.open(QtCore.QIODevice.WriteOnly)
    qimage.save(buf, 'PNG')
    buf.close()
    return image_data


def encode(data):
    return base64.b64encode(data).decode('utf8')


def embed(data, type):
    return "<img src='data:image/%s;base64,%s' alt=''>" % (type, encode(data))


image_template = """<table>
{}
<tr>
    <td>{}</td>
</tr>
<tr>
    <td><div class='temperature'>{}</div><div class='time'>{}</div></td>
</tr>
{}
</table>"""


def embed_with_labels(data,
                      temperature,
                      time,
                      type='gif',
                      characteristic_shape_label='',
                      shape_anchor='',
                      T_anchor='',
                      values=[]):
    
    image_html = embed(data, type)
    shape_label = ''
    if characteristic_shape_label:
        shape_label = "<b>{}</b>".format(characteristic_shape_label)
        if shape_anchor:
            shape_label = "<a name='{}'>{}</a>".format(shape_anchor, shape_label)
        shape_label = '<tr><td align="center">{}</td></tr>'.format(shape_label)
    T_label = "{}&deg;C".format(temperature)
    if T_anchor:
        T_label = "<a name='{}'>{}</a>".format(T_anchor, T_label)
    
    vals = ''
    for key, val in values:
        vals += '\n<tr><td><div class="opt_label">{}:</div>'.format(key)
        vals += '<div class="opt_value">{:.1f}%'.format(val)
        vals += '</div></td></tr>'
        
    return image_template.format(shape_label,
            image_html,
            T_label,
            time,
            vals)


def table_from(images,
               decoder,
               output_file,
               output_dir,
               type='gif',
               images_per_line=5,
               characteristic_temperatures={},
               opt_datasets=[],
               jobs=lambda *x, **k: None,
               job=lambda *x, **k: None,
               done=lambda *x, **k: None,
               check_abort=lambda: False,
               do_abort=lambda: False):
        if output_dir:
            if os.path.exists(output_dir):
                shutil.rmtree(output_dir)
            else:
                os.mkdir(output_dir)
        proxy = decoder.proxy
        jobs(len(images), 'Adding images', abort=do_abort)
        output_file.write("<table><tr>")
        labels = {}
        for name, temp in characteristic_temperatures.items():
                labels[temp] = name 
                
        Tpre = -3000
        shape_anchors = []
        T_anchors = []
        step = int(len(images) / 10)
        for i, (j, T) in enumerate(images):
                html = ''
                if check_abort():
                    logging.debug('Export aborted')
                    done('Adding images')
                    return False
                job(i, 'Adding images')
                t, image = decoder.get_data(j)
                image = byte_array_from(image)
                if output_dir:
                    fn = os.path.join(output_dir, '{}_{:.1f}.png'.format(i, T))
                    logging.debug('Write', fn)
                    f = open(fn, 'wb')
                    f.write(image)
                    f.close()
                Tr = round(T, 1)
                Tnext = T + 3000
                label = ''
                if i < len(images) - 1:
                    Tnext = images[i + 1][1]
                    
                label = get_label(Tpre, T, Tnext, labels)
                shape_anchor = label.replace(' ', '')
                if label:
                    shape_anchors.append('<a href="#{}">{}</a>'.format(shape_anchor, label))
                label += '<br/><br/>'
                
                T_anchor = ''
                if step and i % step == 0:
                    T_anchor = Tr
                    T_anchors.append('<a href="#{}">{}</a>'.format(T_anchor, T_anchor))
                
                values = []
                for key, col, zero in opt_datasets:
                    val = proxy.col_at_time(col, t, raw=True)[1]
                    val = 100.*val / zero
                    values.append((key, val))
                
                html += "<td>%s</td>" % embed_with_labels(image,
                                                                Tr,
                                                                csutil.from_seconds_to_hms(int(t)),
                                                                type,
                                                                label,
                                                                shape_anchor,
                                                                T_anchor,
                                                                values=values)
                
                if (i + 1) % images_per_line == 0:
                        html += "</tr><tr>"
                html += "\n"
                Tpre = T
                output_file.write(html)
                
        # Add floating menu
        menu = '<div id="menu" class="no-print"><a href="#title">Top</a>' + '<br/>'.join(shape_anchors + T_anchors)
        output_file.write(menu + '</div></tr></table>')
        
        done('Adding images')


def encode_image(image_file_name):
    return encode(open(image_file_name, 'rb').read())


def get_label(Tpre, T, Tnext, labels):
    for LT, name in labels.items():
        if LT is None:
            continue
        dt = abs(T - LT)
        if abs(T - Tpre) > dt and abs(Tnext - T) > dt:
            return labels.pop(LT)
    return ''


def create_images_report(decoder,
                         measure,
                         sample,
                         characteristic_shapes,
                         startTemp=0,
                         step=1,
                         timeStep=0,
                         values=[],
                         output=False,
                         output_dir=False,
                         jobs=lambda *x, **k: None,
                         job=lambda *x, **k: None,
                         done=lambda *x, **k: None,
                         check_abort=lambda: False,
                         do_abort=lambda: 0):
    Tpath = decoder.datapath.split('/')
    samplepath = Tpath[:-1]
    Tpath[-1] = 'T'
    
    if Tpath[1].startswith('ver_'):
        Tpath1 = '/'.join(Tpath)
        if not decoder.proxy.has_node(Tpath1):
            Tpath.pop(1)
    
    Tpath = '/'.join(Tpath)
    proxy = decoder.proxy
    # Get index of startTemp in sample/T dataset
    idx0, t0, T0 = proxy.rises(Tpath, startTemp)
    # Get index of start time in sample/profile
    idx0 = decoder.get_time(t0)
    total_number_of_images = len(decoder)
    jobs(3, 'Creating images report', abort=do_abort)
    jobs(total_number_of_images - idx0 + 1, 'Indexing', abort=do_abort)
    
    seq = []
    
    for new_i, new_T in iterate_images(proxy, decoder.get_time, decoder.time_at, len(decoder),
                                       Tpath, t0, T0, step, timeStep):
        if check_abort():
            logging.debug('Export aborted')
            done('Indexing')
            return False
        seq.append((new_i, new_T))
        job(new_i - idx0, 'Indexing')
    
    done('Indexing')
    
    # Collect additional values
    opt_datasets = []
    for v in values:
        p = '/'.join(samplepath + [v])
        if proxy.has_node(p):
            name = proxy.get_node_attr(p, 'name')
            if v in ('cohe', 'pot'):
                zero = 100
            else:
                zero = proxy.col(p, [0, 10])
                zero = zero[:, 1].mean()
            opt_datasets.append([name, p, zero])
    
    if not seq:
        logging.error('No valid images')
        done('Creating images report')
        return False
    
    job(2, 'Creating images report', 'Creating report structure')
    characteristic_temperatures = {}

    for shape in list(characteristic_shapes.keys()):
        characteristic_temperatures[shape] = float_or_none(characteristic_shapes[shape]['temp'])

    sample_title = ''
    sname = sample['name']
    if measure['name'] != sname and not sname.startswith('Sample n.'):
        sample_title = '<div><strong>Sample</strong>: {}</div>'.format(sample['name'])
    substitutions_hash = {"$LOGO$": base64_logo(),
                          "$code$": measure['uid'],
                          "$title$": measure['name'],
                          '$sample$': sample_title,
                          "$date$": measure['date'],
                          }
    output_html = images_template_header[:]
    for key in substitutions_hash:
        print(key, type(substitutions_hash[key]))
        output_html = output_html.replace(key, substitutions_hash[key])
        
    job(2, 'Creating images report', 'Writing report to ' + str(output))
    
    if not output.lower().endswith('.html'):
        output += '.html'
    output_file = open(output, 'w') 
    output_file.write(output_html)
    if output_dir:
        output_dir = output[:-5] + '.images'
        
    table_from(seq, decoder, output_file, output_dir,
                                        'png',
                                        5,
                                        characteristic_temperatures,
                                        opt_datasets=opt_datasets,
                                        jobs=jobs,
                                        job=job,
                                        done=done,
                                        check_abort=check_abort,
                                        do_abort=do_abort)
    
    output_file.write(images_template_footer)
    output_file.close()
    done('Creating images report')


def float_or_none(float_or_none_string):
    if float_or_none_string == 'None':
        return None
    return float_or_none_string


def base64_logo():
    from mdf_client.clientconf import confdb
    from mdf_client import parameters as params
    logo = confdb['rule_logo']
    if not os.path.exists(logo):
        r = open(os.path.join(params.pathArt, 'logo.base64'), 'rb').read()
    else:
        r = base64.b64encode(open(logo, 'rb').read())
    return r.decode('utf8')
        

images_template_header = """
<html>
    <head>
        <style>
            body {
                background-color: white;
            }

            h1 {
                color: maroon;
            }
            h2 {
                color: black;
            }
            .title {
                vertical-align:middle;
                text-align: center;
                display: inline-block;
            }
            .logo {
                padding: 5%;
                display: inline-block;
            }
            .summary{
                padding: 2%;
                line-height: 200%;
            }
            table{
                margin: 10px;
            }
            table img{
                width: 150px;
            }

            td b {
                color: #ff0000;
            }

            .temperature {
                display: inline-block;
            }

            .time {
                float: right;
                margin-right: 0px;
                font-weight: bold;
            }

            .opt_label {
                display: inline-block;
                font-size: small;
            }

            .opt_value {
                float: right;
                margin-right: 0px;
                font-size: small;
            }

            a:link {
                color: #555;
                text-decoration: none;
            }

            a:visited {
                color: #555;
                text-decoration: none;
            }

            a:hover {
                color: #000;
                text-decoration: underline;
            }

            a:active {
                color: #555;
                text-decoration: underline;
            }
            
            #menu {
              position: fixed;
              right: 10px;
              top: 10px;
              width: 8em;
              overflow: hidden;
              background-color: #808080;
            }
            
            #menu a {
              float: left;
              display: block;
              color: black;
              text-align: center;
              padding: 14px 16px;
              text-decoration: none;
              font-size: 17px;
            }
            
            #menu a:hover {
              background-color: #ff0000;
              color: #f2f2f2;
            }


            @media print
            {    
                .no-print, .no-print *
                {
                    display: none !important;
                }
            }
        </style>
    </head>

    <body>
        <div>
            <div class='title-container'>
                <div class='logo'>
                    <img align=\"center\" src=\"data:image/png;base64,$LOGO$\" width=\"200\" alt=\"ELS logo\">
                </div>
                <div class='title'>
                    <h2>Heating Microscope</h2>
                    <h1>MDF</h1>
                    <p>
                        <small><a href=\"http://www.expertlabservice.it\">www.expertlabservice.it</a></small><br/>
                    </p>
                </div>
            </div>

            <div class='summary'>
                <div><strong>Code</strong>: $code$</div>
                <div><a name="title"><strong>Title</strong>: $title$</div>
                $sample$
                <div><strong>Date</strong>: $date$</div>
            </div>

   
"""

images_template_footer = """ 
        </div>
    </body>
</html>
"""

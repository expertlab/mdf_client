#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Versioning management utilities"""
import functools
from datetime import datetime
import os
from traceback import format_exc
from veusz.utils import pixmapAsHtml

from mdf_client.qt import QtWidgets, QtGui, QtCore

from mdf_canon.csutil import validate_filename, unicode_func
from mdf_canon.logger import get_module_logging
from mdf_client.widgets.active import RunMethod
logging = get_module_logging(__name__)

from .. import _
from .. import clientconf
from .. import iutils


class PlotSubMenu(iutils.QMenuWithToolTips):

    def __init__(self, version_menu, version, plot_id, title, date, render=False, render_format=False, parent=False):
        super(PlotSubMenu, self).__init__(parent=parent)
        print(type(title), type(date))
        self.setTitle(' - '.join((title, date)))
        self.version_menu = version_menu
        self.version = version
        self.plot_id = plot_id
        self._title = title
        self.date = date
        self.render = render
        self.render_format = render_format
        self.menuAction().hovered.connect(self.redraw)

    def redraw(self):
        self.clear()
        act = self.addAction(_('Load plot'), self.load_plot)
        if self.render and self.render_format:
            pix = QtGui.QPixmap()
            pix.loadFromData(self.render, self.render_format.upper())
            tooltip = "<html>{}</html>".format(pixmapAsHtml(pix))
            act.setCheckable(True)
            act.setChecked(self.version_menu.current_plot_id == self.plot_id)
            act.setToolTip(tooltip)
        if self.version == self.version_menu.current:
            self.addAction(
                _('Overwrite plot'), self.version_menu.overwrite_plot)
        self.addAction(_('Delete plot'), self.remove_plot)

    def load_plot(self):
        self.version_menu.load_plot(self.version, self.plot_id)

    def remove_plot(self):
        self.version_menu.remove_plot(self.version, self.plot_id)


def new_version(doc, proxy, parent=None, nosync=True, callback_save=lambda *a: 1):
    """Create a new version"""
    qid = QtWidgets.QInputDialog(parent)
    qid.setInputMode(0)
    qid.setWindowTitle(_('Version name'))
    qid.setLabelText(_('Choose a name for this version'))
    qid.setInputMethodHints(QtCore.Qt.ImhEmailCharactersOnly)
    st = qid.exec_()
    if not st:
        return False
    name = qid.textValue()
    try:
        proxy.create_version(unicode_func(name).encode('ascii', 'replace'))
        return save_version(doc, proxy, nosync=nosync, callback_save=callback_save)
    except:
        QtWidgets.QMessageBox.information(parent, _('Could not save version'),
                _('An error occurred while saving version:\n{}').format(format_exc()))
        return False
        
    return True


def save_version(doc, proxy, version_id=False, nosync=True, parent=None, callback_save=lambda *a: 1):
    """Save configuration in current version"""
    # Try to create a new version
    
    logging.debug('save_version', version_id)
    if not version_id:
        version_id = proxy.get_version()
    if version_id in ('', u''):
        logging.debug('Asking a new version name', repr(version_id))
        if not new_version(doc, proxy, nosync=nosync, parent=parent, callback_save=callback_save):
            QtWidgets.QMessageBox.critical(
                parent, _("Not saved"), _("Cannot overwrite original version"))
            return False
        return True
    logging.debug('save_version', repr(version_id))
    vers = proxy.get_versions()
    version_name, version_date = vers[version_id]
    pid = 'Save: {}'.format(version_name)
    
    thread = RunMethod(doc.save_version_and_plot, version_name, pid=pid)
    thread.pid = pid
    thread.notifier.done.connect(functools.partial(callback_save, version_name))
    if nosync:
        thread.do()
    else:
        logging.debug('Saving synchronously...')
        thread.run()
    return thread


class VersionMenu(QtWidgets.QMenu):

    """Available object versions menu"""
    versionChanged = QtCore.pyqtSignal(('QString'))
    plotChanged = QtCore.pyqtSignal(str, str)
    versionSaved = QtCore.pyqtSignal(str)
    versionRemoved = QtCore.pyqtSignal(str, str)  # old, new
    plotSaved = QtCore.pyqtSignal(str)
    current_plot_id = False
    doc = False
    _proxy = False

    def __init__(self, doc, proxy=False, parent=None):
        QtWidgets.QMenu.__init__(self, parent=parent)
        self.setTitle(_('Version'))
        self.doc = doc
        self._proxy = proxy
        self.aboutToShow.connect(self.redraw)

    @property
    def proxy(self):
        # TODO: submenu for each proxy in doc
        if self._proxy is not None:
            return self._proxy
        if not self.doc:
            return False
        return self.doc.proxy

    def load_plot(self, version, plot_id):
        if version != self.current:
            self.load_version(version, latest_plot=False)
        text, attrs = self.proxy.get_plot(plot_id)
        # Try to set the current version to the plot_id
        ver = attrs.get('version', False)
        # TODO: replace with tempfile
        tmp = 'tmp_load_file.vsz'
        open(tmp, 'wb').write(text)
        uid = self.proxy.get_uid()
        path = self.proxy.get_path()
        clientconf.confdb.known_uids[uid] = path
        self.doc.load(tmp)
        os.remove(tmp)
        self.current_plot_id = plot_id
        self.plotChanged.emit(self.current, plot_id)

    def save_plot(self, name=False, page=1):
        """Save overwrite plot in current name"""
        if not name:
            plot_id = self.current_plot_id or 'auto'
        else:
            plot_id = validate_filename(name, bad=[' '])
        r = self.doc.save_plot(self.proxy, plot_id, page, name)
        self.current_plot_id = plot_id
        self.redraw()
        self.plotSaved.emit(plot_id)
        return

    def load_latest_plot(self):
        """Search last occurence of plot with required version"""
        plots = self.proxy.get_plots()
        ok = []
        for plot_id, info in plots.items():
            info = list(info)
            info.append(plot_id)
            info[1] = datetime.strptime(info[1], "%H:%M:%S, %d/%m/%Y")
            ok.append(info)
        if not ok:
            logging.debug('No saved plots in current version', self.current)
            self.current_plot_id = False
            self.redraw()
            return False
        ok.sort(key=lambda el: el[1])
        ok = ok[-1]
        logging.debug('Loading latest plot for version', self.current, ok[-1])
        self.load_plot(self.current, ok[-1])

    def remove_plot(self, version=False, plot_id=False):
        """Delete current plot or plot_id folder structure"""
        if not plot_id:
            plot_id = self.current_plot_id
        if not version:
            version = self.current
        if not plot_id or not version:
            logging.debug('No current plot or version')
            return False
        node = self.proxy.versioned(
            '/plot/{}'.format(plot_id), version=version)
        self.proxy.remove_node(node, recursive=True)
        logging.debug('Removed plot', node)
        self.proxy.flush()
        return True

    def new_plot(self):
        """Create a new plot"""
        # TODO: ask for render and pagenumber
        title = _('Plot name')
        msg = _('Choose a name for this plot')
        name, st = QtWidgets.QInputDialog.getText(self, title, msg)
        if not st:
            return False
        r = self.save_plot(name)

    def overwrite_plot(self):
        self.save_plot(name=self.current_plot_id)

    def add_plot_menu(self, version, menu):
        if version == self.current:
            menu.addAction(_('Save new plot'), self.new_plot)
        p = self.proxy.get_plots(version=version, render=True)
        if not p:
            logging.debug('No plots for selected version', version)
            return False
        menu.addSeparator()
        for plot_id, (title, date, render, render_format) in p.items():
            pmenu = PlotSubMenu(self, version, plot_id, title,
                                date, render, render_format, parent=menu)
            menu.addMenu(pmenu)

    def redraw(self):
        self.clear()
        if not self.proxy:
            self.proxy.reopen(mode='r')
        vd = self.proxy.get_versions()
        logging.debug('Got info', vd)
        if vd is None:
            return
        self.current = self.proxy.get_version()
        logging.debug('Current version', self.current)
        self.loadActs = []
        for v, info in vd.items():
            logging.debug('Found version', v, info)
            p = functools.partial(self.load_version, v)
            vermenu = self.addMenu(' - '.join(info))
            vmact = vermenu.menuAction()
            act = vermenu.addAction(_('Load version'), p)
            act.setCheckable(True)
            if v == self.current:
                act.setChecked(True)
                vmact.setCheckable(True)
                vmact.setChecked(True)
            else:
                vmact.setCheckable(False)
            # Keep in memory
            self.loadActs.append((p, act))
            if v:
                vermenu.addAction(_('Delete version'),
                                  functools.partial(self.remove_version, v))
                vermenu.addAction(_('Overwrite version'),
                                    functools.partial(self.save_version, v))
                self.add_plot_menu(v, vermenu)
        if len(vd) > 0:
            self.addSeparator()
        act = self.addAction(_('New version and plot'), self.new_version)
        self.loadActs.append((self.new_version, act))
        self.actValidate = self.addAction(_('Check signature'), self.signature)

    def load_version(self, v, latest_plot=True):
        """Load selected version"""
        w = self.proxy.writable()
        if not w:
            self.proxy.reopen(mode='a')
        self.proxy.set_version(v)
        if not w:
            self.proxy.reopen(mode='r')
        v = self.proxy.get_version()
        self.current = v
        if latest_plot:
            self.load_latest_plot()
        logging.debug('load_version', v)
        self.doc.set_linked_version(self.proxy.get_path(), v)
        self.versionChanged.emit(v)

    def save_version(self, version_id=False, nosync=True):
        """Save configuration in current version"""
        self.thread = save_version(self.doc, self.proxy, version_id=version_id, nosync=nosync,
                                   callback_save=self.callback_save, parent=self)
        return self.thread is not False
        
    def callback_save(self, version_name):
        logging.debug('callback_save', version_name)
        self.current_plot_id = version_name
        self.current = self.proxy.get_version()
        self.versionSaved.emit(self.current)      

    def new_version(self):
        """Create a new version"""
        self.thread = new_version(self.doc, self.proxy, parent=self, callback_save=self.callback_save)
        return self.thread is not False

    def remove_version(self, version=False):
        """Delete current plot or plot_id folder structure"""
        # mode = self.proxy.get_mode()
        if not version:
            version = self.proxy.get_version()
        if not version:
            logging.debug('No current version can be removed')
            self.actRemove.setEnabled(False)
            return False
        try:
            self.proxy.remove_version(version)
            logging.debug('Removed version', version)
        except:
            logging.error('Could not remove version:', format_exc())
            QtWidgets.QMessageBox.information(self, _('Could not remove version'), format_exc())
            return False
        # Return to original version
        if version == self.current:
            self.load_version('')
        self.versionRemoved.emit(version, self.current)
        return True

    def signature(self):
        """Check file signature"""
        r = self.proxy.verify()
        if not r:
            QtWidgets.QMessageBox.critical(
                self, _("Signature check failed"), _("Test data cannot be trusted."))
        else:
            QtWidgets.QMessageBox.information(
                self, _("Signature check succeeded"), _("Test data is genuine."))

            
class MultiVersionMenu(QtWidgets.QMenu):

    def __init__(self, doc, parent=None):
        QtWidgets.QMenu.__init__(self, parent=parent)
        self.setTitle(_('Version'))
        self.doc = doc
        self.menuAction().hovered.connect(self.redraw)
    
    def redraw(self):
        self.clear()
        for fn, proxy in self.doc.proxies.items():
            m = VersionMenu(self.doc, proxy, self)
            m.setTitle(os.path.basename(fn))
            self.addMenu(m)

import os
from time import time
from collections import defaultdict
from traceback import format_exc
from mdf_canon.csutil import urlopen
from mdf_canon.logger import get_module_logging
from mdf_client.version import __version__
logging = get_module_logging(__name__)

from . import _
from . import widgets
from .clientconf import confdb
from .network import TransferThread
from .parameters import pathSync
from .cloud import get_version_file
from .live import registry

from .qt import QtWidgets

client_changelog_url = "https://gitlab.com/api/v4/projects/17325872/repository/commits?since="


class ServerUpdater(object):

    def __init__(self, server, parent):
        self.server = server
        self.parent = parent
        self.pkg = False
        self.outfile = False
        
    def __call__(self, *a):
        self.pkg = widgets.aFileList(self.server, self.server.support,
                          self.server.support.gete('packages'), parent=self.parent)
        self.pkg.hide()
        if self.pkg.send(filename=self.outfile): 
            self.pkg.transfer.dlFinished.connect(self.apply_update_server)
        
    def apply_update_server(self, *a):
        if self.pkg:
            self.outfile = self.pkg.transfer.outfile
        self.server.support['packages'] = os.path.basename(self.outfile)
        logging.info('Apply server update', self.server.support['packages'], self.outfile)
        
        btn = widgets.aButton(self.server, self.server.support,
                                self.server.support.gete('applyExe'), parent=self.parent)
        btn.hide()
        r = btn.get()
        logging.debug('applyExe replied', r)
        btn.show_msg()
        self.server.restart()
        logging.debug('Closing the client while server restarts.')
        self.parent.quit()

        
class ClientUpdater(object):

    def __init__(self, execfile):
        self.execfile = execfile
    
    def __call__(self):
        logging.debug('EXECUTING', self.execfile)
        import subprocess
        if os.name == 'nt':
            args = [self.execfile]
        else:
            args = ['x-terminal-emulator -e "bash {}"'.format(self.execfile)]
        p = subprocess.Popen(args, shell=True)
        logging.debug('DONE', p)
        from .iutils import app
        app.quit()
        
    
def fi(s0):
    """Return numeric version number"""
    # Extract MajorMinorDateTimeString integer from mdf-client-Major.Minor-DateTimeString-**.run
    s = s0.split('-')
    s = ''.join((s[2], s[3]))
    s = ''.join(c for c in s if c.isdigit())
    s = s.ljust(16, '0')
    s = int(s)
    print('fi', s0, '->', s)
    return s

# TODO: Local deploy
# http://192.168.0.118:1234/deploy


import tokenize


def tokenize_s3(response):
    """S3 xml index tokenizer"""
    b = response.read().split(b'<Key>')
    for toc in b:
        if b'</Key>' not in toc:
            continue
        yield toc.split(b'</Key>')[0].strip()

        
def tokenize_index(response):
    """Plain automatic html index tokenizer"""
    done = set([])
    for toc in tokenize.tokenize(response.readline):
        if toc.type != 3:  # string
            continue
        toc = toc.string
        if len(toc) < 10:
            continue
        toc = toc[1:-1]
        if toc not in done:
            done.add(toc)
            print(toc, repr(toc), type(toc))
            yield toc.encode('utf8')

        
fatal_connection_error = False


def get_packages(*serials):
    global fatal_connection_error
    if fatal_connection_error:
        return False
    # Recognize serials by directory name
    serial_frags = [(i, '/{}/mdf-'.format(s)) for i, s in enumerate(serials)]
    found = defaultdict(list)  # serial:[packages...]
    url = confdb['updateUrl']
    while url.endswith('/'):
        url = url[:-1]
    logging.debug('Update site:', url)
    try:
        response = urlopen(url)
    except:
        fatal_connection_error = True
        logging.error(format_exc())
        return False
    if 'packages.expertlabservice.it' in url:
        toker = tokenize_s3(response)
    else:
        toker = tokenize_index(response)
    for toc in toker:
        if toc[-4:] not in (b'.run', b'.exe', b'r.gz'):
            continue
        if toc[:4] != b'mdf-':
            continue
        if toc.startswith(b'mdf-server-') and toc.endswith(b'.run'):
            name = 'server:'
        elif toc.endswith(b'.run'):
            name = 'xclient:'
        elif toc.endswith(b'.exe'):
            name = 'client:'
        
        toc = toc.decode('utf8')
        notfound = True
        for i, s in serial_frags:
            if s in toc:
                found[name + serials[i]].append(toc)
                notfound = False
                logging.debug('found', name + serials[i], s, toc)
                break
        # Global update
        if notfound and ('/' not in toc):
            found[name].append(toc)
            logging.debug('notfound', name, toc)
    return found


def search_oldest_serial(vlatest, current=0):
    """Returns version integer and complete version name string"""
    newv, newlatest = False, False
    if not vlatest:
        return newv, newlatest
    for latest in vlatest:
        v = fi(latest)
        if v > current and v > newv:
            newv = v
            newlatest = latest
    return newv, newlatest


def get_best_client_version(conf, serials):
    """Get best client version compatible with the oldest server 
    appearing in recent cronology. Returns version integer and name."""
    if os.name != 'nt':
        tag = 'xclient:'
    else:
        tag = 'client:'
    ilatest_client, client = search_oldest_serial(conf.get(tag))
    iclient = ilatest_client
    
    ilatest_server, server = search_oldest_serial(conf.get('server:'))
    iserver = ilatest_server
    
    # Search latest version available for oldest serial getting an update
    oldest_serial = False
    # TODO: update to the list method
    for serial in serials:
        tag_serial = tag + serial
        if tag_serial  not in conf:
            logging.debug('Serial not found:', tag_serial)
            continue
        
        inew_server, new_server = search_oldest_serial(conf.get('server:' + serial))
        inew_client, new_client = search_oldest_serial(conf.get(tag_serial))
        
        # Keep oldest server
        if inew_server <= iserver or iserver == ilatest_server:
            server = new_server
            iserver = inew_server
            oldest_serial = serial
            logging.debug('Oldest server serial updated to:', oldest_serial)
            # Keep also the oldest associated client
            if inew_client < iclient or iclient == ilatest_client:
                iclient = inew_client
                logging.debug('Oldest client serial updated to:', new_client)
            else:
                logging.debug('Skipping client serial', new_client)
        else:
            logging.debug('Skipping serial', new_server, new_client)
        
    logging.debug('Oldest server serial:', oldest_serial)
    logging.debug('Found client:', client, iclient)
    logging.debug('Found server:', server, iserver)
    return iclient, client

    
def get_version_date(versionString, majorminor=''):
    current = -1
    for line in versionString.splitlines():
        if line.startswith('date'):
            line = line.split('=')[-1].replace(' ', '').replace(':', '').replace('-', '')
            current = fi('--' + majorminor + '-' + line[:-2])
            break
    logging.debug('get_version_string', versionString, current)
    return current

    
def get_tempdir():
    if os.name == 'nt':
        tempdir = os.path.join(os.path.join(os.environ['USERPROFILE']), 'Desktop') 
    else:
        tempdir = os.path.join(os.path.expanduser('~')) 
    return tempdir


def confirm_upgrade(old, new, new_name, parent=None):
    if old == -1:
        return False
    ch = ""
    if 'client' in new_name:
        ch = '<a href="https://gitlab.com/expertlab/mdf_client/-/commits/master/">View changelog</a>'
    
    msg = _("""Name: {}<br/>New: {}<br/>Current: {}<br/>{}<br/>Would you like to upgrade?""")
    msg = msg.format(new_name, new, old, ch)
    btn = QtWidgets.QMessageBox.information(parent, _('New version found'), msg,
                                      QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
    return btn == QtWidgets.QMessageBox.Ok


def check_server_updates(remote, parent=None):
    current = -1
    latest = -1
    # Take current last version
    sup = remote.support
    if not sup:
        return False
    if 'versionString' in sup:
        current = get_version_date(sup['versionString'], sup['version'])
    
    serial = remote['eq_sn']
    conf = get_packages(serial)
    if conf is False:
        return False
    vlatest = conf.get('server:' + serial, conf.get('server:', []))
    
    confdb['updateLastServer'] = time()
    confdb.save()
    
    if not vlatest:
        logging.error('No packages')
        return False 
    
    newv, latest = search_oldest_serial(vlatest, current)
    if not newv:
        logging.info('No update was found', current, vlatest)
        return False
    
    server_url = confdb['updateUrl'] + '/' + latest
    tempdir = get_tempdir()
    server_out = os.path.join(tempdir, os.path.basename(latest))
    
    if not confirm_upgrade(current, newv, latest, parent):
        logging.debug('Upgrade cancelled:', current, 'to', newv)
        return False
    
    logging.debug('Downloading server package', server_url)
    
    if not registry.taskswg:
        registry.set_manager()
        
    updater = ServerUpdater(remote, parent)
    updater.outfile = server_out
    tt = TransferThread(server_url, server_out)
    tt.set_tasks(registry.tasks)
    tt.dlFinished.connect(updater)
    tt.updater = updater
    tt.start()
    return tt

    
def update_from_source():
    from mdf_canon import determine_path
    from mdf_canon.csutil import go
    paths = []
    paths.append(os.path.dirname(determine_path(__file__)))
    from mdf_canon import __file__ as canonfile
    paths.append(os.path.dirname(determine_path(canonfile)))
    from veusz import __file__ as veuszfile
    paths.append(os.path.dirname(determine_path(veuszfile)))
    for path in paths:
        s, r = go('git -C "{}" status'.format(path))
        logging.debug(s, r)            
        logging.debug('Updating', path)
        s, r = go('git -C "{}" pull --rebase'.format(path))
        logging.debug(s, r)
    return True 


def check_client_updates(parent=None):
    # if os.name!='nt':
    #    return update_from_source()
    serials = []
    for recent in confdb['recent_server']:
        serials.append(recent[4])
        
    conf = get_packages(*serials)
    if conf is False:
        return False
    iclient, client = get_best_client_version(conf, serials)
    logging.debug('get_best_client_version', client, iclient)
    versionString = get_version_file()
    logging.debug('get_version_file', versionString)
    current = get_version_date(versionString, __version__)
    logging.debug('get_version_date', current)
    confdb['updateLastClient'] = time()
    confdb.save()
    
    if current >= iclient:
        logging.info('No update was found', current, client)
        return False
    if not confirm_upgrade(current, iclient, client, parent):
        logging.debug('Upgrade cancelled:', current, 'to', iclient)
        return False
    logging.debug('Updating', iclient, current)
    tempdir = get_tempdir()
    client_out = os.path.join(tempdir, os.path.basename(client))
    logging.debug('Saving to', client_out)
    url = confdb['updateUrl'] + '/' + client
    updater = ClientUpdater(client_out)
    tt = TransferThread(url, client_out)
    tt.dlFinished.connect(updater)
    tt.updater = updater
    tt.set_tasks(registry.tasks)
    tt.start()
    try:
        from mdf_canon import crystal
        crystal.ensure_stopped(pathSync)
    except:
        logging.debug('Cannot stop cloud service', format_exc())
    return tt
    

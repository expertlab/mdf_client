#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Pending tasks feedback"""
from traceback import format_exc
import functools
import threading
from mdf_canon.logger import get_module_logging
from mdf_canon.csutil import lockme
from mdf_client import _
from mdf_client.qt import QtWidgets, QtGui, QtCore, uconnect
logging = get_module_logging(__name__)


class RemoteTasks(QtWidgets.QWidget):
    server = False
    progress = False
    ch = QtCore.pyqtSignal()
    _lockme_error = False

    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent=parent)
        self.lay = QtWidgets.QVBoxLayout(self)
        self.setLayout(self.lay)
        self.setWindowTitle(_('Remote Pending Tasks'))
        self._lock = threading.Lock()

    def __len__(self):
        if self.progress is False:
            return 0
        r = len(self.progress.prog)
        return r

    def clear_layout(self):
        # Remove all items
        i = 0
        while self.lay.takeAt(0):
            i += 1
            continue
        if self.progress is not False:
            try:
                self.progress.hide()
                self.progress.unregister()
                self.lay.removeWidget(self.progress)
            except:
                logging.debug('While removing old progress... \n',
                    format_exc())
            finally:
                self.progress = False

    @lockme()
    def set_server(self, server):
        self.clear_layout()
        if not server:
            self.server = server
            return False
        self.server = server.copy()
        self.server.connect()
        server = self.server
        self.set_progress()
        return True
        
    def set_progress(self):
        server = self.server
        from ..widgets import RoleProgress
        prop = server.gete('progress')
        self.progress = RoleProgress(server, server, prop, parent=self)
        self.progress.force_update = True
        self.progress.label_widget.hide()
        self.lay.addWidget(self.progress)
        self.progress.sig_changed.connect(self.update, QtCore.Qt.QueuedConnection)
        self.progress.selfchanged.connect(self.update, QtCore.Qt.QueuedConnection)
        self.update()

    def update(self, *a, **k):
        len(self)
        self.ch.emit()


class LocalTasks(QtWidgets.QWidget):

    """Global server 'progress' option widget. This is a RoleIO pointing to the real 'Progress'-type option being performed"""
    ch = QtCore.pyqtSignal()
    sig_done = QtCore.pyqtSignal(str)
    sig_done0 = QtCore.pyqtSignal()
    sig_jobs = QtCore.pyqtSignal([int], [int, str, object])
    sig_job = QtCore.pyqtSignal([int], [int, str], [int, str, str])
    _lockme_error = False

    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent=parent)
        self._lock = threading.Lock()
        self.lay = QtWidgets.QHBoxLayout()
        self.setLayout(self.lay)
        self.setWindowTitle(_('Local Operations in Progress'))
        # Base widget
        self.bw = QtWidgets.QWidget(parent=self)
        self.blay = QtWidgets.QVBoxLayout()
        self.bw.setLayout(self.blay)
        self.log = QtWidgets.QTextEdit(parent=self)
        self.log.setReadOnly(True)
        self.log.setLineWrapMode(self.log.NoWrap)
        self.log.setFont(QtGui.QFont('TypeWriter', 7, 50, False))
        self.log.hide()
        self.more = QtWidgets.QPushButton(_('Log'), parent=self)
        self.more.clicked.connect(self.toggle_log)

        # Progress bars widget
        self.mw = QtWidgets.QWidget(parent=self)
        self.mlay = QtWidgets.QVBoxLayout()
        self.mw.setLayout(self.mlay)
        # Populate the base widget with progress widget, more button, log
        # window
        self.blay.addWidget(self.mw)
        self.blay.addWidget(self.more)
        self.blay.addWidget(self.log)

        self.lay.addWidget(self.bw)

        self.prog = {}
        uconnect(self.sig_jobs[int], self._jobs)
        uconnect(self.sig_jobs[int, str, object], self._jobs)
        uconnect(self.sig_job[int, str, str], self._job)
        uconnect(self.sig_job[int, str], self._job)
        uconnect(self.sig_job[int], self._job)
        uconnect(self.sig_done, self._done)
        uconnect(self.sig_done0, self._done)
        logging.debug('LocalTasks initialized')
        self.log_messages = ''

    def __len__(self):
        return len(self.prog)
    
    def __str__(self):
        r = {k: f'{wg.pb.value()}/{wg.pb.maximum()}' for k, wg in self.prog.items()}
        return str(r)

    def toggle_log(self):
        if self.log.isVisible():
            self.log.hide()
            self.mlay.removeWidget(self.log)
            return
        self.log.setPlainText(self.log_messages)
        self.log.show()

    def msg(self, msg):
        self.log_messages += msg + '\n'
        if self.log.isVisible():
            self.log.append(msg)
        logging.debug('LocalTasks.msg', msg)

    @lockme()
    def _jobs(self, tot, pid='Operation', abort=lambda *a, **k: 0):
        """Initialize a new progress bar for job `pid` having total steps `tot`."""
        wg = self.prog.get(pid, False)
        if wg:
            self.msg('New target: {}, {}/{}'.format(pid, wg.pb.value(), tot))
            wg.pb.setRange(0, tot)
            if not self.isVisible():
                self.ch.emit()
            return
        wg = QtWidgets.QWidget(parent=self)
        pb = QtWidgets.QProgressBar(parent=wg)
        pb.setRange(0, tot)
        pb.setValue(0)
        lbl = QtWidgets.QLabel(pid, parent=wg)
        btn = QtWidgets.QPushButton('X', parent=wg)
        wg._func_close = functools.partial(self.done, pid)
        wg._func_abort = abort  # keep a reference
        btn.clicked.connect(wg._func_close)
        btn.clicked.connect(abort)
        lay = QtWidgets.QHBoxLayout()
        lay.addWidget(lbl)
        lay.addWidget(pb)
        lay.addWidget(btn)
        wg.setLayout(lay)
        wg.lbl = lbl
        wg.btn = btn
        wg.pb = pb
        self.mlay.addWidget(wg)
        self.prog[pid] = wg
        self.ch.emit()
        self.msg('New task: {}, {}'.format(pid, tot))

    def jobs(self, tot, pid='Operation', abort=lambda *a, **k: 0):
        """Thread-safe call for _jobs()"""
        self.sig_jobs[int, str, object].emit(tot, pid, abort)
        
    def sort_jobs(self):
        """Deprecated"""
        all = list(self.prog.values())
        if not all:
            return
        for wg in all:
            self.mlay.removeWidget(wg)
        pval = lambda a: a.pb.value() / a.pb.maximum()
        all = sorted(all, key=pval, reverse=True)
        for wg in all:
            self.mlay.addWidget(wg)
        self.ch.emit()

    @lockme()
    def _job(self, step, pid='Operation', label=''):
        """Progress job `pid` to `step`, and display `label`. A negative step causes the bar to progress by 1."""
        wg = self.prog.get(pid, False)
        if not wg:
            logging.debug('LocalTasks.job: no job defined!', pid)
            return False
        if step < 0:
            step = wg.pb.value() + 1
        wg.pb.setValue(step)
        mx = wg.pb.maximum()
        if label != '':
            self.msg('{} {}/{}: {}'.format(pid, step, mx, label))
        if step > mx and step != 0:
            self._lock.release()
            logging.debug('LocalTasks._job beyond end step', step, mx)
            self._done(pid)
        return True

    def job(self, step, pid='Operation', label=''):
        """Thread-safe call for _job()"""
        self.sig_job[int, str, str].emit(step, pid, label)

    @lockme()
    def _done(self, pid='Operation'):
        """Complete job `pid`"""
        wg = self.prog.get(pid, False)
        if not wg:
            return False
        wg.hide()
        wg.close()
        del self.prog[pid]
        self.mlay.removeWidget(wg)
        del wg
        self.msg('Completed task: ' + str(pid))
        logging.debug('LocalTasks.done', str(pid), list(self.prog.keys()))
        self.ch.emit()
        return True

    def done(self, pid='Operation'):
        """Thread-safe call for _done()"""
        self.sig_done.emit(pid)
        self.sig_done0.emit()
        
    def failed(self, pid='Operation', reason='Unspecified Error'):
        wg = self.prog.get(pid, False)
        if not wg:
            return False
        msg = 'FAILED: {}, {}'.format(pid, reason)
        self.msg(msg)
        wg.lbl.setStyleSheet("color: red")
        wg.btn.setStyleSheet("background-color: red")
        wg.pb.setStyleSheet(wg.pb.styleSheet() + "\nQProgressBar::chunk:horizontal {background-color: red};")
        for el in [wg, wg.pb, wg.lbl, wg.btn]:
            el.setToolTip('FAILED TASK\n{}\n{}'.format(pid, '\n'.join(reason)))


class Tasks(QtWidgets.QTabWidget):
    user_show = False
    """Detect if the user asked to view this widget"""

    hide_signal = QtCore.pyqtSignal()
    show_signal = QtCore.pyqtSignal()

    _instance = None

    @classmethod
    def instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def __init__(self):
        QtWidgets.QTabWidget.__init__(self)
        self._lock = threading.Lock()
        self.setWindowTitle(_('Pending Tasks'))
        self.progress_area = QtWidgets.QScrollArea(self)
        self.progress_area.setWidgetResizable(True)
        self.progress = RemoteTasks(self)
        self.progress_area.setWidget(self.progress)
        self.addTab(self.progress_area, _('Remote'))

        self.tasks = LocalTasks(self)
        self.tasks_area = QtWidgets.QScrollArea(self)
        self.tasks_area.setWidgetResizable(True)
        self.tasks_area.setWidget(self.tasks)
        self.addTab(self.tasks_area, _('Local'))

        self.tasks.ch.connect(self.hide_show, QtCore.Qt.QueuedConnection)
        self.progress.ch.connect(self.hide_show, QtCore.Qt.QueuedConnection)
        self.setMinimumWidth(600)

    @lockme()
    def set_server(self, server):
        old = False
        if self.progress.server:
            old = self.progress.server.conn_addr
        if not server:
            self.progress.set_server(None)
            logging.debug('Tasks.set_server unset')
            return
        if server.conn_addr == old:
            print('Tasks.set_server unchanged', old)
            return
        print('Tasks.set_server', id(self), old, server.conn_addr)
        server = server.copy()
        server.connect()
        self.progress.set_server(server)
        if self.indexOf(self.progress_area) < 0:
            self.insertTab(0, self.progress_area, _('Remote'))

    def update_active(self):
        """Switch the active tab"""
        if not self.progress.server:
            print('update_active: disconnected')
            i = self.indexOf(self.progress_area)
            if i >= 0:
                self.removeTab(i)
            self.setCurrentIndex(0)
            return len(self.tasks) > 0
        p = 0
        if self.progress:
            p = len(self.progress)
        if p:
            self.setCurrentIndex(0)
            print('update_active progress', p)
        elif len(self.tasks):
            self.setCurrentIndex(1)
            print('update_active tasks', len(self.tasks), self.tasks)
        else:
            return False
        return True
    
    def set_user_show(self, value):
        self.user_show = value
        self.hide_show()

    def hide_show(self):
        """Decide if to automatically hide or show this window"""
        ua = self.update_active()
        # logging.debug('hide_show', ua, self.user_show)
        vis = self.isVisible()
        shown = None
        keep = (ua or self.user_show)
        if keep and not vis:
            if not self.parent():
                # DO NOT REMOVE
                logging.debug('hide_show without parent')
                return False
                # return False
            self.show()
            self.show_signal.emit()
            shown = True
            n = max(len(self.progress), len(self.tasks))
            self.setMinimumHeight(min(n, 10) * 50 + 150)
        elif (not keep) and vis:
            self.hide()
            self.hide_signal.emit()
            shown = False
        return shown

    def hideEvent(self, e):
        self.user_show = False
        return QtWidgets.QTabWidget.hideEvent(self, e)
    
    def showEvent(self, e):
        self.tasks.show()

    def closeEvent(self, e):
        self.user_show = False
        self.hide()
        return None

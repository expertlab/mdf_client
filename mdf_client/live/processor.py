#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Live data retrieve and processing"""
from traceback import format_exc, print_exc
from time import sleep, time
from mdf_canon.logger import get_module_logging
from mdf_canon.csutil import limit_freq
from mdf_client.qt import QtGui, QtCore
from mdf_canon.mem import MemBuffer, InvalidBufferEntry
from mdf_client.network.mproxy import InvalidStreamAddress
import setproctitle

logging = get_module_logging(__name__)


class FrameOpener(object):
    buffer = False

    def __init__(self, smp, compression):
        self.smp = smp
        self.compression = compression
        if smp.islocal:
            self.buffer = MemBuffer(readonly=True)
            p0 = '@' + smp['fullpath']
            self.buffer.path = self.buffer.resolve(p0) + 'frame/self'
        self.addr = self.smp.stream_address('frame')
        
    def __call__(self, scale=1):
        if self.buffer is not False:
            frame = self.local(scale)
        frame = self.smp.opener.open(self.addr + f'&scale={scale}').read()
        img = QtGui.QImage()
        img.loadFromData(QtCore.QByteArray(frame), self.compression)
        return img
    
    def local(self, scale):
        ret = self.buffer.read_current(self.buffer.path, -1)[-1]
        if isinstance(ret, InvalidBufferEntry):
            raise BaseException("Invalid frame")
        return ret
        

class FrameProcessor(QtCore.QThread):

    """Separate thread to get frames from webcams"""
    # TODO: move into beholder
    data = None
    pp = False
    stream = False
    analysis = False
    compression = 'PNG'
    fps = 15
    max_freq = 10
    damp_freq = 0.01
    timeout = 15
    readyImage = QtCore.pyqtSignal(int, int, int, int, int, QtGui.QImage)
    crop = QtCore.pyqtSignal(int, int, int, int)
    sig_fps = QtCore.pyqtSignal(float)
    count_freq = 10
    
    def __init__(self, cam, parent=None):
        self.params = {}
        QtCore.QThread.__init__(self, parent)
        self.cam = cam
        
    def run(self):
        # Create new cam object with connection unique to this thread.
        self.cam = self.cam.copy()
        self.cam.connect()
        self.setPriority(QtCore.QThread.IdlePriority)
        self.time = time()
        c = self.compression
        if 'compression' in self.cam:
            c = self.cam['compression']
        self.compression = c
        self.count = 0.
        self.scale = 1.0
        setproctitle.setthreadtitle(f"MdfClient FrameProcessor: {self.cam['fullpath']}")
        try:
            self.control_loop()
        except:
            logging.debug(format_exc())
            self.stream = False
        self.exit(0)
        
    _presets = []
    _openers = []

    @property
    def openers(self):
        if not self._openers:
            self._openers = [FrameOpener(smp, self.compression) for smp in self.cam.samples]
        return self._openers

    # @profile()
    def control_loop(self):
        frame_number = 0
        force = True
        crop = False
        
        tcam = self.cam
        new_api = 'Analysis_dummy' not in tcam
        t = 0
        freq = self.fps
        first_error = False
        while self.stream:
            if not self.parent().isVisible():
                sleep(0.1)
                continue
            fr = tcam.new_frame(frame_number, force)
            t1 = time()
            if first_error and t1 - first_error > self.timeout:
                logging.debug('Camera timed out', tcam['fullpath'])
                self.stream = False
                break
            if not fr:
                if first_error == 0:
                    first_error = t1
                sleep(0.1)
                continue
            if new_api:
                frame_number, freq, frs = fr
                self.fps = freq
            else:
                crop, frs, frame_number = fr
            if force:
                force = False
            if frs is False:
                first_error = t1
                limit_freq(t, t1, freq)
                t = t1
                continue
            first_error = False
            # Emit cropping region signal
            if not new_api:
                self.crop.emit(*crop)
            if len(frs) != len(self.openers):
                logging.debug('Number of frames differs from number of samples, restarting', len(frs), len(self.openers))
                self._openers = []
                sleep(1)
                continue
            n = -1
            r = 0
            if self.count == self.count_freq:
                self._openers = []
            for ent in frs:
                n += 1
                if not ent:
                    continue
                x, y, w, h = ent[:4]
                if len(ent) == 5:
                    fr = ent[-1].data
                else:
                    try:
                        img = self.openers[n](self.scale)
                    except InvalidStreamAddress:
                        logging.debug('Invalid stream address on sample', n, self.openers[n].addr)
                        print_exc()
                        sleep(1)
                        self._openers = []
                        continue
                    except:
                        logging.debug('While opening frame', n, self.openers[n].addr)
                        print_exc()
                        self._openers = []
                        sleep(.1)
                        continue
                self.readyImage.emit(n, x, y, w, h, img)
                r += 1
            # Update counter only for good frames
            if r > 0:
                self.count += 1.
                if self.count % self.count_freq == 0:
                    self.fps = self.count / (time() - self.time)
                    print('client fps', self.fps)
                    self.sig_fps.emit(self.fps)
                    self.time = time()
                    self.count = 0.
            lim = min(self.fps * (1 - self.damp_freq), freq, self.max_freq)
            s = limit_freq(t, t1, lim)
            if s:
                self.fps *= (1 + self.damp_freq)
                if self.fps > self.max_freq:
                    self.fps = self.max_freq
            t = t1

    def toggle_run(self, do=None):
        logging.debug('toggle_run', do, self.isRunning())
        if do == True:
            if not self.isRunning():
                self.stream = True
                self.start()
        elif do == False:
            if self.isRunning():
                self.stream = False
                self.wait(1000)
                self.quit()
        else:
            self.toggle_run(not self.isRunning())


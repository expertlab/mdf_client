#!/usr/bin/python
# -*- coding: utf-8 -*-
"""LIve management"""

from .tasks import RemoteTasks, LocalTasks,  Tasks
from .processor import FrameProcessor
from .sample_processor import SampleProcessor
from .kid_registry import KidRegistry

registry = KidRegistry()

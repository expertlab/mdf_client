#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Client-Server Synchronization"""
from time import time
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from time import sleep
import json
import threading
import collections 
from traceback import format_exc
import functools
import setproctitle
from mdf_canon.csutil import lockme
from mdf_canon.plugin.dataimport import NullTasks
from mdf_client.network import manager as net
from mdf_client.clientconf import confdb
from mdf_client.cloud import notify, notify_sound
from mdf_client.qt import QtCore
from .tasks import Tasks


# TODO: we should NEVER reference widgets here, just KIDs (except pending
# tasks)
class KidRegistry(QtCore.QThread):

    """Configuration keys registry"""
    proxy = False
    stream = False
    interval = 1
    """Update interval (s)"""
    tasks = False  # NullTasks()
    """Local tasks dialog"""
    # taskswg = False
    system_kids = set(['/isRunning'])
    """Set of forced update system kids. These are always updated."""
    # TODO: add queued_kids, a way to asynchronous one-time-update
    system_kid_changed = QtCore.pyqtSignal(str)
    sig_kid_changed = QtCore.pyqtSignal(str)
    """Signal emitted when a system kid changes."""
    cycle = QtCore.pyqtSignal()
    conn_error = QtCore.pyqtSignal(int)
    logMessage = QtCore.pyqtSignal(int, str)
    logMessages = QtCore.pyqtSignal(int, str)
    log = QtCore.pyqtSignal()
    sig_set_server = QtCore.pyqtSignal(object)
    """Signal emitted when connection errors counter changes."""
    connection_error_count = 0
    max_connection_errors = 0
    sig_updated_kid = QtCore.pyqtSignal(str)
    sig_notify = QtCore.pyqtSignal(object)
    _lockme_error = False

    def __init__(self):
        QtCore.QThread.__init__(self)
        self.rid = {}
        """Dict KID:[awg0,awg1,...]"""
        self.times = {}
        """Dict KID: last update server time"""
        self.values = {}
        """Dict KID: value"""
        self.obj = False
        """MisuraProxy"""
        self.curves = {}
        """Dict kid:curve"""
        self.ktime = 0
        """Last recorded kid"""
        self._lock = threading.Lock()
        self.__call__ = self.register
        self.log_buf = []
        self.log_time = 0
        self.updatingCurves = False
        self.doc = False
        self.manager = False
        self.should_update_doc = True

    @property
    def taskswg(self):
        return Tasks.instance()

    @lockme()
    def set_manager(self, man=None):
        old_stream = self.stream
        self.stream = False
        if self.isRunning():
            self.wait(1)
        logging.debug('KidRegistry.set_manager', id(self))
        try:
            self.sig_set_server.disconnect(self.taskswg.set_server)
        except:
            pass
        self.sig_set_server.connect(self.taskswg.set_server)
        unchanged = man and self.manager and self.manager.remote and (self.manager.remote.conn_addr == man.remote.conn_addr)
        self.manager = man
        if unchanged:
            logging.debug('KidRegistry.set_manager unchanged', man.remote)
        else:
            logging.debug('KidRegistry.set_manager with remote', man)
            self.sig_set_server.emit(man.remote)
        if old_stream:
            self.stream = True
            logging.debug('KidRegistry.set_manager: restarting...')
            self.start()

    @property
    def progress(self):
        if self.taskswg is False:
            return False
        return self.taskswg.progress
    
    @property
    def tasks(self):
        """Return local tasks widget"""
        if self.taskswg is False:
            return NullTasks()

        return self.taskswg.tasks

    @lockme()
    def set_doc(self, doc=False):
        """Install new document `doc`"""
        if not doc and self.doc:
            self.lastdoc = False
            logging.debug('KidRegistry.set_doc CLEARED')
        self.doc = doc

# FIXME: should be locked. It was unlocked for performance, but SHOULD BE LOCKED
#   @lockme()
    def register(self, w):
        """Register Active object `w`"""
        if w.type == 'Button':
            return False
        if not w.prop:
            logging.debug(
                'No property for active widget. Impossible to register', w.handle)
            return False
        kid = w.prop['kid']
        # Add to the dictionary
        if kid not in self.rid:
            self.rid[kid] = []
            self.times[kid] = 0
        if w in self.rid[kid]:
            return kid
        w.client_changed.connect(functools.partial(self.sig_updated_kid.emit, kid), QtCore.Qt.QueuedConnection)
        w.client_changed.connect(functools.partial(self.sig_kid_changed.emit, kid), QtCore.Qt.QueuedConnection)
        self.rid[kid].append(w)
        return kid

# FIXME: should be locked. It was unlocked for performance, but SHOULD BE LOCKED
#   @lockme()
    def unregister(self, widget):
        """Removes a widget from the registry."""
        if not widget.prop:
            return
        key_id = widget.prop['kid']
        if key_id in self.rid:
            if widget in self.rid[key_id]:
                self.rid[key_id].remove(widget)
            del widget
            if len(self.rid[key_id]) == 0:
                del self.rid[key_id]
                if key_id in self.times:
                    del self.times[key_id]

    @lockme()
    def clear(self):
        """Removes all registered objects."""
        self.rid = {}
        self.times = {}

    def _build_request(self):
        """Build a request for mapdate() based on valid and visible registered widgets."""
        request = []
        # Force progress update
        cur = self.obj.get('progress')
        run = self.obj['isRunning']
        if self.progress:
            if cur and not self.progress.progress:
                self.progress.set_progress()
            if self.progress.progress:
                self.progress.progress.sig_current.emit(cur)
        self.taskswg.hide_show()
        for kid, ws in self.rid.items():
            t = self.times.get(kid, 0)
            do = False
            # Search for valid entry and forced update
            for w in ws[:]:
                if w == self.progress.progress:
                    continue
                do = 1
                if w.force_update and run:
                    t = -1  # force mapdate to update this value
                break
            # Just progresses
            if not do:
                continue
            # Aggregate request
            request.append((kid, t))
        # Force special requests:
        for kid in self.system_kids:
            request.append((kid, self.times.get(kid, 0)))
        return request

    @lockme()
    def update_all(self):
        """Update registered objects."""
        updated = []
        r = []
        # Prepare request list
        request = self._build_request()
        # Call remote mapdate()
        if request:
            r = self.obj.mapdate(request)
        if not r or r is None:
            logging.debug('KidRegistry.update_all SKIPPING', r)
            return False
        idx, reply = r
        # Decode the reply
        nt = self.obj.time()
        upkids = []
        for i, j in enumerate(idx):
            nval = reply[i]
            kid, ot = request[j]
            self.times[kid] = nt
            # Apply the new value
            ws = self.rid.get(kid, [])
            # forget all widgets for this kid
            self.rid[kid] = []
            self.values[kid] = nval
            for w in ws:
                try:
                    w.sig_current.emit(nval)
                    updated.append(w)
                    upkids.append(kid)
                except:
                    continue
            if kid == '/progress' and self.progress:
                self.progress.sig_current.emit(nval)
            # Notify system kid changes
            if kid in self.system_kids:
                self.system_kid_changed.emit(kid)
            self.sig_kid_changed.emit(kid)
        # logging.debug('KidRegistry.update_all', len(updated), upkids)
        return updated

    def force_redraw(self, kids=False):
        if kids is False:
            kids = list(self.rid.keys())
        # Update all
        logging.debug('force_redraw', len(kids), kids)
        for kid in set(kids):
            for w in self.rid.get(kid, []):
                w.changedOption.emit()

    @lockme()
    def update_log(self):
        r = self.obj.search_log(self.log_time)
        if r is None:
            return False
        ltime, buf = r
        if ltime <= self.log_time:
            return True
        self.log_time = ltime
        self.log_buf = buf
        # Remove duplicate messages
        buf = set([(e[1], e[-1]) for e in buf])
        bylev = collections.defaultdict(str)
        # Emit one-by-one (for statusbar)
        for lev, msg in buf:
            self.logMessage.emit(lev, msg)
            bylev[lev] += '\n' + msg
        # Emit collectively (for systray)
        for lev, msg in bylev.items():
            self.logMessages.emit(lev, msg)
        self.log.emit()
        # logging.debug('KidRegistry.update_log', 
        #              len(buf), len(bylev))
        return True
    
    _already_notified = {}

    @lockme()
    def update_notify(self):
        tab = self.obj.get('log_notify', [])
        for notification in tab:
            t, event, title, msg, data = notification
            ta = self._already_notified.get(event, 0)
            if t <= ta:
                continue
            self.sig_notify.emit(notification)
            self._already_notified[event] = time()
            logging.debug('update_notify', data)
            data = json.loads(data)
            if data.get('cloud', False):
                notify(self.obj, event, title, msg, **data)
            if data.get('sound', True):
                notify_sound()
            # Cancel the event from the queue
            self.obj.client_notified(t)

    def setInterval(self, ms):
        """Change update interval"""
        self.interval = ms * .001

    def stop_updating_doc(self):
        self.should_update_doc = False

    def restart_updating_doc(self):
        self.should_update_doc = True

    def update_doc(self):
        if not self.should_update_doc:
            return True
        if self.doc and (self.doc is not self.lastdoc):
            if self.doc.proxy:
                logging.debug('Reconnecting to a proxy copy...')
                self.proxy = self.doc.proxy.copy()
                self.proxy.connect()
                self.lastdoc = self.doc
        # If a doc is registered and remote is running acquisition, update the
        # document
        if self.obj['isRunning'] and self.doc:
            self.doc.update(proxy=self.proxy)
        return True

    def control_loop(self):
        """Called while the registry is running."""
        self.loops += 1
        self.tstamp = time() 
        self.cycle.emit()
        if self.obj is False:
            if not self.manager:
                logging.debug('KidRegistry.control_loop: No manager registered.')
                return True
            if not self.manager.remote:
                # logging.debug('KidRegistry.control_loop: no remote manager')
                return True
            logging.debug('Reconnecting to manager.remote')
            self.obj = self.manager.remote.copy()
            self.obj.connect()
            self.obj._reg = self
            self.sig_set_server.emit(self.obj)
        if not net.connected:
            logging.debug('KidRegistry.control_loop: Not connected')
            return True
        
        if self.update_doc() is False:
            logging.debug('update_doc failed')
            return False
        
        if self.update_all() is False:
            logging.debug('update_all failed')
            return False
        
        if self.update_log() is False:
            logging.debug('update_log failed')
            return False
        if self.update_notify() is False:
            logging.debug('update_notify failed')
            return False
        return True

    loops = 0
    tstamp = 0

    def run(self):
        """Execution entry for the registry thread.
        Will call control_loop() until self.stream is True."""
        logging.debug('KidRegistry.run in new thread', len(self.rid), self.interval, self.loops)
        self.loops = 0
        self.setPriority(QtCore.QThread.IdlePriority)
        setproctitle.setthreadtitle(f'MdfClient KidRegistry {id(self)}')
        # t0 = time()
        self.stream = True
        self.lastdoc = False
        self.interval = confdb['refresh'] / 1000.
        while self.stream and self.interval:
            sleep(self.interval)
            try:
                # Everything good
                if self.control_loop():
                    self.connection_error(count=-1)
                else:
                    self.connection_error()
            except:
                logging.debug(format_exc())
                sleep(1)
                self.connection_error()
        logging.debug('KidRegistry.run END', self.stream)
        
    def really_running(self):
        if not self.isRunning():
            return False
        if time() - self.tstamp > 5:
            self.stream = False
            self.wait(1)
            self.quit()
            self.terminate()
            self.wait(1)
            logging.debug('really_running forced stop', self.isFinished())
            return False
        return True

    def toggle_run(self, auto=None):
        """Start/stop KID reading thread"""
        if auto == None:
            auto = self.stream ^ 1
        logging.debug('KidRegistry.toggle_run', auto)
        if auto == True:
            self.stream = True
            if not self.really_running():
                self.start()
                logging.debug('KidRegistry.toggle_run starting')
            else:
                logging.debug('KidRegistry.toggle_run already running')
        elif auto == False:
            self.stream = False
            if self.really_running():
                logging.debug('KidRegistry.toggle_run stopping')
                self.wait(1)
                self.quit()
                self.terminate()
                self.wait(1)
                print('FINISCHED?', self.isFinished())
            else:
                logging.debug('KidRegistry.toggle_run already stopped')

    def connection_error(self, count=1):
        """Increase or decrease connection error counter"""
        old = self.connection_error_count
        if count > 0:
            self.connection_error_count += count
        else:
            self.connection_error_count /= 2
            self.connection_error_count -= 1
        # Reset
        if self.connection_error_count <= 0:
            self.connection_error_count = 0
            self.max_connection_errors = 0
            self.tasks.done('Connection errors')
            return
        # Detect new error sequence
        if self.connection_error_count > self.max_connection_errors:
            self.max_connection_errors = self.connection_error_count
            self.tasks.jobs(self.connection_error_count, 'Connection errors')
        # Notify changes
        if old != self.connection_error_count:
            self.tasks.job(1 + self.max_connection_errors - 
                           self.connection_error_count, 'Connection errors')
            self.conn_error.emit(self.connection_error_count)

#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Live data retrieve and processing"""
from traceback import format_exc
from time import sleep, time

from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from mdf_canon.csutil import loads
from mdf_client import _
from mdf_canon.csutil import limit_freq
from mdf_client.qt import QtWidgets, QtGui, QtCore
import setproctitle


class SampleProcessor(QtCore.QThread):

    """Separate thread to get analysis results from sample objects"""
    # TODO: merge with FrameProcessor
    stream = False
    waiting = 0.5
    sig_updated = QtCore.pyqtSignal(int, object)
    
    def __init__(self, samples=[], parent=None):
        QtCore.QThread.__init__(self, parent)
        self.parent = parent
        self.set_samples(samples)
        self.opt = set()
        self.res = []

    def set_samples(self, samples):
        """Build a list of ROI"""
        self.quit()
        self.wait()
        self.samples = samples
        self.res = []
        logging.debug('set_samples', len(samples), samples)
        for i, smp in enumerate(self.samples):
            r = smp.multiget(list(self.opt))
            self.res.append(r)
            
    def run(self):
        self.setPriority(QtCore.QThread.IdlePriority)
        s = [s['fullpath'] for s in self.samples]
        s = '+'.join(s)
        setproctitle.setthreadtitle(f"MdfClient SampleProcessor: {s}")
        self.stream = True
        try:
            self.control_loop()
        except:
            logging.debug(format_exc())
        self.stream = 0
        self.exit(0)
            
#   @profile()
    def control_loop(self):
        if len(self.samples) == 0:
            return
        
        samples = []
        paths = []
        # Create an in-thread copy
        for smp in self.samples:
            smp1 = smp.copy()
            smp1.connect()
            samples.append(smp1)
            paths.append(smp1['fullpath'])
            
        while getattr(self, 'parent', False) and self.stream:
            try:
                if not self.parent.isVisible():
                    sleep(self.waiting)
                    continue
            except:
                break
            if len(self.samples) != len(samples):
                logging.debug('Samples number changed')
                return
            keys = list(self.opt - set(['roi', 'crop']))
            if not keys:
                sleep(self.waiting)
                continue
            for i, smp in enumerate(samples):
                r = smp.multiget(keys)
                if not r:
                    logging.debug('Live update failed: multiget returned', i, paths[i], r, keys, self.opt)
                    self.stream = False
                    break
                if 'profile' in r:
                    r['profile'] = loads(r['profile'].data)
                if 'filteredProfile' in r:
                    r['filteredProfile'] = loads(r['filteredProfile'].data)
                # logging.debug('SampleProcessor.sig_updated', i, r)
                self.sig_updated.emit(i, r)
            sleep(self.waiting)

    def toggle_run(self, do=None):
        #       logging.debug('SampleProcessor.toggle_run', do)
        if do == True:
            if not self.isRunning():
                self.stream = True
                self.start()
        elif do == False:
            if self.isRunning():
                self.stream = False
                self.wait(1000)
                self.quit()
        else:
            self.toggle_run(not self.isRunning())

#!/usr/bin/python
# -*- coding: utf-8 -*-
from mdf_client.widgets.active import ActiveWidget, MIN, MAX
from mdf_client.qt import QtCore,QtWidgets
from .. import _
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)

from mdf_client import units


class aDict(ActiveWidget):

    def __init__(self, server, path, prop, parent=None):
        ActiveWidget.__init__(self, server, path, prop, parent)
        self.map = {}
        self.cmap = {}
        # Cause update immediately after initialization
        self.selfchanged.emit()

    def adapt2srv(self, val):

        rmap = {}
        for key, spinbox in self.map.items():
            rmap[key] = spinbox.value()
        for key, textbox in self.cmap.items():
            rmap[key] = textbox.text()
        # Do unit conversion
        for key, val in rmap.copy().items():
            val = units.Converter.convert(self.prop['unit'].get(key,None), None, val)
            # Time correction
            if key == 'time' and 'Duration' not in self.handle:
                if val > self.server['zerotime']:
                    val += self.server['zerotime']
            rmap[key] = val
        return rmap

    def adapt2gui(self, vdict):
        # Do unit conversion
        for key, val in vdict.copy().items():
            # Time correction
            if key == 'time' and val != 'None' and 'Duration' not in self.handle:
                if val > self.server['zerotime']:
                    val -= self.server['zerotime']
            val = units.Converter.convert(self.prop['unit'].get(key,None), None, val)
            vdict[key] = val
        return vdict

    def update(self):
        self.clear()
        self.map = {}
        self.cmap = {}
        if self.current is None:
            logging.debug('No current value to update', self.label)
            return
        cur = self.adapt2gui(self.current)
        for key, val in cur.items():
            if type(val) == type(''):
                sb = QtWidgets.QLineEdit(val)
                if not self.readonly:
                    sb.textEdited.connect(self.set)
                self.cmap[key] = sb
                self.lay.addWidget(QtWidgets.QLabel(key.capitalize() + ':'))
                self.lay.addWidget(sb)
            else:
                sb = QtWidgets.QDoubleSpinBox()
                sb.setPrefix(key.capitalize() + ': ')
                min = MIN
                max = MAX
                if 'min' in self.prop:
                    min = self.prop['min'][key]
                if 'max' in self.prop:
                    max = self.prop['max'][key]
                sb.setRange(min, max)
                sb.setSingleStep(1)
                logging.debug('updating', self.label, val)
                sb.setValue(val)
                if not self.readonly:
                    sb.valueChanged.connect(self.set)
                self.map[key] = sb
                self.lay.addWidget(sb)
        self.set_enabled()

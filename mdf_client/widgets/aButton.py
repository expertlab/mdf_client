#!/usr/bin/python
# -*- coding: utf-8 -*-
from mdf_client.widgets.active import *
from .. import _
from mdf_client.qt import QtWidgets, QtGui
from mdf_client.iutils import informative_message_box


class aButton(ActiveWidget):
    get_on_enter = False
    get_on_leave = False
    """Do not auto-update when mouse enters."""

    def __init__(self, server, path, prop, parent=None):
        ActiveWidget.__init__(self, server, path, prop, parent)
        self.button = QtWidgets.QPushButton(self.tr(self.name))
        self.button.pressed.connect(self.async_get)
        self.lay.addWidget(self.button)
        self.set_enabled()
        self.sig_changed.connect(self.show_msg, QtCore.Qt.QueuedConnection)
        
    def register(self):
        return None

    def _msgBox(self):
        """Generate message box for display"""
        r = self.current
        if r is True:
            r = _('Done')
        elif r is False:
            r = _('Failed')
        msg = informative_message_box(r, self)
        msg.setText(_('Result for option "{}"').format(self.prop['name']))
        return msg
        
    def show_msg(self):
        """Display informative messagebox"""
        msgBox = self._msgBox()
        return msgBox.exec_()

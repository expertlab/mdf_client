#!/usr/bin/python
# -*- coding: utf-8 -*-
from mdf_client.qt import QtWidgets, QtGui, QtCore
from .active import ActiveWidget, getRemoteDev
from .. import _
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)


class Role(ActiveWidget):
    isIO = False
    bmenu_hide = False

    def __init__(self, *a, **k):
        ActiveWidget.__init__(self, *a, **k)
        self.isIO = self.type.endswith('IO')
        self.subwin = {}  # goto() subwindos mapping
        self.bmenu.setText('None')
        self.bmenu.setMaximumWidth(1000)
        self.emenu.addAction(_('Change'), self.edit)
        self.emenu.addAction(_('Unset'), self.unset)
        self.emenu.addAction(_('Go to'), self.goto)
        self.value = QtWidgets.QLabel('None')
        self.lay.addWidget(self.value)
        self.selfchanged.emit()

    def unset(self):
        self.set(['None', 'default'])

    def goto(self):
        p = self.current[0]
        if p in ['None', None]:
            return False
        # retrieve an existing win
        v = self.subwin.get(p, False)
        if v:
            v.show()
            return True
        from ..conf import mconf
        obj = self.remObj.root.toPath(self.current[0])
        v = mconf.TreePanel(obj, select=obj)
        v.show()
        self.subwin[p] = v
        return True

    def adapt2gui(self, val):
        """Convert the Role list into a string"""
        if self.isIO:
            return str(val)
        # Evaluate if to shorten the fullpath by adding an ellipsis at the
        # beginning
        if self.current in [None, 'None']:
            return 'None'
        val = val[:]
        d = len(val[0]) - 20
        if d > 4:
            val[0] = '...' + val[0][d:]
        r = '{}: {}'
        return r.format(*val)

    def update(self):
        """Update text in the button and help tooltip."""
        if self.current in [None, 'None']:
            self.bmenu.setText('None')
            self.bmenu.setToolTip('Error: Undefined value')
            return
        s = self.adapt2gui(self.current)
        self.bmenu.setText(s)
        tt = 'Object: {}\nPreset: {}'
        tt = tt.format(*self.current)
        self.bmenu.setToolTip(tt)
        self.value.setText(s)

    def edit(self):
        """Opens the Role editing dialog."""
        d = RoleDialog(self)
        return d.exec_()


class PointlessRoleError(RuntimeError):
    pass


class RoleIO(ActiveWidget):
    isIO = True
    value = None
    io_menu = False
    cached_opt = False
    cached_path = False
    pointless = True
    
    def __init__(self, *a, **k):
        ActiveWidget.__init__(self, *a, **k)
        self.subwin = {}  # goto() subwindos mapping
        self.ioact = self.emenu.addAction('None', self.edit)
        self.emenu.addAction(_('Unset'), self.unset)
        self.emenu.addAction(_('Go to'), self.goto)
        self.value = QtWidgets.QLabel('None')
        self.update()
        
    def register(self):
        if self.pointless:
            return super(RoleIO, self).register()
        
    @property
    def force_update(self):
        return False 
    
    @force_update.setter 
    def force_update(self, val):
        return False
    
    def update(self):
        """ Draw the referred widget"""
        self.prop = self.remObj.gete(self.handle)
        if not self.prop:
            self.pointless = True
            logging.warning('Cannot get option  ', self.handle)
            return
        opt = self.prop['options']
        if opt == self.cached_opt and self.cached_path:
            logging.debug('No change in role', self.handle)
            return False
        self.cached_opt = opt
        if opt[0] == '.':
            path = '.'
            obj = self.remObj
        else:
            path = self.server.searchPath(opt[0])
            obj = self.server.toPath(opt[0])
            logging.debug('RoleIO search', self.handle, opt[0], path, type(obj))
        if path == self.cached_path:
            logging.debug('No change in role pointer', self.handle)
            return False
        self.cached_path = path
        fu = False
        # Delete old widget
        if self.value:
            if hasattr(self.value, 'prop'):
                kid = opt[0] + opt[-1]
                if kid == self.value.prop['kid']:
                    # logging.debug('RoleIO: no update', kid)
                    return
                fu = self.value.force_update
            # Remove old widget
            self.lay.removeWidget(self.value)
            self.value.hide()
            self.value.close()
        # Recreate widget
        if path and obj and opt[2] not in ('None', None):
            from mdf_client.widgets import build
            refopt = False
            if opt[2] in obj:
                refopt = obj.gete(opt[2])
            if refopt is False or ('Hidden' in [refopt['type']] + refopt['attr']):
                self.label_widget.hide()
                self.hide()
                raise PointlessRoleError()
            value = build(self.server, obj, refopt)
            value.role_parent = self
            if not value:
                logging.info('Cannot build role', self.handle, opt)
                return
            self.value = value
            if self.io_menu:
                self.emenu.removeAction(self.io_menu)
            self.io_menu = self.emenu.addMenu(self.value.emenu)
            self.io_menu.setText(_('Referred option'))
            self.value.label_widget.hide()
            self.value.force_update = fu
            self.pointless = False
            # TODO: manage units and menu, which is bounded to label_widget
        else:
            logging.debug('Pointless RoleIO', path, obj, opt[2])
            self.value = QtWidgets.QLabel('None')
            self.pointless = True
            
        self.lay.addWidget(self.value)
        self.ioact.setText('{}: {} : {}'.format(*opt))

    def unset(self):
        self.remObj.setattr(
            self.handle, 'options', ['None', 'default', 'None'])
        self.update()

    def goto(self):
        cur = self.prop['options']
        p = cur[0]
        if p in ['None', None]:
            return False
        # retrieve an existing win
        v = self.subwin.get(p, False)
        if v:
            v.show()
            return True
        from ..conf import mconf
        obj = self.remObj.root.toPath(cur[0])
        v = mconf.TreePanel(obj, select=obj)
        v.show()
        self.subwin[p] = v
        return True

    def adapt2gui(self, val):
        """Convert the Role list into a string"""
        return str(val)

    def edit(self):
        """Opens the Role editing dialog."""
        d = RoleDialog(self)
        return d.exec_()


class RoleDialog(QtWidgets.QDialog):

    def __init__(self, parent, server=None, isIO=False):
        QtWidgets.QDialog.__init__(self, parent=parent)
        self.setWindowTitle(_('Select device playing role: ') + getattr(parent, 'label', ''))
        self.lay = QtWidgets.QVBoxLayout()
        self.setLayout(self.lay)
        if server:
            self.editor = SimpleRoleEditor(server, isIO)
        else:
            self.editor = RoleEditor(parent)
        self.lay.addWidget(self.editor)
        self.buttons = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok | QtWidgets.QDialogButtonBox.Cancel,
                                              parent=self)
        self.buttons.accepted.connect(self.accept)
        self.buttons.rejected.connect(self.reject)
        self.lay.addWidget(self.buttons)

    def accept(self, *a):
        self.editor.apply()
        return QtWidgets.QDialog.accept(self, *a)
    
    @property
    def selected(self):
        return self.editor.selected


class SimpleRoleEditor(QtWidgets.QWidget):
    selected = None
    current = [None, 'default']

    def __init__(self, server, isIO=False):
        QtWidgets.QWidget.__init__(self, parent=None)
        self.isIO = isIO
        self.server = server
        self.lay = QtWidgets.QFormLayout()
        self.setLayout(self.lay)
        from mdf_client.conf import ServerView
        self.tree = ServerView(server)
        self.lay.addRow(_('Select object'), self.tree)
        # preset chooser
        self.config = QtWidgets.QComboBox(parent=self)
        self.lay.addRow(_('Configuration preset'), self.config)
        # IO chooser
        if self.isIO:
            self.io = QtWidgets.QComboBox(parent=self)
            self.lay.addRow(_('Option name'), self.io)
        self.tree.selectionModel().currentChanged.connect(self.select)
        
    def update(self, cur=False, prev=False):
        # Set current values
        if cur:
            self.current = cur
        self.config.setCurrentIndex(0)
        devpath = self.current[0]
        objpath = False
        print(cur, self.current, devpath)
        if devpath:
            objpath = self.server.searchPath(devpath)
        logging.debug('server.searchPath', objpath, self.current)
        if not objpath:
            return
        # this will emit a currentChanged(), triggering select()
        self.tree.select_path(objpath)
        
    def select(self, cur=False, prev=False):
        self.remDev = False
        self.redraw_config(cur)
        conf = self.current[1]
        idx = self.config.findData(conf)
        self.config.setCurrentIndex(idx)
        if self.isIO:
            io = self.current[2]
            idx = self.io.findData(io)
            self.io.setCurrentIndex(idx)
            
    def redraw_config(self, idx):
        """Redraw the configuration combobox, reading the possible configurations for the current device."""
        self.config.clear()
        self.config.addItem('default', 'default')
        self.config.setCurrentIndex(0)
        path = self.tree.current_fullpath()
        st, self.remDev = getRemoteDev(self.server, path)
        logging.debug('redraw_config Got Remote Dev',
                      self.current[0], path, self.remDev)
        if st and (self.remDev is not None):
            for pre in self.remDev.listPresets():
                if pre == 'default':
                    continue
                self.config.addItem(pre, pre)
        if self.isIO:
            self.redraw_io()

    def redraw_io(self):
        """Redraw the IO combobox, reading the available IO list for the current device."""
        self.io.clear()
        self.io.addItem('None', 'None')
        if self.remDev:
            keys = sorted(self.remDev.keys())
            for pre in keys:
                logging.debug('IO Listing', pre)
                self.io.addItem(pre, pre)
    
    def apply(self):
        """Reads the comboboxes content and translated in a list to be sent to the server."""
        r, r1, r2 = ['None', 'default', 'None']
        r = self.tree.current_fullpath()

        r1 = self.config.itemData(self.config.currentIndex()) or 'default'
        if type(r1) == type(''):
            r1 = str(r1)

        if self.isIO:
            r2 = self.io.itemData(self.io.currentIndex())
            if type(r2) == type(''):
                r2 = str(r2)

        r = [r, r1]
        if self.isIO:
            r.append(r2)
        if None in r:
            raise BaseException('NoneRequest')
        self.selected = r
        return r   
    

class RoleEditor(SimpleRoleEditor):

    def __init__(self, role_widget):
        super(RoleEditor, self).__init__(role_widget.server, role_widget.isIO)
        self.w = role_widget
        self.update()

    @property
    def current(self):
        if self.isIO:
            return self.w.prop['options']
        return self.w.current

    def apply(self):
        """Reads the comboboxes content and translated in a list to be sent to the server."""
        r = super(RoleEditor, self).apply()
        
        if self.isIO:
            self.w.remObj.setattr(self.w.handle, 'options', r)
            self.w.update()
        else:
            self.w.set(r)
        

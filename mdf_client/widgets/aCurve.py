#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
from mdf_client.widgets.active import *
from .active import ActiveWidget
from .. import _
from .aProfile import Profile
from mdf_canon.logger import get_module_logging
from ..confwidget import RecentMenu
from mdf_canon import notebook
from mdf_canon.calibration import standards
logging = get_module_logging(__name__)


class aCurve(ActiveWidget):
    
    def redraw(self):
        super(aCurve, self).redraw()
        self.browser = QtWidgets.QLineEdit("")
        self.lay.addWidget(self.browser)
        self.browser.setReadOnly(True)
        self.browser.setText(self.current.get('label', '--->'))
        self.selfchanged.emit()
        self.set_enabled()
        
        self.file_actions = QtWidgets.QPushButton('...')
        self.file_actions.setMaximumWidth(40)
        
        self.file_menu = QtWidgets.QMenu() 
        
        self.m_recent_file = RecentMenu(confdb, 'file')
        self.m_recent_file.setTitle('Load from test')
        self.m_recent_file.select.connect(self.load_from_test)
        self.file_menu.addMenu(self.m_recent_file)
        
        self.m_recent_database = RecentMenu(confdb, 'database')
        self.m_recent_database.setTitle('Load from database')
        self.m_recent_database.select.connect(self.load_from_database)
        self.file_menu.addMenu(self.m_recent_database)
        
        self.file_menu.addAction(_('Unset'), self.unset)
        self.act_plot = self.file_menu.addAction(_('Plot'), self.plot)
        if not len(self.current['T']):
            self.act_plot.setEnabled(False)
        self.act_view = self.file_menu.addAction(_('View'), self.view_local_file)
        if not self.current.get('uid', False):
            self.act_view.setEnabled(False)
        self.file_actions.setMenu(self.file_menu)
        self.lay.addWidget(self.file_actions)
        
        self.selfchanged.emit()
        self.set_enabled()
        
    def unset(self):
        self.set({'T':[], 'v':[]})
        self.browser.setText('--->')
        
    def update(self):
        self.browser.setText(self.current.get('label', '--->'))
        return True
        
    def load_from_database(self, path):
        from ..database import getDatabaseWidget
        idb = getDatabaseWidget(path)
        if not idb:
            return False
        idb.show()
        idb.sig_selectedFile.connect(self.load_from_test)
        # Keep a reference
        self.idb = idb
    
    def load_from_test(self, filename):
        # TODO: load content from file and set current value
        out = {'filename':os.path.basename(filename)}
        f = notebook.MdfFile(filename)
        insobj = self.remObj.parent()
        ins = insobj['name']
        if f.instrument!=ins:
            QtWidgets.QMessageBox.warning(self, _('Wrong calibration instrument'),
                _('The selected file was measured with the instrument: "{}", ' \
                'which cannot calibrate the active instrument: "{}". '\
                'Please select a "{}" file').format(f.instrument, ins, ins))
            f.close()
            return False
        kiln = self.remObj.parent().parent().kiln
        cyc = kiln['curve']
        if f.cycle!=cyc:
            logging.error('The calibration thermal cycle differs from the current one', f.cycle, cyc)
            qm =QtWidgets.QMessageBox
            btn = qm.warning(self, _('Wrong thermal cycle'),
                _('The calibration thermal cycle differs from the currently configured ' \
                'thermal cycle. Please abort and select a calibration file with the same thermal cycle, or '\
                'apply the test\'s calibration cycle instead of the current one'), qm.Apply | qm.Abort, qm.Abort)
            if btn == qm.Abort:
                f.close()
                return False
            kiln['thermalCycle'] = 'CAL: '+f.name
            kiln['curve'] = f.cycle
        out['label'] = f.name + ' - ' + f.date
        out['uid'] = f.uid
        out['zerotime'] = f.zerotime
        out['curve'] = f.cycle
        
        d0 = insobj.sample0['initialDimension']
        crv = getattr(f.nb.summary, f.instrument).sample0.d.curve(zero=True, d0=d0).resample()
        out['T'] = list(map(float, crv.T))
        out['v'] = list(map(float, crv.v))
        flav = insobj.measure['flavour']
        out['flavour'] = flav
        if 'reference' in insobj.measure:
            out['reference'] = insobj.measure['reference']
        elif flav in standards:
            out['reference'] = standards[flav]
        f.close()
        self.act_plot.setEnabled(True)
        self.act_view.setEnabled(True)
        return self.set(out)
        
    def plot(self, filename=False):
        """Download currently selected file"""
        self.preview = Profile()
        self.preview.update(self.current['T'], self.current['v'])
        self.preview.show()
        
    def view_local_file(self):
        from .. import clientconf
        
        fn = clientconf.confdb.resolve_uid(self.current['uid'])
        if not fn or not os.path.exists(fn[0]):
            QtWidgets.QMessageBox.warning(self, _('Cannot find original file'),
                _('The file {}, with identifier {}\n  originally at path  {}\ncould not be found.').format(
                    self.current['label'], self.current['uid'], self.current['filename']))
            return False
        logging.debug('Showing original curve file', fn[0])
        
        from mdf_client import browser
        from mdf_client.filedata import MisuraDocument
        doc = MisuraDocument(fn[0])
        
        win = browser.TestWindow(doc)
        win.show()
        return True
        # self.current['uid']
        
               

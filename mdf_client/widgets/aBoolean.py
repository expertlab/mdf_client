#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
from .active import *
from .. import _


class aBoolean(ActiveWidget):

    def __init__(self, server, path, prop, parent=None):
        ActiveWidget.__init__(self, server, path, prop, parent)
        self.chk = QtWidgets.QCheckBox(_('False'), parent=parent)
        self.chk.setCheckable(True)
        self.lay.addWidget(self.chk)
        self.set_enabled()
        # Cause immediate update after complete init
        self.selfchanged.emit()
        if self.readonly:
            self.chk.clicked.connect(self.readonly_set)
        else:
            self.chk.stateChanged.connect(self.set)
            
    def readonly_set(self):
        logging.debug('Attempt to change a readonly boolean', self.handle, self.current)
        self.chk.setChecked(self.adapt(self.current))

    def adapt(self, val):
        if val in [0, '0', 'False', False, QtCore.Qt.Unchecked]:
            return False
        else:
            return True

    def update(self):
        if self.current:
            self.chk.setChecked(QtCore.Qt.Checked)
            self.chk.setText(_('True'))
        else:
            self.chk.setChecked(QtCore.Qt.Unchecked)
            self.chk.setText(_('False'))
        self.colorize()
        self.chk.update(self.chk.visibleRegion())
        
    def colorize(self, color=0):
        c = self.role_prop.get('color', color)
        c = c or -(self.handle in ('isRunning', 'analysis'))
        if c != 0:
            index = self.current
            if c < 0:  # invert colors: True=red, False=green
                index = not index
            c = ['red', 'green'][int(index)]
        return super().colorize(color=c)


class aBooleanAction(QtWidgets.QAction):

    def __init__(self, remObj, prop, parent=None):
        QtWidgets.QAction.__init__(self, prop['name'], parent)
        self.setCheckable(True)
        QtWidgets.QAction.setChecked(self, prop['current'])
        self.remObj = remObj
        self.prop = prop
        self.handle = prop['handle']
        self.hovered.connect(self.get)
        self.triggered.connect(self.set)
        if parent:
            parent.aboutToShow.connect(self.get)

    def get(self):
        r = self.remObj.get(self.handle)
        QtWidgets.QAction.setChecked(self, r)
        return r

    def set(self, val):
        self.remObj.set(self.handle, val)
        self.get()
    
    def toggle(self):
        cur = self.get()
        self.set(not cur)

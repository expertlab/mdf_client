#!/usr/bin/python
# -*- coding: utf-8 -*-
from time import time
from datetime import datetime
from collections import defaultdict
from mdf_client.widgets.active import QtWidgets, QtGui, QtCore, ActiveWidget
from mdf_client import _, iutils
from mdf_canon.csutil import unicode_decode
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)


class StructuredComposer(QtWidgets.QWidget):

    def __init__(self, parent=None, fmt="", current=[None, ]):
        # QtWidgets.QWidget.__init__(self, parent=parent)
        super(StructuredComposer, self).__init__(parent=parent)
        self.setLayout(QtWidgets.QHBoxLayout())
        self.current = current
        self.current_dict = {}
        for group, val in self.current:
            self.current_dict[group] = val
        self.groups = defaultdict(dict)
        from mdf_client.clientconf import confdb
        for el in confdb['rule_structuredGroups']:
            self.groups[el[1]][el[0]] = el[2]
            
        count = 0
        tot = fmt.count('<')
        self.builders = []
        while count < tot:
            i = fmt.find('<')
            if i < 0: 
                logging.debug('Could not find starting <')
                break
            pre = fmt[:i]
            if pre:
                self.add_label(pre)
            i += 1
            e = fmt.find('>', i)
            if e < 0:
                logging.debug('Could not find ending >')
                break
            count += 1
            key = fmt[i:e]
            e += 1
            fmt = fmt[e:]
            if '%' in key:
                self.add_label(datetime.now().strftime(key))
                continue
            self.add_group(key, self.current_dict.get(key, None))
            
    def add_label(self, txt):
        lbl = QtWidgets.QLabel(txt)
        self.builders.append(lambda: (None, lbl.text()))
        self.layout().addWidget(lbl)
            
    def add_group(self, group, selected=None):
        cb = QtWidgets.QComboBox(self)
        currentIndex = None
        for i, (key, val) in enumerate(self.groups[group].items()):
            cb.addItem(val + ' ({})'.format(key), key)
            if selected and key == selected:
                currentIndex = i
        if currentIndex is not None:
            cb.setCurrentIndex(currentIndex)
        f = lambda: (group, cb.itemData(cb.currentIndex()))
        self.builders.append(f)
        self.layout().addWidget(cb)
        
    def build(self):
        out = ''
        self.current = [self.current[0]]
        for b in self.builders:
            group, val = b()
            out += val 
            if group:
                self.current.append([group, val])
            logging.debug('build', b)
        return out
            
            
class StructuredDialog(QtWidgets.QDialog):

    def __init__(self, parent=None, fmt="", current=[None, ]):
        super(StructuredDialog, self).__init__(parent=parent)
        self.setWindowTitle(_('Structured name editing'))
        self.setLayout(QtWidgets.QVBoxLayout())
        self.composer = StructuredComposer(self, fmt=fmt, current=current)
        self.layout().addWidget(self.composer)
        
        qm = QtWidgets.QDialogButtonBox
        box = QtWidgets.QDialogButtonBox(self)
        box.addButton(_('Abort'), qm.DestructiveRole).clicked.connect(self.reject)
        app = box.addButton(_('Apply'), qm.ApplyRole)
        app.clicked.connect(self.accept)
        app.setDefault(True)
        
        self.layout().addWidget(box)

        
# if __name__=='__main__':
#    from mdf_client.tests import iutils_testing
#    iutils_testing.show(StructuredDialog(), __name__)
import re
structured_re = re.compile('/(measure|sample\d)/name$')


class SignalTextEdit(QtWidgets.QTextEdit):
    editingFinished = QtCore.pyqtSignal()

    def focusOutEvent(self, e):
        self.editingFinished.emit()
        return QtWidgets.QTextEdit.focusOutEvent(self, e)


class aString(ActiveWidget):

    """Graphical element for interacting with a text string"""
    
    def __init__(self, server, path, prop, parent=None, extended=False):
        self.extended = extended
        ActiveWidget.__init__(self, server, path, prop, parent)
    
    def redraw(self):
        super(aString, self).redraw()
        if self.extended:
            self.browser = SignalTextEdit()
        else:
            self.browser = QtWidgets.QLineEdit("")
            if "Password" in self.attr:
                self.browser.setEchoMode(QtWidgets.QLineEdit.Password)
        super(aString, self).redraw()
        self.browser.setReadOnly(self.readonly)
        self.browser.editingFinished.connect(self.apply_text)
        self.browser.textChanged.connect(self.text_updated)
        
        self.lay.addWidget(self.browser)
        if structured_re.search(self.prop['kid']):
            btn = QtWidgets.QPushButton(iutils.theme_icon('dialog-ok'), "", self)
            self.lay.addWidget(btn)
            btn.clicked.connect(self.show_structured_dialog)
        self.selfchanged.emit()
        self.set_enabled()
        
    def apply_text(self):
        self.text_updated()
        self.get()

    def adapt(self, val):
        """Enforce unicode everywhere"""
        if isinstance(val, int):
            return str(val)
        if isinstance(val, float):
            return str(val)
        return unicode_decode(val)

    def adapt2gui(self, val):
        """Returns a QString ready for the GUI"""
        return self.adapt(val)

    def adapt2srv(self, val):
        """Converts everything to unicode"""
        return self.adapt(val)

    def update(self):
        if self.browser.hasFocus():
            print('do not update while focused')
            return
        self.browser.textChanged.disconnect(self.text_updated)
        self.browser.setReadOnly(True)
        try:
            val = self.adapt(self.current)
            self.readonly_label.setText(val)
        except:
            logging.error('Invalid current for aString:', self.handle, self.remObj['fullpath'],
                          type(self.current), self.current)
            return False
        if self.extended:
            self.browser.setPlainText(val)
        else:
            self.browser.setText(val)
        if self.readonly:
            self.browser.setReadOnly(True)
            self.browser.hide()
            self.readonly_label.show()
        else:
            self.browser.setReadOnly(False)
            self.browser.show()
            self.readonly_label.hide()
        self.browser.textChanged.connect(self.text_updated)
        return True

    def text_updated(self, *foo):
        if self.extended:
            self.current = self.browser.toPlainText()
        else:
            self.current = self.browser.text()
        self.remObj.set(self.handle, self.current)
        
    def show_structured_dialog(self, fmt):
        from mdf_client.clientconf import confdb
        if self.prop['kid'].endswith('/measure/name'):
            fmt = confdb['rule_structuredFormat']
        else:
            fmt = confdb['rule_structuredSample']
        current = [None, ]
        if 'structuredName' in self.remObj:
            current = self.remObj['structuredName']
        dia = StructuredDialog(self, fmt, current)
        if dia.exec_():
            val = dia.composer.build()
            logging.debug('Setting structured name:', val)
            self.set(val)
            if current[0]:
                self.remObj['structuredName'] = dia.composer.current 
        

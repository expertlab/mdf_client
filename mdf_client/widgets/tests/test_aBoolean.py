#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Tests aButton widget."""
import unittest
from mdf_canon.logger import Log as logging
from mdf_client.tests import iutils_testing
from mdf_client import widgets
from mdf_canon import option
from mdf_client.qt import QtWidgets, QtGui


class aBoolean(unittest.TestCase):

    def setUp(self):
        self.root = option.ConfigurationProxy()

    def wgGen(self):
        self.assertTrue('test' in self.root)
        widget = widgets.build(self.root, self.root, self.root.gete('test'))
        # The current value is not initialized (gete() returns {current:None} )
        self.assertTrue(widget is not False)
        return widget

    def test(self):
        self.root.sete('test', option.ao({}, 'test', 'Boolean', False, attr=['ReadOnly'])['test'])
        # Test with short reply
        widget = self.wgGen()
        iutils_testing.show(widget, __name__)
        
        


if __name__ == "__main__":
    unittest.main(verbosity=2)

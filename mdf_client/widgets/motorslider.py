#!/usr/bin/python
# -*- coding: utf-8 -*-
from mdf_client.qt import QtWidgets, QtGui, QtCore
from .aNumber import aNumber
from .active import ActiveObject
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from .. import _


class MotorSlider(aNumber):
    started = 0
    target = 0
    position = 0
        
    def __init__(self, server, remObj, parent=None):
        prop = remObj.gete('goingTo')
        if prop is None:
            logging.critical('Error retrieving MotorSlider goingTo', remObj['fullpath'], prop)
            return 
        
        super(MotorSlider, self).__init__(server, remObj, prop, parent)
        
        self.pos_obj = ActiveObject(
            server, remObj, remObj.gete('position'), parent=self)
        self.pos_obj.selfchanged.connect(self.update_position, QtCore.Qt.QueuedConnection)
        if self.slider:
            self.slider.setTracking(False)
            self.slider.setFocusPolicy(QtCore.Qt.ClickFocus)
            self.slider.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
            self.slider.customContextMenuRequested.connect(self.showMenu)
            self.slider.wheel.connect(self.slot_slider_wheel)
        self.label_widget.setSizePolicy(QtWidgets.QSizePolicy.Maximum,
                                        QtWidgets.QSizePolicy.Maximum)
        self.menu = QtWidgets.QMenu()
        self.spinact = QtWidgets.QWidgetAction(self.menu)
        self.spinact.setDefaultWidget(self.spinbox)
        self.labelact = QtWidgets.QWidgetAction(self.menu)
        self.labelact.setDefaultWidget(self.label_widget)
        self.menu.addAction(self.spinact)
        self.menu.addAction(self.labelact)
        self.menu.addAction(self.zoomact)
        self.zeroact = self.menu.addAction(_('Find zero'), self.find_zero)
        
        self.menu.addAction(self.arrow_act)
        # self.menu.addAction(self.invert_act)
        self.toggle_arrows(True)
        if self.server._readLevel >= 4:
            self.cfact = self.menu.addAction(_('Configure'), self.hide_show)
            # self.cfact.setCheckable(True)
            if self.remObj['preset'] != 'factory_default':
                self.save_act = self.menu.addAction(_('Save preset'), self.save_preset)
            
        self.cf = False
        
    def find_zero(self):
        self.remObj['findLow']
        self.remObj['wait']
        self.update_option()
        self.changed_option()
        
    def set(self, val, **foo):
        if val <= 0 and self.current != 0:
            self.find_zero()
            return 0
        return super(MotorSlider, self).set(val, **foo)
        
    def slot_slider_wheel(self, e):
        print('WHEEL', e.globalX(), e.globalY(), e.angleDelta().y())
        print('step', self.step, self.divider)
        print('singleStep', self.slider.singleStep(), self.slider.pageStep())
    
    def hide_show(self):
        """Hide/show configuration dialog"""
        from .. import conf
        self.cf = conf.Interface(self.server, self.remObj)
        self.cf.show()
            
    def save_preset(self):
        self.remObj.set_to_preset(self.handle,
                                   self.remObj['preset'],
                                   self.remObj[self.handle])

    def showMenu(self, pt):
        self.update()
        self.menu.popup(self.mapToGlobal(pt))

    def enterEvent(self, e):
        self.update()
        return QtWidgets.QWidget.enterEvent(self, e)

    def setOrientation(self, direction):
        r = aNumber.setOrientation(self, direction)
        # TODO: merge in aNumber?
        if self.slider:
            self.lay.setContentsMargins(0, 0, 0, 0)
            self.lay.setSpacing(0)
            self.slider.setContentsMargins(0, 0, 0, 0)
        return r

    def update_position(self, pos):
        self.update(position=pos)

    def update(self, *a, **k):
        if self.slider and self.slider.paused:
            return False
        s = k.pop('position', None)
        if s:
            k['minmax'] = False
        r = aNumber.update(self, *a, **k)
        if not hasattr(self, 'menu'):
            # MotorSlider not fully initialized
            return r
        # If 'position' argument was not passed,
        # force pos_obj to re-register itself
        if s is None:
            self.pos_obj.register()
            s = self.position  # keep old value
        d = abs(1. * self.current - self.started)
        if self.target != self.current:
            self.target = self.current
            self.started = s
        elif self.current == s or d == 0:
            self.started = s
            d = 0
        if d <= 5 * self.step:
            self.started = s
            s = 100
        else:
            s = 100 * (1 - abs(self.current - s) / d)
        msg = '%i%%' % abs(s)
        self.label_widget.setText(msg)
        return r


class MotorSliderAction(QtWidgets.QWidgetAction):

    def __init__(self, server, remObj, parent=None):
        QtWidgets.QWidgetAction.__init__(self, parent)
        self.wdg = MotorSlider(server, remObj, parent=parent)
        self.setDefaultWidget(self.wdg)

    def showEvent(self, event):
        self.wdg.get()
        return QtWidgets.QWidgetAction.showEvent(self, event)

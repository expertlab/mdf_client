#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Date and Time widget"""

from time import time
from mdf_client.qt import QtWidgets, QtGui, QtCore
from .active import ActiveWidget
from .. import _

from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)


class aTime(ActiveWidget):

    """Date and time widget"""
    delta = 0
    """Local time - remote time delta."""

    def __init__(self, server, path, prop, parent=None):
        ActiveWidget.__init__(self, server, path, prop, parent)
        
    def redraw(self):
        super(aTime, self).redraw()
        self.twg = QtWidgets.QDateTimeEdit(parent=self)
        self.lay.addWidget(self.twg)
        # Cause immediate update after complete init
        self.selfchanged.emit()
        self.twg.setReadOnly(self.readonly)
        if not self.readonly:
            # TODO: add calendar widget if not editing.
            self.twg.dateTimeChanged.connect(self.edited)
        if self.server and self.server.is_live():
            t = time()
            s = self.server.time()
            dt = time() - t
            self.delta = (t - s) + (dt / 3.)

    def adapt2gui(self, val):
        return ActiveWidget.adapt2gui(self, val + self.delta) * 1000

    def adapt2srv(self, val):
        return ActiveWidget.adapt2srv(self, val - self.delta)

    def update(self):
        self.twg.blockSignals(True)
        dt = QtCore.QDateTime()
        dt.setMSecsSinceEpoch(int(self.adapt2gui(self.current)))
        self.twg.setDateTime(dt)
        self.twg.blockSignals(False)
        self.set_enabled()

    def edited(self, qdt=None):
        if qdt is None:
            qdt = self.twg.dateTime()
        t = qdt.toMSecsSinceEpoch() / 1000.
        self.set(t)


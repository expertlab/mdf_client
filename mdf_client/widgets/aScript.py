#!/usr/bin/python
# -*- coding: utf-8 -*-
from mdf_client.qt import QtWidgets, QtGui, QtCore
from mdf_canon.csutil import unicode_func
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from mdf_client.widgets.active import QtCore, QtWidgets, ActiveWidget
from .. import _
from . import highlighter


class ScriptEditor(QtWidgets.QDialog):

    """A simple script editing dialog"""

    def __init__(self, active):
        QtWidgets.QDialog.__init__(self, active)
        self.lay = QtWidgets.QVBoxLayout()
        self.setLayout(self.lay)

        self.active = active
        self.setWindowTitle(_("Script Editor"))
        self.menu = QtWidgets.QMenuBar(self)
        self.menu.setMinimumHeight(25)
        self.menu.addAction(_('Validate'), self.validate)
        act = self.menu.addAction(_('Save'), self.save)
        if self.active.readonly:
            act.setEnabled(False)
        self.area = QtWidgets.QTextEdit()
        self.area.setReadOnly(False)
        self.area.setPlainText(self.active.adapt2gui(self.active.current))
        highlighter.PygmentsHighlighter(self.area)

        self.lay.addWidget(self.menu)
        self.lay.addWidget(self.area)

        self.accepted.connect(self.save)

    @property
    def current(self):
        """Returns the last value got from remote"""
        return self.area.toPlainText()

    def validate(self):
        """Just validate the script, without saving it remotely. Highlight errors."""
        val = unicode_func(self.current)
        err, line, col = self.active.remObj.validate_script_text(val)
        if line < 0:
            return True
        msg = "%s\n%s\n%s %i, col: %i" % (_("Validation failed for the following reason:"),
                                          err, _("Found at line:"), line, col)
        QtWidgets.QMessageBox.warning(self, _("Validation error"), msg)
        pos = 0
        line -= 1
        for i, ent in enumerate(val.splitlines()):
            if i == line:
                pos += col
                break
            pos += len(ent) + 1
        cur = self.area.textCursor()
        cur.setPosition(pos)
        self.area.setTextCursor(cur)
        return False

    def save(self):
        """Saves the current version of the script remotely."""
        if self.active.readonly:
            logging.error('Script is readonly. Cannot save.', self.active.handle)
            return False
        self.active.set(self.current)
        self.area.setPlainText(self.active.adapt2gui(self.active.current))
        return True


class aScript(ActiveWidget):

    """Elemento grafico per una proprietà stringa di testo"""

    def redraw(self):
        super(aScript, self).redraw()
        txt = _('View') if self.readonly else _('Edit')
        self.button = QtWidgets.QPushButton(txt, self)
        self.button.clicked.connect(self.show_editor)
        self.lay.addWidget(self.button)
        self.set_enabled()

    def show_editor(self):
        e = ScriptEditor(self)
        e.exec_()

    def adapt(self, val):
        """I valori in ingresso ed in uscita devono sempre essere unicode"""
        return unicode_func(val)

    def adapt2gui(self, val):
        return unicode_func(val)
    
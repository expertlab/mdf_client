#!/usr/bin/python
# -*- coding: utf-8 -*-
from functools import reduce


def maximize_visible(parent_widget):
    [(win.show(), win.showMaximized(), win.widget().show()) for win in parent_widget.subWindowList() if win.isVisible()]


def visible_windows_of(widget):
    return [win for win in widget.subWindowList() if win.isVisible()]


standard_names = {'main':'camera'}


def key_from_window_title(title):
    for key in ['main', 'camera2', 'left', 'right', 'height', 'base', 'flex', 'flex3', 'center', 'surface', 'camera']:
        if key in title.lower():
            return standard_names.get(key, key)
    return 'default'


def get_geometries(parent_widget):
    parent_geometry = parent_widget.geometry()
    width = parent_geometry.width()
    heigth = parent_geometry.height()

    subwindows_width = width / 2
    subwindows_heigth = heigth / 2

    return width, heigth, subwindows_width, subwindows_heigth


def arrange_windows_with_sizes(windows, sizes):
    for win in windows:
        size = sizes.get(key_from_window_title(win.windowTitle()))
        win.setGeometry(*map(int, size))


def arrange_generic(parent_widget, get_sizes_function):
    arrange_default(parent_widget)
    visible_windows = visible_windows_of(parent_widget)
    number_of_visible_windows = len(visible_windows)
    if number_of_visible_windows == 0:
        return
    elif number_of_visible_windows == 1:
        visible_windows[0].showMaximized()
        return
        
    has_cameras = reduce(lambda  acc, cur: acc or 'Camera' in cur.windowTitle(),
                         visible_windows,
                         False)
    
    if number_of_visible_windows > 3 or not has_cameras:
        arrange_default(parent_widget)
        return

    arrange_windows_with_sizes(
        visible_windows,
        get_sizes_function(*get_geometries(parent_widget))
    )

    
def arrange_default(parent_widget):
    maximize_visible(parent_widget)
    parent_widget.tileSubWindows()
    
    
def arrange(parent_widget, instrument_name):
    arrange_functions = {
        'horizontal': arrange_dilatometer,
        'vertical': arrange_dilatometer,
        'flex': arrange_default,
        'flex3': arrange_flex3,
        'hsm': arrange_hsm,
        'default': arrange_default,
    }

    arrange_function = arrange_functions.get(instrument_name, arrange_default)
    arrange_function(parent_widget)

    
def arrange_hsm(parent_widget):

    def get_sizes_function(width, heigth, subwindows_width, subwindows_heigth):
        return {
            'camera': [0, 0, subwindows_width, subwindows_heigth],
            'camera2': [subwindows_width, 0, subwindows_width, subwindows_heigth],
            'left': [0, 0, subwindows_width, subwindows_heigth],
            'right': [subwindows_width, 0, subwindows_width, subwindows_heigth],
            'default': [0, subwindows_heigth, width, subwindows_heigth]
        }

    arrange_generic(parent_widget, get_sizes_function)

    
def arrange_dilatometer(parent_widget):
    
    def get_sizes_function(width, heigth, subwindows_width, subwindows_heigth):
        return {
            'left': [0, 0, subwindows_width, subwindows_heigth],
            'right': [subwindows_width, 0, subwindows_width, subwindows_heigth],
            'height': [0, 0, subwindows_width, subwindows_heigth],
            'base': [subwindows_width, 0, subwindows_width, subwindows_heigth],
            'default': [0, subwindows_heigth, width, subwindows_heigth],
            'surface': [0, subwindows_heigth, width, subwindows_heigth],
        }
    
    arrange_generic(parent_widget, get_sizes_function)


def arrange_flex3(parent_widget):

    def get_sizes_function(width, heigth, subwindows_width, subwindows_heigth):
        return {
            'left': [subwindows_width, 0, subwindows_width, subwindows_heigth],
            'right': [subwindows_width, subwindows_heigth, subwindows_width, subwindows_heigth],
            'center': [0, 0, subwindows_width, heigth],
        }

    arrange_generic(parent_widget, get_sizes_function)


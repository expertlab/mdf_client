#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Delayed test start dialog"""
from time import time
import sys
from .. import widgets
from mdf_client.qt import QtWidgets, QtGui, QtCore
from .. import _

from .messages import initial_sample_dimension


class DelayValidator(object):

    def __init__(self, func):
        self.func = func

    def __call__(self, val):
        if val and not self.func():
            return 0
        return val


class DelayedStart(QtWidgets.QDialog):

    def add_option(self, handle):
        if handle not in self.server:
            return False
        wg = widgets.build(self.server, self.server, self.server.gete(handle))
        wg.lay.insertWidget(0, wg.label_widget)
        self.lay.addWidget(wg)
        return wg
    
    def __init__(self, parent):
        QtWidgets.QDialog.__init__(self, parent)
        server = parent.server
        self.server = server
        self.ins = getattr(server, server['lastInstrument'])

        self.setWindowTitle('Delayed Test Start')
        self.lay = QtWidgets.QVBoxLayout()
        
        self.eng = self.add_option('delayStart')
        self.eng.validator = DelayValidator(self.parent().controls.validate)
        
        self.delayT = self.add_option('delayT')
        self.sequenceIndex = self.add_option('sequenceIndex')
        
        self.run = QtWidgets.QLabel(
            _('Target instrument: ') + server['lastInstrument'].capitalize())
        self.lay.addWidget(self.run)

        self.op = QtWidgets.QLabel(_('Operator: {}').format('------'))
        self.lay.addWidget(self.op)

        self.quit = QtWidgets.QPushButton(_("Save and exit"), parent=self)
        self.quit.clicked.connect(self.save_exit)
        self.lay.addWidget(self.quit)
        self.abrt = QtWidgets.QPushButton(_("Abort delayed start"), parent=self)
        self.abrt.clicked.connect(self.reject)
        self.lay.addWidget(self.abrt)

        self.setLayout(self.lay)
        self.rejected.connect(self.unset)
        self.setWindowModality(QtCore.Qt.WindowModal)

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.update)
        self.timer.start(1000)
        
    def keyPressEvent(self, ev):
        if ev.key() == QtCore.Qt.Key_Enter:
            ev.ignore()
            return
        return QtWidgets.QDialog.keyPressEvent(self, ev)

    def unset(self, *a):
        """Disable delayed start on exit"""
        self.eng.set(0)
        self.quit.setEnabled(False)

    def save_exit(self):
        """Completely close the client"""
        if self.eng.current:
            btn = QtWidgets.QMessageBox.warning(self, "Delayed start is active",
                "You are exiting from the client application, \nbut a delayed start will remain active on the instrument.")
            self.accept()
            sys.exit(0)
            return
        self.reject()

    def update(self):
        # Exit when acquisition starts
        if self.server['isRunning']:
            self.accept()
            return
        if not self.server['lastInstrument']:
            return
        self.ins = getattr(self.server, self.server['lastInstrument'])
        if self.eng.current:
            self.quit.setEnabled(True)
        else:
            self.quit.setEnabled(False)
        self.op.setText(_('Operator: {}').format(self.ins.measure['operator']))

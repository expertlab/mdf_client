from traceback import format_exc
from .. import cloud
from .. import _
from .. import widgets
from mdf_client.qt import QtWidgets
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)


def add_option_to_initial_dialog(wg, root, sample, handle, position, required=False):
    """Add `sample` option `handle` to initial dialog `wg` in grid `position`"""
    if handle not in sample:
        return False
    w = widgets.build(root,
                      sample,
                      sample.gete(handle))
    if w:
        if required:
            w.label_widget.set_highlighted(True)
        wg.layout().addWidget(w.label_widget, position, 0)
        wg.layout().addWidget(w, position, 1)
    return True
    

def initial_sample_dimension(instrument, parent=None):
    """Show a confirmation dialog immediately before starting a new test"""
    # TODO: generalize
    opts = 0
    wg = QtWidgets.QWidget()
    wg.setLayout(QtWidgets.QGridLayout())
    root = instrument.root
    # Check measurement name
    m = instrument['mro'][0]
    if instrument.measure['name'] in ['measure', m]:
        w = widgets.build(root,
                      instrument.measure,
                      instrument.measure.gete('name'))
        if w:
            w.label_widget.set_highlighted(1)
            wg.layout().addWidget(w.label_widget, opts, 0)
            wg.layout().addWidget(w, opts, 1)        
            opts += 1
    # Check initial dimension
    if instrument['devpath'] in ['horizontal', 'vertical', 'flex', 'flex3']:
        diff = 'differential' in instrument and instrument['differential']
        if diff:
            opts += add_option_to_initial_dialog(wg, root, instrument.measure, 'flavour', opts,
                                                 required=True)
            
        for i in range(instrument.measure['nSamples']):
            smp = getattr(instrument, 'sample{}'.format(i))
            if i > 0:
                wg.layout().addWidget(QtWidgets.QLabel('Sample {}: {}'.format(i, smp['name'])), opts, 0)
                opts += 1
            for handle in ('initialDimension', 'initialThickness', 'initialLength', 'initialDepth'):
                opts += add_option_to_initial_dialog(wg, root, smp, handle, opts,
                                                 required=handle == 'initialDimension')
            
    if not opts:
        return True
    
    label = QtWidgets.QLabel(_('Please review these important configurations:'))
    dia, btn_start, btn_cancel = create_widgets_dialog([label, wg])
    dia.setWindowTitle(_('Review test configuration'))
    if dia.exec_():
        return True
    return False
   

def create_widgets_dialog(widget_list, dia=False, msg_ok=_('Start test')):
    if dia is False:
        dia = QtWidgets.QDialog()
    dia.setLayout(QtWidgets.QVBoxLayout())
    for wg in widget_list:
        dia.layout().addWidget(wg)
    
    btn_cancel = QtWidgets.QPushButton(_('Cancel'))
    btn_cancel.setDefault(False)
    btn_cancel.setFocus(False)
    btn_start = QtWidgets.QPushButton(msg_ok)
    btn_start.setDefault(True)
    btn_start.setFocus(True)
    dia.layout().addWidget(btn_cancel)
    dia.layout().addWidget(btn_start)
    btn_start.clicked.connect(dia.accept)
    btn_cancel.clicked.connect(dia.reject)
    return dia, btn_start, btn_cancel


ppm_failed_msg = _("""<html>Cannot get a quote for this Pay Per Measurement test.
<br>Please check your internet connection. 
<br><a href="https://www.expertlabcloud.it/rpc/status.php">Click here to check the licensing service status</a>
<br>Copy and paste the below message to get support.</html>""")


class ValidationDialog(QtWidgets.QDialog):
    quote_wg = False

    def __init__(self, server, parent=None):
        super(ValidationDialog, self).__init__(parent)
        self.setWindowTitle(_('Review and confirm'))
        self.setMinimumWidth(400)
        self.server = server
        
        opt = self.server.gete('validate')
        self.table = widgets.build(server, server, opt, force=True)
        self.table.get_on_enter = False
        self.table.get_on_leave = False
        self.label = QtWidgets.QLabel(_('A new test will start. Do you confirm?'))
        
        self.btn_update = QtWidgets.QPushButton(_('Update'))
        self.btn_update.clicked.connect(self.update_status)
        
        wg_list = [self.label, self.table]
        
        ppm_ok = True
        if 'ppm' in server.license['functionalities']:
            try:
                ret, quote = cloud.ppm_command(server, getattr(server, server['lastInstrument']), 'ppm_quote')
                self.quote_wg = QtWidgets.QLabel(quote)
                server.license['ppmToken'] = ret['ppm_token']
                wg_list.append(self.quote_wg)
                wg_list.append(self.btn_update)
            except:
                logging.critical('Cannot get a quote for PPM test:', format_exc())
                self.setWindowTitle(_('License generation failed'))
                lbl = QtWidgets.QLabel(ppm_failed_msg)
                lbl.setOpenExternalLinks(True);
                txt = QtWidgets.QTextBrowser()
                txt.setText(format_exc())
                wg_list = [lbl, txt]
                ppm_ok = False
        
        dia, self.btn_start, self.btn_cancel = create_widgets_dialog(wg_list, dia=self)
        dia.layout().addWidget(self.btn_update)
        
        if ppm_ok:
            self.update_status(init=True)
        
    def check_status(self):
        vals = self.table.current
        ok = True
        
        for (status, msg, path) in vals:
            ok = ok * status
        
        return ok, vals
        
    def update_status(self, init=False):
        if not init:
            self.table.update()
        ok, vals = self.check_status()
        # Default button
        self.btn_start.setDefault(ok)
        self.btn_start.setFocus(ok)
        # Hide unrelevant buttons
        if len(vals):
            self.table.show()
            self.btn_update.show()
            self.btn_update.setDefault(not ok)
            self.btn_update.setFocus(not ok)
        else:
            self.table.hide()
            self.btn_update.hide()
            
        if ok and not len(vals) and not self.quote_wg:
            logging.debug('No validation required: accepting the dialog.')
            self.accept()
            return ok
            
        self.btn_start.setEnabled(ok)
        return ok
        
    def accept(self):
        if self.check_status()[0]:
            return super(ValidationDialog, self).accept()
        else:
            logging.error('Cannot start: validation failed!')
            return False
        

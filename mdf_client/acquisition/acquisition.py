#!/usr/bin/python
# -*- coding: utf-8 -*-
from time import time, sleep
from traceback import format_exc
import os
from mdf_canon import csutil
from .. import _
from ..live import registry
from .. import network
from ..network import TransferThread, normalize_address
from .. import widgets, beholder
from .. import filedata
from .. import connection
from ..livelog import LiveLog
from ..clientconf import confdb
from ..confwidget import RecentWidget, check_default_database
from .selector import InstrumentSelector
from .controls import Controls, MotionControls
from .delay import DelayedStart
from ..license import check_server_license
from ..filedata import RemoteFileProxy
from . import windows
from mdf_client.beholder.camera_toolbar import CameraToolbar
from mdf_client.qt import QtWidgets, QtCore
from mdf_client.single_application import SingleApplication
from .abstract_test_window import AbstractTestWindow, do_blockade
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
subWinFlags = QtCore.Qt.CustomizeWindowHint | QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowMinMaxButtonsHint

roles = {'motorBase': 'Base Position', 'motorHeight': 'Height Position',
         'motorRight': 'Right Position', 'motorLeft': 'Left Position',
         'focus': 'Focus adjust', 'motor': 'Position', 'camera': 'Main',
         'cameraBase': 'Base', 'cameraHeight': 'Height', 'cameraRight': 'Right',
         'cameraLeft': 'Left', 'force': 'Weight', 'cameraCenter':'Center',
         'angleHeight': 'Height Inclination', 'angleBase': 'Base Inclination',
         'angleRight': 'Right Inclination', 'angleLeft': 'LeftInclination'}

failed_connection_message = _("""Connection failed to: 
    {}
Please retry later. 

If connection keeps failing for more than 2 minutes, please restart the instrument.""")

failed_login_message = _("""Login failed for: 
    {}
Please check that user and password are correct 
and CAPS key was not locked. """)


def check_time_delta(server):
    """Detect time delta, warn the user, restart if delta is approved."""
    if server['isRunning'] or server['runningInstrument']:
        logging.debug('running analysis: do not check time delta')
        return True
    t = time()
    s = server.time()
    if not s:
        logging.debug('Could not read remote time!')
        return True
    dt = time() - t
    delta = int((t - s) + (dt / 3.))
    if delta < 500:
        logging.debug('Time delta is not significant', delta)
        return True
    pre = server['timeDelta']
    if pre:
        logging.debug('Time delta already set:', server['timeDelta'])
    btn = QtWidgets.QMessageBox.warning(None, _('Hardware clock error'),
                      _('Instrument time is different from your current time (delta: {}s).\n Apply difference and restart?').format(delta),
                      QtWidgets.QMessageBox.Cancel | QtWidgets.QMessageBox.Ok, QtWidgets.QMessageBox.Ok)
    if btn != QtWidgets.QMessageBox.Ok:
        logging.debug('Delta correction aborted')
        return True
    # TODO: warn the user about time delta
    logging.info('Apply time delta to server', delta)
    server['timeDelta'] = delta
    r = server.restart()
    QtWidgets.QMessageBox.information(None, 'Restarting', 'Instrument is restarting: ' + r)
    return False


def tray_menu_actions(mainwindow, menu=None, title='MDF Live'):
    if menu is None:
        menu = QtWidgets.QMenu()
    menu.setTitle(title)
    menu.addAction(_('Show') + ' ' + title, mainwindow.show)
    menu.addAction(_('Hide to tray'), mainwindow.hide)
    menu.addAction(_('Quit') + ' ' + title, mainwindow.close)
    icon = mainwindow.tray_icon
    icon.setContextMenu(menu)
    icon.activated.connect(mainwindow.show)
    return menu


class LiveAcquisitionWindow(AbstractTestWindow):
    name = 'LiveAcquisitionWindow'
    
    def __init__(self, doc=False, parent=None):
        AbstractTestWindow.__init__(self, doc=doc, parent=parent)
        self.setMinimumSize(800, 600)
        self.setWindowTitle(_('MDF Live'))
        self.add_server_selector()
        self.single = SingleApplication()
        self.single.upfront.connect(self.bring_upfront)
        self.tray_icon.messageClicked.connect(self.focus_logging)
        self.reset_instrument.connect(self.setInstrument)
        self.showMaximized()
        if confdb['autoConnect'] and len(confdb['recent_server']) > 0:
            self.set_addr(confdb['recent_server'][-1][0])
        tray_menu_actions(self)
        self.tray_icon.show()

    def add_server_selector(self):
        """Server selector dock widget"""
        self.serverDock = QtWidgets.QDockWidget(self.centralWidget())
#       self.serverSelector=connection.ServerSelector(self.serverDock)
        self.serverSelector = RecentWidget(confdb, 'server', self.serverDock)
        self.serverSelector.select.connect(self.set_addr)
        self.serverDock.setWindowTitle(self.serverSelector.label)
        self.serverDock.setWidget(self.serverSelector)
        self.addDockWidget(QtCore.Qt.TopDockWidgetArea, self.serverDock)
        
    def _init_instrument(self, soft=False, name=None):
        """Called in a different thread. Need to recreate connection."""
        r = self.remote.copy()
        r.connect()
        if not name:
            name = r['preset']
        result = r.init_instrument(soft, name)
        print('done _init_instrument')

    def init_instrument(self, soft=False, name=None):
        # TODO: this scheme could be automated via a decorator: @thread
        logging.debug('Calling init_instrument in QThreadPool')
        r = widgets.RunMethod(self._init_instrument, soft, name)
        r.pid = 'Instrument initialization '
        QtCore.QThreadPool.globalInstance().start(r)
        logging.debug('active threads:',
                      QtCore.QThreadPool.globalInstance().activeThreadCount(),
                      QtCore.QThreadPool.globalInstance().maxThreadCount())
        
    indicators_toolbar = False

    def add_indicators_toolbar(self):
        logging.debug('add_indicators_toolbar')
        if self.indicators_toolbar:
            if self.indicators_toolbar in self.toolbars:
                self.toolbars.remove(self.indicators_toolbar)
            try:
                self.removeToolBar(self.indicators_toolbar)
            except:
                pass
            self.indicators_toolbar.close()
            self.indicators_toolbar.deleteLater()
            self.indicators_toolbar = False
            # return self._corner
        if self.fixedDoc:
            logging.debug('fixedDoc corner')
            return False
        server = self.server
        if not server.has_child('kiln'):
            logging.debug('no kiln for corner')
            return False
        w = QtWidgets.QWidget()
        w.setLayout(QtWidgets.QHBoxLayout())
        
        T = widgets.aNumber(server, server.kiln, server.kiln.gete('T'))
        w.layout().addWidget(T.label_widget)
        w.layout().addWidget(T)
        w.layout().addWidget(T.bmenu)
        
        if 'remaining' in server.kiln:
            R = widgets.aNumber(server, server.kiln, server.kiln.gete('remaining'))
            w.layout().addWidget(R.label_widget)
            w.layout().addWidget(R)
            w.layout().addWidget(R.bmenu)
        
        tb = QtWidgets.QToolBar("Indicators")
        tb.addWidget(w)
        tb.setFloatable(True)
        tb.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        tb.setMaximumWidth(600)
        self.indicators_toolbar = tb
        if self.indicators_toolbar:
            self.addToolBar(tb)
            self.toolbars.append(tb)
        return tb
    
    def add_instruments_selector(self):
        self.rem('instrumentDock')
        self.instrumentDock = QtWidgets.QDockWidget(self.centralWidget())
        self.instrumentDock.setWindowTitle(_('Activate measurement functionality'))
        self.instrumentSelector = InstrumentSelector(self, self.setInstrument)
        self.instrumentDock.setWidget(self.instrumentSelector)
        self.instrumentDock.hide()
        self.addDockWidget(QtCore.Qt.TopDockWidgetArea, self.instrumentDock)
        
    def add_logDock(self):
        r = super().add_logDock()
        self.logDock.setWidget(LiveLog(self.logDock))
        return r
    
    def closeEvent(self, ev):
        registry.toggle_run(False)
        self.tasks.close()
        self.single.detach()
        return super().closeEvent(ev)
        
    def set_server(self, server=False):
        if not server:
            network.manager.remote.connect()
            server = network.manager.remote
        check_default_database()
        if not check_time_delta(server):
            return False
        self.license = check_server_license(server)
        if AbstractTestWindow.set_server(self, server=server):
            self.add_indicators_toolbar()
            if not (self.server['isRunning'] or self.server['delayStart']):
                self.add_instruments_selector()
                self.instrumentDock.show()
            self.check_meridian()
            return True
        return False
    
    def check_meridian(self) -> bool:
        import subprocess
        import sys
        cmd = []
        for part in sys.argv:
            if '--' in part:
                continue
            cmd.append(part)
        cmd.append('--meridian')
        if os.name != 'nt':
            cmd = ' '.join(cmd)
        logging.debug('Meridian command:', cmd)
        try:
            p = subprocess.Popen(cmd, shell=True)
            logging.debug('Meridian PID:', p.pid)
            return True
        except:
            logging.error('Cannot start meridian', format_exc())
            return False

    def uptime(self):
        # Automatically pop-up delayed start dialog
        if self.server:
            self.server.connect()
            isRunning = self.server['isRunning']
            if self.server['delayStart'] and not isRunning:
                self.delayed_start()
            elif self.delayed and isRunning:
                self.delayed.hide()
        self.single.uptime()

    def bring_upfront(self):
        logging.debug('Bring upfront...')
        # Minimize first
        self.setWindowState(self.windowState() | QtCore.Qt.WindowMinimized)
        QtCore.QTimer.singleShot(1000, self._bring_upfront)

    def _bring_upfront(self):
        self.show()
        self.raise_()
        self.setWindowState(self.windowState() & ~QtCore.Qt.WindowMinimized | QtCore.Qt.WindowActive)
        self.setFocus(True)
    
    def set_addr(self, addr=False):
        if addr:
            self._last_connection_addr = addr
        else:
            addr = self._last_connection_addr
        addr, user, password, auth_addr = normalize_address(addr)
        addr = connection.auto_address(addr)
        tp = os.path.join(confdb['logdir'], addr.replace('/', '-').replace(':', '__') + '.lock')
        if not self.single.check_new_instance(tp):
            return self.set_addr(addr)
        registry.cycle.connect(self.uptime)
        registry.sig_notify.connect(self.notify_message)
        entry = confdb.get_from_key('recent_server', addr)
        user = user or entry[1]
        password = password or entry[2]
        mac = entry[3]
        logging.debug('LiveAcquisitionWindow.set_addr', addr, user, password, entry)
        self.login_window = connection.LoginWindow(
            addr, user=user, password=password, mac=mac, globalconn=False)
        self.login_window.login_failed.connect(
            self.retry_login, QtCore.Qt.QueuedConnection)
        self.login_window.connection_failed.connect(self.notify_failed_connection,
                                                    QtCore.Qt.QueuedConnection)
        self.login_window.login_succeeded.connect(
            self.succeed_login, QtCore.Qt.QueuedConnection)
        r = widgets.RunMethod(self.login_window.tryLogin, user, password, mac=mac)
        r.pid = 'Connecting to ' + addr
        QtCore.QThreadPool.globalInstance().start(r)
    
    notify_message_box = None

    def notify_message(self, notification):
        t, event, title, msg, data = notification
        if 'emergency' in event.lower():
            if self.notify_message_box:
                logging.debug('Could not notify', event, title)
                return
            self.notify_message_box = True
            QtWidgets.QMessageBox.critical(self, title, msg)
            self.notify_message_box = False
            
    def notify_failed_connection(self):
        logging.debug('notify_failed_connection', self.login_window.addr)
        self.single.detach()
        btn = QtWidgets.QMessageBox.information(self, _('Connection failed'),
                                      _(failed_connection_message).format(self.login_window.addr),
                                      QtWidgets.QMessageBox.Cancel | QtWidgets.QMessageBox.Retry)
        
        if btn == QtWidgets.QMessageBox.Retry:
            sleep(1)
            self.set_addr()

    def retry_login(self):
        """Called when set_addr fails"""
        logging.debug('retry_login')
        QtWidgets.QMessageBox.information(self, _('Login failed'),
                                      failed_login_message.format(self.login_window.addr))
        self.login_window.set_default_if_empty()
        self.login_window.exec_()
        if not self.login_window.obj:
            self.login_window.close()
    
    def succeed_login(self, rem=False):
        """Called on new address successfully connected"""
        logging.debug('succeed_login', rem)
        if not rem:
            rem = self.login_window.obj
        network.manager.set_remote(rem)
        registry.set_manager(network.manager)
        self.set_server(rem)
        self.tasks_dock = QtWidgets.QDockWidget(self.centralWidget())
        self.tasks_dock.setWindowTitle("Pending Tasks")
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.tasks_dock)
        self.tasks_dock.setWidget(registry.taskswg)
        registry.taskswg.setParent(self.tasks_dock)
        registry.taskswg.show_signal.connect(self.pending_task_shown)
        registry.taskswg.hide_signal.connect(self.pending_task_hidden)
        registry.logMessage.connect(self.notify_status)
        registry.logMessages.connect(self.notify_tray)

    def pending_task_shown(self):
        print('pending_task_shown')
        self.tasks_dock.show()

    def pending_task_hidden(self):
        print('pending_task_hidden')
        self.tasks_dock.hide()

    def add_camera(self, obj, role=''):
        logging.debug('addCamera', obj['fullpath'])
        pic = beholder.ViewerControl(obj, self.server, parent=self)
        pic.role = role
        win = self.centralWidget().addSubWindow(pic, subWinFlags)
        tit = 'Camera %s (%s)' % (roles.get(role, role), obj['name'])
        win.setWindowTitle(tit)
        win.resize(640, 480)
        if not self.fixedDoc:
            win.show()
        else:
            win.hide()
        self.cameras[obj['fullpath']] = (pic, win)
        QtCore.QTimer.singleShot(1000, pic.viewer.toggle)
        pic.viewer.set_toolbar(self.camera_toolbar)
        logging.debug('addCamera done', obj['fullpath'])
        return win
    
    @do_blockade
    def updateInstrumentInterface(self, remote=False):
        if not super().updateInstrumentInterface(remote=remote):
            return False
        self.controls = Controls(self.remote, parent=self)
        logging.debug('Created controls')
        self.controls.started.connect(self.resetFileProxy)
        self.controls.mute = bool(self.fixedDoc)
        self.addToolBar(self.controls)
        self.toolbars.append(self.controls)
        self.camera_toolbar = CameraToolbar(parent=self)
        self.addToolBar(self.camera_toolbar)
        self.toolbars.append(self.camera_toolbar)
        self.add_indicators_toolbar()

        # Arrange windows
        self.reset_instrument_timer.singleShot(3000, self.add_cameras)
        self.tasks.done(self.instrument_pid())
        return True
    
    def add_cameras(self):
        paths = self.remote['devices'] if not self.fixedDoc else []
        logging.debug('setInstrument PATHS:', paths)
        for path in paths:
            lst = self.server.searchPath(path[1])
            if not lst:
                continue
            obj = self.server.toPath(lst)
            if obj is None:
                continue
#           role=obj['role'][self.name]
            role = path[0]
            if ('AbstractCamera' in obj['mro']):
                if role == 'NoRole':
                    role = 'Camera'
                self.add_camera(obj, role)
        windows.arrange(self.centralWidget(), self.name)
        # Additional motor controls
        self.mcontrols = MotionControls(self.remote, parent=self)
        self.addToolBar(QtCore.Qt.BottomToolBarArea, self.mcontrols)
        self.toolbars.append(self.mcontrols)
        
        # Connect to "id" property
        self.idobj = widgets.ActiveObject(
            self.server, self.remote.measure, self.remote.measure.gete('id'), parent=self)
        self.idobj.sig_changed.connect(self.resetFileProxy, QtCore.Qt.QueuedConnection)
    
    def setInstrument(self, remote=False, server=False, preset='default'):
        if server is not False:
            self.blocked = False
            self.set_server(server)
        if remote is False:
            remote = self.remote
        else:
            self.remote = remote
        
        running = self.get_runningInstrument()
        if running and running['fullpath'] == remote['fullpath'] and remote['preset'] == preset:
            sleep(2)
            self.updateInstrumentInterface_when_instrument_is_ready(remote)
            return True
        self.blocked = False
        self.clean_interface(remote)
        self.init_instrument(soft=True, name=preset)
        sleep(2)
        self.updateInstrumentInterface_when_instrument_is_ready(remote)
        return False
        
    def get_live_uid_or_retry_later(self, retry, recursion):
        logging.debug('LiveAcquisitionWindow.get_live_uid_or_retry_later')
        pid = 'Waiting for data'
        if self.server['initTest'] or self.server['closingTest']:
            self.tasks.jobs(0, 'Test initialization')
            if recursion == 0:
                self.tasks.setFocus()
            logging.debug('Waiting for initialization to complete...')
            self.resetFileProxyLater(0, recursion + 1)
            return False
        else:
            if not self.server['isRunning']:
                retry = self.max_retry
            self.tasks.jobs(self.max_retry, pid)
            self.tasks.done('Test initialization')
            self.tasks.job(retry, pid)
            if retry < self.max_retry and self.remote.measure['elapsed'] < 10:
                self.resetFileProxyLater(retry + 1, recursion + 1)
                return False
            if retry > self.max_retry:
                self.tasks.done(pid)
                QtWidgets.QMessageBox.critical(self, _('Impossible to retrieve the ongoing test data'),
                                           _("""A communication error with the instrument does not allow to retrieve the ongoing test data.
                        Please restart the client and/or stop the test."""))
                return False
            fid = self.remote.measure['uid']
            if fid == '':
                logging.debug('no active test', fid)
                self.tasks.done(pid)
                return False
            logging.debug('resetFileProxy to live ', fid)
            self.server.connect()
            
            try:
                live_uid = self.server.storage.test.live.get_uid()
            except:
                logging.debug('Error retrieving live uid:', format_exc())
                live_uid = False

            if not live_uid:
                logging.debug('No live_uid returned')
                return False

            if fid == self.uid:
                logging.debug(
                    'Measure id is still the same. Aborting resetFileProxy.')
                self.tasks.job(0, pid,
                               'Measure id unchanged. Aborting resetFileProxy.')
                self.tasks.done(pid)
                return False

        return live_uid
    
    def resetFileProxyLater(self, retry, recursion):
        self.reset_file_proxy_timer.singleShot(1000, lambda: self.resetFileProxy(retry, recursion))
    
    def resetFileProxy(self, *a, **k):
        logging.debug('LiveAcquisitionWindow.resetFileProxy')
        if not self._lock.acquire(False):
            logging.debug('ANOTHER RESETFILEPROXY IS RUNNING!')
            return False

        self._blockResetFileProxy = False
        logging.debug('LiveAcquisitionWindow.resetFileProxy: Stopping registry')

        registry.stop_updating_doc()

        return self._resetFileProxy(*a, **k)
    
    def get_live_file_or_retry_later(self, retry, recursion):
        logging.debug('LiveAcquisitionWindow.get_live_file_or_retry_later')
        live_uid = self.get_live_uid_or_retry_later(retry, recursion)

        if live_uid:
            live_file = getattr(self.server.storage.test, live_uid)

            if not live_file.has_node('/conf'):
                live_file.load_conf()

                if not live_file.has_node('/conf'):
                    logging.debug('Conf node not found: acquisition has not been initialized.')
                    return False
            return live_file
        return False
    
    @csutil.unlockme()
    def _resetFileProxy(self, retry=0, recursion=0):
        """Resets acquired data widgets"""
        logging.debug('LiveAcquisitionWindow.resetFileProxy')
        if self._blockResetFileProxy:
            return False

        if self.doc:
            self.doc.close()
            self.doc = False

        doc = False
        fid = False
        pid = 'Reading remote data file'
        self.tasks.jobs(6, pid)
        self.tasks.job(1, pid, _('Checking if remote file is ready'))
        live_file = self.get_live_file_or_retry_later(retry, recursion)

        if live_file:
            try:
                self.tasks.job(2, pid, _('Connecting remote data file'))
                fp = RemoteFileProxy(live_file, conf=self.server, live=True)
                self.tasks.job(3, pid, _('Updating data header'))
                self.refresh_header()
                self.tasks.job(4, pid, _('Creating the plotting document'))
                doc = filedata.MisuraDocument.unique(proxy=fp)
                # Remember as the current uid
                self.uid = fid
                self.tasks.job(5, pid, _('Setting the plotting document'))
            except:
                logging.debug('RESETFILEPROXY error')
                logging.debug(format_exc())
                doc = False
                self.resetFileProxyLater(retry + 1, recursion + 1)
                return
            self.set_doc(doc)
        if doc:
            registry.restart_updating_doc()
            registry.toggle_run(True)
            doc.up = True
        self.tasks.done(pid)
        self.tasks.done('Waiting for data')
        self.tasks.hide()

    ###########
    # ## Start/Stop utilities
    ###########
    delayed = False

    def delayed_start(self):
        """Configure delayed start"""
        if not self.delayed:
            self.delayed = DelayedStart(self)
        self.delayed.show()


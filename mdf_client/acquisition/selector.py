#!/usr/bin/python
# -*- coding: utf-8 -*-
from mdf_canon.logger import get_module_logging
from mdf_client.qt import QtWidgets, QtGui, QtCore
import functools
from ..iutils import theme_icon
from .. import widgets
logging = get_module_logging(__name__)


class OneShotButton(QtWidgets.QToolButton):

    def mouseReleaseEvent(self, *a, **k):
        self.setEnabled(False)


def add_button(parent, layout, name, preset, text, size, row, column, rowspan=1, columnspan=1):
    button = OneShotButton(parent)
    icon = theme_icon(preset)
    if icon.isNull():
        icon = theme_icon(name)
    button.setIcon(icon)
    if text.endswith('(active)'):
        button.setCheckable(True)
        button.setChecked(True)
        button.setStyleSheet("font-weight: bold;")
    button.setText(text)
    button.setIconSize(QtCore.QSize(size, size))
    button.setToolButtonStyle(QtCore.Qt.ToolButtonTextUnderIcon)
    layout.addWidget(button, row, column, rowspan, columnspan, QtCore.Qt.AlignTop)
    if not parent.parent().license:
        button.setEnabled(False)
    return button


class InstrumentSelector(QtWidgets.QWidget):

    def __init__(self, parent, setInstrument):
        QtWidgets.QWidget.__init__(self, parent)
        self.lay = QtWidgets.QHBoxLayout(self)
        self.setLayout(self.lay)
        self._setInstrument = setInstrument
        self.redraw()
    
    def setInstrument(self, *a, **k):
        if self._setInstrument is None:
            logging.debug('Cannot setInstrument twice: close and reopen the app')
            return False
        r = self._setInstrument(*a, **k)
        self._setInstrument = None
        return r

    def redraw(self):
        logging.debug('InstrumentSelector.redraw')
        while True:
            item = self.lay.takeAt(0)
            if item in [0, None]:
                break
        self.func = []
        server = self.parent().server
        inlist = []
        if 'instruments' in server:
            inlist = server['instruments']
        
        for (title, name) in inlist:
            if name.lower() == 'kiln':
                if server._writeLevel < 4:
                    continue
            instrument_enabled_option = 'eq_' + name
            if instrument_enabled_option in server:
                instrument_enabled = server[instrument_enabled_option]
                if not instrument_enabled:
                    logging.debug('Instrument ' + name + ' disabled')
                    continue
            else:
                logging.debug('Ignoring unknown instrument', title, name)
                continue
            obj = getattr(server, name, False)
            if obj is False:
                logging.debug('Instrument not found', name)
                continue
            title = obj.get_from_preset('comment', 'default')
            if not title:
                title = obj.get_from_preset('comment', 'factory_default')
            atitle = title
            if server['lastInstrument'] == name and obj['preset'] == 'default':
                atitle = title + ' (active)'
            f = functools.partial(self.setInstrument, obj, preset='default')
            self.func.append(f)
            current_instrument_layout = QtWidgets.QGridLayout()
            self.lay.addLayout(current_instrument_layout)

            button = add_button(self,
                                current_instrument_layout,
                                name,
                                'default',
                                atitle,
                                200,
                                0,
                                0,
                                1,
                                -1)
            button.pressed.connect(f)

            presets = filter(lambda preset: preset not in ['default', 'factory_default'],
                             obj.listPresets())
            for current_column, preset in enumerate(presets):
                atitle = obj.get_from_preset('comment', preset)
                if server['lastInstrument'] == name and obj['preset'] == preset:
                    atitle = atitle + ' (active)'
                button = add_button(self,
                                    current_instrument_layout,
                                    name,
                                    preset,
                                    atitle,
                                    100,
                                    1,
                                    current_column)

                f = functools.partial(self.setInstrument, preset=preset, remote=obj)
                button.pressed.connect(f)

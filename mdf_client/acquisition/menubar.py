#!/usr/bin/python
# -*- coding: utf-8 -*-
import os
import functools
from time import sleep
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from .. import network, conf, _
from ..clientconf import confdb
from ..connection import addrConnection
from ..confwidget import RecentMenu
from mdf_client.helpmenu import HelpMenu
from . import windows
from .. import license
from .. import iniconf
from .. import widgets
from ..live import registry
from ..connection import addrConnection
from ..autoupdate import ServerUpdater
from mdf_client.qt import QtWidgets, QtCore
from . import messages

calibration_doc = _('Warning: this factor must be calculated from a test started with measure Type=Calibration. \
<a href="https://www.expertlabservice.it/support/en/mdf/acquisition/dilatometer.html#calibration"> Read the manual for the complete procedure.</a>')


def save_no_name_comment(obj, name, preset='default'):
    logging.debug('save_no_name_comment', obj['fullpath'], name, preset)
    old = obj['name']
    oldc = obj['comment']
    obj['name'] = name
    obj['comment'] = ''
    r = obj.save(preset)
    obj['name'] = old
    obj['comment'] = oldc
    return r


class MenuBar(QtWidgets.QMenuBar):

    """Main acquisition menus"""
    quitClient = QtCore.pyqtSignal()
    act_license = False
    
    def __init__(self, server=False, parent=None):
        QtWidgets.QMenuBar.__init__(self, parent)
        self.remote = False
        self.server = server
        self.windows = {}
        self.objects = {}
        self.lstActions = []
        if self.fixedDoc is False:
            self.set_acquisition_mode()
        else:
            self.set_browser_mode()
        self.measure = self.addMenu(_('Measure'))
        self.measure.aboutToShow.connect(self.updateActions)
        self.settings = self.addMenu(_('Settings'))
        
        self.settings.aboutToShow.connect(self.updateActions)
        self.view_menu = self.addMenu(_('View'))
        self.view_menu.aboutToShow.connect(self.updateActions)
        if self.fixedDoc is False:
            self.measure.setEnabled(False)
            self.settings.setEnabled(False)

        self.help_menu = HelpMenu()
        self.help_menu.add_help_menu(self)

        if server is not False:
            self.setServer(server)

    @property
    def fixedDoc(self):
        if self.parent() is None:
            return False
        return self.parent().fixedDoc
    
    @property
    def doc(self):
        if not self.parent():
            return None
        return self.parent().doc

    def set_acquisition_mode(self):
        self.connectTo = self.addMenu(_('Server'))
        self.servers = RecentMenu(confdb, 'server', self)
        self.servers.setTitle(_('Change server'))
        self.servers.select.connect(self.setAddr)
        self.connectTo.addMenu(self.servers)
        # self.actLogout = self.connectTo.addAction(_('Logout'), self.logout)
        self.actLogout = QtWidgets.QAction(_('Logout'), self)
        self.actLogout.setEnabled(False)
        self.actShutdown = self.connectTo.addAction(
            _('Shutdown electronics'), self.shutdown)
        self.actShutdown.setEnabled(False)
        self.actRestart = self.connectTo.addAction(_('Restart Server'), self.restart)
        self.actRestart.setEnabled(False)
        self.connectTo.addAction(_('License'), self.show_license_info)
        self.connectTo.addAction(_('Quit Client'), self.quit)

    def set_browser_mode(self):
        self.connectTo = QtWidgets.QMenu()

    def setAddr(self, addr):
        logging.debug('MenuBar.setAddr', addr)
        addr = str(addr)
        obj = addrConnection(addr)
        if not obj:
            logging.debug('MenuBar.setAddr: Failed!')
            return
        network.setRemote(obj)

    def logout(self):
        if not self.server:
            return
        r = self.server.users.logout()
        confdb.logout(self.server.addr)
        msg = _('You have been logged out (%s): \n %r') % (self.server.user, r)
        QtWidgets.QMessageBox.information(self, _('Logged out'), msg)

    def shutdown(self):
        btn = QtWidgets.QMessageBox.warning(None, _('Confirm Shutdown'),
                          _('Do you really want to shutdown the instrument operative system?'),
                          QtWidgets.QMessageBox.Cancel | QtWidgets.QMessageBox.Ok, QtWidgets.QMessageBox.Cancel)
        if btn != QtWidgets.QMessageBox.Ok:
            logging.debug('Shutdown request aborted')
            return False
        status, msg = self.server.support['halt']
        if status != 0:
            QtWidgets.QMessageBox.warning(self, _('Shutdown request failed'),
                                      _('Shutdown failed with the following error:\n {!r}').format((status, msg))
                                      )
            return False
        msg = _('Server is shutting down.\nPlease close any client window and shutdown power interruptor in 30 seconds.\nReply: \n %r') % msg
        QtWidgets.QMessageBox.information(self, _('Shutting Down'), msg)
        self.quit()
        return True

    def restart(self):
        QtWidgets.QMessageBox.information(self, _('Restart Down'),
                                      'Server is restarting:\n %r' % self.server.restart(1))
        self.quit()

    def setServer(self, server=False):
        logging.debug('setServer', server)
        self.server = server
        if not server:
            self.server = network.manager.remote
            
        self.appendGlobalConf()
        
        # Enable menu and menu items after server connection
        if self.fixedDoc is False:
            self.actLogout.setEnabled(True)
            self.actShutdown.setEnabled(True)
            self.actRestart.setEnabled(True)
            self.settings.setEnabled(True)
            
        if self.server._readLevel < 4:
            self.removeAction(self.settings.menuAction())
            self.settings.setEnabled(False)
            
    def show_license_info(self):
        dia = license.License(self.server)
        dia.exec_()

    def get_window(self, key):
        d = self.windows.get(key, False)
        if isinstance(key, str) and not d:
            d = getattr(self.parent(), key, False)
        return d

    def hideShow(self, key):
        d = self.get_window(key)
        if not d and key in self.objects:
            d = self.objects[key]()
            self.windows[key] = d
            print('Creating object', key, d)
        if not d:
            if not self.parent().doc:
                logging.debug('Creating document on-demand...')
                self.parent().resetFileProxy()
                sleep(2)
            logging.debug('Creating missing window', key)
            d = getattr(self.parent(), 'add_' + key)()
            self.arrange_windows()
        if not d:
            print('Could not hide/show', key)
            return
        if d.isVisible():
            d.hide()
        else:
            d.show()
        
        self.arrange_windows()

    def arrange_windows(self):
        windows.arrange(self.parent().centralWidget(), self.parent().name)
        
    def clear_windows(self):
        """Closes all configuration-dependent windows"""
        for key, w in list(self.windows.items()):
            if not isinstance(w, conf.TreePanel) and not isinstance(w, conf.MConf):
                continue
            logging.debug('clear_windows', key)
            w.hide()
            w.close()
            self.windows.pop(key)
            
    def add_view_plotboard(self):
        if self.parent().plotboardDock is False:
            return False
        self.windows['plotboardDock'] = self.parent().plotboardDock
        f = functools.partial(self.hideShow, 'plotboardDock')
        act = self.view_menu.addAction(_('Plots Board Window'), f)
        act.setCheckable(True)
        act.setChecked(self.parent().plotboardDock.isVisible())
        self.lstActions.append((act, 'plotboardDock', f, lambda:bool(self.doc)))
        return True
    
    def update_measure_actions(self):
        if self.repeatAct:
            self.repeatAct.setEnabled(self.parent().controls.startAct.isEnabled())
            
    def set_calibration_factor(self):
        if self.server._writeLevel >= 4:
            admin, admin_remote = self.server, self.remote
        else:
            admin = addrConnection(self.server.addr, user='An admin user', password='The admin password', signal=False)
            if not admin:
                return False
            admin_remote = admin.toPath(self.remote['fullpath'], cache=False)
        lst = []
        warn = QtWidgets.QLabel(calibration_doc)
        warn.setWordWrap(True)
        warn.setOpenExternalLinks(True)
        prop = admin_remote.gete('factor')
        wg = widgets.build(admin, admin_remote, prop)
        lst.append(warn)
        lst.append(wg.label_widget)
        lst.append(wg)
        dia, save, cancel = messages.create_widgets_dialog(lst, msg_ok=_("Save"))
        dia.admin_remote = admin_remote
        save.clicked.connect(admin_remote.save)
        dia._revert_calibration_factor = functools.partial(self.revert_calibration_factor, admin_remote, prop['current'])
        cancel.clicked.connect(dia._revert_calibration_factor)
        dia.setWindowTitle(_('Set calibration factor'))
        dia.exec_()
        
    def revert_calibration_factor(self, admin_remote, old, *a):
        logging.debug('Revert calibration factor', admin_remote['factor'], old)
        admin_remote['factor'] = old
        
    def is_document_available(self):
        if bool(self.doc):
            return True
        if self.server['isRunning'] and self.remote.measure['elapsed'] > 30:
            return True
        return False
        
    def create_window_show_action(self, name, label, func_enabled=None):
        func = functools.partial(self.hideShow, name)
        act = self.view_menu.addAction(_(label), func)
        self.lstActions.append((act, name, func, func_enabled or self.is_document_available))
        return act

    def setInstrument(self, remote, server=None):
        self.clear_windows()
        self.setServer(server)
        self.remote = remote
        self.lstActions = []
        preset = self.remote['preset']
        parent = self.parent()
        name = self.remote['devpath']
        name_from_preset = ' '.join(preset.split('_'))

        self.measure.clear()
        if not self.fixedDoc:
            if parent.controls:
                self.measure.addAction(parent.controls.startAct)
                self.measure.addAction(parent.controls.stopAct)
                self.measure.addAction(parent.controls.delayAct)
            self.repeatAct = self.measure.addAction(
                _('Repeat initialization'), parent.init_instrument)
            if name in ['horizontal', 'vertical']:
                self.measure.addAction(_('Set calibration factor'), self.set_calibration_factor)
                
        else:
            self.repeatAct = False
            
        self.measure.aboutToShow.connect(self.update_measure_actions)
        act = self.view_menu.addAction(_('Arrange Windows'), self.arrange_windows)
        act.setChecked(False)
        act.setCheckable(False)
        
        self.create_window_show_action('measureDock', 'Test Configuration', lambda:bool(self.remote))
        self.create_window_show_action('snapshotsDock', 'Storyboard')
        self.create_window_show_action('graphWin', 'Data Plot')
        self.create_window_show_action('tableWin', 'Data Table')
        self.create_window_show_action('logDock', 'Log Window')
        
        self.add_view_plotboard()

        self.measure.addAction(_('Reload data'), self.reload_data)

        if self.fixedDoc:
            self.measure.addSeparator()
            self.measure.addAction(_('Close'), self.quit)
            
        self.measure.setEnabled(True)

        # SETTINGS Menu
        self.settings.clear()
        func = functools.partial(self.hideShow, 'iconf')
        if self.server._readLevel >= 4:
            act = self.settings.addAction(_('Instrument'), func)
            self.objects['iconf'] = functools.partial(
                conf.TreePanel, self.remote, None, self.remote)
            self.lstActions.append((act, self.remote, func, lambda: True))

            # DEVICES SubMenu
            self.devices = self.settings.addMenu(_('Devices'))
            self.devices.aboutToShow.connect(self.updateActions)
            
            # FIXME: devices are empty in browser!
            paths = self.remote['devices']

            for path in paths:
                role, path = path
                lst = self.server.searchPath(path)
                if lst is False:
                    logging.debug('Undefined path for role', role, path)
                    continue
                obj = self.server.toPath(lst)
                if obj is None:
                    logging.debug('Path not found')
                    continue
                self.addDevConf(obj, role)
            
        self.appendGlobalConf()

        for act, cf, func, func_enabled in self.lstActions:
            act.setCheckable(True)
            act.setEnabled(func_enabled())
            
    def quit(self):
        print('QUIT')
        self.quitClient.emit()

    def appendGlobalConf(self):
        logging.debug('appendGlobalConf')
        self.settings.addAction(_('Export configuration'), self.export_configuration)
        
        if self.server._readLevel < 4:
            return 
        if self.server and not self.fixedDoc:
            self.settings.addAction(_('Import configuration'), self.import_configuration)
            self.settings.addAction(_('Send update package'), self.update_server)
            self.settings.addAction(_('Verify all motor limits'), self.check_motor_limits)
            self.settings.addAction(_('Save camera/motor conf'), self.save_camera_config)
            self.settings.addAction(_('Save measure/sample conf'), self.save_sample_config)
        self.objects['mconf'] = functools.partial(conf.MConf, self.server)
        func = functools.partial(self.hideShow, 'mconf')
        act = self.settings.addAction(_('Global'), func)
        act.setCheckable(True)
        self.lstActions.append((act, 'mconf', func, lambda:bool(self.server)))
        
    def list_encoder_motors(self, cam):
        motors = []
        for enc in cam.encoder.devices:
                path = enc['motor'][0]
                motors.append(enc['name'], path)
        return motors
    
    def saved_message_dialog(self, msg, i):
        r = list(map(lambda e: '{}, {}: {}'.format(*e), msg))
        r = '\n'.join(r)
        msg = widgets.informative_message_box(r, self)
        msg.setText(_('Saved {} configurations').format(i))
        msg.exec_()
        
    def get_preset_name(self):
        if self.remote['preset'] in ['default', 'factory_default']:
            return self.remote['name']
        else:
            return self.remote['preset']
        
    def save_camera_config(self):
        """Save all cameras configurations and related motors"""
        msg = []
        i = 0
        preset = self.get_preset_name()
        for role, path in self.remote['devices']:
            if not path.startswith('/beholder/'):
                continue
            cam = self.server.toPath(path)
            if cam['preset'] == 'factory_default':
                r = 'New '
                r += cam.save('default')
                msg.append((role, path, r))
            r = cam.save(preset)
            msg.append((role, path, r))
            i += 1
            if cam.has_child('encoder'):
                motors = self.list_encoder_motors(cam)
            else:
                motors = [(opt, cam[opt][0]) for opt in ('motor_x', 'motor_y', 'motor_z')]
            for name, path in motors:
                if path in ('None', '', None, False):
                    continue
                mot = self.server.toPath(path)
                if mot['preset'] == 'factory_default':
                    r = 'New '
                    r += mot.save('default')
                    msg.append((role + ' ' + name, path, r))
                    
                r = mot.save(preset)
                msg.append((role + ' ' + name, path, r))
                i += 1
        
        self.saved_message_dialog(msg, i)
        return i
        
    def save_sample_config(self):
        preset = self.get_preset_name()
        r = save_no_name_comment(self.remote.measure, 'measure', preset)
        msg = [(self.remote.measure['fullpath'], '', r)]
        n = 1
        for i, smp in enumerate(self.remote.samples):
            r = save_no_name_comment(smp, 'Sample n. {}'.format(i), preset)
            msg.append((smp['fullpath'], '', r))
            n += 1
            for j, subsmp in enumerate(smp.devices):
                logging.debug('saving subsample', subsmp['fullpath'])
                r = subsmp.save(preset)
                msg.append((subsmp['fullpath'], '', r))
                n += 1
        
        self.saved_message_dialog(msg, n)
        return n
           
    def export_configuration(self):
        iniconf.export_configuration(self.server, self)
        
    def import_configuration(self):
        iniconf.import_configuration(self.server, self)

    def addDevConf(self, obj, role):
        if self.server._readLevel < 4:
            return False
        self.objects[obj] = functools.partial(conf.TreePanel, obj, None, obj)
        f = functools.partial(self.hideShow, obj)
        act = self.devices.addAction('%s (%s)' % (role, obj['name']), f)
        self.lstActions.append((act, obj, f, lambda:bool(self.server)))
        return True

    def updateActions(self):
        for act, key, func, func_enabled in self.lstActions:
            conf = self.get_window(key)
            act.setEnabled(func_enabled())
            print('update actions', key, conf)
            if not conf:
                act.setChecked(False)
            elif hasattr(conf, '_Method__name'):
                act.setChecked(False)
            else:
                act.setChecked(conf.isVisible())

    def reload_data(self):
        self.parent().uid = False
        self.parent().resetFileProxy()
        
    def update_server(self):
        self.server_updater = ServerUpdater(self.server, parent=self)
        self.server_updater()
        
    def _apply_update_server(self, *a):
        self.server.support['packages'] = os.path.basename(self.pkg.transfer.outfile)
        logging.info('Apply server update', self.server.support['packages'], self.pkg.transfer.outfile)
        
        btn = widgets.aButton(self.server, self.server.support,
                                self.server.support.gete('applyExe'), parent=self)
        btn.hide()
        btn.get()
        btn.show_msg()
        self.server.restart()
        logging.debug('Closing the client while server restarts.')
        self.quit()
        
    def check_motor_limits(self):
        from .. import autoconf
        w = autoconf.FirstSetupWizard(self.server, jobs=registry.tasks.jobs,
                                      job=registry.tasks.job, done=registry.tasks.done)
        w.read_serials()
        w.set_objects()
        r = widgets.RunMethod(w.configure_limits)
        r.pid = 'Checking motor limits'
        r.abort = w.abort
        r.do()
        
        self._motor_limits_check = r


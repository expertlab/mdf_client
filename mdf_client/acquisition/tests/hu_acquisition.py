#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Tests Archive"""
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
import unittest
from mdf_client.acquisition import MainWindow

from mdf_client.tests import iutils_testing as iut
from mdf_client.qt import QtWidgets, QtGui

logging.debug('Importing', __name__)

ut = False


def setUpModule():
    global ut
    from mdf_server import utils_testing
    ut = utils_testing
    logging.debug('setUpModule', __name__)


def tearDownModule():
    logging.debug('tearDownModule', __name__)


class HuAcquisition(unittest.TestCase):
    __test__ = False
    
    def setUp(self):
        self._root = ut.full_hsm()
        self.root = iut.FakeProxy(self._root)

    def tearDown(self):
        self._root.close()

# 	@unittest.skip('')
    def test_setInstrument(self):
        self.mw = MainWindow()
        logging.debug('setting instrument', self.root.hsm, self.root)
        self.mw.setInstrument(self.root.hsm, self.root)
        self.mw.show()
        QtWidgets.qApp.exec_()

    @unittest.skip('')
    def test_serve(self):
        p, main = None, None  # ut.serve(self.root, 3880)
        p.start()
        p.join()


if __name__ == "__main__":
    unittest.main()

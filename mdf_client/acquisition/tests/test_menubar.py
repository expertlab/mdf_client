#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Tests Archive"""
import unittest

try:
    from mdf_droid import server
except:
    server = False
import logging
from mdf_client.acquisition import menubar
from mdf_client.acquisition.tests.test_controls import Parent
from mdf_droid.tests import reset_test_memory
from mdf_client.qt import QtWidgets, QtGui, QtCore


def setUpModule():
    logging.debug('setUpModule', __name__)
    reset_test_memory('mdf_client')


def tearDownModule():
    logging.debug('tearDownModule', __name__)


class Parent(QtWidgets.QWidget):
    fixedDoc = False
    measureDock = False
    snapshotsDock = False
    graphWin = False
    tableWin = False
    logDock = False
    plotboardDock = False
    controls = False
    doc = None

    def setInstrument(self, instrument): 
        self.ins = instrument

    def init_instrument(self):
        return True

    def delayed_start(self):
        return True


@unittest.skipIf(not server, 'MenuBar test requires mdf_droid module')
class MenuBar(unittest.TestCase):

    def setUp(self):
        
        self.root = server.MainServer(plug='mdf_droid.instrument.Instrument')
        self.root._readLevel = 5
        self.remote_instrument = self.root.instruments[0]
        self.remote_instrument.parent = lambda: self.root
        self.remote_instrument.server = self.root
        self.parent = Parent()
        self.menu_bar = menubar.MenuBar(self.root, self.parent)

    @unittest.skip('FIXME')
    def test_init(self):
        number_of_instruments = len(self.root.instruments)
        self.assertEqual(
            len(self.menu_bar.lstInstruments), number_of_instruments)
        self.assertEqual(
            len(self.menu_bar.instruments.actions()), number_of_instruments)

    def test_setInstrument(self):
        self.menu_bar.setInstrument(self.remote_instrument, self.root)

    def test_updateActions(self):
        self.menu_bar.updateActions()


if __name__ == "__main__":
    unittest.main()

#!/usr/bin/python
# -*- coding: utf-8 -*-
from time import time, sleep
import threading
from traceback import format_exc
import os
import functools
from veusz import utils as vutils
from mdf_canon.logger import get_module_logging
from mdf_canon import csutil
from mdf_canon.plugin import NullTasks
from .. import _
from ..live import registry
from .. import network
from .. import fileui, filedata
from .. import iutils
from .. import parameters
from ..clientconf import confdb
from .menubar import MenuBar
from .measureinfo import MeasureInfo
from .results import Results
from .. import graphics
from sys import argv
from mdf_client.qt import QtWidgets, QtGui, QtCore, uconnect

logging = get_module_logging(__name__)
subWinFlags = QtCore.Qt.CustomizeWindowHint | QtCore.Qt.WindowTitleHint | QtCore.Qt.WindowMinMaxButtonsHint

blockade = False


class BlockadeFilter(QtCore.QObject):

    def eventFilter(self, obj, ev):
        if blockade and isinstance(ev, (QtGui.QMouseEvent, QtGui.QKeyEvent)):
            print('BLOCKADE', ev, ev.type())
            return True
        return False


def do_blockade(func):

    @functools.wraps(func)
    def do_blockade_wrapper(self, *a, **k):
        global blockade
        blockade = True
        ret = None
        try:
            ret = func(self, *a, **k)
        except:
            logging.error('Error in blockade')
            raise
        finally:
            blockade = False
        return ret

    return do_blockade_wrapper


class AbstractTestWindow(QtWidgets.QMainWindow):

    """Generalized Acquisition Interface"""
    remote = None
    doc = False
    uid = False
    name = 'AbstractTestWindow'
    reset_instrument = QtCore.pyqtSignal()
    """Connected to setInstrument"""
    server = False
    remote = False
    myMenuBar = False
    plotboardDock = False
    serverDock = False
    instrumentDock = False
    license = True
    controls = False
    _tasks = False
    _blocked = None
    
    @property
    def blocked(self):
        return blockade
    
    @blocked.setter
    def blocked(self, val):
        global blockade
        if val and not blockade:
            pass
        blockade = val
    
    def delayed_start(self):
        return False

    def instrument_pid(self):
        return 'Instrument: ' + self.name

    def __init__(self, doc=False, parent=None):
        super(AbstractTestWindow, self).__init__(parent)
        self._lock = threading.Lock()
        self.saved_set = set()
        self.cameras = {}
        self.toolbars = []
        self.fixedDoc = doc
        self.server = False
        self.area = QtWidgets.QMdiArea()
        self.setCentralWidget(self.area)
        
        self.tray_icon = QtWidgets.QSystemTrayIcon(self)
        self.tray_icon.setIcon(iutils.theme_icon('icon'))
        
        self.add_statusbar()

        self.reset_file_proxy_timer = QtCore.QTimer(self)
        self.reset_instrument_timer = QtCore.QTimer(self)
        self.setWindowIcon(iutils.theme_icon('icon'))
    
    def add_menubar(self, setInstrument=True):
        logging.debug('add_menubar')
        if self.myMenuBar:
            self.myMenuBar.clear_windows()
            self.myMenuBar.hide()
            self.myMenuBar.close()
        self.myMenuBar = MenuBar(server=self.server, parent=self)
        if self.remote and self.server and setInstrument:
            self.myMenuBar.setInstrument(self.remote, self.server)
        self.setMenuBar(self.myMenuBar)
        self.myMenuBar.quitClient.connect(self.close)
        
    def add_statusbar(self):
        """Add statusBar widgets"""
        statusbar = self.statusbar = QtWidgets.QStatusBar(self)
        self.setStatusBar(statusbar)
        self.updateStatusbar(_('Ready'))

        # a label for the picker readout
        self.pickerlabel = QtWidgets.QLabel(statusbar)
        self._setPickerFont(self.pickerlabel)
        statusbar.addPermanentWidget(self.pickerlabel)
        self.pickerlabel.hide()

        # plot queue - how many plots are currently being drawn
        self.plotqueuecount = 0
        
        self.plotqueuelabel = QtWidgets.QLabel()
        self.plotqueuelabel.setToolTip(_("Number of rendering jobs remaining"))
        statusbar.addWidget(self.plotqueuelabel)
        self.plotqueuelabel.show()

        # a label for the cursor position readout
        self.widgetnamelabel = QtWidgets.QLabel(statusbar)
        statusbar.addPermanentWidget(self.widgetnamelabel)
        self.widgetnamelabel.show()
        
        # a label for the cursor position readout
        self.axisvalueslabel = QtWidgets.QLabel(statusbar)
        statusbar.addPermanentWidget(self.axisvalueslabel)
        self.axisvalueslabel.show()

        # a label for the page number readout
        self.pagelabel = QtWidgets.QLabel(statusbar)
        statusbar.addPermanentWidget(self.pagelabel)
        self.pagelabel.show()
        
    def connect_statusbar(self):
        logging.debug('AbstractTestWindow.connect_statusbar')
        uconnect(self.summaryPlot.plot.sigQueueChange, self.plotQueueChanged)
        uconnect(self.summaryPlot.plot.sigPointPicked, self.slotUpdatePickerLabel)
        uconnect(self.summaryPlot.plot.sigAxisValuesFromMouse, self.slotUpdateAxisValues)
        uconnect(self.summaryPlot.plot.sigNearestWidget, self.slotUpdateNearWidget)
        uconnect(self.summaryPlot.plot.sigUpdatePage, self.slotUpdatePage)
        
    def slotUpdateNearWidget(self, widget):
        txt = widget.path
        for s in ('notes', 'key', 'option', 'label'):
            if s in widget.settings:
                txt = widget.settings.get(s).val
                break
        txt = txt[:45] + (txt[45:] and '..')
        self.widgetnamelabel.setText(txt.replace('\n', '\\n') + ' ')
        
    def slotUpdatePickerLabel(self, info):
        """Display the picked point"""
        # TODO: disentangle from Veusz code and import from there
        xv, yv = info.coords
        xn, yn = info.labels
        xt, yt = info.displaytype
        ix = str(info.index)
        if ix:
            ix = '[' + ix + ']'

        # format values for display
        def fmt(val, dtype):
            if dtype == 'date':
                return vutils.dateFloatToString(val)
            elif dtype == 'numeric':
                return '%0.5g' % val
            elif dtype == 'text':
                return val
            else:
                raise RuntimeError

        xtext = fmt(xv, xt)
        ytext = fmt(yv, yt)

        t = '%s: %s%s = %s, %s%s = %s' % (
            info.widget.name, xn, ix, xtext, yn, ix, ytext)
        self.pickerlabel.setText(t)
        
    def slotUpdateAxisValues(self, values):
        """Update the position where the mouse is relative to the axes."""
        # TODO: disentangle from Veusz code and import from there
        if values:
            # construct comma separated text representing axis values
            valitems = [
                '%s=%#.4g' % (name, values[name])
                for name in sorted(values) ]
            self.axisvalueslabel.setText(', '.join(valitems))
        else:
            self.axisvalueslabel.setText(_('No position'))
            
    def slotUpdatePage(self, number):
        """Update page number when the plot window says so."""
        if not self.doc:
            logging.debug('Cannot update page: no document', self.doc, number)
            return False
        np = self.doc.getNumberPages()
        if np == 0:
            self.pagelabel.setText(_("No pages"))
        else:
            self.pagelabel.setText(_("Page %i/%i") % (number + 1, np))
            
    def plotQueueChanged(self, incr):
        self.plotqueuecount += incr
        text = u'•' * self.plotqueuecount
        self.plotqueuelabel.setText(text)
            
    def updateStatusbar(self, text):
        '''Display text for a set period.'''
        self.statusBar().showMessage(text, 2000)
        
    def _setPickerFont(self, label):
        f = label.font()
        f.setBold(True)
        f.setPointSizeF(f.pointSizeF() * 1.2)
        label.setFont(f)

    def notify_status(self, level, msg):
        """Notify single message into the statusbar"""
        if level < confdb['lognotify']:
            return False
        self.updateStatusbar(msg)
        return True
    
    def notify_tray(self, level, msgs, title='MDF Server'):
        """Notify messages into the systray"""
        if level < confdb['lognotify']:
            return False
        self.tray_icon.show()
        self.tray_icon.showMessage(title, msgs, msecs=level * 50)
        self.notify_status(level, msgs)

    def focus_logging(self):
        self.logDock.show()

    def rem(self, name=False, win=False):
        """Removes a dock widget by name"""
        d = False
        if name:
            d = getattr(self, name, False)
            if d:
                self.removeDockWidget(d)
                d.deleteLater()
            setattr(self, name, None)
        if not win:
            return
        w = getattr(self, win, False)
        if w:
            try:
                w.blockSignals(True)
                w.deleteLater()
            except: pass
        setattr(self, win, None)
 
    def closeEvent(self, ev):
        self.clean_interface()
        ev.accept()
        return super(AbstractTestWindow, self).closeEvent(ev)

    _blockResetFileProxy = False
    
    def get_runningInstrument(self):
        ri = self.server['runningInstrument']  # currently running
        if ri in ['None', '']:
            ri = self.server['lastInstrument']  # configured, ready, finished
        if ri not in ['None', '']:
            remote = getattr(self.server, ri)
            return remote
        return False
    
    def add_logDock(self):
        self.rem('logDock')
        self.logDock = QtWidgets.QDockWidget(self.centralWidget())
        self.logDock.setWindowTitle('Log Messages')
        self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, self.logDock)
        return self.logDock

    def set_server(self, server=False):
        self._blockResetFileProxy = True
        logging.debug('Setting server to', server)
        self.server = server
        registry.toggle_run(True)
        if self.serverDock:
            self.serverDock.hide()
        self.add_menubar(setInstrument=False)
        remote = self.get_runningInstrument()
        load_running = self.server['isRunning'] or self.server['delayStart'] or '--active' in argv
        if (self.fixedDoc or load_running) and remote is not False:
            self.updateInstrumentInterface_when_instrument_is_ready(remote)
        return True

    def add_measure(self):
        self.rem('measureDock', 'measureTab')
        self.measureDock = QtWidgets.QDockWidget(self.centralWidget())
        self.measureDock.setWindowTitle(_('Test Configuration'))
        self.measureTab = MeasureInfo(
            self.remote, self.fixedDoc, parent=self.measureDock)
        
        self.measureDock.setWidget(self.measureTab)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, self.measureDock)
        self.measureTab.setMaximumWidth(600)

    imageSlider = False
    snapshotsDock = False

    def add_snapshotsDock(self):
        self.rem('snapshotsDock')
        if self.imageSlider:
            self.imageSlider.close()
            self.imageSlider.deleteLater()
        self.snapshotsDock = QtWidgets.QDockWidget(self.centralWidget())
        self.snapshotsDock.setWindowTitle(_('Story Board'))
        self.imageSlider = fileui.ImageSlider()
        self.snapshotsDock.setWidget(self.imageSlider)
        self.addDockWidget(QtCore.Qt.BottomDockWidgetArea, self.snapshotsDock)
        self.interconnect()
        return self.snapshotsDock
    
    plot = False
    navigator = False

    def interconnect(self):
        # connect
        if not self.doc:
            return
        if self.imageSlider:
            self.imageSlider.set_doc(self.doc)
            uconnect(self.imageSlider.sig_set_time, self.set_slider_position)
            uconnect(self.imageSlider.sliderReleased, self.slider_released)
            uconnect(self.imageSlider.sig_set_time, self.navigator.set_time)
            if self.summaryPlot:
                uconnect(self.summaryPlot.sig_move_line, self.imageSlider.set_time)
            if self.dataTable:
                uconnect(self.dataTable.sig_user_changed_idx, self.imageSlider.set_time)
        if self.summaryPlot and self.dataTable:
            uconnect(self.summaryPlot.sig_move_line, self.dataTable.set_time)
            uconnect(self.summaryPlot.sig_set_idx, self.dataTable.set_idx)
            uconnect(self.dataTable.sig_user_changed_idx, self.summaryPlot.set_time)

    def add_sumtab(self):
        self.measureTab.set_results(Results(self.measureTab, self.summaryPlot, self.server._readLevel))
        self.navigator = self.measureTab.results.navigator
        self.navigator.set_doc(self.doc)
        self.server._navigator = self.navigator
        self.measureTab.set_doc(self.doc)
    
    summaryPlot = False
    graphWin = False

    def add_graphWin(self):
        self.rem(win='graphWin')
        self.summaryPlot = graphics.Plot()
        self.graphWin = self.centralWidget().addSubWindow(
            self.summaryPlot, subWinFlags)
        self.graphWin.setWindowTitle(_('Data Plot'))
        self.summaryPlot.set_doc(self.doc)
        if not self.fixedDoc:
            self.summaryPlot.plot.actionSetTimeout(2000, True)
        self.add_sumtab()
        self.interconnect()
        self.connect_statusbar()
        return self.graphWin

    dataTable = False
    tableWin = False

    def add_tableWin(self):
        self.rem(win='tableWin')
        self.dataTable = fileui.SummaryView(parent=self)
        self.tableWin = self.centralWidget().addSubWindow(
            self.dataTable, subWinFlags)
        self.dataTable.set_doc(self.doc)
        self.interconnect()
        return self.tableWin
        
    def add_compare(self):
        return False
    
    @property
    def tasks(self):
        if not self.remote:
            return NullTasks()
        if getattr(self, '_tasks', False):
            return self._tasks
        if self.blocked:
            return registry.tasks
        if self._tasks is not registry.tasks:
            logging.debug('Preparing registry.tasks object')
            registry.set_manager(network.manager)
            if self.server and not self.fixedDoc:
                registry.progress.set_server(self.server)
            self._tasks = registry.tasks
        return self._tasks
    
    def updateInstrumentInterface(self, remote=False):
        logging.debug('AbstractTestWindow.updateInstrumentInterface')
        self.tasks.done('Waiting for server')
        
        if remote is False:
            remote = self.remote
        else:
            self.remote = remote
        
        self.clean_interface(remote)
        
        logging.debug('Done controls')
        
        pid = self.instrument_pid()
        self.tasks.job(-1, pid, 'Status panel')
        self.add_measure()

        # Update Menu
        self.myMenuBar.setInstrument(remote, server=self.server)
        self.myMenuBar.show()
        
        # Reset decoder and plot
        self.tasks.job(-1, pid, 'Document')
        if self.fixedDoc:
            self.resetFileProxy()
        return True

    def updateInstrumentInterface_when_instrument_is_ready(self, remote):
        if remote is False:
            remote = self.remote
        else:
            self.remote = remote
       
        if self.fixedDoc:
            self.clean_interface(remote)
            self.updateInstrumentInterface(remote)
            return True

        self.tasks.jobs(-1, 'Waiting for server')
        if self.remote['initInstrument'] or self.server['initInstrument'] or self.server['progress']:
            self.blocked = False
            self.reset_instrument_timer.singleShot(
                2500,
                lambda: self.updateInstrumentInterface_when_instrument_is_ready(
                    remote)
            )
            return False
        
        self.clean_interface(remote)
        self.updateInstrumentInterface(remote)
        return True

    def clean_interface(self, remote=False):
        logging.debug('AbstractTestWindow.clean_interface')
        self._blockResetFileProxy = True
        self.remote = remote
        self.name = 'unknown'
        title = 'MDF Acquisition - Waiting'
        if self.remote:
            name = self.remote['devpath']
            self.name = name
            logging.debug('Setting remote ', remote, self.remote, name)
            title = _('MDF Acquisition: %s (%s)') % (name, self.remote['comment'])
            
        self.setWindowTitle(title)
        self.tray_icon.setToolTip(title)
        pid = self.instrument_pid()
        self.tasks.jobs(11, pid)
        QtWidgets.qApp.processEvents()
        # Close cameras
        for p, (pic, win) in self.cameras.items():
            logging.debug('deleting cameras', p, pic, win)
            pic.close()
            win.hide()
            win.close()
            win.deleteLater()
        self.cameras.clear()
        QtWidgets.qApp.processEvents()

        self.tasks.job(1, pid, 'Preparing menus')
        self.add_menubar(setInstrument=False)
        logging.debug('Done menubar')
        
        # Remove any remaining subwindow
        self.centralWidget().closeAllSubWindows()
        
        self.tasks.job(1, pid, 'Controls')
        for tb in self.toolbars:
            self.removeToolBar(tb)
            tb.disconnect()
            tb.close()
            del tb
        self.toolbars = []
        logging.debug('Cleaned toolbars')
        
        if self.instrumentDock:
            self.instrumentDock.hide()
            
        self.rem(win='graphWin')
        self.rem(win='tableWin')
        self.rem('snapshotDock')

#   @csutil.lockme()
    def set_doc(self, doc=False):
        if doc is False:
            doc = filedata.MisuraDocument(root=self.server)
        doc.up = True
        pid = 'Data display'
        self.tasks.jobs(10, pid)
        self.doc = doc
        self.tasks.job(-1, pid, 'Setting document in live registry')
        
        registry.set_doc(doc)
        
        if self.graphWin:
            self.rem(win='graphWin')
        if self.tableWin:
            self.rem(win='tableWin')
        if self.snapshotsDock:
            self.rem('snapshotDock')
        self.interconnect()
        self.tasks.done(pid)

    max_retry = 10

    def set_slider_position(self, position):
        self.current_slider_position = position

    def slider_released(self):
        self.summaryPlot.set_time(self.current_slider_position)

    def resetFileProxy(self, *a, **k):
        doc = self.doc
        logging.debug('RESETFILEPROXY', doc.proxy_filename, tuple(doc.data.keys()), doc.up)
        self.set_doc(self.doc)
        self.doc.reloadData()
        logging.debug('RESETFILEPROXY2', self.doc.proxy_filename, tuple(self.doc.data.keys()), self.doc.up)

    def refresh_header(self):
        self.server.storage.test.live.header(['Array', 'FixedTimeArray'], False, True)


#!/usr/bin/python
# -*- coding: utf-8 -*-
from threading import Lock
from traceback import format_exc
from time import sleep

from mdf_canon.logger import get_module_logging
from mdf_client.beholder.motionplane import motor_align_factor
from mdf_client.iutils import theme_icon
logging = get_module_logging(__name__)

from .. import widgets, _
from ..live import registry
from .. import iutils
from .. import cloud

from .messages import initial_sample_dimension, ValidationDialog

from mdf_client.qt import QtWidgets, QtGui, QtCore
qm = QtWidgets.QMessageBox

started_set = set([''])
stopped_set = set([''])


class Controls(QtWidgets.QToolBar):

    """Start/stop toolbar"""
    mute = False
    motor = False
    coolAct = False
    focusAct = False
    findAct = False
    isRunning = None
    """Local running status"""
    paused = False
    """Do not update actions"""
    _lock = False
    """Multithreading lock"""
    started = QtCore.pyqtSignal()
    stopped = QtCore.pyqtSignal()
    sig_warning = QtCore.pyqtSignal(str, str)
    closingTest_kid = False
    stop_mode = True
    stop_message = ''
    uid = None
    
    @property
    def started_set(self):
        global started_set
        return started_set
     
    @property
    def stopped_set(self):
        global stopped_set
        return stopped_set

    def __init__(self, remote, parent=None):
        QtWidgets.QToolBar.__init__(self, parent)
        self.setWindowTitle(_('Controls'))
        self._lock = Lock()
        self.remote = remote
        logging.debug('Controls: init')
        self.server = remote.parent()
        self.startAct = self.addAction(_('Start'), self.start)
        self.stopAct = self.addAction(_('Stop'), self.stop)
        self.delayAct = self.addAction(_('Delay'), parent.delayed_start)
        self.name = self.remote['devpath'].lower()

        if self.name != 'kiln':
            self.coolAct = self.addAction(_('Cool'), self.stop_kiln)
        if 'autofocus' in self.remote:
            self.focusAct = self.addAction(_('Focus'), self.autofocus)
        if 'autofind' in self.remote:
            self.findAct = self.addAction(_('Find'), self.autofind)

        logging.debug('Controls: ', self.name)
        self.isRunning = self.server['isRunning']
        self.updateActions()
        logging.debug('Controls end init')
        self.stopped.connect(self.hide_prog)
        self.started.connect(self.hide_prog)
        self.sig_warning.connect(self.warning)
        self.closingTest_kid = self.remote.gete('closingTest')['kid']
        registry.system_kids.add(self.closingTest_kid)
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.updateActions)
        self.timer.start(2000)

    @property
    def tasks(self):
        """Shortcut to pending tasks dialog"""
        return registry.tasks
    
    def show_message(self, message):
        self.parent().tray_icon.showMessage('MDF Server', message, 10000)
        
    def enterEvent(self, event):
        self.updateActions()

    def leaveEvent(self, event):
        self.updateActions()

    def updateActions(self):
        """Update status visualization and notify third-party changes"""
        if self.paused:
            logging.debug('Cannot updateActions: paused')
            return self.isRunning
        if self.parent().fixedDoc:
            return False
        # Always reconnect in case it is called in a different thread
        remote_server = self.server.copy()
        remote_server.connect()
        last = remote_server['lastInstrument']
        if not last:
            logging.debug('updateActions: No active instrument', last)
            return False
        ins = getattr(remote_server, last)
        init_test = ins['initTest']
        remote_is_running = bool(remote_server['isRunning'])
        closingTest = self.remote['closingTest']
        notrun = not remote_is_running and not closingTest and not init_test
        
        if init_test:
            self.show_prog_starting()
        self.stopAct.setEnabled(not closingTest and (init_test or remote_is_running))
        self.startAct.setEnabled(notrun)
        self.delayAct.setEnabled(notrun)
        if self.coolAct:
            self.coolAct.setEnabled(remote_server.kiln['analysis'] and not closingTest)
        if self.focusAct:
            self.focusAct.setEnabled(notrun)
        if self.findAct:
            self.findAct.setEnabled(notrun)
        
        tray_icon = self.parent().tray_icon
        if remote_is_running:
            name = remote_server['runningInstrument']
            tray_icon.setIcon(iutils.theme_icon(name))
            tray_icon.setToolTip(_('MDF is running a {} test').format(ins['comment']))
        else:
            tray_icon.setIcon(iutils.theme_icon('icon'))
            tray_icon.setToolTip(_('MDF is ready'))
        tray_icon.setContextMenu(self.parent().myMenuBar.measure)
        tray_icon.show()
        
        uid = ins.measure['uid']
        if self.isRunning is None:
            [self.stopped_set, self.started_set][int(remote_is_running)].add(uid)
        elif remote_is_running and (uid not in self.started_set):
                self.started_set.add(uid)
                if self.isRunning != remote_is_running:
                    self.started.emit()
                    self.show_message(_('A new test was started'))
        elif (not remote_is_running) and (uid not in self.stopped_set):
                self.stopped_set.add(uid)
                if self.isRunning != remote_is_running:
                    self.stopped.emit()
                    self.show_message(_('Finished test'))
        # Locally remember remote_is_running status
        self.isRunning = remote_is_running
        return remote_is_running or init_test

    def _async(self, method, *a, **k):
        """Execute `method` in global thread pool, passing `*a`,`**k` arguments."""
        r = widgets.RunMethod(method, *a, **k)
        r.pid = self.async_pid
        QtCore.QThreadPool.globalInstance().start(r)
        return True

    def _sync(self, method, *a, **k):
        """Synchronously execute `method`,passing `*a`,`**k` arguments."""
        method(*a, **k)
        return True

    def warning(self, title, msg=False):
        """Display a warning message box and update actions"""
        logging.debug(title, msg, self.mute)
        if not self.mute:
            if not msg:
                msg = title
            self.parent().notify_tray(30, msg, title=title)

            # qm.warning(self, title, msg)
    msgs = set([])

    def show_prog(self, msg):
        if msg in self.msgs:
            return
        self.msgs.add(msg)
        self.tasks.jobs(0, msg)
        self.tasks.setFocus()

    def hide_prog(self):
        for msg in self.msgs:
            self.tasks.done(msg)
        self.msgs.clear()

    def _start(self):
        # Renovate the connection: we are in a sep thread!
        # self.paused = True
        rem = self.remote.copy()
        rem.connect()
        try:
            msg = rem.start_acquisition()
            self.started.emit()
            self.send_ppm_start(rem)
        except:
            msg = format_exc()
            logging.debug(msg)
        # self.paused = False
        if not self.mute:
            self.started_set.add(rem.measure['uid'])
            self.sig_warning.emit(_('Start Acquisition'),
                      _('Result: ') + msg)
            
    def send_ppm_start(self, rem):
        if 'ppm' not in rem.root.license['functionalities']:
            return
        r, msg = cloud.ppm_command(rem.root, rem, 'ppm_start')
        logging.debug('sent ppm start', r, msg)
        
    def validate(self):
        """Show a confirmation dialog immediately before starting a new test"""
        self.mainWin = self.parent()
        self.mDock = self.mainWin.measureDock
        self.measureTab = self.mDock.widget()
        self.measureTab.setCurrentIndex(0)
        
        self.measureTab.checkCurve()
        
        if not initial_sample_dimension(self.remote, parent=self):
            return False
        if self.updateActions():
            self.show_message(_('Acquisition is already running. Nothing to do.'))
            return False
        
        confirmation = ValidationDialog(self.server, self)
        if confirmation.result() != QtWidgets.QDialog.Accepted:
                confirmation.exec_()
                if confirmation.result() != QtWidgets.QDialog.Accepted:
                    logging.debug('Test start was aborted')
                    return False
        
        return True

    def start(self):
        self.msgs.clear()
        self.async_pid = "Starting"
        self.tasks.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Preferred)

        if not self.validate():
            logging.debug('Not starting - user cancelled')
            return False
        
        self.isRunning = True
        self._async(self._start)
        self.show_prog_starting()
        return True
    
    def show_prog_starting(self):
        logging.debug('show_prog_starting')
        self.show_prog(_("Starting new test"))
        if self.server.kiln.has_child('gas'):
            if self.server.kiln.gas['initCycle']:
                self.show_prog(_("Initializing atmosphere"))

    def _stop(self):
        self.paused = True
        rem = self.remote.copy()
        rem.connect()
        try:
            self.stop_message = rem.stop_acquisition(True)
        except:
            self.stop_message = format_exc()
        self.paused = False

    def stop(self):
        self.msgs.clear()
        self.async_pid = "Stopping"
        self.tasks.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Preferred)
        self.updateActions()
        if not self.stopAct.isEnabled():
            self.warning(
                _('Already stopped'), _('No acquisition is running. Nothing to do.'))
            return
        if not self.mute:
            btn = qm.question(self, _('Warning'),
                              _('Do you want to stop this measurement?'),
                              qm.No | qm.Yes, qm.No)
            if btn == qm.No:
                qm.information(self,
                               _('Nothing done.'),
                               _('Action aborted. The measurement maybe still running.'))
                return False

        self.show_prog("Stopping current test")
        self._async(self._stop)

    def stop_kiln(self):
        """Stop thermal cycle without interrupting the acquisition"""
        # Disable auto-stop on thermal cycle end
        self.remote.measure.setFlags('onKilnStopped', {'enabled': False})
        self.server.kiln['analysis'] = False
        dur = self.remote.measure['duration']
        elp = self.remote.measure['elapsed']
        msg = _('Thermal cycle interrupted')
        if dur > 0:
            rem = (dur * 60 - elp) / 60.
            qm.information(self, msg,
                           _('Thermal cycle interrupted.\nThe test will finish in {:.1f} minutes.').format(rem))
        else:
            self.warning(msg,
                         _('Thermal cycle interrupted, but no test termination is set: acquisition  may continue indefinitely. \nManually interrupt or set a maximum test duration.'))

    def new(self):
        self.parent().init_instrument()
    
    def auto_procedure(self, opt0):
        for opt in ['autofocus', 'autofind']:
            if self.remote[opt]:
                msg = _('Procedure is already running')
                if opt != opt0:
                    msg = _('Another procedure is already running')
                btn = qm.information(self, msg,
                                     _('Procedure "{}" is still running.\nWould you like to interrupt it?').format(opt),
                                     qm.Yes | qm.No)
                if btn == qm.No:
                    return
                if btn == qm.Yes:
                    self.remote[opt] = False
                    if opt == opt0:
                        return
                    sleep(1)
                    break
        
        self.remote[opt0] = True
        QtCore.QTimer.singleShot(1000, self.auto_procedure_show)
        
    def auto_procedure_show(self):
        registry.taskswg.progress.progress.get()
        registry.taskswg.setCurrentIndex(0)
    
    def autofocus(self):
        self.auto_procedure('autofocus')
        
    def autofind(self):
        self.auto_procedure('autofind')


class MotionControls(QtWidgets.QToolBar):

    """Motion toolbar"""
    mute = False
    motor = False
    # cycleNotSaved=False

    def __init__(self, remote, parent=None):
        QtWidgets.QToolBar.__init__(self, "Motion", parent)
        self.remote = remote
        self.server = remote.parent()

        if self.server.kiln['motorStatus'] >= 0:
            self.kmotor = widgets.build(
                self.server, self.server.kiln, self.server.kiln.gete('motorStatus'))
            self.kmotor.label_widget.setText('Furnace:')
            self.kmotor.lay.insertWidget(0, self.kmotor.label_widget)
            self.addWidget(self.kmotor)

        paths = {}
        # Collect all focus paths
        for pic, win in self.parent().cameras.values():
            obj, align = motor_align_factor(pic.remote, 'z')
            if not obj:
                continue
            paths[obj['fullpath']] = obj
        for obj in paths.values():
            self.add_focus(obj)

    def add_focus(self, obj):
        slider = widgets.MotorSlider(self.server, obj, self.parent())
        slider.lay.insertWidget(0, QtWidgets.QLabel(_('    Focus:')))
        self.addWidget(slider)

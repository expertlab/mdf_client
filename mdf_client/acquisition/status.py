#!/usr/bin/python
# -*- coding: utf-8 -*
from mdf_client.qt import QtWidgets, QtCore
from .. import widgets
from .. import _
from ..clientconf import confdb
from mdf_canon import dashboard
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
import xmlrpc


class Status(QtWidgets.QWidget):

    def __init__(self, server, remObj, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.setWindowTitle(_('Status'))
        self.widgets = {}
        # TODO: accept drops
        self.lay = QtWidgets.QFormLayout()
        self.lay.setLabelAlignment(QtCore.Qt.AlignRight)
        self.lay.setRowWrapPolicy(QtWidgets.QFormLayout.WrapLongRows)
        opts = server.dashboard_options()
        if not opts:
            logging.debug('Creating client-side dashboard')
            opts = dashboard.create_dashboard(server, remObj, confdb)
        nSamples = remObj.measure['nSamples']
        done_motor = False
        positions = [] if not opts else sorted(opts.keys())
        for pos in positions:
            # Inject motor after third position
            if len(self.widgets) > 3 and not done_motor:
                self.add_motorStatus(server)
                done_motor = True
            fullpath, handle, force, opt = opts[pos]
            parent = server.toPath(fullpath)
            # Skip empty IO pointers
            if 'options' in opt and opt['options'][0] == 'None':
                    continue
            wg = widgets.build(server, parent, opt)
            if wg is False:
                logging.error('Cannot display position', pos, handle, opt)
                continue
            if wg.type.endswith('IO'):
                wg.value.force_update = force
            elif wg.type == 'Button':
                wg.force_update = False
            else:
                wg.force_update = force
            self.insert_widget(wg)
        self.setLayout(self.lay)
        
    def add_motorStatus(self, server):
        if server.has_child('kiln'):
            if server.kiln['motorStatus'] >= 0:
                wg = widgets.build(
                    server, server.kiln, server.kiln.gete('motorStatus'))
                wg.force_update = True
                self.insert_widget(wg)

    def insert_widget(self, wg):
        if wg is False:
            logging.debug("Cannot insert widget", wg)
            return False
        self.widgets[wg.prop['kid']] = wg
        self.lay.addRow(wg.label_widget, wg)
        if wg.type == 'Button':
            wg.label_widget.hide()
        logging.debug('Inserted widget', wg.type, wg.handle, wg.force_update)
        return True

    def showEvent(self, event):
        s = []
        for kid, wg in self.widgets.items():
            if wg.type == 'Button':
                continue
            if not wg.force_update:
                s.append(wg.handle)
                wg.soft_get()
        logging.debug('Status.showEvent', s)
        return super(Status, self).showEvent(event)

#!/usr/bin/python
# -*- coding: utf-8 -*-
import functools

from .. import _
from mdf_client.qt import QtWidgets, QtGui, QtCore
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)



class DatabaseHeader(QtWidgets.QHeaderView):
    selectDistinct = QtCore.pyqtSignal()
    
    def __init__(self, orientation=QtCore.Qt.Horizontal, parent=None):
        QtWidgets.QHeaderView.__init__(self, orientation, parent=parent)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.show_menu)
        self.menu = QtWidgets.QMenu(self)
        self.setSectionsMovable(True)
        self.setSectionsClickable(True)
        self.setSortIndicatorShown(True)
        

    def show_menu(self, pt):
        QtWidgets.qApp.processEvents()
        self.menu.clear()
        
        section = self.logicalIndexAt(pt)
        column = self.model().header[section]
        distinct = []
        if column in ['serial','instrument','nSamples','flavour','preset','verify']:
            distinct = self.model().remote.distinct(column)
            
        m = self.menu
        if distinct:
            m = self.menu.addMenu(_('Fields'))
        for i, h in enumerate(self.model().sheader):
            act = m.addAction(h, functools.partial(self.switch, i))
            act.setCheckable(True)
            act.setChecked((not self.isSectionHidden(i)) * 2)
        
        for d in distinct:
            act = self.menu.addAction('{}'.format(d), functools.partial(self.toggle_distinct, column, d))
            act.setCheckable(True)
            if d in self.model().distincts[column]:
                act.setChecked(True)
        
        self.menu.popup(self.mapToGlobal(pt))
        
    def toggle_distinct(self, column, value):
        s = self.model().distincts[column]
        if value in s:
            s.remove(value)
        else:
            s.add(value)
        self.selectDistinct.emit()
            

    def switch(self, i):
        if self.isSectionHidden(i):
            logging.debug('showSection', i, self.model().header[i])
            self.showSection(i)
        else:
            logging.debug('hideSection', i, self.model().header[i])
            self.hideSection(i)
            
    def show_all_sections(self):
        for sec in range(len(self.model().header)):
            self.setSectionHidden(sec, False)
        self.reset()
            
    def hide_sections(self, sections):
        self.show_all_sections()
        for sec in sections:
            n = self.model().ncol(sec)
            if n<0:
                continue
            self.setSectionHidden(n, True)
            
    def restore_visual_indexes(self):
        for i in range(len(self.model().header)):
            j = self.visualIndex(i)
            if i!=j:
                self.moveSection(i,j)
                
    def visibility(self):
        ret = []
        for sec in range(len(self.model().header)):
            ret.append(self.isSectionHidden(sec))
        return ret
#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import shutil
import tempfile
from functools import partial
from time import time, sleep
from traceback import format_exc
from urllib.request import URLError
from mdf_canon.logger import get_module_logging
from mdf_canon import option
from mdf_canon.csutil import isWindows
import uuid
from . import _, open_url
from . import conf
from . import widgets
from .confwidget import ClientConf
from .live import registry
from .cloud import query
from .clientconf import confdb
from mdf_canon.version import __version__ as canon_version
from . import parameters
from .version import __version__ as client_version
from .autoupdate import check_server_updates, check_client_updates
from .cloud import get_version_file
from . import parameters as params
import subprocess
from mdf_canon.csutil import go, isWindows
from mdf_client.qt import QtWidgets, QtGui, QtCore, uic
from mdf_canon.csutil import executor

logging = get_module_logging(__name__)

about = """
<h1> MDF &trade;</h1>
<p>mdf_client version: {}<br/>
mdf_canon version: {}</p>
<p>Misura is trademark of Expert System Solutions</p>
<p><small>See public source code repository page for licensing information</small></p>
""".format(client_version, canon_version)


def showAbout():
    dialog = QtWidgets.QDialog()
    ui = os.path.join(parameters.pathUi, 'about_mdf.ui')
    uic.loadUi(ui, dialog)
    dialog.logo_label.setScaledContents(True)
    
    dialog.label_client_version.setText('client: ' + client_version)
    dialog.label_canon_version.setText('canon: ' + canon_version)
    dialog.label_extended_version.setText(get_version_file())

    dialog.logo_label.setPixmap(QtGui.QPixmap(os.path.join(parameters.pathArt, 'logo.png')))
    dialog.setWindowIcon(QtGui.QIcon(os.path.join(parameters.pathArt, 'icon.svg')))

    dialog.exec_()

    
seconds_in_a_week = 3600 * 24 * 7


def start_in_terminal_window(cmd, log=None):
    output = ''
    if log and os.path.exists(log):
        os.remove(log)
    sh = '/tmp/mdf_terminal_script.sh'
    with open(sh, 'w') as file:
        file.write(cmd)
    s, out = go('which konsole||which xterm')
    print('POPEN', out)
    subprocess.Popen([out.replace('\n', ''), '-e', 'bash', '-c', 'source {}'.format(sh)])
    if log:
        sleep(3)
        output = open(log, 'r').read()
    return output


class HelpMenu(QtCore.QObject):

    def add_help_menu(self, menu_bar):
        logging.debug('add_help_menu')
        self.menu_bar = menu_bar
        self.help = menu_bar.addMenu('Help')
        self.help.aboutToShow.connect(self.update_menu)
        self.delay_timer = QtCore.QTimer(self.menu_bar)
        self.delay_timer.timeout.connect(self.delayed_check)
        self.delay_timer.setSingleShot(True)
        self.delay_timer.start(3000)
        
    @property
    def server(self):
        return getattr(self.menu_bar, 'server', False)
        
    def update_menu(self):
        self.help.clear()
        self.help.addAction(_('Client configuration'), self.showClientConf)
        self.help.addAction(_('Documentation'), self.showDocSite)
        self.help.addAction(_('Pending operations'), self.showTasks)
        self.help.addAction(_('Bug report'), self.bug_report)
        if self.server:
            self.help.addAction(_('Check server updates'), self.check_server_updates)
        self.help.addAction(_('Check client updates'), self.check_client_updates)
        self.help.addAction(_('Client license'), self.client_license)
        self.help.addAction(_('Cloud backup'), self.activate_backup)
        self.help.addAction(_('Support request'), self.support_request)
        if not isWindows:
            self.help.addAction(_('Install DWService'), self.reinstall_dwservice)
            self.help.addAction(_('Open DWAgent'), self.open_dwagent)
            self.help.addAction(_('Remote tunnel'), self.activate_remote_assistance)
        
        self.help.addAction(_('Videocall support'), self.videocall_support)
        self.help.addAction(_('About'), showAbout)

    def showClientConf(self):
        """Show client configuration panel"""
        self.cc = ClientConf()
        self.cc.show()

    def showDocSite(self):
        name = QtWidgets.QApplication.applicationName().split(' ')[-1].lower()
        open_url(f'https://app.ceramics-genome.ai/#help/en/mdf/{name}')
        
    def showTasks(self):
        registry.taskswg.set_user_show(True)
    
    def activate_remote_assistance(self):
        log = '/tmp/mdf_remote_support.log'
        uid = str(uuid.uuid4())
        cmd = 'echo "Please wait few minutes for an operator to connect";'
        cmd += 'echo "Close this window to interrupt the remote support connection";'
        cmd += 'date; ssh -o "StrictHostKeyChecking no" -R els-{}.{}:443:localhost:22 '.format(uid, confdb['remoteSupport'])
        cmd += confdb['remoteSupport']
        cmd += ' 2>&1 | tee {}; echo "EXITED"; date; sleep 30'.format(log)
        output = start_in_terminal_window(cmd, log)
        query({'cmd': 'ping', 'remoteSupport': output})
        
    def reinstall_dwservice(self):
        cmd = "rm -f dwagent.sh; wget https://www.dwservice.net/download/dwagent.sh -O dwagent.sh;"
        cmd += "sudo dwagent_uninstall"
        cmd += "sudo bash dwagent.sh"
        start_in_terminal_window(cmd)
        
    def videocall_support(self):
        serial = ''
        if (confdb['recent_server']):
            serial = "&title=" + confdb['recent_server'][0][-2]
        url = "https://www.expertlabservice.it/media/meet/with.php?name=support" + serial
        open_url(url)
        
    def support_request(self):
        url = "https://app.ceramics-genome.ai/#support"
        open_url(url)
        
    def open_dwagent(self):
        go('dwagent_monitor')
        
    def bug_report(self):
        report = {}
        option.ao(
            report, 'title', 'String', name=_("Short title"))
        option.ao(report, 'type', 'Chooser', name=_("Type"), options=[_("Bug"), _("Improvement"), _("New feature")])
        option.ao(report, 'severity', 'Chooser', name=_("Severity"), options=[_("Lowest"),
                                                                              _("Low"),
                                                                              _("Mean"),
                                                                              _("High"),
                                                                              _("Highest"), ])
        option.ao(report, 'steps', 'TextArea', name=_("Steps to reproduce"))
        option.ao(report, 'problem', 'TextArea', name=_("Description"))

        cp = option.ConfigurationProxy({'self': report})
        dia = conf.InterfaceDialog(cp, cp, report)
        dia.setWindowTitle(_('Bug report'))
        
        if not dia.exec_():
            logging.debug('Bug report was aborted')
            return False
        
        # Create a temporary directory, to ensure the path is available
        self._debug_dir = tempfile.mkdtemp()
        # Delete it
        os.removedirs(self._debug_dir)
        logging.debug('Creating bug report archive in', self._debug_dir)
        # Copy all available client logs 
        shutil.copytree(confdb['logdir'], self._debug_dir)
        
        # Write report
        s = option.CsvStore()
        s.desc = cp.desc 
        s.write_file(os.path.join(self._debug_dir, 'report.csv'))
        
        self._backupFileTransfer = False
        self._logsFileTransfer = False
        if self.server:
            # Update debug values
            self.server.support['libs']
            self.server.support['env']
            self.server.support['dmesg']
            self.server.support.save('default')
            # Create configuration backup
            self.server.support['doBackup']
            self._backupFileTransfer = widgets.aFileList(self.server, self.server.support,
                          self.server.support.gete('backups'))
            # Create logs backup
            self.server.support['doLogs']
            self._logsFileTransfer = widgets.aFileList(self.server, self.server.support,
                          self.server.support.gete('logs'))
            # Download them
            for wg in [self._backupFileTransfer, self._logsFileTransfer]:
                wg.hide()
                wg.get()
                if wg.download(os.path.join(self._debug_dir, wg.handle + '.tar.bz2')): 
                    wg.transfer.dlFinished.connect(self._make_debug_archive)
        else:
            self._make_debug_archive()
                    
    def _make_debug_archive(self, url=False, outfile=False):
        """Create the final debug archive"""
        if self._backupFileTransfer and url and '/logs/' in url:
                logging.debug('Backup file transfer finished')
                self._backupFileTransfer = False
        elif self._logsFileTransfer and url and '/backups/' in url:
                logging.debug('Logs file transfer finished')
                self._logsFileTransfer = False
        if self._logsFileTransfer != False or self._backupFileTransfer != False:
            logging.debug('Waiting for file transfers to finish')
            return False
            
        filename = QtWidgets.QFileDialog.getSaveFileName(None,
                                                     _('Choose a filename where to archive the report'),
                                                     '', 'ZIP (*.zip *.ZIP)')
        if not filename:
            logging.debug('Bug report aborted')
            return False
        filename = filename[0]
        if filename.lower().endswith('.zip'):
            filename = filename[:-4]
        if os.name != 'nt':
            from mdf_canon.csutil import go
            go('dmesg > "{}/dmesg.log"'.format(self._debug_dir))
        shutil.make_archive(filename, 'zip', self._debug_dir)
        logging.debug('Created archive', filename, self._debug_dir)
        shutil.rmtree(self._debug_dir)
        logging.debug('Removed temporary debug dir', self._debug_dir)
        
    def check_client_updates(self, verbose=True):
        try:
            r = check_client_updates(self.menu_bar)
            if not r:
                if verbose:
                    QtWidgets.QMessageBox.information(self.menu_bar,
                                                      _('Up to date'),
                            _('MDF Client is up to date.\nNo newer version was found.'))
                return True
            if r is True:
                QtWidgets.QMessageBox.information(self.menu_bar, 'Updated', 'MDF Client sources were updated.')
                return True
            self._client_updater = r  
            return
        except URLError:
            QtWidgets.QMessageBox.warning(self.menu_bar,
                                      'MDF Client update error',
                                      format_exc())    

    def check_server_updates(self, verbose=True):
        try:
            r = check_server_updates(self.server, self.menu_bar)
            if not r:
                if verbose:
                    QtWidgets.QMessageBox.information(self.menu_bar,
                                                      _('Up to date'),
                            _('MDF Server is up to date.\nNo newer version was found.'))
                return True
            self._server_updater = r
        except: 
            QtWidgets.QMessageBox.warning(self.menu_bar,
                                      'MDF Server update error',
                                      format_exc())
            
    def autoupdate(self):
        if not confdb['updateAuto']:
            return True
        if time() - confdb['updateLastClient'] > seconds_in_a_week:
            return self.check_client_updates(verbose=False)
        if self.server and (time() - confdb['updateLastServer'] > seconds_in_a_week):
            return self.check_server_updates(verbose=False)
        return True
    
    def activate_backup(self):
        if not confdb['cloud_enable']:
            r = QtWidgets.QMessageBox.question(self.menu_bar,
                                           _('Cloud backup is not active'),
                                           _('Cloud backup is disabled. Would you like to activate it now?'))
            if not r:
                return False
        confdb['cloud_enable'] = True
        self.backup_status()
    
    _verbose_backup_status = False

    def backup_status(self, verbose=True):
        self._verbose_backup_status = verbose
        logging.debug('backup_status')
        if not confdb['cloud_enable']:
            # TODO: offer first configuration!
            return False
        
        confdb.load_license()
        if not confdb['license_status']:
            self.client_license()
            return False
        
        if not confdb['cloud_remote']:
            if not verbose:
                return False
            remote = QtWidgets.QInputDialog.getText(self.menu_bar,
                                                  _('Please obtain a Remote ID from Expert Lab Service'),
                                                  _('Remote ID, as provided by ELS:'))[0]
            if not remote:
                return False
            confdb['cloud_remote'] = remote
            confdb.save()
        
        if not confdb['cloud_localNick']:
            if not verbose:
                return False
            nick = QtWidgets.QInputDialog.getText(self.menu_bar,
                                                  _('Please obtain a nickname from Expert Lab Service'),
                                                  _('Local nickname, as provided by ELS:'))[0]
            if not nick:
                return False
            confdb['cloud_localNick'] = nick
            confdb.save()
        
        if not confdb['cloud_remoteNick']:
            if not verbose:
                return False
            nick = QtWidgets.QInputDialog.getText(self.menu_bar,
                                                  _('Please obtain remote nickname from Expert Lab Service'),
                                                  _('Remote nickname, as provided by ELS:'))[0]
            if not nick:
                return False
            confdb['cloud_remoteNick'] = nick
            confdb.save()

        from mdf_canon import crystal
        self._backup_status_callback.connect(self.backup_status_callback)
        future = executor().submit(crystal.check_status, params.pathSync, confdb)
        future.add_done_callback(lambda f: self._backup_status_callback.emit(f.result()))
    
    _backup_status_callback = QtCore.pyqtSignal(object)
    
    def backup_status_callback(self, st):
        print('backup_status_callback', st)
        from mdf_canon import crystal
        ret = True
        if st.code not in (0, crystal.ERR_START):
            QtWidgets.QMessageBox.warning(self.menu_bar,
                                          _('Could not start cloud services'),
                                          _('Cloud service could not be started. \n Please review your configuration or contact Expert Lab Service.'))
            ret = False
            
        if ret and self._verbose_backup_status:
            logging.debug('Opening syncthing: ', st.address)
            url = QtCore.QUrl('http://' + st.address)
            QtCore.QTimer.singleShot(3000, partial(open_url, url))
            
            QtWidgets.QMessageBox.information(self.menu_bar,
                                          _('Cloud service is running.'),
                                          _('The cloud service is running fine'))
        
        s = False
        if st.local:
            confdb['cloud_local'] = st.local
            s = True
        if st.remote:
            confdb['cloud_remote'] = st.remote
            s = True
        if s:
            confdb.save()
        
        return ret
    
    _client_license = False

    def client_license(self):
        if self._client_license:
            if self._client_license.isVisible():
                logging.debug('Already asked for client license')
                return False
            logging.debug('Closing stale client license dialog')
            self._client_license.close()
        from . import license
        self._client_license = license.License()
        self._client_license.exec_()
        return True
            
    def delayed_check(self):
        if not self.menu_bar.isVisible():
            logging.debug('delayed_check: skipped')
            return 
        logging.debug('delayed_check: run')
        if self.autoupdate():
            self.backup_status(verbose=False)
        

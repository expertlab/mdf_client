#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Tests DataDecoder"""
import unittest

import os
from mdf_client.tests import iutils_testing
from mdf_client import filedata

temp_file = os.path.join(iutils_testing.data_dir, 'delete_me')


class DataDecoder(unittest.TestCase):

    def tearDown(self):
        iutils_testing.silent_remove(temp_file)

    def test(self):
        nativem4 = os.path.join(iutils_testing.data_dir, 'test_video.h5')
        fp = filedata.getFileProxy(nativem4)

        decoder = filedata.DataDecoder()
        decoder.reset(fp, '/hsm/sample0/profile')

        self.assertEqual(decoder.ext, 'Profile')

        r = decoder.get_data(0)
        self.assertIn(0, decoder.cached_profiles)

        self.assertFalse(os.path.exists(temp_file))
        r[1].save(temp_file, 'JPG')
        self.assertTrue(os.path.exists(temp_file))

        r = decoder.get_data(1)
        self.assertIn(1, decoder.cached_profiles)

        r = decoder.get_data(2)
        self.assertIn(2, decoder.cached_profiles)

        fp.close()

    def test_2(self):
        return
        nativem4 = '/home/daniele/tmp/ccimola/GFM343LT25_0  GFM343LT25_1 1300C - 20Cmin.h5'
        nativem4 = '/data/MisuraData/hsm/measure_8.h5'
        fp = filedata.getFileProxy(nativem4)
        decoder = filedata.DataDecoder()
        decoder.reset(fp, '/hsm/sample0/profile')
        self.assertEqual(decoder.ext, 'CumulativeProfile')
        t, qimg = decoder.get_data(0)
        qimg.save('test.png')
        dat = decoder.dat
        import pickle
        pickle.dump(dat, open('dat', 'wb'))
        ((w, h), x, y) = dat
        # from matplotlib import pylab as plt
        # plt.plot(x, y)
        # plt.show()


if __name__ == "__main__":
    unittest.main(verbosity=2)

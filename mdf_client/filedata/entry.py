#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Classes representing datasets in the DocumentModel."""
import os
from mdf_canon.logger import get_module_logging
from veusz import document
from itertools import chain
import collections
import re
import numpy as np
from mdf_canon.csutil import natural_keys
from mdf_client.clientconf import confdb

logging = get_module_logging(__name__)
sep = '/'
# Statuses
dstats = collections.namedtuple(
    'DocumentModelEntryStatus', ('empty hidden outline available loaded'))(0, 1, 2, 3, 4)


def iterpath(name, parent=False, splt=sep):
    v = name.split(splt)
    # Split the prefix, if found
    if ':' in v[0]:
        g = v[0].split(':')
        v = g + v[1:]
        
    # Recursively build the path
    isLeaf = False
    for i, sub in enumerate(v):
        if not parent and i == 1:
            parent = v[0]
        elif parent:
            parent = v[0] + ':' + splt.join(v[1:i])
        if i == len(v) - 1:
            isLeaf = True
        yield sub, parent, isLeaf


def find_pos(dslist, p=0):
    for d in dslist:
        if hasattr(d, 'm_pos'):
            p = max(p, d.m_pos)
        else:
            d.m_pos = p + 1
            logging.debug('Assign pos', d.path, p + 1)
        p += 1
    for d in dslist:
        p = find_pos(list(d.children.keys()), p)
    return p


def node_natural_keys(node):
    return natural_keys(node.name())


class AllDocDataAccessor(object):

    """Simulates a document with access to all data, comprising loadable datasets not in the document."""

    def __init__(self, doc):
        self.doc = doc

    @property
    def data(self):
        return self.doc.data

    @property
    def available_data(self):
        return self.doc.available_data

    def has_key(self, k, ret=False):
        if k in self.data:
            if ret:
                return self.data[k]
            return True
        if k in self.available_data:
            if ret:
                return self.available_data[k]
            return True
        return False
    
    def __contains__(self, k):
        return self.has_key(k)

    def get(self, k, *a):
        val = k in self
        if not val:
            if len(a) == 1:
                return a[0]
            raise KeyError("Key not found: {}".format(k))
        val = self.has_key(k, ret=True)
        return val

    def __getitem__(self, k):
        return self.get(k)

    def items(self):
        return [(k, self.get(k)) for k in self.keys()]

    def keys(self):
        k = list(self.data.keys()) + list(self.available_data.keys())
        return sorted(k)


class NodeEntry(object):

    """Generic node representation for the document model"""
    _status = dstats.loaded
    """Accessibility status"""
    statuses = {v: [] for v in range(0, 4)}
    """Children accessibility status"""
    idx = -1
    _name = 'root'
    """Node base name"""
    _path = False  # auto calc
    """Node full path or dataset key in doc.data"""
    _root = False
    _depth = None
    _parent_nodes = None
    _model_path = False
    parent = False
    """Parent node"""
    parents = []
    """Multiple parents"""
    splt = sep
    """Hierarchy splitter symbol"""
    _linked = False
    """Linked file for first-level nodes"""
    status_cache = (None, False)
    regex_rule = ''
    regex = False
    is_derived = False

    def __init__(self, doc=False, name='', parent=False, path=False, model_path=False, splt=sep):
        self.splt = splt
        self.doc = doc
        self.alldoc = AllDocDataAccessor(doc)
        self._name = name
        self._children = collections.OrderedDict()
        self._path = path
        self._parents = []
        self._model_path = model_path
        if parent is not False:
            parent.append(self)

    def name(self):
        return self._name
    
    def __iter__(self):
        return self._children.__iter__()
    
    def items(self):
        return self._children.items()
    
    def values(self):
        return self._children.values()
    
    def free(self):
        self._linked = False
        self.doc = False
        self._root = False
        self.parent = False
        self._parents = []
        self.alldoc = {}
        self.names = {}
        self.status_cache = (None, False)
        for ent in list(self._children.values()) + self.parents:
            ent.free()
        self._children = collections.OrderedDict()

    def copy(self):
        c = self.__class__()
        c.doc = self.doc
        c._name = self._name
        c._children = self._children.copy()
        c._path = self.path
        c.parent = self.parent
        c._linked = self.linked
        c.splt = self.splt
        return c

    @property
    def path(self):
        """Node full path/dataset name"""
        if self._path:
            return self._path
        # Root and file entries
        if not self.parent:
            self._path = self._name
        # File entry
        elif not self.parent.parent:
            self._path = self._name
        # First level entries (t, groups)
        elif not self.parent.parent.parent:
            self._path = self.parent.path + ':' + self._name
        # Normal entries
        else:
            self._path = self.splt.join([self.parent.path, self._name])
        return self._path

    @property
    def model_path(self):
        if not self._model_path:
            return self.path
        return self._model_path

    @path.setter
    def path(self, nval):
        self._path = nval

    @property
    def ds(self):
        return False

    @property
    def unit(self):
        if not self.ds:
            return False

        return getattr(self.ds, 'unit', False)
    
    @property
    def attr(self):
        if not self.ds:
            return False

        return getattr(self.ds, 'attr', False)        

    @property
    def data(self):
        if not self.ds:
            return np.array([])
        return self.ds.data

    @property
    def mid(self):
        return id(self)

    @property
    def children(self):
        return self._children

    @property
    def mtype(self):
        return self.__class__.__name__

    def __len__(self):
        return len(self._children)

    def __nonzero__(self):
        return True

    def __bool__(self):
        return True

    def keys(self, status=dstats.loaded):
        """List keys based on status"""
        r = []
        for sub, item in self.children.items():
            if item.status == status:
                r.append(sub)
        return r

    def __repr__(self):
        return '%s.%i.%i.(%s):%i' % (self._name, self.idx, len(self.children), self.parent, self.status)

    def append(self, item):
        item.parent = self
        self._children[item._name] = item
        
    def detach(self):
        for p in self.parent_nodes:
            r = p._children.pop(self._name, False)
            print('Detach node', self._name, p.model_path, r)
        self.parent = None

    def get(self, *a):
        ret = self.children.get(*a)
        return ret

    def traverse(self, path):
        item = self
        for sub, parent, isLeaf in iterpath(path):
            item1 = item.get(sub, False)
            if item1 is False:
                break
            if isLeaf:
                break
            item = item1
            
        # Resolving leaf
        if not item1:
            for k, e in item._children.items():
                if e.model_path == path:
                    item1 = e
                    break
                
        return item1

    def traverse_model_path(self, path):
        """path in the model tree"""
        item = self
        e = False
        for e in item._children.values():
            if e.model_path == path:
                break
            e = e.traverse_model_path(path)
            if e:
                break
        return e
    
    def traverse_path(self, path):
        """path is the unique dataset name"""
        item = self
        e = False
        for e in item._children.values():
            if e.path == path:
                break
            e = e.traverse_path(path)
            if e:
                break
        return e
    
    def calc_root(self):
        """Retrieve the root node of the tree"""
        if self._root:
            return self._root, self._depth, self._parent_nodes
        item = self
        items = []
        depth = 0
        while item.parent is not False:
            item = item.parent
            if item:
                items.append(item)
            depth += 1
        self._root = item
        self._depth = depth
        self._parent_nodes = items[::-1]
        return self._root, self._depth, self._parent_nodes
    
    @property
    def root(self):
        if self._root:
            return self._root
        return self.calc_root()[0]
    
    @property
    def depth(self):
        if self._depth is not None:
            return self._depth
        return self.calc_root()[1]
    
    @property
    def parent_nodes(self):
        if self._parent_nodes is not None:
            return self._parent_nodes
        return self.calc_root()[2]
    
    def set_filter(self, rule):
        """Set the filtering regular expression"""
        self.root.regex_rule = rule
        if rule:
            self.root.regex = re.compile(rule)
        else:
            self.root.regex = False
        self.status_cache = (None, False)

    @property
    def linked(self):
        """Recursive downward search for a valid linked file"""
        if self._linked:
            return self._linked
        for c in self.children.values():
            if c.linked:
                self._linked = c.linked
                return c.linked
        return False

    @linked.setter
    def linked(self, LF):
        self._linked = LF
        
    @property
    def conf_path(self):
        return self.path.split(':')[-1]

    def get_configuration(self):
        if not self.linked:
            logging.debug('Node does not have a linked file', self.path)
            return False
        path = self.conf_path
        configuration_proxy = getattr(self.linked, 'conf', False)
        if not configuration_proxy:
            logging.debug('Linked file has not conf')
            return False
        
        if len(path) > 1:
            configuration_proxy = configuration_proxy.toPath(path)
        return configuration_proxy

    # ROOT ENTRY METHODS

    def insert(self, path, status=dstats.loaded):
        """Insert a node by path"""
        if not self.doc:
            return
        if (path in self.doc.data) and isinstance(self.doc.data[path], document.datasets.Dataset1DPlugin):
            return False
        splt = self.splt
        if self.parent:
            assert path.startswith(self.path)
            # Cut away the common part
            path = path[len(self.path) + len(splt):]
        item = self
        linked = False
        
        for sub, parent, leaf in iterpath(path):
            # Remember the first part of the path (0:summary, etc)
            if not parent:
                linked = sub
            if leaf:
                item = DatasetEntry(doc=self.doc, name=sub, parent=item)
                # Propagate the linked file to the first part of the path
                if item.ds and item.ds.linked:
                    linked_item = self.root.get(linked, None)
                    if linked_item and linked_item.linked == False:
                        linked_item.linked = item.ds.linked
                continue
            new = item.get(sub, False)
            dspath = (parent or '') + ' /'[bool(parent)] + sub
            isds = self.alldoc.get(dspath, False)
            if not new:
                # Existing dataset
                if parent and isds:
                    new = DatasetEntry(doc=self.doc, name=sub, parent=item)
                else:
                    new = NodeEntry(doc=self.doc, name=sub, parent=item)
            item = new

    def remove(self, child):
        k = child._name
        if k not in self._children:
            return False
        self._children.pop(k)
        return True
    
    @property
    def status(self):
        if confdb.rule_nav_hide(self.path):
            return dstats.hidden
        s = [c.status for c in list(self._children.values())]
        if not len(s):
            return dstats.empty
        s = max(s)
        return s
    
    def recursive_status(self, st=dstats.loaded, depth=-1, cls=False):
        """Recursively list children with status in `st`. `st` can be an iterable (ideally a set()), an integer or a status name.
        `exclude`: regular expression to filter node names
        `depth`<0: infinite recursion
        `depth`==0: only direct children
        `cls` restrict results pertaining to `cls` node class
        """
        r = []
        # Get the status by name
        if isinstance(st, str):
            st = getattr(dstats, st)
        # Build an iterable
        if isinstance(st, int):
            st = set([st])
        # Calculate cache key
        key = '.'.join([str(i) for i in sorted(list(st))]) 
        key += '::' + confdb.rule_nav_hide.lines[0]
        # Check filter cache
        if key == self.status_cache[0]:
            return self.status_cache[1]
        m = min(st)
        for child in self.children.values():
            # TODO: an include_rule should enable an excluded parent to appear anyway
            if confdb.rule_nav_hide(child.model_path):
                continue
            if isinstance(child, DatasetEntry):
                if child.status not in st:
                    continue
            if child.status >= m:
                if (not cls) or isinstance(child, cls):
                    r.append(child)
            if depth > 0 or depth < 0:  # depth=0 will block!
                r += child.recursive_status(st, depth=depth - 1)
        self.status_cache = (key, r)
        r.sort(key=node_natural_keys)
        return r

    def set_doc(self, doc, default_status=dstats.available):
        """Build a Hierarchy out of `doc`
        `status`: default status"""
        old = self.copy()
        self.free()
        self.doc = doc
        self.alldoc = AllDocDataAccessor(doc)
        for dn, d in self.alldoc.items():
            dn1 = dn
            if hasattr(d, 'm_var'):
                dn1 = d.m_var
            elif dn.startswith('summary' + self.splt):
                dn1 = dn[7 + len(self.splt):]
            self.names[dn] = dn1
            status = default_status
            if confdb.rule_nav_hide(dn):
                status = dstats.hidden
            elif len(d) == 0 and default_status >= dstats.available:
                status = dstats.available  # put on available
            else:
                status = default_status
            oldentry = old.traverse(dn)
            if oldentry:
                oldst = oldentry.status
                # if it was and still is loaded, keep the old status
                if status >= dstats.available and oldst >= dstats.available:
                    status = oldst
                    
            self.insert(dn, status)


class DatasetEntry(NodeEntry):

    """A wrapper object to represent a dataset by name and document,
    without actually keeping any reference to it."""
    _parents = []
    _linked = None

    @property
    def ds(self):
        """Retrieve the original dataset from the document,
        not keeping any reference to it"""
        return self.alldoc.get(self.path, False)

    @property
    def status(self):
        # Hide if filtered out
        if self.root.regex:
            if not self.root.regex.search(self.path):
                return dstats.empty
        ds = self.alldoc.get(self._path, False)
        if ds is False:
            if self.parent:
                print('DatasetEntry.status: missing ', self.model_path, self.doc.data.keys())
                self.parent.remove(self)
            return dstats.empty
        if len(ds) > 0:
            return dstats.loaded
        return dstats.available
    
    @property
    def is_derived(self):
        return isinstance(self.ds, document.datasets.Dataset1DPlugin)

    @property
    def children(self):
        """Scan the document for other datasets depending on itself."""
        for name, ds in self.alldoc.items():
            if name == self.path:
                continue
            if not isinstance(ds, document.datasets.Dataset1DPlugin):
                continue
            involved = set(chain(ds.pluginmanager.fields.values()))
            involved = involved - set(ds.pluginmanager.datasetnames)
            if self.path in involved:
                sub = name.replace('/', '-').replace(':', '_')
                model_path = self.model_path + '/' + sub
                entry = self._children.get(sub, False)
                if entry is False:
                    entry = DatasetEntry(
                        doc=self.doc, name=sub, path=name, model_path=model_path, parent=self)
        return self._children

    @property
    def parents(self):
        """Scan the document for all possible parents of this dataset"""
        if len(self._parents) > 0:
            return self._parents
        if not self.is_derived:
            return []
        involved = list(chain(self.ds.pluginmanager.fields.values()))
        outputs = set([e.name for e in self.ds.pluginmanager.datasets])
        self._parents = []
        for name in involved:
            if name in outputs:
                continue
            if name == self._name:
                continue
            if name not in self.alldoc:
                continue
            self._parents.append(name)
        return self._parents

    @property
    def m_smp(self):
        return getattr(self.ds, 'm_smp', False)

    @property
    def vars(self):
        suffixes = collections.defaultdict(list)
        for p in self.parents[:]:
            # ds = self.alldoc[p]
            if p == self.path:
                continue
            e = self.root.traverse_path(p)
            if e == self or not e:
                continue
            fn = e.legend_suffix
            human = e.human_name
            suffixes[fn].append(human)
        r = []
        for fn, legends in suffixes.items():
            lg = ','.join(legends)
            if fn and len(suffixes):
                r.append('{}[{}]'.format(fn, lg))
            else:
                r.append(lg)
        r = ','.join(r)
        return r

    @property
    def m_var(self):
        ds = self.ds
        if getattr(ds, 'm_smp', False) is not False:
            return ds.m_var

        pm = ds.pluginmanager

        if len(self.parents) == 1 or pm.plugin.name in ('SmoothData', 'Add', 'CurveOperation'):
            if pm.plugin.name != 'Viscosity':
                return self.parent.m_var
        if not self.is_derived:
            return getattr(ds, 'm_var', self.path)
        
        if pm.plugin.name == 'Coefficient':
            v = 'Coeff(%i,%s{\deg}C) ' % (pm.fields['start'], self.vars)
        elif pm.plugin.name == 'Derive':
            v = 'Der(%i{\deg},%s)' % (pm.fields['order'], self.vars)
        elif pm.plugin_name == 'Viscosity':
            v = 'Viscosity({})'.format(self.vars)
        else:
            v = getattr(ds, 'm_var', self.vars)
        return v

    @property
    def file_name(self):
        lk = self.linked
        fileName = '' if (not lk) else os.path.basename(lk.filename)
        if fileName.endswith('.h5'):
            fileName = re.sub('_[0-9]+.\.h5$', '', fileName)
            fileName = re.sub('\.h5$', '', fileName)
        return fileName
    
    @property
    def sample_name(self):
        lk = self.linked
        sampleName = ''
        m_smp = self.m_smp
        if lk is not None and m_smp is not False:
            if len(lk.instr.list()) <= 2:
                sampleName = ''
            elif 'name' in m_smp:
                sampleName = m_smp['name']
            else:
                sampleName = lk.instr.measure['name']       
        return sampleName
    
    def calc_human_name(self):
        if getattr(self.ds, 'm_smp', False) is not False:
            return self.ds.m_var
        if not self.is_derived:
            return getattr(self.ds, 'm_var', self.path)
        pm = self.ds.pluginmanager
        name = pm.plugin.name
        if  name == 'SmoothData':
            return 'Smooth(%s,%ipts)' % (self.parent.legend, pm.fields['window'])
        else:
            return '%s - %s(%s)' % (self._name, name, self.vars)
        
    @property
    def human_name(self):
        return self.calc_human_name()
        
    @property
    def legend_suffix(self):
        sn = self.sample_name
        fn = self.file_name
        legend = ''
        if sn:
            legend = sn
        if fn:
            if legend:
                legend += ' - '
            legend += fn
        return legend

    @property
    def legend(self):
        s = self.legend_suffix
        h = self.human_name
        if s and s in h:
            s = ''
        elif s:
            s = ' - ' + s
        return self.human_name + s

    @property
    def m_name(self):
        return getattr(self.ds, 'm_name', self._name)

    @property
    def m_col(self):
        return self.ds.m_col

    @property
    def m_keep(self):
        return getattr(self.ds, 'm_keep', False)

    @m_keep.setter
    def m_keep(self, b):
        self.ds.m_keep = b

    @property
    def m_pos(self):
        if hasattr(self.ds, 'm_pos'):
            return self.ds.m_pos
        if self.linked is None:
            return -1
        p = find_pos(list(self.linked.children.keys()))
        return p

    @property
    def m_percent(self):
        return getattr(self.ds, 'm_percent', False)

    @property
    def _old_linked(self):
        """Recursive upward search for a linked file"""
        entry = self
        while True:
            ds = entry.ds
            if ds is False:
                entry = entry.parent
                if entry is False:
                    break
                continue
            if ds.linked is not None:
                return ds.linked
            if entry.parent is False:
                return None
            entry = entry.parent
        return None
            
    @property
    def linked(self):
        if not self._linked:
            self._linked = linked_upward_search(self)
        return self._linked

            
def linked_upward_search(entry):
    if (entry.ds is not False) and (entry.ds.linked is not None):
        return entry.ds.linked
    if not entry.is_derived:
        return False
    for dsname in entry.parents:
        ds = entry.alldoc[dsname]
        if (ds is not False) and (ds.linked is not None):
            return ds.linked
        parent = entry.traverse_path(dsname)
        if not parent:
            continue
        r = linked_upward_search(parent)
        if r is not None:
            return r
    if entry.parent is False:
        return None
    return linked_upward_search(entry.parent)

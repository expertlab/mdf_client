#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Interfaces for local and remote file access"""
import setproctitle
from mdf_canon.logger import get_module_logging
from mdf_client.qt import QtWidgets, QtGui, QtCore
from time import sleep, time
import tempfile
import traceback
import shutil
import os
from .. import parameters as params
from threading import Lock
from mdf_canon import bitmap, csutil
from mdf_canon import reference
from mdf_canon.csutil import unicode_func

MAX = 10 ** 5
MIN = -10 ** 5
logging = get_module_logging(__name__)


def draw_profile(x, y, w=0, h=0, contour_only=False, pen_width=0,
                 closed=True, zoom=1, foreground=QtGui.QColor('black'),
                 background=QtGui.QColor('white')):
    """Draw an x,y profile onto a QImage.
    margin: upper, bottom, left, right"""
    lst = list(QtCore.QPointF(ix * zoom, y[i] * zoom) for i, ix in enumerate(x))
    # Close the polygon
    if closed:
        lst.append(QtCore.QPointF(x[-1], h))
        lst.append(QtCore.QPointF(x[0], h))
        lst.append(QtCore.QPointF(x[0], y[0]))
    if not w:
        w = max(x) - min(x)
    if not h:
        h = max(h) - min(h)

    # Create a painter path
    qpath = QtGui.QPainterPath()
    qpath.addPolygon(QtGui.QPolygonF(lst))
    qpath.setFillRule(QtCore.Qt.WindingFill)

    pix = QtGui.QImage(w, h, QtGui.QImage.Format_ARGB32)
    # Clear memory (Qt bug)
    pix.fill(background)
    # QtGui.QImage.Format_ARGB32_Premultiplied
    p = QtGui.QPainter(pix)
    p.setRenderHint(QtGui.QPainter.Antialiasing)
    
    # Only contour
    pen = QtGui.QPen(foreground)
    pen.setJoinStyle(QtCore.Qt.RoundJoin)
    pen.setCapStyle(QtCore.Qt.RoundCap)
    pen.setWidthF(pen_width)
    
    if contour_only:
        p.setBrush(QtGui.QBrush(background))
    else:
        p.setBrush(QtGui.QBrush(foreground))
    
    p.setPen(pen)
    # Fill background
    r = pix.rect()
    p.fillRect(r, background)
    # Add profile
    p.drawPath(qpath)
    p.end()
    return pix


visibleOptions = ['seqnum', 't', 'T', 'd', 'Sint']


class DataDecoder(QtCore.QThread):

    """Cached image reader and decoder from MDF files. To be run in a separate thread."""
    proxy = False
    datapath = False
    tmpdir = False
    ext = False
    zerotime = -1
    comp = 'JPG'
    comps = set(('JPG', 'PNG'))
    prefix = '0:'
    _len = 0
    contour_only = False
    contour_width = 0
    contour_closed = True
    sig_reset = QtCore.pyqtSignal()
    sig_cached = QtCore.pyqtSignal(int)

    def __init__(self, parent=None, maxWidth=100):
        QtCore.QThread.__init__(self, parent)
        self._lock = Lock()
        self.reset()
        self.opt_images = True
        self.opt_profiles = False
        self.destroyed.connect(self.close)
        QtWidgets.qApp.lastWindowClosed.connect(self.close)

    def close(self, *foo):
        logging.debug('CLOSING DataDecoder', self.tmpdir)
        if self.tmpdir and os.path.exists(self.tmpdir):
            shutil.rmtree(self.tmpdir)
        self.ok = False
        sleep(.05)
        if self.proxy:
            self.proxy.close(all_handlers=False)
        self.quit()
        self.terminate()
        self.deleteLater()

    def setDataPath(self, datapath):
        if self.isRunning():
            self.quit()
            self.wait()
        
        self.x_translate = 0
        self.y_translate = 0
        self.width = 0
        self.height = 0
            
        self.names = [-1] * params.maxImageCacheSize
        self.data = {}
        self.queue = []
        if datapath:
            self.datapath = datapath
        
        if self.datapath and '/hsm/' in self.datapath:
            self.contour_closed = True
        else:
            self.contour_closed = False
        # Clear old tmpdir
        if self.tmpdir:
            shutil.rmtree(self.tmpdir)
        # Create tmpdir
        self.tmpdir = tempfile.mkdtemp()

        # Read metadata about the path
        if (self.proxy is not False) and (self.datapath is not False):
            self.refClassName = unicode_func(self.proxy.get_node_attr(self.datapath, '_reference_class'))
#            self.ext = self.proxy.get_node_attr(self.datapath, 'type')
            self.ext = self.refClassName
            logging.debug(
                'DataDecoder.setDataPath', datapath, self.refClassName, self.ext)
            self.refclass = getattr(reference, self.refClassName)

        self.sig_reset.emit()
        self.ok = True

    def reset(self, proxy=False, datapath=False, prefix='0:'):
        self._len = 0
        self.zerotime = -1
        self.cached_profiles = {}
        self.prefix = prefix
        self.ok = False
        if self.isRunning():
            self.exit()
            self.wait()
        if (not proxy) and self.proxy:
            proxy = self.proxy
        if proxy:
            # 			self.proxy=proxy.copy()
            self.proxy = proxy
        self.setDataPath(datapath)

    def get_len(self, fp=False):
        """Cross thread __len__ retrieval"""
        if not fp:
            fp = self.proxy
        if not fp.isopen():
            fp.reopen()
        if fp is False or self.datapath is False:
            logging.debug('DataDecoder: no proxy or datapath')
            self._len = 0
            return self._len
        try:
            r = fp.len(self.datapath)
            assert r is not None
        except:
            logging.debug('get_len first pass', traceback.format_exc())
            fp = fp.copy()
            try:
                r = fp.len(self.datapath)
            except:
                logging.debug('get_len second pass', traceback.format_exc())
                r = 0

        logging.debug('DataDecoder.get_len', r, id(self))
        if r is not None:
            self._len = r
        return self._len

    def __len__(self):
        if self.isRunning():
            return self._len
        r = 0
        try:
            r = self.get_len()
        except:
            logging.debug('Getting __len__', self.datapath, traceback.format_exc())
        return r

    def get_time(self, t):
        if not self.proxy.isopen():
            self.proxy.reopen()
        if self.ext == 'Profile':
            f = self.proxy.get_time_profile
        elif self.ext == 'CumulativeProfile':
            f = self.proxy.get_time_cumulative_profile
        else:
            f = self.proxy.get_time_image
        # Compatibility with absolute-time datasets
        if t > self.zerotime:
            t += self.zerotime
        return f(self.datapath, t)
    
    def time_at(self, idx):
        if not self.proxy.isopen():
            self.proxy.reopen()
        func = self.refclass.unbound['decode_time']
        return self.proxy.time_at(self.datapath, idx, func)
    
    def get_batch(self, batch, fp=False):
        """Retrieves a batch of seq indexes in a single call"""
        if not fp:
            fp = self.proxy
        if fp is False:
            logging.debug('get_data: no file proxy')
            return False
        if not fp.isopen():
            logging.debug('get_data: reopening')
            fp.reopen(mode='r')
        batch = list(filter(lambda e: e not in self.cached_profiles, batch))
        res = fp.col_at(self.datapath, batch, True)
        for i, seq in enumerate(batch):
            if i > len(res):
                logging.warning('get_batch: Non-existent index', i, seq)
                break
            if res[i] is None:
                continue
            self.cached_profiles[int(seq)] = res[i]
    
    def get_data(self, seq, fp=False, cache=True):
        if not fp:
            fp = self.proxy
        if fp is False:
            logging.debug('get_data: no file proxy')
            return False
        if not fp.isopen():
            logging.debug('get_data: reopening')
            fp.reopen(mode='r')
        seq = int(seq)

        if cache and seq in self.cached_profiles:
            entry = self.cached_profiles[seq]
        else:
            t0 = time()
            entry = fp.col_at(self.datapath, seq, True)
            t1 = time()
            logging.debug('DataDecoder entry search', t1 - t0, type(entry))
            if entry is None or len(entry) <= 5:
                logging.debug('NO DATA', self.datapath, seq, self.ext, entry)
                return False
            self.cached_profiles[seq] = entry

        # Data decoding
        try:
            t, dat = self.refclass.decode(entry)
        except:
            logging.debug('DataDecoder getting', self.datapath, seq,
                          traceback.format_exc())
            return False
        self.dat = dat  # for debugging
        # Direct image conversion
        if self.ext in ['Image', 'Binary']:
            logging.debug('toQImage', seq, len(dat), type(dat))
            # Create a QImage for signalling
            pix = QtGui.QImage()
            pix.loadFromData(dat, self.comp)
            if pix.isNull():
                self.comp = (self.comps - set([self.comp])).pop()
                pix.loadFromData(dat, self.comp)
            return t, pix
        # FIXME: how to detect misura compression? Different Ext? Or different
        # ReferenceClass?
        elif self.ext == 'ImageM3':
            logging.debug('decompressing', seq)
            dat = bitmap.decompress(dat)
            qimg = QtGui.QImage()
            qimg.loadFromData(dat, 'BMP')
            return t, qimg
        # Writing a profile onto an image
        elif self.ext in ('Profile', 'CumulativeProfile'):
            ((w, h), x, y) = dat
            logging.debug('drawing profile', self.ext, seq, w, h)
            if self.width == 0:
                r = reference.video_export.center_profile(x, y, w, h, contour_only=self.contour_only, pen_width=self.contour_width)
                self.contour_width, self.x_translate, self.y_translate, self.width, self.height = r
            pix = draw_profile(x + self.x_translate, y + self.y_translate, self.width, self.height,
                               contour_only=self.contour_only,
                               pen_width=self.contour_width,
                               closed=self.contour_closed)
            return t, pix

        logging.debug('Format not recognized', self.ext)
        return False

    def dequeue(self, seq):
        """Remove from queue an entry which is no longer needed"""
        if seq in self.queue:
            self.queue.remove(seq)

    def cache(self, seq, fp=False):
        """Read image and save in cache"""
        if not fp:
            fp = self.proxy
        if not fp:
            return True
        if seq in self.names:
            return True
        dat = self.get_data(seq, fp)
        if not dat:
            return False
        t, dat = dat
        if not dat:
            return False
        self.names.append(seq)
        self.data[seq] = t, dat
        # keep image cache length
        d = self.names.pop(0)
        if d >= 0:
            del self.data[d]
        return True

    def get(self, seq):
        if self.proxy is False or self.datapath is False:
            logging.debug('proxy', self.proxy)
            logging.debug('datapath', self.datapath)
            return False
        if not self.proxy.isopen():
            logging.debug('get: reopening')
            self.proxy.reopen(mode='r')
        if not (seq in self.names):
            logging.debug('queue', seq, 'len', len(self.queue))
            self.queue.append(seq)
            try:
                self._lock.release()
            except:
                pass
            # If queue is longer than maximum cache length, remove oldest point
            if len(self.queue) >= params.maxImageCacheSize:
                self.queue.pop(0)
            if not self.isRunning():
                logging.debug('Restart decoder')
                self.start()
            return False
        if self.zerotime < 0:
            self.zerotime = self.proxy.get_node_attr('/conf', 'zerotime')
        if self.zerotime is None:
            self.zerotime = -1
            return False
        t, dat = self.data[seq]
        logging.debug('found data', self.zerotime, t, type(dat), dat.isNull())
        # Compatibility with absolute-time datasets
        if t > self.zerotime:
            t -= self.zerotime
        return t, dat

    t0 = time()

    def run(self):
        """Runs the decoding in a separate thread"""
        if self.proxy is False:
            logging.debug('No proxy defined')
            return
        
        fp = False
        print('DataDecoder.run START', id(self))
        
        while self.ok:
            # Local copy
            if not fp:
                fp = self.proxy.copy()
                fp.connect()
                setproctitle.setthreadtitle(f'MdfClient DataDecoder: {fp.get_path()}')
            ok = False
            try:
                self._lock.acquire(blocking=True, timeout=2)
            except:
                pass
            # Keep recording len
            if not self.queue:
                self.get_len(fp)
                continue
            # Always read last requested data
            seq = self.queue.pop(-1)
            logging.debug('Caching seq', seq)
            self.cache(seq, fp)
            self.sig_cached.emit(seq)
        if fp:
            fp.close()
        print('DataDecoder.run END', id(self))

    def toggle_run(self):
        pass

    @property
    def stream(self):
        """Is the decoder streaming?"""
        if self.ok and self.isRunning():
            return True
        return False

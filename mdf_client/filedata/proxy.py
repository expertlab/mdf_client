#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Interfaces for local and remote file access"""
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
from mdf_client.connection import addrConnection
from mdf_canon.indexer import SharedFile
import functools
from mdf_canon.csutil import xmlrpclib
from pickle import load, dump, loads
from mdf_canon import option
from .. import _

def get_file_tooltip(instrument, sh_file):
    """This is shown in tabbar"""
    m = instrument.measure
    tt = [sh_file.get_path(), m['name']]
    cm = m['comment']
    if cm:
        tt.append(cm)
    for smp in instrument.samples:
        tt.append(smp['name'])
    tt.append(m['date'])
    return '\n'.join(tt)

class RemoteFileProxy(object):

    """Wrapper class for SharedFile. Does the necessary binary extraction and pickle/unpickle"""
    conf = False
    _lockme_error = None
    def __init__(self, obj, conf=False, live=None):
        self.obj = obj
        # It is considered live if has the same uid as parent FileServer live
        # uid.
        my = self.obj.get_uid()
        if live is None:
            live = self.obj.parent().get_live()
            live = my == live
        self.live = live
        self.conf = conf
        if conf is False:
            self.load_conf()
        else:
            self.conf = conf
            
    @classmethod
    def from_address(cls, address):
        if address not in cloud_connector_cache:
            obj = CloudConnector(address)
            r = RemoteFileProxy(obj, live=False)
            cloud_connector_cache[address]=r
        return cloud_connector_cache[address]
        

    def load_conf(self):
        """Override remote load_conf method in order to receive remote tree 
        and build a local ConfigurationProxy object."""
        # The configuration of a live file is the remote server itself!
        if self.live:
            if not self.conf:
                self.conf = self.obj.root
            return True
        # Otherwise unpickle the configuration tree dict and get a
        # ConfigurationProxy for it
        d = self._decode(self.obj.conf_tree)
        logging.debug('loading conf', len(d), list(d.keys()))
        
        self.conf = option.ConfigurationProxy(desc=d)
        return True

    def copy(self):
        ro = self.obj.copy()
        ro.connect()
        r = RemoteFileProxy(ro, self.conf, self.live)
        return r

    def get_path(self):
        """Extends the path by adding the address| part"""
        uid = self.obj.get_uid()
        h, addr = self.obj.addr.split('//')
        path = '{}//{}:{}@{}|{}'.format(h,
                                        self.obj.user, self.obj.password, addr, uid)
        return path

    def isopen(self):
        """Returns both if the remote file is opened and if the connection is still valid"""
        try:
            r = self.obj.isopen()
        except:
            return False
        return r
    
    def iscloud(self):
        return isinstance(self.obj, CloudConnector)

    def connect(self):
        return self.obj.connect()

    def reopen(self, *a, **k):
        """Do nothing on remote objects!"""
        return True

    def close(self, *a, **k):
        """Do nothing on remote objects!"""
        return True

    def __getattr__(self, path):
        if path.startswith('_') or (path in dir(self)):
            return object.__getattribute__(self, path)
        # Get the function from the object
        a = getattr(self.obj, path)
        # If the function requires a decoding, build the partial decoding
        # function
        if self.obj.decode(path):
            a = functools.partial(self._decode, a)
        return a

    def _decode(self, func, *a, **k):
        """Wrapper caller for functions which needs decoding of their return type."""
        r = func(*a, **k)
        #print('_decode',r)
        if isinstance(r, xmlrpclib.Binary):
            r = r.data
        if isinstance(r, bytes):
            r = loads(r)
        return r
    
    
import json, requests
from mdf_canon import csutil
cloud_connector_cache = {}
from hashlib import md5
import os

class CallCache(object):
    def __init__(self, fileid):
        self.cumul_length = 0
        self.fileid = fileid
        self.dir = os.path.join(os.path.dirname(confdb.path), 'cache')
        self.count = 0
        
    def __len__(self):
        return self.count
        
    def copy(self):
        return self
    
    def pointer(self, *args, **kwargs):
        d = self.fileid+'#'
        for a in args:
            d += json.dumps(a)+"#"
        for k in sorted(list(kwargs.keys())):
            d += k+"#"
            d += json.dumps(kwargs[k])+"#"
        d = md5(d.encode()).hexdigest()
        return d
    
    def file(self, pointer):
        p = os.path.join(self.dir, pointer[:2], pointer[2:4])
        os.makedirs(p, exist_ok=True)
        p = os.path.join(p, pointer[4:])+'.cache'
        return p
    
    def get(self, *args, **kwargs):
        d = self.pointer(*args, **kwargs)
        path = self.file(d)
        if os.path.exists(path):
            return load(open(path,'rb'))
        return None
    
    def set(self, result, *args, **kwargs):
        d = self.pointer(*args, **kwargs)
        path = self.file(d)
        if not os.path.exists(path):
            self.count+=1
            dump(result, open(path,'wb'))
        
        
from ..clientconf import confdb
auth_failed_msg = _("Could not authenticate with user: {}\n Please change your Client Configuration under Cloud tab.")

def get_cloud_user_password():
    user = confdb['cloud_user']
    password = confdb['cloud_password']
    if not user or not password:
        from mdf_client.connection import BaseLoginWindow
        blw = BaseLoginWindow(user, password)
        blw.exec_()
        user, password = blw.credentials()
        if blw.ckSave.isChecked():
            confdb['cloud_user'], confdb['cloud_password'] = user,password
            confdb.save()
    return user,password

class CloudConnector(object):
    """Replaces the obj RemoteFileProxy"""
    _cache={}
    db = None
    def __init__(self, address):
        addr, hash, user, password, db, uid = decode_address(address)
        addr += 'rpc/rpc.php' 
        self.testid = uid
        self.address = address
        self.db = db
        self.addr = addr
        if not user or not password: 
            user, password = get_cloud_user_password()
            
        self.user = user
        self.password = password
        
        self._cache = CallCache(address)
        self._cache.set(True, 'isopen')
        self._cache.set(True, 'connect')
        self._cache.set(uid, 'get_uid')
        
        meta = self._caller('get_metadata')
        for k, result in meta.items():
            self._cache.set(result, k)
        
        
    def copy(self):
        return self
        
        
    def _caller(self, path, *a, **kw):
        r = self._cache.get(path, *a, **kw)
        if r is not None:
            print('Found cached', path, a, kw)
            return r
        
        print('not cached',path)
        if path=='get_node_attr':
            gha = self._caller('get_header_attrs')
            if (len(a)==2 and not kw) and a[0] in gha:
                print('found cached attrs for', a)
                return gha[a[0]].get(a[1], None)
            else:
                print('not found cached attrs for', a)
                
        if path=='header':
            gha = self._caller('get_header_attrs')
            r = []
            if len(a) in (1,2) and not kw:
                s = '' if len(a)==1 else a[1]
                a = set(a[0])
                for path, attr in gha.items():
                    if not path.startswith(s):
                        continue
                    if attr.get('_reference_class', None) in a:
                        r.append(path)
                return r
            else:
                print('not found header', a)
            
            
        packet = {'cmd':'data_'+path,'doc_id':self.testid, 'args':a, 'kwargs':kw,'user':self.user,'password':self.password,
                  'compression':True}
        if self.db:
            packet['db']=self.db
        print("ZZZZ----",len(self._cache),"\n----EOK---\n",packet, '\n', self.addr)
        r = requests.post(self.addr,json=packet)
        res = r.content.split(b"!$%&_json_separator_&%$!")
        if len(res)==1:
            j = json.loads(res[0])
            meta = j
        else:
            j = json.loads(csutil.gzinflate(res[1]))
            meta = json.loads(res[0])
        if r.status_code==400:
            from PyQt5 import QtWidgets
            QtWidgets.QMessageBox.critical(None, _("Authentication failed"),auth_failed_msg.format(self.user))
            from ..confwidget import ClientConf
            global cc
            cc = ClientConf()
            cc.conf.highlight_option('cloud_user')
            cc.conf.highlight_option('cloud_password')
            cc.show()
            return
        if r.status_code!=200:
            print(r)
            from PyQt5 import QtWidgets
            QtWidgets.QMessageBox.critical(None, _("Connection error"), "Error: {}".format(r.status_code))
            return
        # Never cache actual table readings: it would be a waste
        # if not path.startswith('data_col'):
        self._cache.set(j, path, *a, **kw)
        return j
    
    def decode(self, name):
        return name in set(self._caller('decode_list'))
    
    def _decode(self, func, *a, **k):
        """Wrapper caller for functions which needs decoding of their return type."""
        r = func(*a, **k)
        if isinstance(r, xmlrpclib.Binary):
            r = r.data
        if isinstance(r, bytes):
            r = loads(r)
        return r
    
    def __getattr__(self, path):
        if path.startswith('_') or (path in dir(self)):
            return object.__getattribute__(self, path)
        # Get the function from the object
        a = functools.partial(self._caller, path)
        # If the function requires a decoding, build the partial decoding
        # function
        if self.decode(path):
            a = functools.partial(self._decode, a)
        return a
    
import urllib 

def decode_address(address):
    v= urllib.parse.unquote(address).split('#')
    addr = v.pop(0)
    hash = '#'.join(v)
    user,password = None, None
    if '@' in addr and addr[:4] in ('http', 'mdf:'):
        cred, addr = addr.split('@')
        h, cred = cred.split('//')
        user, password = cred.split(':')
        addr = h + '//' + addr
    if addr.startswith('mdf:/'):
        addr = addr.replace('mdf://','mdf:/')
        addr = 'https://'+addr[5:]
    db, uid = None, None
    if '?' in hash:
        db, params = hash.split('?')
    if 'uid=' in params:
        uid = params.split('uid=')[-1].split('&')[0]
    return addr, hash, user, password, db, uid

def getRemoteFile(address):
    """Decode address in the form http(s)://(user):(password)@host....|uid, connects to the host and 
    returns the remote file object corresponding to uid"""    
    if ('rpc.php' in address) or address.startswith('mdf:') or 'ceramics-genome.ai' in address:
        return RemoteFileProxy.from_address(address)
    # Direct instrument connection
    addr, hash, user, password, db, uid = decode_address(address)
    srv = addrConnection(addr, user, password)
    obj = getattr(srv.storage.test, uid)
    obj.connect()    
    return RemoteFileProxy(obj)


def getFileProxy(filename, **kw):
    if filename[:4] in ('http', 'mdf:'):
        r = getRemoteFile(filename)
    else:
        r = SharedFile(filename, **kw)
    return r


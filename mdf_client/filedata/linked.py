#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Libreria per il plotting semplice durante l'acquisizione."""
import os
from mdf_canon.logger import get_module_logging
logging = get_module_logging(__name__)
import veusz.dataimport.base as base
from mdf_canon import option, csutil
from mdf_client.clientconf import confdb

class LinkedMisuraFile(base.LinkedFileBase):
    mtype = 'LinkedMisuraFile'

    def __init__(self, params):
        if params.filename and not params.filename[:4] in ('http','mdf:'):
            params.filename = os.path.abspath(params.filename)
            self.basename = os.path.basename(csutil.unicode_func(params.filename))
        else:
            self.basename = False
        base.LinkedFileBase.__init__(self, params)
        self.samples = []
        """List of samples contained in this file"""
        self.prefix = ''
        """Dataset prefix"""
        self.conf = option.ConfigurationProxy()
        """Additional configuration parameters"""
        self.instr = option.ConfigurationProxy()
        """Instrument configuration"""
        self.cycle = []
        """Thermal cycle"""
        self.header = []
        """Available column names"""
        self.name = 'linkedfile'
        self.instrument = False
        """Instrument which produced the file"""
        self.title = 'default'
        """File title"""
        self.tool_tip = False
#		self.filename=params.filename
        
    def get_instrument(self):
        self.instrument = self.conf['runningInstrument'] 
        return self.conf['runningInstrument']
    
    def get_title(self, comment=False):
        m = getattr(self.conf, self.get_instrument()).measure
        t = m['name']
        if comment and m['comment']:
            t = m['comment']+', '+t
        return t
        
    def _getSaveFilename(self, relpath):
        if self.filename[:4] in ('http','mdf:'):
            return self.filename
        return base.LinkedFileBase._getSaveFilename(self, relpath)

    def saveToFile(self, fileobj, relpath=None):
        """Save the link to the document file."""
        params = [repr(self._getSaveFilename(relpath))]
        if self.prefix:
            params.append('prefix=' + repr(self.prefix))
        if self.instr:
            params.append('uid=' + repr(self.instr.measure['uid']))
        
        for key in self.params.defaults.keys():
            if not key in ['prefix', 'uid', 'filename']:
                s = key + "=" + repr(getattr(self.params, key))
                if s in params:
                    continue
                params.append(s)

        fileobj.write('ImportMisura(%s)\n' % (', '.join(params)))

    def createOperation(self):
        """Returns the operation needed for data reloading"""
        from .operation import OperationMisuraImport
        return OperationMisuraImport

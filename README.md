# MDF Client (Measurement Development Platform)

Welcome to the repository for the MDF Client component development. 
``mdf_client`` contains the user interface and client-side code, mainly written in Python3/Qt5 and released with an GPLv2 license.
Depends on ``mdf_canon`` for data access and ``veusz`` for plotting.

MDF is an open source software platform divided in a client package (``mdf_client``), a server package (``mdf_droid``) and a bridge package (``mdf_canon``).

``mdf_client`` was forked in 2019 from the github project ``misura.client``, and since substantially diverged.